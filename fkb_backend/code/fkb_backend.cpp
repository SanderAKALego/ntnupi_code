#include <stdio.h>
#include <SDL.h>
#include <SDL_net.h>
#include <signal.h>
#include "net.h"
#include "fkb_backend.h"
#include "fkb_mysql.cpp"
#include "srp.c"

static char GlobalBuffer[4096];

static SRP_HashAlgorithm alg = SRP_SHA1;
static SRP_NGType ng_type = SRP_NG_2048;

#define MAX_CONNECTED_CLIENTS 200
static TCPsocket ConnectedClients[MAX_CONNECTED_CLIENTS] = {0};
static SDL_mutex *ConnectedClientsMutex = 0;

static TCPsocket ServerSocket = {0};

static void
CloseSignalHandler(int Signal)
{
    printf("Signal: %d, exiting...", Signal);
    SDL_LockMutex(ConnectedClientsMutex);
    for(int SocketIndex = 0;
        SocketIndex < MAX_CONNECTED_CLIENTS;
        ++SocketIndex)
    {
        if(ConnectedClients[SocketIndex])
        {
            SDLNet_TCP_Close(ConnectedClients[SocketIndex]);
            ConnectedClients[SocketIndex] = 0;
        }
    }
    SDL_UnlockMutex(ConnectedClientsMutex);
    
    SDLNet_TCP_Close(ServerSocket);
    SDLNet_Quit();
    SDL_Quit();
    
    exit(0);
}

static char*
PrintArrayHex(unsigned char *Arr, int Length)
{
    char *w = GlobalBuffer;
    char *e = GlobalBuffer + sizeof(GlobalBuffer);
    w += snprintf(w, e-w, "{");
    for(int hex = 0; hex < Length; ++hex)
    {
        w += snprintf(w, e-w, "%x", Arr[hex]);
        if(hex < Length - 1)
        {
            w += snprintf(w, e-w, ",");
        }
    }
    w += snprintf(w, e-w, "}");
    return(GlobalBuffer);
}

static int
ProcessClientThread(void *lpParameter)
{
    int SocketIndex = (int)lpParameter;
    SDL_LockMutex(ConnectedClientsMutex);
    TCPsocket ClientSocket = ConnectedClients[SocketIndex];
    SDL_UnlockMutex(ConnectedClientsMutex);
    
    MYSQL *conn = MySQL_Connect(MYSQL_SERVER, MYSQL_USERNAME, MYSQL_CONPASSWORD, MYSQL_DATABASENAME, MYSQL_PORT);
    
    if(!conn)
    {
        printf("Failed to connect to MySQL database!\n");
        mysql_error(conn);
        goto DisconnectClient;
    }
    
    printf("Processing client!\n");
    
    bool IsLoggedIn = false;
    int Result = 0;
    struct SRPVerifier *ver = 0;
    do
    {
        printf("Retrieving remote call\n");
        int RemoteCallType = 0;
        if(!FKBNet_Recv(ClientSocket, &RemoteCallType, 0, sizeof(int)))
        {
            goto DisconnectClient;
        }
        printf("Processing remote call %d\n", RemoteCallType);
        if(!FKBNet_Send(ClientSocket, &RemoteCallType, sizeof(int)))
        {
            goto DisconnectClient;
        }
        switch(RemoteCallType)
        {
            case MSG_UserExists:
            {
                char Username[128] = {0};
                int UsernameLength = 0;
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLength, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                printf("Checking if username '%s' of length %d is taken\n", Username, UsernameLength);
                Result = MySQL_HasUser(conn, Username);
                printf( "Username was %s!\n", Result ? "found" : "not found");
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
            }break;
            case MSG_Register:
            case MSG_UpdateCredentials:
            {
                char Username[128] = {0};
                unsigned char Salt[512] = {0};
                unsigned char Verifier[512] = {0};
                
                int UsernameLength = 0;
                int SaltLength = 0;
                int VerifierLength = 0;
                
                printf("%s user\n", RemoteCallType == MSG_UpdateCredentials ? "Updating credentials for" : "Registering");
                
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLength, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                
                printf("UsernameLength: %d\nUsername: %s\n", UsernameLength, Username);
                
                if(!FKBNet_Recv(ClientSocket, Salt, &SaltLength, MAX_SALT_LENGTH))
                {
                    goto DisconnectClient;
                }
                
                printf("SaltLength: %d\nSalt: %s\n", SaltLength, PrintArrayHex(Salt,SaltLength));
                
                if(!FKBNet_Recv(ClientSocket, Verifier, &VerifierLength, MAX_VERIFIER_LENGTH))
                {
                    goto DisconnectClient;
                }
                
                printf("VerifierLength: %d\nVerifier: %s\n", VerifierLength, PrintArrayHex(Verifier, VerifierLength));
                
                if(RemoteCallType == MSG_UpdateCredentials)
                {
                    if(!IsLoggedIn)
                    {
                        Result = MSG_Result_NotLoggedIn;
                    }
                    else
                    {
                        if(MySQL_UpdateUserCredentials(conn, Username, Salt, Verifier, UsernameLength, SaltLength, VerifierLength))
                        {
                            Result = MSG_Result_NoError;
                        }
                        else
                        {
                            Result = MSG_Result_Failed;
                        }
                    }
                }
                else
                {
                    if(MySQL_HasUser(conn, Username))
                    {
                        Result = MSG_Result_AlreadyExists;
                    }
                    else
                    {
                        if(MySQL_AddUser(conn, Username, Salt, Verifier, UsernameLength, SaltLength, VerifierLength))
                        {
                            printf("User created!\n");
                            Result = MSG_Result_NoError;
                        }
                        else
                        {
                            Result = MSG_Result_Failed;
                        }
                    }
                }
                
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
            }break;
            case MSG_LogIn:
            {
                char Username[MAX_USERNAME_LENGTH] = {0};
                unsigned char BytesA[256] = {0};
                unsigned char BytesMIn[256] = {0};
                const unsigned char *BytesB = 0;
                const unsigned char *BytesM = 0;
                const unsigned char *BytesHAMK = 0;
                int UsernameLength = 0;
                int LengthA = 0;
                int LengthB = 0;
                int LengthM = 0;
                int LengthMIn = 0;
                
                Result = MSG_Result_NoError;
                
                printf( "Start SRP authentication:\n");
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLength, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                
                printf( "Username: %s\nUsernameLength: %d\n", Username, UsernameLength);
                
                user User = {0};
                if(!MySQL_GetUser(conn, Username, &User))
                {
                    Result = MSG_Result_Failed;
                }
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                if(Result != MSG_Result_NoError)
                {
                    printf("Unrecognized user tried to login!\n");
                    continue;
                }
                printf("Salt:%s\n",PrintArrayHex(User.Salt, User.SaltLength));
                printf("Verifier:%s\n",PrintArrayHex(User.Verifier, User.VerifierLength));
                // TODO(Sanderkp): Case for when aready logged in
                if(!FKBNet_Recv(ClientSocket, BytesA, &LengthA, sizeof(BytesA)))
                {
                    goto DisconnectClient;
                }
                
                ver = srp_verifier_new(alg, ng_type, Username, User.Salt, User.SaltLength, User.Verifier, User.VerifierLength, 
                                       BytesA, LengthA, &BytesB, &LengthB, 0, 0, 1);
                
                printf("LengthA: %d\nBytesA: %s\n", LengthA, PrintArrayHex(BytesA, LengthA));
                
                if(!BytesB)
                {
                    Result = MSG_Result_Failed;
                }
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                if(Result != MSG_Result_NoError) 
                {
                    printf("Verifier SRP-6a safety check violated!\n");
                    continue;
                }
                
                if(!FKBNet_Send(ClientSocket, User.Salt, User.SaltLength))
                {
                    goto DisconnectClient;
                }
                
                if(!FKBNet_Send(ClientSocket, (void*)BytesB, LengthB))
                {
                    goto DisconnectClient;
                }
                
                printf( "SaltLength: %d\nSalt: %s\n", User.SaltLength, PrintArrayHex(User.Salt, User.SaltLength));
                printf( "LengthB: %d\nBytesB: %s\n", LengthB, PrintArrayHex((unsigned char*)BytesB, LengthB));
                
                if(!FKBNet_Recv(ClientSocket, &Result, 0, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                if(Result != MSG_Result_NoError)
                {
                    printf("User SRP-6a safety check violation!\n");
                    continue;
                }
                
                if(!FKBNet_Recv(ClientSocket, BytesMIn, &LengthMIn, sizeof(BytesMIn)))
                {
                    goto DisconnectClient;
                }
                
                printf( "LengthM: %d\nBytesM: %s\n", LengthMIn, PrintArrayHex(BytesMIn, LengthMIn));
                
                srp_verifier_verify_session(ver, BytesMIn, &BytesHAMK);
                
                if(!BytesHAMK)
                {
                    printf("User SRP-6a safety check violation!\n");
                    Result = MSG_Result_Failed;
                }
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                if(Result != MSG_Result_NoError) 
                {
                    printf("User authentication failed!\n");
                    continue;
                }
                
                int LengthHAMK = hash_length(alg);
                
                if(!FKBNet_Send(ClientSocket, (void*)BytesHAMK, LengthHAMK))
                {
                    goto DisconnectClient;
                }
                
                printf( "LengthHAMK: %d\nHAMK: %s\n", LengthHAMK, PrintArrayHex((unsigned char*)BytesHAMK, LengthHAMK));
                
                if(!FKBNet_Recv(ClientSocket, &Result, 0, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                if(Result != MSG_Result_NoError)
                {
                    printf("Server authentication failed!\n");
                    continue;
                }
                printf( "User successfully logged in!\n");
                IsLoggedIn = true;
            }break;
            case MSG_LogOut:
            {
                char Username[MAX_USERNAME_LENGTH] = {0};
                int UsernameLength = 0;
                printf( "Logging out\n");
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLength, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                if(IsLoggedIn)
                {
                    Result = MSG_Result_NoError;
                }
                else
                {
                    Result = MSG_Result_AlreadyLoggedOut;
                }
                
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                
                srp_verifier_delete(ver);
                ver = 0;
                IsLoggedIn = false;
            }break;
            case MSG_Delete:
            {
                int Result = 0;
                char Username[MAX_USERNAME_LENGTH] = {0};
                int UsernameLength = 0;
                
                Result = MSG_Result_Failed;
                printf("Attempting to delete user: ");
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLength, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                printf("%s\n", Username);
                
                if(IsLoggedIn)
                {
                    if(MySQL_DeleteUser(conn, Username))
                    {
                        Result = MSG_Result_NoError;
                    }
                    else
                    {
                        Result = MSG_Result_Failed;
                    }
                }
                else
                {
                    Result = MSG_Result_NotLoggedIn;
                }
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    goto DisconnectClient;
                }
            }break;
            case MSG_UpdateInfo:
            {
                printf( "Updating user info\n");
                Result = MSG_Result_NoError;
                if(!IsLoggedIn)
                {
                    Result = MSG_Result_NotLoggedIn;
                    printf( "Error not logged in\n");
                }
                char Username[MAX_USERNAME_LENGTH] = {0};
                char Fullname[MAX_FULL_NAME_LENGTH] = {0};
                char Description[MAX_DESCRIPTION_LENGTH] = {0};
                char FilePath[MAX_FILEPATH_LEGNTH] = {0};
                
                void *FileContent = 0;
                
                int UsernameLen = 0;
                int FullnameLen = 0;
                int DescriptionLen = 0;
                int ProfileImgSize = 0;
                
                unsigned int FieldsToUpdate = 0;
                if(!FKBNet_Recv(ClientSocket, &FieldsToUpdate, 0, sizeof(int)))
                {
                    goto DisconnectClient;
                }
                
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLen, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                printf("Username:%s\nUsernameLen:%d\n", Username, UsernameLen);
                
                if(!FKBNet_Recv(ClientSocket, Fullname, &FullnameLen, MAX_FULL_NAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                printf("Fullname:%s\nFullnameLen:%d\n", Fullname, FullnameLen);
                if(!FKBNet_Recv(ClientSocket, Description, &DescriptionLen, MAX_DESCRIPTION_LENGTH))
                {
                    goto DisconnectClient;
                }
                printf( "Description:%s\nDescriptionLen:%d\n", Description, DescriptionLen);
                FileContent = malloc(MAX_PROFILE_IMG_SIZE);
                int ActualFileSize = 0;
                if(!FKBNet_Recv(ClientSocket, FileContent, &ActualFileSize, MAX_PROFILE_IMG_SIZE))
                {
                    free(FileContent);
                    goto DisconnectClient;
                }
                if(ActualFileSize)
                {
                    snprintf(FilePath, sizeof(FilePath), "img\\%d.prf", Hash(Username));
                    printf("FilePath: %s\n", FilePath);
                    WriteEntireFile(FilePath, FileContent, ActualFileSize);
                    free(FileContent);
                }
                if(!MySQL_UpdateUserInfo(conn, Username, FieldsToUpdate, Fullname, Description, FilePath))
                {
                    Result = MSG_Result_Failed;
                }
                
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    Result = MSG_Result_Error;
                    printf("SDLNet error: %s\n", SDLNet_GetError());
                    goto DisconnectClient;
                }
                
            }break;
            case MSG_GetInfo:
            {
                char Username[MAX_USERNAME_LENGTH] = {0};
                int UsernameLen = 0;
                Result = MSG_Result_NoError;
                
                if(!FKBNet_Recv(ClientSocket, Username, &UsernameLen, MAX_USERNAME_LENGTH))
                {
                    goto DisconnectClient;
                }
                
                user User = {0};
                if(!MySQL_GetUser(conn, Username, &User))
                {
                    Result = MSG_Result_Failed;
                }
                
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    Result = MSG_Result_Error;
                    printf("SDLNet error: %s\n", SDLNet_GetError());
                    goto DisconnectClient;
                }
                if(Result == MSG_Result_NoError)
                {
                    int FullnameLength = strlen(User.Fullname);
                    int DescriptionLength = strlen(User.Description);
                    
                    if(!FKBNet_Send(ClientSocket, User.Fullname, FullnameLength))
                    {
                        Result = MSG_Result_Error;
                        printf("SDLNet error: %s\n", SDLNet_GetError());
                        goto DisconnectClient;
                    }
                    
                    if(!FKBNet_Send(ClientSocket, User.Description, DescriptionLength))
                    {
                        Result = MSG_Result_Error;
                        printf("SDLNet error: %s\n", SDLNet_GetError());
                        goto DisconnectClient;
                    }
                    
                    file_read FileRead = ReadEntireFile(User.FilePath);
                    
                    if(!FKBNet_Send(ClientSocket, FileRead.Content, FileRead.Size))
                    {
                        Result = MSG_Result_Error;
                        printf("SDLNet error: %s\n", SDLNet_GetError());
                        goto DisconnectClient;
                    }
                }
                
                if(!FKBNet_Send(ClientSocket, &Result, sizeof(int)))
                {
                    Result = MSG_Result_Error;
                    printf("SDLNet error: %s\n", SDLNet_GetError());
                    goto DisconnectClient;
                }
            }break;
            default:
            {
                assert(0);
            }break;
        }
    }while(1);
    
    DisconnectClient:
    if(conn)
    {
        mysql_close(conn);
    }
    SDLNet_TCP_Close(ClientSocket);
    ConnectedClients[SocketIndex] = 0;
    if(ver)
    {
        srp_verifier_delete(ver);
    }
    printf("Client disconnected");
    return(0);
}
/*
static int
PingClientThread(void *lpParameter)
{
int SocketIndex = (int)lpParameter;
TCPsocket ClientSocket = ConnectedClients[SocketIndex];
while(true)
{
unsigned char Ping = 0;
int recv = SDLNet_TCP_Recv(ClientSocket, &Ping, sizeof(unsigned char));
if(recv <= 0)
{
SDLNet_TCP_Close(ClientSocket);
ConnectedClients[SocketIndex] = 0;
break;
}
SDL_Delay(1000);
}
printf("Lost connection to client\n");
return(0);
}
*/
static int
AcceptClientsThread(void *lpParameter)
{
    TCPsocket ServerSocket = (TCPsocket)lpParameter;
    while(1)
    {
        TCPsocket ClientSocket = SDLNet_TCP_Accept(ServerSocket);
        if(ClientSocket)
        {
            int Found = -1;
            SDL_LockMutex(ConnectedClientsMutex);
            for(int FreeSocket = 0; FreeSocket < MAX_CONNECTED_CLIENTS; ++FreeSocket)
            {
                if(ConnectedClients[FreeSocket] == 0)
                {
                    Found = FreeSocket;
                    ConnectedClients[Found] = ClientSocket;
                    break;
                }
            }
            SDL_UnlockMutex(ConnectedClientsMutex);
            if(Found != -1)
            {
                printf("Client connected\n");
                SDL_CreateThread(ProcessClientThread, "ProcessClientThread", (void*)Found);
            }
            else
            {
                printf("Client was rejected because there are too many clients are connected\n");
                SDLNet_TCP_Close(ClientSocket);
            }
        }
        else
        {
            //printf("Waiting for client\n");
            SDL_Delay(250);
        }
    }
}

int main(int argc, char **argv)
{
    if(SDL_Init(0) == -1) 
    {
        printf("SDL_Init: %s\n", SDL_GetError());
        return(3);
    }
    
    ConnectedClientsMutex = SDL_CreateMutex();
    
    if(SDLNet_Init() == -1) 
    {
        printf("SDLNet_Init: %s\n", SDLNet_GetError());
        return(4);
    }
    
    if(!ConnectedClientsMutex)
    {
        printf("ConnectedClientsMutex is null!: %s\n", SDL_GetError());
        return(2);
    }
    
    signal(SIGABRT, CloseSignalHandler);
    signal(SIGILL, CloseSignalHandler);
    signal(SIGINT, CloseSignalHandler);
    signal(SIGSEGV, CloseSignalHandler);
    signal(SIGTERM, CloseSignalHandler);
    
    IPaddress IP = {0};
    
    if(SDLNet_ResolveHost(&IP, 0, 41231) == -1) 
    {
        printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
        return(5);
    }
    
    ServerSocket = SDLNet_TCP_Open(&IP);
    if(!ServerSocket) 
    {
        printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
        return(6);
    }
    
    SDL_Thread *AcceptClientsThreadHandle = SDL_CreateThread(AcceptClientsThread, "AcceptClientsThread", ServerSocket);
    if(!AcceptClientsThreadHandle)
    {
        printf("SDL_CreateThread: %s\n", SDL_GetError());
        return(7);
    }
    int ReturnValue = 0;
    SDL_WaitThread(AcceptClientsThreadHandle, &ReturnValue);
    /*
    char B[10];
    do
    {
    printf("Command: ");
    scanf("%s", B);
    }while(strcmp(B, "q") != 0);
    */
    SDL_LockMutex(ConnectedClientsMutex);
    for(int FreeSocket = 0; FreeSocket < MAX_CONNECTED_CLIENTS; ++FreeSocket)
    {
        if(ConnectedClients[FreeSocket] == 0)
        {
            SDLNet_TCP_Close(ConnectedClients[FreeSocket]);
            ConnectedClients[FreeSocket] = 0;
        }
    }
    SDL_UnlockMutex(ConnectedClientsMutex);
    
    SDLNet_TCP_Close(ServerSocket);
    SDLNet_Quit();
    SDL_Quit();
    return(ReturnValue);
}