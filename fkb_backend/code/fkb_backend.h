#include <stdio.h>
////////////////DATABASE NAME
#define MYSQL_DATABASENAME "fkb_database"
////////////////TABLE NAME
#define MYSQL_TBLNAME      "userdata"
////////////////SERVER
#define MYSQL_SERVER       "localhost"
////////////////USER
#define MYSQL_USERNAME     "root"
////////////////PASSWORD -OFC JUST FOR TESTING
#define MYSQL_CONPASSWORD  "root"
////////////////DEFINING OUR TABELS
#define MYSQL_PORT         3306

struct file_read
{
    int Size;
    void *Content;
};

static file_read
ReadEntireFile(char *Path)
{
    file_read Result = {0};
    FILE *FileHandle = fopen(Path, "rb");
    if(FileHandle)
    {
        fseek(FileHandle, 0, SEEK_END);
        Result.Size = ftell(FileHandle);
        fseek(FileHandle, 0, SEEK_SET);
        Result.Content = malloc(Result.Size);
        fread(Result.Content, Result.Size, 1, FileHandle);
        fclose(FileHandle);
    }
    return(Result);
}

static bool
WriteEntireFile(char *Path, void *Content, int Size)
{
    FILE *FileHandle = fopen(Path, "wb");
    if(!FileHandle)
    {
        return(false);
    }
    fwrite(Content, Size, 1, FileHandle);
    fclose(FileHandle);
    return(true);
}

static unsigned int
Hash(char *String)
{
    int hashAddress = 5381;
    for(int Counter = 0; String[Counter] != '\0'; Counter++)
    {
        hashAddress = ((hashAddress << 5) + hashAddress) + String[Counter];
    }
    return(hashAddress);
}