#include <stdio.h>
#include <SDL.h>
#include <SDL_net.h>
#include "net.h"
#include "srp.c"

#define Min(a,b) ((a)>(b)?(b):(a))

struct file_read
{
    int Size;
    void *Content;
};

static file_read
ReadEntireFile(char *Path)
{
    file_read Result = {0};
    FILE *FileHandle = fopen(Path, "rb");
    if(FileHandle)
    {
        fseek(FileHandle, 0, SEEK_END);
        Result.Size = ftell(FileHandle);
        fseek(FileHandle, 0, SEEK_SET);
        Result.Content = malloc(Result.Size);
        fread(Result.Content, Result.Size, 1, FileHandle);
        fclose(FileHandle);
    }
    return(Result);
}

static bool
WriteEntireFile(char *Path, void *Content, int Size)
{
    FILE *FileHandle = fopen(Path, "wb");
    if(!FileHandle)
    {
        return(false);
    }
    fwrite(Content, Size, 1, FileHandle);
    fclose(FileHandle);
    return(true);
}

static int
RemoteCall(TCPsocket Socket, int RemoteCallType, ...)
{
    int Result = 0;
    int Response = 0;
    
    static const unsigned char *BytesS = 0;
    static const unsigned char *BytesV = 0;
    static int LengthS = 0;
    static int LengthV = 0;
    static SRP_HashAlgorithm alg = SRP_SHA1;
    static SRP_NGType ng_type = SRP_NG_2048;
    static struct SRPUser *usr = 0;
    
    va_list VaList;
    va_start(VaList, RemoteCallType);
    
    SDLNet_TCP_Send(Socket, &RemoteCallType, sizeof(int));
    SDLNet_TCP_Recv(Socket, &Response, sizeof(int));
    assert(Response == RemoteCallType);
    switch(RemoteCallType)
    {
        case MSG_UserExists:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            
            if(!FKBNet_Send(Socket, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
        }break;
        case MSG_Register:
        case MSG_UpdateCredentials:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            char *Password = va_arg(VaList, char*);
            int PasswordLen = va_arg(VaList, int);
            
            srp_create_salted_verification_key(alg, ng_type, Username, (const unsigned char*)Password, strlen(Password), &BytesS, &LengthS, &BytesV, &LengthV, 0, 0);
            
            if(!FKBNet_Send(Socket, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Send(Socket, (void*)BytesS, LengthS))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Send(Socket, (void*)BytesV, LengthV))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(BytesS)
            {
                free((char*)BytesS);
                BytesS = 0;
            }
            if(BytesV)
            {
                free((char*)BytesV);
                BytesV = 0;
            }
        }break;
        case MSG_LogIn:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            char *Password = va_arg(VaList, char*);
            int PasswordLen = va_arg(VaList, int);
            
            usr = srp_user_new(alg, ng_type, Username, (const unsigned char*)Password, strlen(Password), 0, 0, 1);
            char *AuthUsername = 0;
            const unsigned char *BytesM = 0;
            
            unsigned char BytesB[512] = {0};
            unsigned char SaltIn[512] = {0};
            unsigned char BytesHAMK[512] = {0};
            
            char *BytesA = 0;
            int LengthA = 0;
            int LengthSIn = 0;
            int LengthB = 0;
            int LengthM = 0;
            int LengthHAMK = 0;
            
            srp_user_start_authentication(usr, (const char**)&AuthUsername, (const unsigned char**)&BytesA, &LengthA);
            
            if(!FKBNet_Send(Socket, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            // NOTE(Sanderkp): Result if user exists
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result != MSG_Result_NoError)
            {
                srp_user_delete(usr);
                goto ExitRemoteCall;
            }
            if(!FKBNet_Send(Socket, BytesA, LengthA))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result != MSG_Result_NoError)
            {
                srp_user_delete(usr);
                printf("Verifier SRP-6a safety check violated!\n");
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, SaltIn, &LengthSIn, sizeof(SaltIn)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, BytesB, &LengthB, sizeof(BytesB)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            srp_user_process_challenge(usr, SaltIn, LengthSIn, BytesB, LengthB, &BytesM, &LengthM);
            
            if(!BytesM)
            {
                Result = MSG_Result_Failed;
            }
            if(!FKBNet_Send(Socket, &Result, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result != MSG_Result_NoError) 
            {
                printf("User SRP-6a safety check violation!\n");
                goto ExitRemoteCall;
            }
            if(!FKBNet_Send(Socket, (void*)BytesM, LengthM))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            if(Result != MSG_Result_NoError)
            {
                printf("User authentication failed!\n");
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, BytesHAMK, &LengthHAMK, sizeof(BytesHAMK)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            srp_user_verify_session(usr, BytesHAMK);
            Result = srp_user_is_authenticated(usr);
            if(!FKBNet_Send(Socket, &Result, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
        }break;
        case MSG_LogOut:
        case MSG_Delete:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            
            if(!FKBNet_Send(Socket, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result == MSG_Result_NoError ||
               Result == MSG_Result_NoError)
            {
                if(usr)
                {
                    srp_user_delete(usr);
                    usr = 0;
                }
            }
        }break;
        case MSG_UpdateInfo:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            char *Fullname = va_arg(VaList, char*);
            int FullnameLen = va_arg(VaList, int);
            char *Description = va_arg(VaList, char*);
            int DescriptionLen = va_arg(VaList, int);
            char *ProfileImageFilePath = va_arg(VaList, char*);
            unsigned int FieldsToUpdate = va_arg(VaList, unsigned int);
            
            if(!FKBNet_Send(Socket, &FieldsToUpdate, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Send(Socket, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Send(Socket, Fullname, FullnameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Send(Socket, Description, DescriptionLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            file_read FileRead = {0};
            if(ProfileImageFilePath)
            {
                FileRead = ReadEntireFile(ProfileImageFilePath);
            }
            
            if(!FKBNet_Send(Socket, FileRead.Content, FileRead.Size))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
        }break;
        case MSG_GetInfo:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            user *User  = va_arg(VaList, user*);
            printf("Getting user info\n");
            
            if(!FKBNet_Send(Socket, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            if(Result != MSG_Result_NoError)
            {
                goto ExitRemoteCall;
            }
            
            int FullnameLen = 0;
            int DescriptionLen = 0;
            int ImageSize = 0;
            
            if(!FKBNet_Recv(Socket, User->Fullname, &FullnameLen, MAX_FULL_NAME_LENGTH))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!FKBNet_Recv(Socket, User->Description, &DescriptionLen, MAX_DESCRIPTION_LENGTH))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            User->ImageData = malloc(MAX_PROFILE_IMG_SIZE);
            if(!FKBNet_Recv(Socket, User->ImageData, &ImageSize, MAX_PROFILE_IMG_SIZE))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            if(ImageSize)
            {
                WriteEntireFile("temp.prf", User->ImageData, ImageSize);
            }
            free(User->ImageData);
            
            if(!FKBNet_Recv(Socket, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
        }break;
    }
    
    ExitRemoteCall:
    va_end(VaList);
    return(Result);
}


int main(int argc, char **argv)
{
    if(SDL_Init(0) == -1) 
    {
        printf("SDL_Init: %s\n", SDL_GetError());
        return(1);
    }
    if(SDLNet_Init() == -1) 
    {
        printf("SDLNet_Init: %s\n", SDLNet_GetError());
        return(2);
    }
    
    IPaddress IP = {0};
    TCPsocket TCPSocket = {0};
    
    if(SDLNet_ResolveHost(&IP, "localhost", 41231) == -1) 
    {
        printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
        return(3);
    }
    
    char Cmd[100] = {0};
    while(1)
    {
        TCPSocket = SDLNet_TCP_Open(&IP);
        if(!TCPSocket) 
        {
            printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
            printf("Retry? Y/n: ");
            scanf("%s", Cmd);
            if(!strcmp(Cmd, "n"))
            {
                return(4);
            }
        }
        else
        {
            break;
        }
    }
    
    int IsLoggedIn = 0;
    do
    {
        static char Username[128] = {0};
        static char Password[128] = {0};
        
        printf("Cmd: %s\n", IsLoggedIn ? "logout,revoke,refresh,getuserinfo,updateuserinfo" : "login,register");
        printf("Enter command: ");
        scanf("%s", Cmd);
        
        if(!IsLoggedIn)
        {
            if(!strcmp(Cmd, "login"))
            {
                printf("Enter username: ");
                scanf("%s", Username);
                printf("Enter password: ");
                scanf("%s", Password);
                int Res = RemoteCall(TCPSocket, MSG_LogIn, Username, strlen(Username), Password, strlen(Password));
                if(Res == MSG_Result_NoError)
                {
                    printf("Logged in!\n");
                    IsLoggedIn = true;
                }
                else if(Res == MSG_Result_Failed)
                {
                    printf("Login failed, wrong password or username!\n");
                }
                else if(Res == MSG_Result_AlreadyLoggedIn)
                {
                    printf("User is already logged in!\n");
                }
                else
                {
                    printf("An unknown error occured!\n");
                }
            }
            else if(!strcmp(Cmd, "register"))
            {
                printf("Enter username: ");
                scanf("%s", Username);
                printf("Enter password: ");
                scanf("%s", Password);
                int Res = RemoteCall(TCPSocket, MSG_Register, Username, strlen(Username), Password, strlen(Password));
                if(Res == MSG_Result_NoError)
                {
                    printf("User created! Please log in\n");
                }
                else if(Res == MSG_Result_AlreadyExists)
                {
                    printf("User already exists!\n");
                }
                else if(Res == MSG_Result_Failed)
                {
                    printf("Register failed!\n");
                }
                else
                {
                    printf("An unknown error occured!\n");
                }
            }
            else
            {
                printf("Unknown command");
            }
        }
        else
        {
            if(!strcmp(Cmd, "logout"))
            {
                int Res = RemoteCall(TCPSocket, MSG_LogOut, Username, strlen(Username));
                if(Res == MSG_Result_NoError)
                {
                    IsLoggedIn = false;
                    printf("Logged out!\n");
                }
                else if(Res == MSG_Result_AlreadyLoggedOut)
                {
                    printf("Error already logged out!\n");
                }
                else
                {
                    printf("Failed to log out for an unknown reason!\n");
                }
            }
            else if(!strcmp(Cmd, "revoke"))
            {
                printf("Are you sure?\n");
                printf("Cmd: y/N\n");
                printf("Enter command: ");
                char Option[10];
                scanf("%s", Option);
                if(strcmp(Option, "y"))
                {
                    continue;
                }
                int Res = RemoteCall(TCPSocket, MSG_Delete, Username, strlen(Username));
                if(Res == MSG_Result_NoError)
                {
                    printf("Your account has been deleted!\n");
                    IsLoggedIn = 0;
                }
                else if(Res == MSG_Result_NotLoggedIn)
                {
                    printf("You need to be logged in to do that!\n");
                }
                else if(Res == MSG_Result_Failed)
                {
                    printf("Failed to delete user!\n");
                }
                else
                {
                    printf("An unknown error occured!\n");
                }
            }
            else if(!strcmp(Cmd, "refresh"))
            {
                printf("Are you sure?\n");
                printf("Cmd: y/N\n");
                printf("Enter command: ");
                char Option[10];
                scanf("%s", Option);
                if(strcmp(Option, "y"))
                {
                    continue;
                }
                printf("Enter the new password: ");
                scanf("%s", Password);
                int Res = RemoteCall(TCPSocket, MSG_UpdateCredentials, Username, strlen(Username), Password, strlen(Password));
                if(Res == MSG_Result_NoError)
                {
                    printf("Credentials updated!\n");
                }
                else if(Res == MSG_Result_NotLoggedIn)
                {
                    printf("You need to be logged in to do that!\n");
                }
                else if(Res == MSG_Result_Failed)
                {
                    printf("Failed to refresh credentials!\n");
                }
                else
                {
                    printf("An unknown error occured!\n");
                }
            }
            else if(!strcmp(Cmd, "updateuserinfo"))
            {
                char Fullname[MAX_FULL_NAME_LENGTH] = {0};
                char Description[MAX_DESCRIPTION_LENGTH] = {0};
                char FilePath[MAX_FILEPATH_LEGNTH] = {0};
                unsigned int FieldsToUpdate = 0;
                char Option[10];
                printf("Update fullname?\nCmd: y/N\nEnter command: ");
                scanf("%s", Option);
                if(!strcmp(Option, "y"))
                {
                    printf("Enter fullname: ");
                    scanf("%s", Fullname);
                    FieldsToUpdate |= MYSQL_UPDATE_FULLNAME;
                }
                printf("Update description?\nCmd: y/N\nEnter command: ");
                scanf("%s", Option);
                if(!strcmp(Option, "y"))
                {
                    printf("Enter description: ");
                    scanf("%s", Description);
                    FieldsToUpdate |= MYSQL_UPDATE_DESCRIPTION;
                }
                printf("Update profile image?\nCmd: y/N\nEnter command: ");
                scanf("%s", Option);
                if(!strcmp(Option, "y"))
                {
                    printf("Enter path to image: ");
                    scanf("%s", FilePath);
                    FieldsToUpdate |= MYSQL_UPDATE_PROFILE_IMAGE;
                }
                int Res = RemoteCall(TCPSocket, MSG_UpdateInfo, Username, strlen(Username), Fullname, strlen(Fullname), Description, strlen(Description), FilePath, FieldsToUpdate);
                if(Res == MSG_Result_NoError)
                {
                    printf("User info updated!\n");
                }
                else if(Res == MSG_Result_NotLoggedIn)
                {
                    printf("You need to be logged in to do that!\n");
                }
                else if(Res == MSG_Result_Failed)
                {
                    printf("Failed to update user info!\n");
                }
                else
                {
                    printf("An unknown error occured!\n");
                }
            }
            else if(!strcmp(Cmd, "getuserinfo"))
            {
                user User = {0};
                int Res = RemoteCall(TCPSocket, MSG_GetInfo, Username, strlen(Username), &User);
                if(Res == MSG_Result_NoError)
                {
                    printf("Username: %s\nFullName: %s\nDescription: %sFilePath: %s\n", User.Username, User.Fullname, User.Description, User.FilePath);
                    
                }
                else if(Res == MSG_Result_NotLoggedIn)
                {
                    printf("You need to be logged in to do that!\n");
                }
                else if(Res == MSG_Result_Failed)
                {
                    printf("Failed to get user info!\n");
                }
                else
                {
                    printf("An unknown error occured!\n");
                }
            }
            else
            {
                printf("Unknown command");
            }
        }
    }while(strcmp(Cmd, "q") != 0);
    
    char B[10];
    scanf("%s", B);
    SDLNet_TCP_Close(TCPSocket);
    SDLNet_Quit();
    SDL_Quit();
    return(0);
}