#define assert(exp) if(!(exp)){*(int*)(0)=0;}

#define MAX_USERNAME_LENGTH 64
#define MAX_FULL_NAME_LENGTH 64
#define MAX_SALT_LENGTH 32
#define MAX_VERIFIER_LENGTH 256
#define MAX_FILEPATH_LEGNTH 128
// TODO(Sanderkp): HACK@
#define MAX_DESCRIPTION_LENGTH 512

//500kb
#define MAX_PROFILE_IMG_SIZE 1024*500

typedef struct user
{
    char Username[MAX_USERNAME_LENGTH];
    char Fullname[MAX_FULL_NAME_LENGTH];
    char Description[MAX_DESCRIPTION_LENGTH];
    char FilePath[MAX_FILEPATH_LEGNTH];
    unsigned char Salt[MAX_SALT_LENGTH];
    unsigned char Verifier[MAX_VERIFIER_LENGTH];
    
    int SaltLength;
    int VerifierLength;
    
    void *ImageData;
}user;

enum RemoteCallTypes
{
    MSG_NullCall,
    MSG_UserExists,
    
    MSG_Register,
    MSG_LogIn,
    MSG_LogOut,
    MSG_Delete,
    MSG_UpdateCredentials,
    MSG_UpdateInfo,
    MSG_GetInfo
};

enum RemoteCallResultTypes
{
    MSG_Result_Null,
    
    MSG_Result_NoError,
    MSG_Result_Failed,
    MSG_Result_AlreadyLoggedIn,
    MSG_Result_NotLoggedIn,
    MSG_Result_AlreadyExists,
    MSG_Result_AlreadyLoggedOut,
    
    MSG_Result_Error,
};

enum UserFieldsToUpdate
{
    MYSQL_UPDATE_FULLNAME = 0x1,
    MYSQL_UPDATE_DESCRIPTION = 0x2,
    MYSQL_UPDATE_PROFILE_IMAGE = 0x4,
};

#define Min(a,b) ((a)>(b)?(b):(a))

static bool
FKBNet_Send(TCPsocket Socket, void *Source, int Size)
{
    if(SDLNet_TCP_Send(Socket, &Size, sizeof(int)) < sizeof(int))
    {
        return(false);
    }
    int Ok = 0;
    if(SDLNet_TCP_Recv(Socket, &Ok, sizeof(int)) <= 0)
    {
        return(false);
    }
    if(!Ok)
    {
        return(false);
    }
    if(Size)
    {
        if(Size > 1024)
        {
            int Count = (Size/1024) + (Size%1024 != 0 ? 1 : 0);
            int DataSent = 0;
            for(int Block = 0; Block < Count; ++Block)
            {
                int S = Min(Size-DataSent, 1024);
                if(SDLNet_TCP_Send(Socket, (unsigned char*)Source + DataSent, S) < S)
                {
                    return(false);
                }
                DataSent += S;
            }
        }
        else
        {
            if(SDLNet_TCP_Send(Socket, Source, Size) < Size)
            {
                return(false);
            }
        }
    }
    return(true);
}

static bool
FKBNet_Recv(TCPsocket Socket, void *Dest, int *Size, int MaxSize)
{
    int _Size = 0;
    if(SDLNet_TCP_Recv(Socket, &_Size, sizeof(int)) < sizeof(int))
    {
        return(false);
    }
    int Ok = (_Size <= MaxSize);
    if(SDLNet_TCP_Send(Socket, &Ok, sizeof(int)) <= 0)
    {
        return(false);
    }
    if(!Ok)
    {
        return(false);
    }
    if(_Size)
    {
        if(_Size > 1024)
        {
            int Recieved = 0;
            while(Recieved < _Size)
            {
                int Recv = SDLNet_TCP_Recv(Socket, (unsigned char*)Dest + Recieved, 1);
                if(Recv > 0)
                {
                    Recieved += Recv;
                }
                else
                {
                    return(false);
                }
            }
        }
        else
        {
            if(SDLNet_TCP_Recv(Socket, Dest, _Size) < _Size)
            {
                return(false);
            }
        }
    }
    if(Size)
    {
        *Size = _Size;
    }
    return(true);
}
