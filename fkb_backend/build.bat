@echo off

ctime -begin "fkb_backend.ctm"

set VCVarsPath="A:/SYSTEM/Visual Studio 2015/VC/vcvarsall.bat"
if not exist %VCVarsPath% (
	echo vcvarsall.bat not found, please provide a valid path
	goto end
)

call %VCVarsPath% x64

set dir=%~dp0

set CommonCompilerFlags=-Zi -MD -Gm -nologo -EHa -Od -wd4311 -wd4312 -DDEBUG /I %dir%code\sdl2\include /I %dir%code\sdl2_net\include /I %dir%code\mysql\include /I%dir%code\openssl-1.1.0e-vs2015\include64
set CommonLinkerFlags=/incremental:no /opt:ref -SUBSYSTEM:CONSOLE -LIBPATH:%dir%code\sdl2\lib\x64 -LIBPATH:%dir%code\sdl2_net\lib\x64 -LIBPATH:%dir%code\openssl-1.1.0e-vs2015\lib64 -LIBPATH:%dir%code\mysql\lib SDL2.lib SDL2main.lib SDL2_net.lib Advapi32.lib libcryptoMD.lib libmysql.lib

pushd %dir%build

cl %CommonCompilerFlags% -Fesrp_client %dir%code\client_main.cpp /link %CommonLinkerFlags%
cl %CommonCompilerFlags% -Fesrp_server %dir%code\fkb_backend.cpp /link %CommonLinkerFlags%

set LastError=%ERRORLEVEL%

popd

ctime -end "fkb_backend.ctm" %ERRORLEVEL%
