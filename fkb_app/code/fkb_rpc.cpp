///##################################################################################
/// fkb_rpc.cpp
/// 
/// fkb_rpc contains the remote call procedure implementation. This is used when
/// communicating with the back-end server and when using SRP. (See fkb_gui.cpp etc)
/// 
/// 
///##################################################################################

///##################################################################################
/// Function:
/// int RemoteCall(net_state *Net, int RemoteCallType, ...)
///
/// About:
/// Performs a remote procedure call, see enum RemoteCallTypes in fkb_net.h.
/// 
/// Parameters:
/// This function takes a variable number of arguments depending on what the 
/// parameter RemoteCallType is.
/// If called with RemoteCallType == MSG_UserExists:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///
/// If called with RemoteCallType == MSG_Register:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///     char *Password - Pointer to password, CAN NOT be NULL
///     int PasswordLen - Length of password
///
/// If called with RemoteCallType == MSG_LogIn:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///     char *Password - Pointer to password, CAN NOT be NULL
///     int PasswordLen - Length of password
///
/// If called with RemoteCallType == MSG_LogOut:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///
/// If called with RemoteCallType == MSG_Delete:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///
/// If called with RemoteCallType == MSG_UpdateCredentials:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///     char *Password - Pointer to password, CAN NOT be NULL
///     int PasswordLen - Length of password
///
/// If called with RemoteCallType == MSG_UpdateInfo:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///     char *Fullname - Pointer to fullname, can be NULL depending on FieldsToUpdate
///     int FullnameLen - Length of fullname
///     char *Description - Pointer to Description, can be NULL depending on FieldsToUpdate
///     int DescriptionLen - Length of Description
///     char *ProfileImageFilePath - Pointer to profile image path, can be NULL depending on FieldsToUpdate
///     int FieldsToUpdate - Which fields to update, ORed together by fields in enum UserFieldsToUpdate (See fkb_net.h)
///
/// If called with RemoteCallType == MSG_GetInfo:
///
///     char *Username - Pointer to username, CAN NOT be NULL
///     int UsernameLen - Length of username
///     char *Password - Pointer to password, CAN NOT be NULL
///     int PasswordLen - Length of password
///
/// Return value:
///
/// If called with RemoteCallType == MSG_UserExists:
///
///     True                        if success (The user exists)
///     False                       if the user does not exist
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_Register:
///     
///     MSG_Result_NoError          if success
///     MSG_Result_Failed           if server side error
///     MSG_Result_AlreadyExists    if user already exists
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_LogIn:
///
///     MSG_Result_NoError          if success
///     MSG_Result_Failed           if server side error
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_LogOut:
///
///     MSG_Result_NoError          if success
///     MSG_Result_AlreadyLoggedOut if already logged out
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_Delete:
///
///     MSG_Result_NoError          if success
///     MSG_Result_Failed           if not logged in or server error
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_UpdateCredentials:
///
///     MSG_Result_NoError          if success
///     MSG_Result_Failed           if server side error
///     MSG_Result_NotLoggedIn      if user is not logged in
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_UpdateInfo:
///
///     MSG_Result_NoError          if success
///     MSG_Result_Failed           if server side error
///     MSG_Result_NotLoggedIn      if user is not logged in
///     MSG_Result_Error            if error
///
/// If called with RemoteCallType == MSG_GetInfo:
///
///     MSG_Result_NoError          if success
///     MSG_Result_Failed           if server side error
///     MSG_Result_NotLoggedIn      if user is not logged in
///     MSG_Result_Error            if network error
/// 
///##################################################################################
internal int
RemoteCall(net_state *Net, int RemoteCallType, ...)
{
    int Result = 0;
    int Response = 0;
    
    const unsigned char *BytesS = 0;
    const unsigned char *BytesV = 0;
    int LengthS = 0;
    int LengthV = 0;
    SRP_HashAlgorithm alg = SRP_SHA1;
    SRP_NGType ng_type = SRP_NG_2048;
    struct SRPUser *usr = 0;
    
    va_list VaList;
    va_start(VaList, RemoteCallType);
    
    if(!Net_Send(Net, &RemoteCallType, sizeof(int)))
    {
        Result = MSG_Result_Error;
        printf("SDLNet error: %s\n", SDLNet_GetError());
        goto ExitRemoteCall;
    }
    if(!Net_Recv(Net, &Response, 0, sizeof(int)))
    {
        Result = MSG_Result_Error;
        printf("SDLNet error: %s\n", SDLNet_GetError());
        goto ExitRemoteCall;
    }
    assert(Response == RemoteCallType);
    switch(RemoteCallType)
    {
        case MSG_UserExists:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            
            if(!Net_Send(Net, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
        }break;
        case MSG_Register:
        case MSG_UpdateCredentials:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            char *Password = va_arg(VaList, char*);
            int PasswordLen = va_arg(VaList, int);
            
            srp_create_salted_verification_key(alg, ng_type, Username, (const unsigned char*)Password, PasswordLen, &BytesS, &LengthS, &BytesV, &LengthV, 0, 0);
            
            if(!Net_Send(Net, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Send(Net, (void*)BytesS, LengthS))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Send(Net, (void*)BytesV, LengthV))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(BytesS)
            {
                free((char*)BytesS);
                BytesS = 0;
            }
            if(BytesV)
            {
                free((char*)BytesV);
                BytesV = 0;
            }
        }break;
        case MSG_LogIn:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            char *Password = va_arg(VaList, char*);
            int PasswordLen = va_arg(VaList, int);
            
            usr = srp_user_new(alg, ng_type, Username, (const unsigned char*)Password, PasswordLen, 0, 0, 1);
            char *AuthUsername = 0;
            const unsigned char *BytesM = 0;
            
            unsigned char BytesB[512] = {0};
            unsigned char SaltIn[512] = {0};
            unsigned char BytesHAMK[512] = {0};
            
            char *BytesA = 0;
            int LengthA = 0;
            int LengthSIn = 0;
            int LengthB = 0;
            int LengthM = 0;
            int LengthHAMK = 0;
            
            srp_user_start_authentication(usr, (const char**)&AuthUsername, (const unsigned char**)&BytesA, &LengthA);
            
            if(!Net_Send(Net, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            // NOTE(Sanderkp): Result if user exists
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result != MSG_Result_NoError)
            {
                srp_user_delete(usr);
                goto ExitRemoteCall;
            }
            if(!Net_Send(Net, BytesA, LengthA))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result != MSG_Result_NoError)
            {
                srp_user_delete(usr);
                printf("Verifier SRP-6a safety check violated!\n");
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, SaltIn, &LengthSIn, sizeof(SaltIn)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, BytesB, &LengthB, sizeof(BytesB)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            srp_user_process_challenge(usr, SaltIn, LengthSIn, BytesB, LengthB, &BytesM, &LengthM);
            
            if(!BytesM)
            {
                Result = MSG_Result_Failed;
            }
            if(!Net_Send(Net, &Result, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result != MSG_Result_NoError) 
            {
                printf("User SRP-6a safety check violation!\n");
                goto ExitRemoteCall;
            }
            if(!Net_Send(Net, (void*)BytesM, LengthM))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            if(Result != MSG_Result_NoError)
            {
                printf("User authentication failed!\n");
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, BytesHAMK, &LengthHAMK, sizeof(BytesHAMK)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            srp_user_verify_session(usr, BytesHAMK);
            Result = srp_user_is_authenticated(usr);
            if(!Net_Send(Net, &Result, sizeof(int)))
            {
                Result = MSG_Result_Error;
                srp_user_delete(usr);
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
        }break;
        case MSG_LogOut:
        case MSG_Delete:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            
            if(!Net_Send(Net, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(Result == MSG_Result_NoError ||
               Result == MSG_Result_NoError)
            {
                if(usr)
                {
                    srp_user_delete(usr);
                    usr = 0;
                }
            }
        }break;
        case MSG_UpdateInfo:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            char *Fullname = va_arg(VaList, char*);
            int FullnameLen = va_arg(VaList, int);
            char *Description = va_arg(VaList, char*);
            int DescriptionLen = va_arg(VaList, int);
            char *ProfileImageFilePath = va_arg(VaList, char*);
            unsigned int FieldsToUpdate = va_arg(VaList, unsigned int);
            
            if(!Net_Send(Net, &FieldsToUpdate, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Send(Net, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Send(Net, Fullname, FullnameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Send(Net, Description, DescriptionLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            fkb_read_file FileRead = {0};
            if(ProfileImageFilePath)
            {
                FileRead = Platform.ReadEntireFile(ProfileImageFilePath);
                
            }
            
            if(!Net_Send(Net, FileRead.Content, (s32)FileRead.Size))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
        }break;
        case MSG_GetInfo:
        {
            char *Username = va_arg(VaList, char*);
            int UsernameLen = va_arg(VaList, int);
            user *User  = va_arg(VaList, user*);
            printf("Getting user info\n");
            
            if(!Net_Send(Net, Username, UsernameLen))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            if(Result != MSG_Result_NoError)
            {
                goto ExitRemoteCall;
            }
            
            int FullnameLen = 0;
            int DescriptionLen = 0;
            int ImageSize = 0;
            
            if(!Net_Recv(Net, User->Fullname, &FullnameLen, MAX_FULL_NAME_LENGTH))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!Net_Recv(Net, User->Description, &DescriptionLen, MAX_DESCRIPTION_LENGTH))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            
            if(!User->ImageData)
            {
                User->ImageData = malloc(MAX_PROFILE_IMG_SIZE);
            }
            if(!Net_Recv(Net, User->ImageData, &ImageSize, MAX_PROFILE_IMG_SIZE))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
            if(ImageSize)
            {
                User->ImageDataSize = ImageSize;
                SDL_AtomicSet(&User->HasImage, true);
            }
            
            if(!Net_Recv(Net, &Result, 0, sizeof(int)))
            {
                Result = MSG_Result_Error;
                printf("SDLNet error: %s\n", SDLNet_GetError());
                goto ExitRemoteCall;
            }
        }break;
    }
    
    ExitRemoteCall:
    va_end(VaList);
    return(Result);
}


