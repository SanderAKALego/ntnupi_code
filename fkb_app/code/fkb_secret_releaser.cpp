///##################################################################################
/// fkb_secret_release.cpp
/// 
/// This is the back bone of the fingerprint method, there are two algorithms here
/// (enrol and authenticate) that makes up most of the secret releaser.
/// The secret releaser can also be compiled as a standalone application for
/// testing purposes. #define SECRET_RELEASER_STANDALONE and then compile
/// fkb_secret_release.cpp
/// 
/// 
///##################################################################################
#ifdef SECRET_RELEASER_STANDALONE
///
/// If compiling as a standalone application we need to include these
///
#include <SDL.h>
#include <AES.h>
#include <AES.cpp>
#include <tiny_aes/aes.c>
#include <duthomhas/csprng.h>
#include "fkb_platform.h"
#include "bch.cpp"
#include "fkb_work_queue.cpp"
#endif

//
// NOTE(Sanderkp): BITS(x) gets the minimum number of bytes required to store x bits
//                 Examples: 8 bits -> 1 byte
//                           16 bits -> 2 bytes
//                           39 bits -> 5 bytes (some wasted bits)
//
#define BITS(x) ((x/8)+(x%8?1:0))

//
// NOTE(Sanderkp): L2 vicinity area in protected template
//
struct l2_vicinity
{
    //
    // NOTE(Sanderkp): This is where the DAT/RAT triplets are stored
    //               FlagCode[0] = D
    //               FlagCode[1] = A
    //               FlagCode[2] = T
    //
    u8 FlagCode[4];
    //
    // NOTE(Sanderkp): Layer 2 information (not 100% sure what these contain)
    //
    u8 VICCode[16];
};

//
// NOTE(Sanderkp): L1 vicinity area in protected template
//
struct l1_vicinity
{
    //
    // NOTE(Sanderkp): Number of L2 vicinities
    //
    u8 L2VicinityCount;
    //
    // NOTE(Sanderkp): Not 100% sure what this is, is it used to determine roughly
    //                 what quadrant it is in?
    //
    u8 MinutiaMap[5];
    //
    // NOTE(Sanderkp): L2 vicinities
    //
    l2_vicinity *L2Vicinity;
};

//
// NOTE(Sanderkp): Protected template struct
//
struct protected_template
{
    //
    // NOTE(Sanderkp): Size of the ENTIRE protected template file,
    //               if this does not match the filesize we assume error
    //
    u32 FileSize;
    //
    // NOTE(Sanderkp): Number of L1 vicinities
    //
    u8 L1VicinityCount;
    //
    // NOTE(Sanderkp): Pointer to l1 vicinities
    //
    l1_vicinity *L1Vicinity;
};

//
// NOTE(Sanderkp): Auxiliary data/helper data structure
//                 As you can see we keep the notion of mega vicinities
//
struct auxiliary_data
{
    //
    // NOTE(Sanderkp): Number of mega vicinities
    //
    u8 MVCount;
    //
    // NOTE(Sanderkp): This struct defines each mega vicinity
    //
    struct auxiliary_mv
    {
        //
        // NOTE(Sanderkp): Number of DAT triplets
        //
        u8 DATCount;
        //
        // NOTE(Sanderkp): Structure for each DAT triplet
        //
        struct auxiliary_dat
        {
            //
            // NOTE(Sanderkp): The DAT triplets are 244 bits (100+72+72) but we store 256 bits either way
            //                 (just padded with zero).
            //
            u8 e[BITS(256)];
        }*DAT;
    }*mv;
};

//
// NOTE(Sanderkp): We pack all paramteres to cross matching function in here (makes threading easy)
//
struct cross_match_params
{
    //
    // NOTE(Sanderkp): The L1 vicinity area to cross match
    //
    l1_vicinity *L1Vicinity;
    //
    // NOTE(Sanderkp): The secret candiate the cross matching produced, to expand this
    //               to support multiple candiates, just add a u32 *Count pointer and u32 MaxCount
    //               And pass a pointer to a sufficiently large enough array
    //
    u8 *SecretCandidate;
    
    //
    // NOTE(Sanderkp): The current mega vicinity area to operate inside
    //
    auxiliary_data::auxiliary_mv *ad;
};

///##################################################################################
/// 
/// GLOBALS
/// 
///##################################################################################
//
// NOTE(Sanderkp): BCH instance, must be created at start before calling other functions
//
global bch *BCH;
//
// NOTE(Sanderkp): Key to AES, not very secure!
//
global u8 SecretKey[BITS(128)] = "yshjs29sjvnqalf";
//
// NOTE(Sander): AES iv
//
global u8 iv[] = {0xda,0x39,0xa3,0xee,0x5e,0x6b,0x4b,0x0d,0x32,0x55,0xbf,0xef,0x95,0x60,0x18,0x90};
//
// NOTE(Sander): Crypto random number generator instance, must be initialized before authentication/enrolment
//
global CSPRNG rng = {0};

///##################################################################################
/// Function:
/// protected_template  ReadProtectedTemplate(char *Path)
///
/// About:
/// Reads a protected template (.tpl) from disk.
/// 
/// Parameters:
/// Path - Filepath to template, relative to working directory, or absolute file path
///
/// Return value:
/// The protected template if parsing succeeded, or an empty template if fail
///
/// Note:
/// The data structure as I understand it:
/// u32 FileSize
/// u8 L1VicinityCount
/// FOR EACH L1 VICINITY
///     u8 L2VicinityCount
///     FOR EACH L2 VICINITY
///         u8 FlagCode[4]
///         u8 VICCode[16]
///     u8 MinutiaMap[5]
///
///##################################################################################
internal protected_template 
ReadProtectedTemplate(char *Path)
{
    protected_template pt = {};
    file_handle *f = Platform.OpenFile(Path, FileMode_Open);
    if(f)
    {
        Platform.Read(&pt.FileSize, sizeof(u32), f);
        Platform.Read(&pt.L1VicinityCount, sizeof(u8), f);
        pt.L1Vicinity = (l1_vicinity*)Platform.Allocate(sizeof(l1_vicinity) * pt.L1VicinityCount);
        for(u32 i = 0; i < pt.L1VicinityCount; ++i)
        {
            Platform.Read(&pt.L1Vicinity[i].L2VicinityCount, sizeof(u8), f);
            pt.L1Vicinity[i].L2Vicinity = (l2_vicinity*)Platform.Allocate(sizeof(l2_vicinity) * pt.L1Vicinity[i].L2VicinityCount);
            for(u32 j = 0; j < pt.L1Vicinity[i].L2VicinityCount; ++j)
            {
                Platform.Read(&pt.L1Vicinity[i].L2Vicinity[j].FlagCode, sizeof(u8)*4, f);
                Platform.Read(&pt.L1Vicinity[i].L2Vicinity[j].VICCode, sizeof(u8)*16, f);
            }
            Platform.Read(&pt.L1Vicinity[i].MinutiaMap, sizeof(u8)*5, f);
        }
        Platform.CloseFile(f);
    }
    return(pt);
}

///##################################################################################
/// Function:
/// void FreeProtectedTemplate(protected_template *pt)
///
/// About:
/// Frees all the memory allocated by ReadProtectedTemplate
/// 
/// Parameters:
/// pt - A valid pointer to a protected_template structure
///
/// Return value:
/// None
/// 
///##################################################################################
internal void
FreeProtectedTemplate(protected_template *pt)
{
    for(u32 i = 0; i < pt->L1VicinityCount; ++i)
    {
        Platform.Free(pt->L1Vicinity[i].L2Vicinity);
    }
    Platform.Free(pt->L1Vicinity);
}

///##################################################################################
/// Function:
///  void ToUnaryRepresentation(u8 *Dest, umm Offset, int Num)
///
/// About:
/// Converts a number to the unary represented number
/// 
/// Parameters:
/// Dest - Array of u8's to store the bits
/// Offset - A bit offset into Dest where the resulting bits should be written
/// Num - The actual number to represent in unary
///
/// Return value:
/// None
///
/// Note:
/// You MUST ensure that Dest is big enough because no bounds checking is done!
/// 
///##################################################################################
internal void
ToUnaryRepresentation(u8 *Dest, umm Offset, u32 Num)
{
    for(u32 i = 0; i < (u32)Num; ++i)
    {
        Dest[(Offset+i) / 8] |= (1 << ((Offset + i) % 8));
    }
}

///##################################################################################
/// Function:
/// int FromUnaryRepresentation(u8 *Source, umm Offset, umm Length)
///
/// About:
/// Convert a number represented in unary to a two's compliment number (IE a regular
/// binary number)
/// 
/// Parameters:
/// Source - The array of bits containing the number
/// Offset - Bit offset into Source array
/// Length - Length of the unary number
///
/// Return value:
/// A regular binary number
///
/// Note:
/// You MUST ensure that Source is big enough, no bounds checking is done!
/// 
///##################################################################################
internal int
FromUnaryRepresentation(u8 *Source, umm Offset, umm Length)
{
    int Result = 0;
    for(u32 i = 0; i < Length; ++i)
    {
        if(Source[((Offset + i) / 8)] & ((Offset + i) % 8))
        {
            Result++;
        }
    }
    return(Result);
}

///##################################################################################
/// Function:
/// void BitCopy(u8 *Dest, umm Offset, u8 *Source, umm Length)
///
/// About:
/// Copies bits (not bytes!) from Source to Dest
/// 
/// Parameters:
/// Dest - Destination array
/// Offset - Offset into Destination
/// Source - Source bits to copy
/// Length - Number of bits to copy from Source to Dest
///
/// Return value:
/// None
///
/// Note:
/// No bounds checking is done, make sure your arrays are big enough!
/// 
///##################################################################################
internal void
BitCopy(u8 *Dest, umm Offset, u8 *Source, umm Length)
{
    for(u32 i = 0; i < Length; ++i)
    {
        umm _ByteIndex = (Offset + i) / 8;
        umm _BitIndex = (Offset + i) % 8;
        Dest[_ByteIndex] &= ~(1 << _BitIndex);
    }
    for(u32 i = 0; i < Length; ++i)
    {
        umm _ByteIndex = (Offset + i) / 8;
        umm _BitIndex = (Offset + i) % 8;
        Dest[_ByteIndex] |= Source[i / 8] & (1 << (i%8));
    }
}

///##################################################################################
/// Function:
/// void BitCopy(u8 *Dest, umm DestOffset, u8 *Source, umm SourceOffset, umm Length)
///
/// About:
/// Copies bits from Source to Dest, similiar to the other BitCopy except that this
/// supports offsets into Source. 
/// 
/// Parameters:
/// Dest - Destination array
/// Offset - Bit offset into Destination
/// Source - Source bits to copy
/// SourceOffset - Bit offset into Source
/// Length - Number of bits to copy from Source to Dest
///
/// Return value:
/// None
///
/// Note:
/// No bounds checking is done, make sure your arrays are big enough!
/// 
///##################################################################################
internal void
BitCopy(u8 *Dest, umm DestOffset, u8 *Source, umm SourceOffset, umm Length)
{
    for(u32 i = 0; i < Length; ++i)
    {
        umm _ByteIndex = (DestOffset + i) / 8;
        umm _BitIndex = (DestOffset + i) % 8;
        Dest[_ByteIndex] &= ~(1 << _BitIndex);
    }
    for(u32 i = 0; i < Length; ++i)
    {
        umm _ByteIndex = (DestOffset + i) / 8;
        umm _BitIndex = (DestOffset + i) % 8;
        Dest[_ByteIndex] |= Source[(i + SourceOffset) / 8] & (1 << ((SourceOffset + i) % 8));
    }
}

///##################################################################################
/// Function:
/// auxiliary_data ReadAuxiliaryData(char *Path)
///
/// About:
/// Reads auxiliary data/helper data from file.
/// 
/// Parameters:
/// Path to ad file, relative to working dir or absolute path
///
/// Return value:
/// A filled out auxiliary_data structure if success, our an empty one if fail
///
/// 
///##################################################################################
internal auxiliary_data
ReadAuxiliaryData(char *Path)
{
    auxiliary_data ad = {0};
    file_handle *FileHandle = Platform.OpenFile(Path, FileMode_Open);
    if(FileHandle)
    {
        Platform.Read(&ad.MVCount, sizeof(u8), FileHandle);
        ad.mv = (auxiliary_data::auxiliary_mv*)Platform.Allocate(sizeof(ad.mv[0]) * ad.MVCount);
        for(int MVIndex = 0; MVIndex < ad.MVCount; ++MVIndex)
        {
            Platform.Read(&ad.mv[MVIndex].DATCount, sizeof(u8), FileHandle);
            ad.mv[MVIndex].DAT = (auxiliary_data::auxiliary_mv::auxiliary_dat*)Platform.Allocate(sizeof(u8)*BITS(256)*ad.mv[MVIndex].DATCount);
            Platform.Read(ad.mv[MVIndex].DAT, sizeof(u8)*BITS(256)*ad.mv[MVIndex].DATCount , FileHandle);
        }
        Platform.CloseFile(FileHandle);
    }
    return(ad);
}

///##################################################################################
/// Function:
/// int SecretCompareFunc(const void *a, const void *b)
///
/// About:
/// Compares two secrets (using memcmp), this is used for sorting
/// 
/// Parameters:
/// a - First secret
/// b - Second secret
///
/// Return value:
/// See return values of memcmp
///
/// Note:
/// Only used for qsort
/// 
///##################################################################################
internal int
SecretCompareFunc(const void *a, const void *b)
{
    return(memcmp(a, b, sizeof(u8)*BITS(128)));
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(CrossMatchFunc)
///
/// About:
/// This function does the following: 
///    1) Generate a lot of secrets using cross matching
///    2) Use majority voting to get the most similiar candiate
/// 
/// Parameters:
/// Data - Valid pointer to a cross_match_params
///
/// Return value:
/// A secret candiate in cross_match_params structure passed to this function
///
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(CrossMatchFunc)
{
    cross_match_params *Params = (cross_match_params*)Data;
    u8 *L2SecretCandidates = (u8*)Platform.Allocate(sizeof(u8)*BITS(128) * 255 * 255);
    u32 L2CandidateIndex = 0;
    
    //
    // NOTE(Sanderkp): Cross XORing
    //
    for(int L2Index = 0; L2Index < Params->L1Vicinity->L2VicinityCount; ++L2Index)
    {
        for(int AdIndex = 0; AdIndex < Params->ad->DATCount; ++AdIndex)
        {
            int D = Params->L1Vicinity->L2Vicinity[L2Index].FlagCode[0];
            int A = Params->L1Vicinity->L2Vicinity[L2Index].FlagCode[1];
            int T = Params->L1Vicinity->L2Vicinity[L2Index].FlagCode[2];
            
            assert(D < 100 && A < 72 && T < 72);
            
            u8 _DATPrime[BITS(256)] = {0};
            ToUnaryRepresentation(_DATPrime, 0, D);
            ToUnaryRepresentation(_DATPrime, 100, A);
            ToUnaryRepresentation(_DATPrime, 172, T);
            
            //
            // NOTE(Sanderkp): Ecc(p) = DAT XOR AD
            //
            u8 EccP[BITS(256)] = {0};
            for(u32 Index = 0; Index < ArrayCount(EccP); ++Index)
            {
                EccP[Index] = _DATPrime[Index] ^ Params->ad->DAT[AdIndex].e[Index];
            }
            
            //
            // NOTE(Sanderkp): Ecc(p) -> decode BCH -> p
            //
            u8 p[BITS(207)] = {0};
            if(BCH->Decode2(p, EccP))
            {
                //
                // NOTE(Sanderkp): p = AES(Km, s|r)
                //                 -> decrypt AES -> s|r
                //                 -> extract s -> s
                //
                u8 _s[BITS(128)] = {0};
                u8 AESOut[BITS(256)] = {0};
                u8 AESIn[BITS(256)] = {0};
                u8 __r[BITS(79)] = {0};
                BitCopy(AESIn, 0, p, 207);
                
                AES_CBC_decrypt_buffer(AESOut, AESIn, ArrayCount(AESIn), SecretKey, iv);
                
                //
                // NOTE(Sanderkp): The random value is 79 bits and the Secret is 128 bits
                //
                BitCopy(_s, 0, AESOut, 128);
                BitCopy(__r, 0, AESOut, 128, 79);
                
                memcpy(L2SecretCandidates + (L2CandidateIndex++ * sizeof(u8)*BITS(128)), _s, sizeof(_s));
            }
        }
    }
    
    //
    // NOTE(Sanderkp): Majority voting:
    //                 We sort the secrets first,
    //                 then we count the most popular ones in a single loop,
    //                 and finally we pick the most popular secret again in a single loop
    //                 This is (as we found) faster than a straight up n^2 comparison,
    //                 there are faster ways to do this though.
    //
    qsort(L2SecretCandidates, L2CandidateIndex, sizeof(u8)*BITS(128), SecretCompareFunc);
    s32 BestS = 0;
    s32 BestSCount = -1;
    s32 *Counts = (s32*)Platform.Allocate(L2CandidateIndex*sizeof(s32));
    ZeroMem(Counts, sizeof(s32)*L2CandidateIndex);
    s32 Current = 0;
    for(u32 i = 0; i < L2CandidateIndex; i++)
    {
        if(memcmp(L2SecretCandidates + (i * sizeof(u8)*BITS(128)), L2SecretCandidates + (Current * sizeof(u8)*BITS(128)), sizeof(u8)*BITS(128)) != 0)
        {
            Current++;
        }
        Counts[Current]++;
    }
    for(u32 i = 0; i < L2CandidateIndex; i++)
    {
        if(Counts[i] > BestSCount)
        {
            BestSCount = Counts[i];
            BestS = i;
        }
    }
    memcpy(Params->SecretCandidate, L2SecretCandidates + (BestS * sizeof(u8)*BITS(128)), sizeof(u8)*BITS(128));
    
    Platform.Free(L2SecretCandidates);
    Platform.Free(Counts);
    Platform.Free(Data);
}

///##################################################################################
/// Function:
/// b32 GenerateSecretAndAuxiliaryData(char *ProtectedTemplate, char *ADFileName, u8 Secret[BITS(128)])
///
/// About:
/// Enrolment function, call this to enrol a user with the given paramters.
/// 
/// Parameters:
/// ProtectedTemplate - Path to the protected template to use
/// ADFileName - Path to the AD file to output
/// Secret - Pointer to an array to store the secret, this function will generate the secret
///          and store it here.
///
/// Return value:
/// The file pointed to by ADFileName will be written to and the Secret will be generated
///
/// Note:
/// How it works:
/// r - Random value = 79 bits
/// s - Secret = 128 bits
/// p = AES(Km, s|r)
/// DAT XOR Ecc(p) = ad -> Stored in ad file
/// 
///##################################################################################
internal b32
GenerateSecretAndAuxiliaryData(char *ProtectedTemplate, char *ADFileName, u8 Secret[BITS(128)])
{
    //
    // NOTE(Sanderkp): Generate secret
    //
    csprng_get(rng, Secret, sizeof(u8)*BITS(128));
    protected_template pt = ReadProtectedTemplate(ProtectedTemplate);
    if(!pt.L1VicinityCount)
    {
        FreeProtectedTemplate(&pt);
        return(false);
    }
    
    file_handle *ad = Platform.OpenFile(ADFileName, FileMode_Create);
    if(!ad)
    {
        Platform.CloseFile(&pt);
        return(false);
    }
    
    Platform.Write(&pt.L1VicinityCount, sizeof(unsigned char), ad);
    for(int L1VicinityIndex = 0; L1VicinityIndex < pt.L1VicinityCount; ++L1VicinityIndex)
    {
        Platform.Write(&pt.L1Vicinity[L1VicinityIndex].L2VicinityCount, sizeof(unsigned char), ad);
        for(int L2VicinityIndex = 0; L2VicinityIndex < pt.L1Vicinity[L1VicinityIndex].L2VicinityCount; ++L2VicinityIndex)
        {
            int D = pt.L1Vicinity[L1VicinityIndex].L2Vicinity[L2VicinityIndex].FlagCode[0];
            int A = pt.L1Vicinity[L1VicinityIndex].L2Vicinity[L2VicinityIndex].FlagCode[1];
            int T = pt.L1Vicinity[L1VicinityIndex].L2Vicinity[L2VicinityIndex].FlagCode[2];
            
            assert(D < 100 && A < 72 && T < 72);
            
            u8 _DAT[BITS(256)] = {0};
            ToUnaryRepresentation(_DAT, 0, D);
            ToUnaryRepresentation(_DAT, 100, A);
            ToUnaryRepresentation(_DAT, 172, T);
            
            u8 p[BITS(207)] = {0};
            
            //
            // NOTE(Sanderkp): Generate random value
            //
            u8 r[BITS(79)] = {0};
            csprng_get(rng, r, sizeof(r));
            
            u8 AESIn[BITS(256)] = {0};
            u8 AESOut[BITS(256)] = {0};
            //
            // NOTE(Sanderkp): Concatinate Secret and r
            //
            BitCopy(AESIn, 0, Secret, 128);
            BitCopy(AESIn, 128, r, 79);
            AES_CBC_encrypt_buffer(AESOut, AESIn, ArrayCount(AESIn), SecretKey, iv);
            
            //
            // NOTE(Sanderkp): AES only works on 16 byte boundaries but we only want the first 207 bits (26 bytes)
            //                 so we copy those out.
            //
            BitCopy(p, 0, AESOut, 207);
            
            u8 EccP[BITS(256)] = {0};
            BCH->Encode2(EccP, p);
            
            u8 AD[BITS(256)] = {0};
            for(u32 Index = 0; Index < ArrayCount(EccP); ++Index)
            {
                AD[Index] = _DAT[Index] ^ EccP[Index];
            }
            
            Platform.Write(AD, sizeof(u8)*ArrayCount(AD), ad);
        }
    }
    FreeProtectedTemplate(&pt);
    Platform.CloseFile(ad);
    return(true);
}

///##################################################################################
/// Function:
/// b32 GetSecretFromAuxiliaryData(work_queue *WorkQueue, char *ProtectedTemplate, 
///                                char *ADFileName, u8 *Secret, u32 *SecretCount, u32 MaxSecrets)
///
/// About:
/// Authentication function, this will attempt to extract a number of secrets by reversing
/// the process during enrolment: DAT' XOR AD = Ecc(p) -> p -> s|r -> s
/// 
/// Parameters:
/// WorkQueue - WorkQueue for threading the workload (NOT CURRENTLY USED, SEE NOTES)
/// ProtectedTemplate - Path to the probe protected template file
/// ADFileName - Auxiliary data file generated during enrolment
/// Secret - Pointer to a block of memory to store the secrets
/// SecretCount - Number of secrets extracted
/// MaxSecrets - The maximum number of secrets to extracts
///
/// Return value:
/// A number of secrets stored in Secret, the count stored in SecretCount
///
/// Note:
/// IMPORTANT(Sanderkp): For some reason enabling threading triggers a bug somewhere
///                      and I don't know why. I don't really have time to track it
///                      down so I just disable threading for now.
/// 
///##################################################################################
internal b32
GetSecretFromAuxiliaryData(work_queue *WorkQueue, char *ProtectedTemplate, char *ADFileName, u8 *Secret, u32 *SecretCount, u32 MaxSecrets)
{
    protected_template pt = ReadProtectedTemplate(ProtectedTemplate);
    if(!pt.L1VicinityCount)
    {
        return(false);
    }
    u8 L1SecretCandidates[BITS(128)*255] = {0};
    u8 *L2SecretCandidates = (u8*)Platform.Allocate(sizeof(u8)*BITS(128)*255*255);
    int L1CandidateIndex = 0;
    int L2CandidateIndex = 0;
    auxiliary_data ad = ReadAuxiliaryData(ADFileName);
    
    if(!ad.MVCount)
    {
        return(false);
    }
    //
    // NOTE(Sanderkp): The biometric probe may have fewer or more mega vicinity areas than the auxiliary data, 
    //                 so we Clamp to the smallest amount (so we don't crash)
    //
    int Iterations = Min(pt.L1VicinityCount, ad.MVCount);
    for(int L1Index = 0; L1Index < Iterations; ++L1Index)
    {
        cross_match_params *Params = (cross_match_params*)Platform.Allocate(sizeof(cross_match_params));
        ZeroMem(Params, sizeof(cross_match_params));
        Params->L1Vicinity = pt.L1Vicinity + L1Index;
        Params->SecretCandidate = L1SecretCandidates + (L1CandidateIndex++ * sizeof(u8)*BITS(128));
        Params->ad = ad.mv + L1Index;
        //Platform.AddWork(WorkQueue, CrossMatchFunc, Params);
        CrossMatchFunc(Params);
    }
    //Platform.CompleteAllWork(WorkQueue);
    
    //
    // NOTE(Sanderkp): Majority voting:
    //                 This process is very similiar to majorty voting in CrossMatchFunc but
    //                 here we keep more than one secret.
    //                 (Yes I know this could have been pulled out to a separate function, but
    //                  we didn't have time)
    //
    qsort(L1SecretCandidates, L1CandidateIndex, sizeof(u8)*BITS(128), SecretCompareFunc);
    s32 BestS = 0;
    s32 BestSCount = -1;
    s32 *Counts = (s32*)Platform.Allocate(L1CandidateIndex*sizeof(s32));
    ZeroMem(Counts, sizeof(s32)*L1CandidateIndex);
    s32 Current = 0;
    for(u32 i = 0; i < (u32)L1CandidateIndex; i++)
    {
        if(memcmp(L1SecretCandidates + (i * sizeof(u8)*BITS(128)), L1SecretCandidates + (Current * sizeof(u8)*BITS(128)), sizeof(u8)*BITS(128)) != 0)
        {
            Current++;
        }
        Counts[Current]++;
    }
    u32 SecretIndex = 0;
    for(SecretIndex = 0;
        SecretIndex < Min((u32)Current + 1, MaxSecrets);
        ++SecretIndex)
    {
        for(u32 i = 0; i < (u32)L1CandidateIndex; i++)
        {
            if(Counts[i] > BestSCount)
            {
                BestSCount = Counts[i];
                BestS = i;
            }
        }
        memcpy(Secret + (SecretIndex * sizeof(u8)*BITS(128)), L1SecretCandidates + (BestS * sizeof(u8)*BITS(128)), sizeof(u8)*BITS(128));
        Counts[BestS] = 0;
        BestSCount = 0;
    }
    
    //
    // NOTE(Sanderkp): Store the secret count if the user passed a valid pointer
    //
    if(SecretCount)
    {
        *SecretCount = SecretIndex;
    }
    
    Platform.Free(Counts);
    Platform.Free(L2SecretCandidates);
    
    return(true);
}

///##################################################################################
/// 
/// 
/// THE FOLLOWING CODE BELOW IS ONLY EVER USED WHEN COMPILING THE SECRET RELEASER
/// IN STANDALONE MODE, A LOT OF THIS CODE IS DUPLICATED FROM ELSEWHERE SO I WILL
/// NOT BOTHER DOCUMENTING IT.
/// 
/// 
///##################################################################################

#ifdef SECRET_RELEASER_STANDALONE

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

//
// NOTE(Sanderkp): Copied from fkb_main.cpp
//
internal
PLATFORM_RUN_AND_WAIT_FOR_PROCESS(RunAndWaitForProcess)
{
    STARTUPINFO StartupInfo = {sizeof(PROCESS_INFORMATION)};
    PROCESS_INFORMATION ProcessInformation = {0};
    if(!CreateProcess(Executable, Arguments, 0, 0, FALSE, NORMAL_PRIORITY_CLASS,0,0, &StartupInfo,&ProcessInformation))
    {
        return(false);
    }
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);
    CloseHandle(ProcessInformation.hProcess);
    CloseHandle(ProcessInformation.hThread);
    return(true);
}
#else

internal
PLATFORM_RUN_AND_WAIT_FOR_PROCESS(RunAndWaitForProcess)
{
    return(0);
}

#endif

//
// NOTE(Sanderkp): Copied from fkb_main.h
//
internal
PLATFORM_ALLOCATE(Allocate)
{
    if(Size == 0)
    {
        return(0);
    }
    void *Memory = malloc(Size);
    return(Memory);
}


//
// NOTE(Sanderkp): Copied from fkb_main.h
//
internal
PLATFORM_FREE(Free)
{
    if(Memory)
    {
        free(Memory);
    }
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_OPEN_FILE(OpenFile)
{
    char *Mode = "wb+";
    switch(CreationMode)
    {
        case FileMode_Create:
        {
            Mode = "wb+";
        }break;
        case FileMode_Open:
        {
            Mode = "rb+";
        }break;
    }
    FILE *FileHandle = fopen(FilePath, Mode);
    return(FileHandle);
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_CLOSE_FILE(CloseFile)
{
    if(FileHandle)
    {
        fclose((FILE*)FileHandle);
    }
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_WRITE(Write)
{
    return(fwrite(Buffer, Size, 1, (FILE*)FileHandle));
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_READ(Read)
{
    return(fread(Buffer, Size, 1, (FILE*)FileHandle));
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_DELETE_FILE_AT(DeleteFileAt)
{
    BOOL Result = DeleteFile(FilePath);
    return(Result);
}
///##################################################################################
/// Function:
/// double DoMatching(char *ProtectedTemplateFileNameCopy, char *ProtectedTemplateFileName, char *ScoreFilePath)
///
/// About:
/// Calls the matcher and gets the score
/// 
/// Parameters:
/// ProtectedTemplateFileNameCopy - The first template to match
/// ProtectedTemplateFileName - The second template to match
/// ScoreFilePath - Path of score file, this file is deleted afterwards but is required
///
/// Return value:
/// The matcher score
///
/// Note:
/// Don't expect this to be a fast function.
/// 
///##################################################################################
internal double
DoMatching(char *ProtectedTemplateFileNameCopy, char *ProtectedTemplateFileName, char *ScoreFilePath)
{
    double Score = 0;
    
    Platform.DeleteFileAt(ScoreFilePath);
    
    char Cmd[250];
    snprintf(Cmd, sizeof(Cmd), "--tpl1 %s --tpl2 %s --log %s", ProtectedTemplateFileNameCopy, ProtectedTemplateFileName, ScoreFilePath);
    if(!Platform.RunAndWaitForProcess("GUCMatcher.exe", Cmd))
    {
        return(-1.0);
    }
    
    fkb_read_file ScoreFile = Platform.ReadEntireFile(ScoreFilePath);
    if(!ScoreFile.Content) 
    {
        return(-1.0);
    }
    *((char*)ScoreFile.Content + ScoreFile.Size-1) = 0;
    
    char *Scan = (char*)ScoreFile.Content + strlen(ProtectedTemplateFileNameCopy) + 1 + strlen(ProtectedTemplateFileName) + 1;
    if(strstr(Scan, "OK"))
    {
        Scan += strlen("OK") + 1;
    }
    else if(strstr(Scan, "FAIL"))
    {
        Scan += strlen("FAIL") + 1;
    }
    
    Score = atof(Scan);
    
    Platform.FreeEntireFile(ScoreFile);
    
    Platform.DeleteFileAt(ScoreFilePath);
    
    return(Score);
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_READ_ENTIRE_FILE(ReadEntireFile)
{
    fkb_read_file ReadFile = {0};
    FILE *FileContext =  fopen(Path, "rb");
    if(FileContext)
    {
        fseek(FileContext, 0, SEEK_END);
        s64 FileSize = ftell(FileContext);
        if(FileSize != -1)
        {
            fseek(FileContext, 0, SEEK_SET);
            ReadFile.Size = (u64)FileSize;
            ReadFile.Content = Allocate(ReadFile.Size);
            fread(ReadFile.Content, ReadFile.Size, 1, FileContext);
        }
        fclose(FileContext);
    }
    return(ReadFile);
}

//
// NOTE(Sanderkp): Copied from fkb_file_handling.cpp
//
internal
PLATFORM_FREE_ENTIRE_FILE(FreeEntireFile)
{
    if(File.Content)
    {
        Free(File.Content);
    }
}

///##################################################################################
/// Function:
/// int main(int argc, char **argv)
///
/// About:
/// Main entry point
/// 
/// Parameters:
/// argv[1] - "a" for authentication, "e" for enrolment
/// argv[2] - Path to protected template if authenticating, path to iso template if enroling
/// argv[3] - Path to protected template to match if authenticating or output protected template if enroling
/// argv[4] - Log file (will be _appended_)
///
/// Return value:
/// Always zero
///
/// 
///##################################################################################
int main(int argc, char **argv)
{
    if(argc != 5)
    {
        printf("Arguments:\n"
               "\ta/e     - authentication/enrolment\n"
               "\ttpl/iso - Tpl if authenticating, iso if enroling\n"
               "\ttpl     - Matching tpl if authenticating, output tpl if enroling\n"
               "\tlog     - Output log file");
        return(0);
    }
    printf("%s %s ", argv[2], argv[3]);
    //
    // NOTE(Sanderkp): Initialize variables
    //
    BCH = new bch();
    BCH->Init(255, 207, 6);
    rng = csprng_create();
    //
    // NOTE(Sanderkp): Set up boring function pointers
    //
    Platform.Allocate = Allocate;
    Platform.Free = Free;
    Platform.RunAndWaitForProcess = RunAndWaitForProcess;
    Platform.OpenFile = OpenFile;
    Platform.CloseFile = CloseFile;
    Platform.Write = Write;
    Platform.Read = Read;
    Platform.DeleteFileAt = DeleteFileAt;
    Platform.ReadEntireFile = ReadEntireFile;
    Platform.FreeEntireFile = FreeEntireFile;
    Platform.AddWork = AddWork;
    Platform.CompleteAllWork = CompleteAllWork;
    //
    // NOTE(Sanderkp): Create work queue
    //
    work_queue *WorkQueue = CreateWorkQueue(4);
    char Buffer[4000] = {0};
    //
    // NOTE(Sanderkp): Authentication branch
    //
    if(!strcmp(argv[1], "a"))
    {
#define MaxSecrets 5
        u32 SecretCount = 0;
        u8 Secret[sizeof(u8)*BITS(128)*MaxSecrets] = {0};
        u8 OriginalSecret[sizeof(u8)*BITS(128)] = {0};
        snprintf(Buffer, sizeof(Buffer), "%s_secret", argv[3]);
        file_handle *fh = Platform.OpenFile(Buffer, FileMode_Open);
        if(!fh)
        {
            printf("Failed to open secret file\n");
            return(1);
        }
        //
        // NOTE(Sanderkp): The original secret was stored to file when enroling, read in the original secret
        //
        Platform.Read(OriginalSecret, sizeof(OriginalSecret), fh);
        Platform.CloseFile(fh);
        snprintf(Buffer, sizeof(Buffer), "%s_ad", argv[3]);
        //
        // NOTE(Sanderkp): Authentication function, this (should) extract one or more secrets
        //
        if(!GetSecretFromAuxiliaryData(WorkQueue, argv[2], Buffer, Secret, &SecretCount, MaxSecrets))
        {
            printf("Failed to get secret from auxiliary data!\n");
            return(1);
        }
        b32 AcceptedBySRP = false;
        for(u32 Index = 0;
            Index < SecretCount;
            ++Index)
        {
            //
            // NOTE(Sanderkp): We don't use actual SRP in this test program,
            //                 instead we simulate using memcmp since it only gives
            //                 a binary answer.
            //
            if(!memcmp(OriginalSecret, Secret, sizeof(OriginalSecret)))
            {
                AcceptedBySRP = true;
                break;
            }
        }
        //
        // NOTE(Sanderkp): Run the matcher and extract score
        //
        double Score = DoMatching(argv[2], argv[3], "temp_score.txt");
        if(Score == -1.0)
        {
            printf("Matcher failed (score == -1.0)");
            return(1);
        }
        //
        // NOTE(Sanderkp): Append to log file
        //
        FILE *f = fopen(argv[4], "a");
        if(!f)
        {
            printf("Failed to open score file!\n");
            return(1);
        }
        fprintf(f, "%s %s OK %f SRP:%s\n", argv[2], argv[3], Score, AcceptedBySRP ? "OK" : "FAIL");
        fclose(f);
        printf("Score: %g SRP: %s\n", Score, AcceptedBySRP ? "OK" : "FAIL");
    }
    //
    // NOTE(Sanderkp): Enrolment branch
    //
    else if(!strcmp(argv[1], "e"))
    {
        //
        // NOTE(Sanderkp): Run the generator
        //
        u8 Secret[sizeof(u8)*BITS(128)] = {0};
        snprintf(Buffer, sizeof(Buffer), "--aeskm AESKm.cf --nonce nonce.cf --isotpl %s --outfile %s --log %s", argv[2], argv[3], argv[4]);
        if(!Platform.RunAndWaitForProcess("GUCGenerator.exe", Buffer))
        {
            printf("Failed to run template generator\n");
            return(1);
        }
        //
        // NOTE(Sanderkp): Generate auxiliary data and secret
        //
        snprintf(Buffer, sizeof(Buffer), "%s_ad", argv[3]);
        if(GenerateSecretAndAuxiliaryData(argv[3], Buffer, Secret))
        {
            //
            // NOTE(Sanderkp): For testing purposes we write the secret to a file as well
            //
            snprintf(Buffer, sizeof(Buffer), "%s_secret", argv[3]);
            file_handle *fh = Platform.OpenFile(Buffer, FileMode_Create);
            if(!fh)
            {
                printf("Failed to create secret file\n");
                return(1);
            }
            Platform.Write(Secret, sizeof(Secret), fh);
            Platform.CloseFile(fh);
            printf("User enroled\n");
        }
        else
        {
            printf("User FAILED enroled\n");
            return(1);
        }
    }
    else
    {
        printf("Invalid argument, must be 'a' or 'e', was %s\n", argv[1]);
        return(1);
    }
    return(0);
}
#include "csprng.c"
#endif