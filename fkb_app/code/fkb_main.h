///##################################################################################
/// fkb_main.h
/// 
/// This file contains a few functions and globals used in the host/exe portion that
/// wouldn't make sense to put anywhere else.
/// 
/// 
///##################################################################################

//
// NOTE(Sanderkp): Global stored value required by SDL when regestering user events
//
global Uint32 ForceUpdateEvent;

///##################################################################################
/// Function:
/// PLATFORM_ALLOCATE(Allocate)
///
/// About:
/// Implementation of Allocate specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_ALLOCATE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_ALLOCATE(Allocate)
{
    if(Size == 0)
    {
        return(0);
    }
    void *Memory = SDL_malloc(Size);
    return(Memory);
}

///##################################################################################
/// Function:
/// PLATFORM_FREE(Free)
///
/// About:
/// Implementation of Free specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_FREE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_FREE(Free)
{
    if(Memory)
    {
        SDL_free(Memory);
    }
}

