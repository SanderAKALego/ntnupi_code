///##################################################################################
/// fkb_file_handling.cpp
/// 
/// In this file we implement all of the file system specific functions.
/// 
/// 
///##################################################################################

///##################################################################################
/// Function:
/// PLATFORM_READ_ENTIRE_FILE(ReadEntireFile)
///
/// About:
/// Implementation of ReadEntireFile specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_READ_ENTIRE_FILE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_READ_ENTIRE_FILE(ReadEntireFile)
{
    fkb_read_file ReadFile = {0};
    SDL_RWops *FileContext =  SDL_RWFromFile(Path, "rb");
    if(FileContext)
    {
        SDL_RWseek(FileContext, 0, RW_SEEK_END);
        s64 FileSize = SDL_RWtell(FileContext);
        if(FileSize != -1)
        {
            SDL_RWseek(FileContext, 0, RW_SEEK_SET);
            ReadFile.Size = (u64)FileSize;
            ReadFile.Content = Allocate(ReadFile.Size);
            SDL_RWread(FileContext, ReadFile.Content, ReadFile.Size, 1);
        }
        SDL_RWclose(FileContext);
    }
    return(ReadFile);
}

///##################################################################################
/// Function:
/// PLATFORM_FREE_ENTIRE_FILE(FreeEntireFile)
///
/// About:
/// Implementation of FreeEntireFile specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_FREE_ENTIRE_FILE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_FREE_ENTIRE_FILE(FreeEntireFile)
{
    if(File.Content)
    {
        Free(File.Content);
    }
}

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Commdlg.h>

///##################################################################################
/// Function:
/// PLATFORM_DELETE_FILE_AT(DeleteFileAt)
///
/// About:
/// Implementation of DeleteFileAt specified in fkb_platform.h
///
/// Note:
/// Win32 specific implementation. See PLATFORM_DELETE_FILE_AT in fkb_platform.h for more info.
/// 
///##################################################################################
internal
PLATFORM_DELETE_FILE_AT(DeleteFileAt)
{
    BOOL Result = DeleteFile(FilePath);
    return(Result);
}

///##################################################################################
/// Function:
/// PLATFORM_COPY_FILE_TO(CopyFileTo)
///
/// About:
/// Implementation of CopyFileTo specified in fkb_platform.h
///
/// Note:
/// Win32 specific implementation. See PLATFORM_COPY_FILE_TO in fkb_platform.h for more info.
/// 
///##################################################################################
internal
PLATFORM_COPY_FILE_TO(CopyFileTo)
{
    BOOL Result = CopyFile(From, To, FALSE);
    return(Result);
}

///##################################################################################
/// Function:
/// PLATFORM_OPEN_FILE_DIALOG(OpenFileDialog)
///
/// About:
/// Implementation of OpenFileDialog specified in fkb_platform.h
///
/// Note:
/// Win32 specific implementation. See PLATFORM_OPEN_FILE_DIALOG in fkb_platform.h for more info.
/// 
///##################################################################################
internal
PLATFORM_OPEN_FILE_DIALOG(OpenFileDialog)
{
    char WorkingDir[500];
    GetCurrentDirectory(ArrayCount(WorkingDir),WorkingDir);
    
    b32 Result = false;
    OPENFILENAME OpenFileName = {0};
    OpenFileName.lStructSize = sizeof(OPENFILENAME);
    OpenFileName.lpstrFilter = Extensions;
    OpenFileName.lpstrFile = File;
    OpenFileName.nMaxFile = MaxFile;
    BOOL Res = GetOpenFileName(&OpenFileName);
    if(Res)
    {
        Result = true;
    }
    SetCurrentDirectory(WorkingDir);
    return(Result);
}

#else

#error PLATFORM_OPEN_FILE_DIALOG  is not implemented for other platforms yet!
///##################################################################################
/// Function:
/// PLATFORM_OPEN_FILE_DIALOG(OpenFileDialog)
///
/// Note:
/// Not implemented
/// 
///##################################################################################
internal
PLATFORM_OPEN_FILE_DIALOG(OpenFileDialog)
{
    return(0);
}

#endif

///##################################################################################
/// Function:
/// PLATFORM_SIZE_OF_FILE(SizeOfFile)
///
/// About:
/// Implementation of SizeOfFile specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_SIZE_OF_FILE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_SIZE_OF_FILE(SizeOfFile)
{
    s64 FileSize = 0;
    SDL_RWops *FileHandle = SDL_RWFromFile(Path, "rb");
    if(FileHandle)
    {
        SDL_RWseek(FileHandle, 0, RW_SEEK_END);
        FileSize = SDL_RWtell(FileHandle);
        SDL_RWclose(FileHandle);
    }
    return(FileSize);
}

///##################################################################################
/// Function:
/// PLATFORM_OPEN_FILE(OpenFile)
///
/// About:
/// Implementation of OpenFile specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_OPEN_FILE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_OPEN_FILE(OpenFile)
{
    char *Mode = "wb+";
    switch(CreationMode)
    {
        case FileMode_Create:
        {
            Mode = "wb+";
        }break;
        case FileMode_Open:
        {
            Mode = "rb+";
        }break;
    }
    SDL_RWops *FileHandle = SDL_RWFromFile(FilePath, Mode);
    return(FileHandle);
}

///##################################################################################
/// Function:
/// PLATFORM_CLOSE_FILE(CloseFile)
///
/// About:
/// Implementation of CloseFile specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_CLOSE_FILE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_CLOSE_FILE(CloseFile)
{
    if(FileHandle)
    {
        SDL_RWclose((SDL_RWops*)FileHandle);
    }
}

///##################################################################################
/// Function:
/// PLATFORM_WRITE(Write)
///
/// About:
/// Implementation of Write specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_WRITE in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_WRITE(Write)
{
    return(SDL_RWwrite((SDL_RWops*)FileHandle,Buffer, Size, 1));
}

///##################################################################################
/// Function:
/// PLATFORM_READ(Read)
///
/// About:
/// Implementation of Read specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_READ in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_READ(Read)
{
    return(SDL_RWread((SDL_RWops*)FileHandle,Buffer, Size, 1));
}

///##################################################################################
/// Function:
/// PLATFORM_GET_CURRENT_FILE_POSITION(GetCurrentFilePosition)
///
/// About:
/// Implementation of GetCurrentFilePosition specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_GET_CURRENT_FILE_POSITION in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_GET_CURRENT_FILE_POSITION(GetCurrentFilePosition)
{
    return(SDL_RWtell((SDL_RWops*)FileHandle));
}

///##################################################################################
/// Function:
/// PLATFORM_SET_CURRENT_FILE_POSITION(SetCurrentFilePosition)
///
/// About:
/// Implementation of SetCurrentFilePosition specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_SET_CURRENT_FILE_POSITION in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_SET_CURRENT_FILE_POSITION(SetCurrentFilePosition)
{
    int whence = 0;
    switch(RelativeTo)
    {
        case FilePosition_RelativeToStart:
        {
            whence = RW_SEEK_SET;
        }break;
        case FilePosition_RelativeToCurrent:
        {
            whence = RW_SEEK_CUR;
        }break;
        case FilePosition_RelativeToEnd:
        {
            whence = RW_SEEK_END;
        }break;
    }
    return(SDL_RWseek((SDL_RWops*)FileHandle, Position,whence));
}

