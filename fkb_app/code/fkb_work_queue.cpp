///##################################################################################
/// fkb_work_queue.cpp
/// 
/// This file contains the implementation of the work queue.
/// It is a locking queue so it's not very fast, but it's simple and works.
/// 
/// 
///##################################################################################
//
// NOTE(Sanderkp): Structure containing a work unit, a function pointer and pointer to user defined structure
//
typedef struct work_queue_work
{
    work_queue_func *Func;
    void *Data;
    b8 UpdateScreenOnCompletion;
}work_queue_work;

//
// NOTE(Sanderkp): A locking work queue
//
struct work_queue
{
    //
    // NOTE(Sanderkp): Number of work orders pushed on, volatile because multiple threads are accessing this
    //
    volatile u32 WorkCount;
    //
    // NOTE(Sanderkp): Number of work orders completed, if WorkCompleted != WorkCount it means there is still work left
    //
    volatile u32 WorkCompleted;
    //
    // NOTE(Sanderkp): Array containing work units, at most 512 work orders can be submitted
    //
    work_queue_work Work[512];
    //
    // NOTE(Sanderkp): Mutex used for locking the queue when threads access it for work
    //
    SDL_mutex *Mutex;
    //
    // NOTE(Sanderkp): Used to put threads to sleep when there is no work, it is essentially a WorkLeft counter
    //
    SDL_sem *Semaphore;
};

///##################################################################################
/// Function:
/// void DoWorkerWork(work_queue *WorkQueue)
///
/// About:
/// This does the actual work by locking the queue, getting the work order and executing it
/// 
/// Parameters:
/// WorkQueue - A valid pointer to a work_queue structure
///
/// Return value:
/// None
///
/// Note:
/// Do NOT call this manually, this is to be used internally by the queue only.
/// 
///##################################################################################
internal void
DoWorkerWork(work_queue *WorkQueue)
{
    work_queue_work *Work = 0;
    SDL_LockMutex(WorkQueue->Mutex);
    if(WorkQueue->WorkCompleted < WorkQueue->WorkCount)
    {
        Work = &WorkQueue->Work[(WorkQueue->WorkCompleted++)%ArrayCount(WorkQueue->Work)];
    }
    SDL_UnlockMutex(WorkQueue->Mutex);
    if(Work)
    {
        Work->Func(Work->Data);
#ifndef SECRET_RELEASER_STANDALONE
        if(Work->UpdateScreenOnCompletion)
        {
            SDL_Event Event = {0};
            Event.type = ForceUpdateEvent;
            SDL_PushEvent(&Event);
        }
#endif
    }
}

///##################################################################################
/// Function:
/// int WorkerThread(void *Parameter)
///
/// About:
/// This is the start up routine for threads. Threads will call DoWorkerWork when there is work to be done.
/// 
/// Parameters:
/// Parameters - A valid pointer to a work_queue structure
///
/// Return value:
/// Always returns zero
///
/// Note:
/// This function is to be used by the Work queue internally, do not call it manually!
/// 
///##################################################################################
internal int
WorkerThread(void *Parameter)
{
    work_queue *WorkQueue = (work_queue*)Parameter;
    for(;;)
    {
        SDL_SemWait(WorkQueue->Semaphore);
        DoWorkerWork(WorkQueue);
    }
    return(0);
}

///##################################################################################
/// Function:
/// work_queue* CreateWorkQueue(u32 WorkerCount)
///
/// About:
/// Creates a work queue with a given number of workers (threads).
/// 
/// Parameters:
/// WorkerCount - Number of workers to spawn, limit is defined by the platform.
///
/// Return value:
/// A pointer to a valid WorkQueue structure or NULL if fail.
///
///##################################################################################
internal work_queue*
CreateWorkQueue(u32 WorkerCount)
{
    work_queue *WorkQueue = AllocStruct(work_queue);
    WorkQueue->WorkCount = 0;
    WorkQueue->WorkCompleted = 0;
    WorkQueue->Mutex = SDL_CreateMutex();
    WorkQueue->Semaphore = SDL_CreateSemaphore(0);
    for(u32 WorkerIndex = 0;
        WorkerIndex < WorkerCount;
        ++WorkerIndex)
    {
        SDL_CreateThread(WorkerThread, 0, WorkQueue);
    }
    return(WorkQueue);
}

///##################################################################################
/// Function:
/// PLATFORM_ADD_WORK(AddWork)
///
/// About:
/// Implementation of AddWork specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_ADD_WORK in fkb_platform.h
/// 
///##################################################################################
internal 
PLATFORM_ADD_WORK(AddWork)
{
    SDL_LockMutex(WorkQueue->Mutex);
    WorkQueue->Work[(WorkQueue->WorkCount++)%ArrayCount(WorkQueue->Work)] = {Func, Data};
    SDL_UnlockMutex(WorkQueue->Mutex);
    SDL_SemPost(WorkQueue->Semaphore);
}

///##################################################################################
/// Function:
/// PLATFORM_COMPLETE_ALL_WORK(CompleteAllWork)
///
/// About:
/// Implementation of CompleteAllWork specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_COMPLETE_ALL_WORK in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_COMPLETE_ALL_WORK(CompleteAllWork)
{
    while(WorkQueue->WorkCompleted < WorkQueue->WorkCount)
    {
        DoWorkerWork(WorkQueue);
    }
    WorkQueue->WorkCompleted = 0;
    WorkQueue->WorkCount = 0;
}
