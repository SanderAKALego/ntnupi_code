///##################################################################################
/// build.cpp
/// 
/// Fingerprint key box demo front-end application
/// 
/// To compile the demo you only have to compile this (build.cpp) file.
/// 
/// To enable the hot reloader pass HOT_RELOADER_ENABLED=1 as a compiler definition
/// or set it to 1 below and define COMPILE_AS_DLL when compiling the dll (see build.bat).
//  It's usually faster to compile everything at once (IE set
/// HOT_RELOADER_ENABLED 0) but the hot reloader allows altering the code in the dll
/// and recompile without restarting the demo.
///
/// STATE OF THE HOT RELOADER: There is a problem with nuklear and hot reloading,
/// to get hot reloading to work reliably with nuklear you may have to change the
/// memory model for nuklear. (You can't use strings defined in the DLL for example
/// as GUIDs/identifiers in nuklear because they change when recompiling).
///
/// It is recommended to not compile with the hot reloader for now.
/// 
///##################################################################################

#ifndef HOT_RELOADER_ENABLED
//
// NOTE(Sanderkp): If HOT_RELOADER_ENABLED is not defined we fall back to not using it
//
#define HOT_RELOADER_ENABLED 0
#endif

//
// NOTE(Sanderkp): Hot reloader supported build
//
#if HOT_RELOADER_ENABLED

//
// NOTE(Sanderkp): There is a problem with including AES before shared.cpp, so we have to do it here
//
#if defined COMPILE_AS_DLL

#include <AES.h>
#include <AES.cpp>

#endif

//
// NOTE(Sanderkp): Shared files between dll and exe
//
#include "shared.cpp"

//
// NOTE(Sanderkp): When compiling the DLL you must define COMPILE_AS_DLL
//
#if defined COMPILE_AS_DLL

// NOTE(Sanderkp): Compile the dll part
#include "dll.cpp"

#else

// NOTE(Sanderkp): Compile the exe part
#include "exe.cpp"

#endif // NOTE(Sanderkp): HOT_RELOADER_ENABLED == 0
#else

//
// NOTE(Sanderkp): If HOT_RELOADER_ENABLED is disabled we compile everything in one pass
//
#include <AES.h>
#include <AES.cpp>
#include "shared.cpp"
#include "dll.cpp"
#include "exe.cpp"

#endif

