///##################################################################################
/// fkb_net.h
/// 
/// This file contains functions responsible for send and receiving network packets
/// in addition to connecting to the server and a few other things.
/// 
///##################################################################################

//
// NOTE(Sanderkp): Constants used in database
//
#define MAX_USERNAME_LENGTH 64
#define MAX_FULL_NAME_LENGTH 64
#define MAX_SALT_LENGTH 32
#define MAX_VERIFIER_LENGTH 256
#define MAX_FILEPATH_LEGNTH 128
//
// TODO(Sanderkp): Limit in database is ~65535 but for simplicity we only handle max 512 here
//
#define MAX_DESCRIPTION_LENGTH 512

//
// NOTE(Sanderkp): Max image size: 500kb
//
#define MAX_PROFILE_IMG_SIZE 1024*500

//
// NOTE(Sanderkp): Server address and port
//
#define ServerIP "localhost"
#define ServerPort 41231

//
// NOTE(Sanderkp): User struct in database
//
typedef struct user
{
    //
    // NOTE(Sanderkp): These are stored in the database
    //
    char Username[MAX_USERNAME_LENGTH];
    char Fullname[MAX_FULL_NAME_LENGTH];
    char Description[MAX_DESCRIPTION_LENGTH];
    char FilePath[MAX_FILEPATH_LEGNTH];
    unsigned char Salt[MAX_SALT_LENGTH];
    unsigned char Verifier[MAX_VERIFIER_LENGTH];
    
    //
    // NOTE(Sanderkp): The following values are not stored in the database and are more used as temp variables
    //
    int SaltLength;
    int VerifierLength;
    void *ImageData;
    umm ImageDataSize;
    SDL_atomic_t HasImage;
    u32 FieldsToUpdate;
    char TempFilePath[500];
}user;

//
// NOTE(Sanderkp): RPC enum
//
enum RemoteCallTypes
{
    //
    // NOTE(Sanderkp): We treat a RPC call with type 0 as NULL (This should never be used unless there is an error)
    //
    MSG_NullCall,
    //
    // NOTE(Sanderkp): Asks the backe-end if a user exists
    //
    MSG_UserExists,
    //
    // NOTE(Sanderkp): Registers/Signs up a new user
    //
    MSG_Register,
    //
    // NOTE(Sanderkp): Logs a user in
    //
    MSG_LogIn,
    //
    // NOTE(Sanderkp): Logs a user out
    //
    MSG_LogOut,
    //
    // NOTE(Sanderkp): Deletes a user (used for revoke)
    //
    MSG_Delete,
    //
    // NOTE(Sanderkp): Updates a users credentials (used for refresh)
    //
    MSG_UpdateCredentials,
    //
    // NOTE(Sanderkp): Update a piece of user information (profile image, description etc)
    MSG_UpdateInfo,
    //
    // NOTE(Sanderkp): Gets user information (image, description etc)
    //
    MSG_GetInfo
};

//
// NOTE(Sanderkp): Results after performing an RPC call
//
enum RemoteCallResultTypes
{
    //
    // NOTE(Sanderkp): Treat code 0 as Null, this should never be used
    //
    MSG_Result_Null,
    //
    // NOTE(Sanderkp): Success! No errors happened during call.
    //
    MSG_Result_NoError,
    //
    // NOTE(Sanderkp): Sign in, sign up etc failed
    //
    MSG_Result_Failed,
    //
    // NOTE(Sanderkp): Returned when the user tries to login to an account that is already logged in
    //
    MSG_Result_AlreadyLoggedIn,
    //
    // NOTE(Sanderkp): Returned when someone tries to access information for a user without being logged in 
    //
    MSG_Result_NotLoggedIn,
    //
    // NOTE(Sanderkp): Returned when trying to register with a user that already exists
    //
    MSG_Result_AlreadyExists,
    //
    // NOTE(Sanderkp): Returned when trying to log out with a user already logged out.
    //
    MSG_Result_AlreadyLoggedOut,
    //
    // NOTE(Sanderkp): Generic error, something went wrong.
    //
    MSG_Result_Error,
};

//
// NOTE(Sanderkp): Which fields to update in the user struct. These are flags so multiple ones can be combined.
//
enum UserFieldsToUpdate
{
    UPDATE_FULLNAME = 0x1,
    UPDATE_DESCRIPTION = 0x2,
    UPDATE_PROFILE_IMAGE = 0x4,
};

//
// NOTE(Sanderkp): Network connection enum
//
enum NetworkConnectionState
{
    ConnectionState_Disconnected,
    ConnectionState_Connecting,
    ConnectionState_Connected,
    ConnectionState_Error,
};

//
// NOTE(Sanderkp): Networking structure
//
struct net_state
{
    //
    // NOTE(Sanderkp): Connection state variable, accessed from multiple threads so it needs to be atomic.
    //
    SDL_atomic_t ConnectionState;
    //
    // NOTE(Sanderkp): Socket connected to server (via TCP)
    //
    TCPsocket Socket;
    //
    // NOTE(Sanderkp): IPAddress of server
    //
    IPaddress IPAddress;
};

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(ConnectToServer)
///
/// About:
/// Repeatedly tries to connect to the server
/// 
/// Parameters:
/// Data - Pointer to a net_state structure
///
/// Return value:
/// None
///
/// Note:
/// Tries to connect every 1000 ms (1 second), after connected it checks every 1 second if it is still connected
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(ConnectToServer)
{
    net_state *Net = (net_state*)Data;
    while(true)
    {
        switch(SDL_AtomicGet(&Net->ConnectionState))
        {
            case ConnectionState_Disconnected:
            {
                if(SDLNet_ResolveHost(&Net->IPAddress, ServerIP, ServerPort) == -1) 
                {
                    printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
                    SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
                }
                else
                {
                    SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Connecting);
                }
            }break;
            case ConnectionState_Connecting:
            {
                Net->Socket = SDLNet_TCP_Open(&Net->IPAddress);
                if(!Net->Socket) 
                {
                    printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
                    printf("Retrying....\n");
                    SDL_Delay(1000);
                }
                else
                {
                    SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Connected);
                }
            }break;
            case ConnectionState_Connected:
            {
                SDL_Delay(1000);
            }break;
            case ConnectionState_Error:
            {
                SDL_Delay(1000);
            }break;
            InvalidDefaultCase;
        }
    }
}

///##################################################################################
/// Function:
/// b32 Net_Send(net_state *Net, void *Source, int Size)
///
/// About:
/// Sends data to the back-end server.
/// 
/// Parameters:
/// Net - Pointer to net_state structure, must be valid
/// Source - A buffer containing the data to be sent
/// Size - Size of the buffer
///
/// Return value:
/// True if success, false if:
///  1. Sending the size to the receiver fails
///  2. Receiving the OK message fails
///  3. OK = 0, happens if Size is larger then what the receiver expects
///  4. Send the data fails, due to socket disconnecting
///
/// Note:
/// If sending more than 1024 bytes this routine will splits the buffer up and sends each 1024 chunk in order.
/// The function may fail if you try to send more then what the receiver expects.
/// The function may block.
/// 
///##################################################################################
internal b32
Net_Send(net_state *Net, void *Source, int Size)
{
    if(SDLNet_TCP_Send(Net->Socket, &Size, sizeof(int)) < sizeof(int))
    {
        SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
        return(false);
    }
    int Ok = 0;
    if(SDLNet_TCP_Recv(Net->Socket, &Ok, sizeof(int)) <= 0)
    {
        SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
        return(false);
    }
    if(!Ok)
    {
        SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
        return(false);
    }
    if(Size)
    {
        if(Size > 1024)
        {
            int Count = (Size/1024) + (Size%1024 != 0 ? 1 : 0);
            int DataSent = 0;
            for(int Block = 0; Block < Count; ++Block)
            {
                int S = Min(Size-DataSent, 1024);
                if(SDLNet_TCP_Send(Net->Socket, (unsigned char*)Source + DataSent, S) < S)
                {
                    SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
                    return(false);
                }
                DataSent += S;
            }
        }
        else
        {
            if(SDLNet_TCP_Send(Net->Socket, Source, Size) < Size)
            {
                SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
                return(false);
            }
        }
    }
    return(true);
}

///##################################################################################
/// Function:
/// b32 Net_Recv(net_state *Net, void *Dest, int *Size, int MaxSize)
///
/// About:
/// Receives data from the network.
/// 
/// Parameters:
/// Net - A pointer to a valid net_state structure
/// Dest - Buffer to store the data
/// Size - Pointer to an integer to store the size, can be null
/// MaxSize - Size of buffer
///
/// Return value:
/// True if success, false if:
///   1. Receiving the size fails
///   2. Fails to send the OK message to the receiver
///   3. If the sender responds with OK = 0
///   4. Recv fails, most likely because the socket disconnected
///
/// Note:
/// If receiving more than 1024 bytes, multiple chunks will be read in order.
/// This call is blocking.
/// 
///##################################################################################
internal b32
Net_Recv(net_state *Net, void *Dest, int *Size, int MaxSize)
{
    int _Size = 0;
    if(SDLNet_TCP_Recv(Net->Socket, &_Size, sizeof(int)) < sizeof(int))
    {
        SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
        return(false);
    }
    int Ok = (_Size <= MaxSize);
    if(SDLNet_TCP_Send(Net->Socket, &Ok, sizeof(int)) <= 0)
    {
        SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
        return(false);
    }
    if(!Ok)
    {
        SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
        return(false);
    }
    if(_Size)
    {
        if(_Size > 1024)
        {
            int Recieved = 0;
            while(Recieved < _Size)
            {
                int Recv = SDLNet_TCP_Recv(Net->Socket, (unsigned char*)Dest + Recieved, 1);
                if(Recv > 0)
                {
                    Recieved += Recv;
                }
                else
                {
                    SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
                    return(false);
                }
            }
        }
        else
        {
            if(SDLNet_TCP_Recv(Net->Socket, Dest, _Size) < _Size)
            {
                SDL_AtomicSet(&Net->ConnectionState, ConnectionState_Error);
                return(false);
            }
        }
    }
    if(Size)
    {
        *Size = _Size;
    }
    return(true);
}