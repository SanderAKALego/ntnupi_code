///##################################################################################
/// fkb_gui.cpp
/// 
/// This is the interface module described in the thesis. This contains all the GUI
/// for the various screens. 
/// 
/// 
///##################################################################################

struct login_params
{
    b8 IsEnrolling;
    b8 IsRefreshing;
    program_state *State;
    char FingerprintScanImagePath[500];
    char ISOTemplatePath[500];
    char ProtectedTemplatePath[500];
};

struct load_profile_image_params
{
    struct nk_image *NkImage;
    union
    {
        char File[500];
        struct
        {
            void *ImageData;
            umm ImageDataSize;
        };
    };
};

///
// NOTE(Sanderkp): Custom button style with no border/rectangle part, basically a clickable label
//
local struct nk_style_button LabelButtonStyle = 
{
    nk_style_item_color(nk_rgba(255,255,255,0)),
    nk_style_item_color(nk_rgba(255,255,255,0)),
    nk_style_item_color(nk_rgba(255,255,255,0)),
    nk_rgba(255,255,255,0),
    
    nk_rgba(0,0,0,255),
    nk_rgba(128,128,128,255),
    nk_rgba(60,60,60,255),
    nk_rgba(180,180,180,255),
    NK_TEXT_CENTERED,
    
    1,
    0,
};


//#################################################################################################
//                                       Global variables
//
// NOTE(Sander): GotUserInfo is set to true when we start the work thread that gets the profile data
//               such as image, name, description etc. This prevents us from spamming profile requests
//               from the server.
//
global b32 GotUserInfo = false;
//
// NOTE(Sander): If true then the 'About' window will be shown.
//
global b32 ShowAboutWindow = false;
//
// NOTE(Sander): if true then a dialog box complaining about the size of the profile image will be shown.
//
global b32 ShowProfileImgFileSizeWarning = false;
//
// NOTE(Sander): This shows the other options window (containing Sign out, refresh credentials, etc..) in
//               the profile menu
//
global b32 ShowOtherOptionsWindow = false;
//
// NOTE(Sander): When true Shows a confirmation box, alter the confirmation text to change the content.
//               Used when for example deleting a user
//
global b32 ShowConfirmationScreen = false;
//
// NOTE(Sander): Are we currently enroling at the moment (this changes to GUI and login proceedure slightly)
//
global b32 IsEnrolling = false;
//
// NOTE(Sander): Are we currently refreshing credentials (this changes to GUI and login proceedure slightly)
//
global b32 IsRefreshing = false;
//
// NOTE(Sander): The text to display in the confirmation dialog box
//
global char *ConfirmationText = "";
//
// NOTE(Sander): Text to display in the confirmation dialog when refreshing
//
global char *RefreshCredText = "Are you sure you want to refresh your credentials?";
//
// NOTE(Sander): Text to display in the confirmation dialog when revoking
//
global char *DeleteUserText = "Are you sure you want to delete your account?";
//
// NOTE(Sander): Temporary variable used to hold the user name when the user types in the textfield
//
global char Username[MAX_USERNAME_LENGTH] = {0};
//
// NOTE(Sander): Holds the size of the selected profile image (selected from the dialog box)
//
global umm CurrentProfileImgSize = 0;
//
// NOTE(Sander): Profile image to show
//
global struct nk_image ProfileImg = {0};

global b32 ShowCredits = false;
global b32 IsLoadingProfileImage;
global minutia_point MinutiaePoints[200] = {0};
global u32 MinutiaPointCount = {0};
global int ScanImageWidth = 0;
global int ScanImageHeight = 0;
global struct nk_image FingerprintImage;
global volatile b32 IsAuthenticating = false;
global u32 TempMinutiaPointCount = 0;

//
//#################################################################################################

//
// NOTE(Sander): Debug function, prints an array as a hex sequence
//
internal void
PrintHex(u8 *p, u32 Size)
{
    for(u32 i = 0; i < Size; ++i)
    {
        printf("%x,", p[i]);
    }
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(UploadUserInfo)
///
/// About:
/// Uploads user info to the server
/// 
/// Parameters:
/// Data - Pointer to a valid program_state structure where the data under 'User' must be valid and contain
///        the information to upload.
///
/// Return value:
/// None
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(UploadUserInfo)
{
    program_state *State = (program_state*)Data;
    user User = State->User;
    int Res = RemoteCall(&State->Net, MSG_UpdateInfo, User.Username, strlen(User.Username), User.Fullname, strlen(User.Fullname), User.Description, strlen(User.Description), User.TempFilePath, User.FieldsToUpdate);
    if(Res == MSG_Result_NoError)
    {
        printf("User info updated!\n");
    }
    else if(Res == MSG_Result_NotLoggedIn)
    {
        printf("You need to be logged in to do that!\n");
    }
    else if(Res == MSG_Result_Failed)
    {
        printf("Failed to update user info!\n");
    }
    else
    {
        printf("An unknown error occured!\n");
    }
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(GetUserInfo)
///
/// About:
/// Gets info from the server about a user
/// 
/// Parameters:
/// Data - Pointer to a valid program_state structure where the data under 'User' must be valid
///
/// Return value:
/// User data will be stored in the 'User' member in program_state
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(GetUserInfo)
{
    program_state *State = (program_state*)Data;
    user User = {0};
    strcpy_s(User.Username, MAX_USERNAME_LENGTH, State->User.Username);
    int Res = RemoteCall(&State->Net, MSG_GetInfo, State->User.Username, strlen(State->User.Username), &User);
    if(Res == MSG_Result_NoError)
    {
        State->User = User;
        State->UserOrig = State->User;
    }
    else if(Res == MSG_Result_NotLoggedIn)
    {
        printf("You need to be logged in to do that!\n");
    }
    else if(Res == MSG_Result_Failed)
    {
        printf("Failed to get user info!\n");
    }
    else
    {
        printf("An unknown error occured!\n");
    }
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(DoLogout)
///
/// About:
/// Logs the currently logged in user out.
/// 
/// Parameters:
/// Data - Pointer to a valid program_state structure where the data under 'User' must be valid
///
/// Return value:
/// None
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(DoLogout)
{
    program_state *State = (program_state*)Data;
    int Res = RemoteCall(&State->Net, MSG_LogOut, State->User.Username, strlen(State->User.Username));
    if(Res == MSG_Result_NoError)
    {
        printf("Logged out!\n");
    }
    else if(Res == MSG_Result_AlreadyLoggedOut)
    {
        printf("Error already logged out!\n");
    }
    else
    {
        printf("Failed to log out for an unknown reason!\n");
    }
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(DoDeleteUser)
///
/// About:
/// Deletes the currently logged in user.
/// 
/// Parameters:
/// Data - Pointer to a valid program_state structure where the data under 'User' must be valid
///
/// Return value:
/// None
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(DoDeleteUser)
{
    program_state *State = (program_state*)Data;
    int Res = RemoteCall(&State->Net, MSG_Delete, State->User.Username, strlen(State->User.Username));
    if(Res == MSG_Result_NoError)
    {
        printf("Logged out!\n");
    }
    else if(Res == MSG_Result_AlreadyLoggedOut)
    {
        printf("Error already logged out!\n");
    }
    else
    {
        printf("Failed to log out for an unknown reason!\n");
    }
}

///##################################################################################
/// Function:
/// u32 QuickHash(char *String)
///
/// About:
/// Creates a 4 byte hash from a string.
/// 
/// Parameters:
/// String - Null-terminated string to hash
///
/// Return value:
/// A 4 byte hashed string
///
/// Note:
/// Inspired by djb2 from http://www.cse.yorku.ca/~oz/hash.html
/// 
///##################################################################################
internal u32
QuickHash(char *String)
{
    s32 hashAddress = 5381;
    for(s32 Counter = 0; String[Counter] != '\0'; Counter++)
    {
        hashAddress = ((hashAddress << 5) + hashAddress) + String[Counter];
    }
    return(hashAddress);
}

///##################################################################################
/// Function:
/// int DoLogin(login_params *Params, char *ProtectedTemplateFileName, char *ADFileName)
///
/// About:
/// Performs the login routine by running the Secret Releaser and gets confirmation from SRP
/// 
/// Parameters:
/// Params - Pointer to a valid login_params structure where the State pointer must be valid
/// ProtectedTemplateFileName - Filepath of the biometric probe
/// ADFileName - Filepath of the auxiliary data/helper data
/// IsRefreshing - Set to true if refreshing credentials
///
/// Return value:
/// Result of enroling, MSG_Result_NoError if success, -1 if error or MSG_Result_Failed if failed.
/// 
///##################################################################################
internal int
DoEnrolment(login_params *Params, char *ProtectedTemplateFileName, char *ADFileName, b32 IsRefreshing)
{
    int Res = -1;
    u8 Password[sizeof(u8)*BITS(128)] = {0};
    if(GenerateSecretAndAuxiliaryData(ProtectedTemplateFileName, ADFileName, Password))
    {
        printf("Secret is: ");
        PrintHex(Password, sizeof(u8)*BITS(128));
        printf("\n");
        Res = RemoteCall(&Params->State->Net, IsRefreshing ? MSG_UpdateCredentials : MSG_Register, Params->State->User.Username, strlen(Params->State->User.Username), Password, sizeof(u8)*BITS(128));
        ZeroMem(Password, sizeof(u8)*BITS(128));
    }
    return(Res);
}

///##################################################################################
/// Function:
/// int DoLogin(login_params *Params, char *ProtectedTemplateFileName, char *ADFileName)
///
/// About:
/// Performs the login routine by running the Secret Releaser and gets confirmation from SRP
/// 
/// Parameters:
/// Params - Pointer to a valid login_params structure where the State pointer must be valid
/// ProtectedTemplateFileName - Filepath of the biometric probe
/// ADFileName - Filepath of the auxiliary data/helper data
///
/// Return value:
/// Result of login, MSG_Result_NoError if success, -1 if error or MSG_Result_Failed if failed.
/// 
///##################################################################################
internal int
DoLogin(login_params *Params, char *ProtectedTemplateFileName, char *ADFileName)
{
    int Res = -1;
#define MaxSecrets 5
    u32 SecretCount = 0;
    u8 Password[sizeof(u8)*BITS(128)*MaxSecrets] = {0};
    if(GetSecretFromAuxiliaryData(Params->State->WorkQueue, ProtectedTemplateFileName, ADFileName, Password, &SecretCount, MaxSecrets))
    {
        printf("Recovered secret is: ");
        PrintHex(Password, sizeof(u8)*BITS(128));
        printf("\n");
        for(u32 Index = 0;
            Index < SecretCount;
            ++Index)
        {
            Res = RemoteCall(&Params->State->Net, MSG_LogIn, Params->State->User.Username, strlen(Params->State->User.Username), Password, sizeof(u8)*BITS(128));
            if(Res == MSG_Result_NoError)
            {
                break;
            }
        }
        
    }
    return(Res);
}

///##################################################################################
/// Function:
/// double DoMatching(char *ProtectedTemplateFileNameCopy, char *ProtectedTemplateFileName, char *ScoreFilePath)
///
/// About:
/// Runs the matcher and returns the score.
/// 
/// Parameters:
/// ProtectedTemplateFileNameCopy - Filepath of the first template to match
/// ProtectedTemplateFileName - Filepath of the second template to match
/// ScoreFilePath - Filepath to where to store the score results
///
/// Return value:
/// The score values stored in ScoreFile
/// 
///##################################################################################
internal double
DoMatching(char *ProtectedTemplateFileNameCopy, char *ProtectedTemplateFileName, char *ScoreFilePath)
{
    double Score = 0;
    
    Platform.DeleteFileAt(ScoreFilePath);
    
    char Cmd[250];
    snprintf(Cmd, sizeof(Cmd), "--tpl1 %s --tpl2 %s --log %s", ProtectedTemplateFileNameCopy, ProtectedTemplateFileName, ScoreFilePath);
    if(!Platform.RunAndWaitForProcess("GUCMatcher.exe", Cmd))
    {
        return(-1.0);
    }
    
    fkb_read_file ScoreFile = Platform.ReadEntireFile(ScoreFilePath);
    if(!ScoreFile.Content) 
    {
        return(-1.0);
    }
    *((char*)ScoreFile.Content + ScoreFile.Size-1) = 0;
    
    char *Scan = (char*)ScoreFile.Content + strlen(ProtectedTemplateFileNameCopy) + 1 + strlen(ProtectedTemplateFileName) + 1;
    if(strstr(Scan, "OK"))
    {
        Scan += strlen("OK") + 1;
    }
    else if(strstr(Scan, "FAIL"))
    {
        Scan += strlen("FAIL") + 1;
    }
    
    Score = atof(Scan);
    
    Platform.FreeEntireFile(ScoreFile);
    
    Platform.DeleteFileAt(ScoreFilePath);
    
    return(Score);
}

///##################################################################################
/// Function:
/// void UpdateProgressBar(program_state *State, s32 Value)
///
/// About:
/// Helper function that sets the progressbar progress.
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Value - The new value to set the progress to
///
/// Return value:
/// Sets the CurrentScanProgress value and also triggers an update event (updates the screen).
/// 
///##################################################################################
internal void
UpdateProgressBar(program_state *State, s32 Value)
{
    State->CurrentScanProgress = Value;
    Platform.UpdateScreen();
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(UpdateMinutiaePointGrahpic)
///
/// About:
/// Very hacky function that gradually displays the minutiae over time
/// 
/// Parameters:
/// None (Pass NULL/0)
///
/// Return value:
/// Increments TempMinutiaPointCount until it equals MinutiaPointCount
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(UpdateMinutiaePointGrahpic)
{
    while(TempMinutiaPointCount < MinutiaPointCount)
    {
        TempMinutiaPointCount++;
        Platform.UpdateScreen();
        SDL_Delay(rand()%100);
    }
    TempMinutiaPointCount = MinutiaPointCount;
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(DoEnrolOrLogin)
///
/// About:
/// Performs enrolment or login depending on what fields are filled out in login_params passed to this function
/// 
/// Parameters:
/// Data - Pointer to a login_params structure, State must be valid, if a ISOTemplatePath is valid then minutiae extracting
///        is skipped and we just generate the protected template, FingerprintScanImagePath must be valid since there is no sensor
///
/// Return value:
/// If login/enrolment is successful then the active window will be profile page, if failed then an error message is displayed.
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(DoEnrolOrLogin)
{
    login_params *Params = (login_params*)Data;
    char ADFileName[200];
    char *ScoreFilePath = "score.temp";
    char *ProtectedTemplateFileName = "template.tpl";
    char ProtectedTemplateFileNameCopy[200] = {0};
    char Cmd[200] = {0};
    u32 UserHash = QuickHash(Params->State->User.Username);
    snprintf(ProtectedTemplateFileNameCopy, sizeof(ProtectedTemplateFileNameCopy), "template_%u.tpl", UserHash);
    snprintf(ADFileName, sizeof(ADFileName), "ad_%u.dat", UserHash);
    Params->State->AuthenticationResult = AuthenticationResult_Processing;
    strcpy_s(Params->ProtectedTemplatePath, sizeof(ProtectedTemplateFileNameCopy), ProtectedTemplateFileName);
    
    UpdateProgressBar(Params->State, 0);
    if(strlen(Params->FingerprintScanImagePath))
    {
        if(FingerprintImage.handle.ptr)
        {
            FreeTexture((fkb_texture*)FingerprintImage.handle.ptr);
        }
        FingerprintImage = nk_image_ptr(LoadTexture(Params->FingerprintScanImagePath));
    }
    else
    {
        // TODO(Sanderkp): This is where the sensor should do its thing
    }
    UpdateProgressBar(Params->State, 10);
    
    if(strlen(Params->ISOTemplatePath) == 0)
    {
        if(!ExtractMinutiaeAndWriteISOTemplate(Params->FingerprintScanImagePath, Params->ISOTemplatePath, ArrayCount(Params->ISOTemplatePath),&ScanImageWidth, &ScanImageHeight, MinutiaePoints, &MinutiaPointCount, ArrayCount(MinutiaePoints)))
        {
            IsAuthenticating = false;
            Params->State->AuthenticationResult = AuthenticationResult_Failed;
            DebugLog("Failed to extract minutiae and write iso template");
            return;
        }
        else
        {
            Platform.AddWork(Params->State->WorkQueue, UpdateMinutiaePointGrahpic, 0);
        }
    }
    
    UpdateProgressBar(Params->State, 20);
    
    snprintf(Cmd, sizeof(Cmd), "--aeskm AESKm.cf --nonce nonce.cf --isotpl %s --outfile %s", Params->ISOTemplatePath, Params->ProtectedTemplatePath);
    
    if(!Platform.RunAndWaitForProcess("GUCGenerator.exe", Cmd))
    {
        IsAuthenticating = false;
        Params->State->AuthenticationResult = AuthenticationResult_Failed;
        DebugLog("Failed to run template generator");
        return;
    }
    
    UpdateProgressBar(Params->State, 30);
    
    if(Params->IsEnrolling || Params->IsRefreshing)
    {
        if(!Platform.CopyFileTo(ProtectedTemplateFileName, ProtectedTemplateFileNameCopy))
        {
            IsAuthenticating = false;
            Params->State->AuthenticationResult = AuthenticationResult_Failed;
            DebugLog("Failed to make copy of protected template");
            return;
        }
    }
    
    UpdateProgressBar(Params->State, 35);
    
    if(Params->IsEnrolling || Params->IsRefreshing)
    {
        if(DoEnrolment(Params, ProtectedTemplateFileName, ADFileName, Params->IsRefreshing) != MSG_Result_NoError)
        {
            IsAuthenticating = false;
            Params->State->AuthenticationResult = AuthenticationResult_Failed;
            DebugLog("Enrolment failed");
            return;
        }
        else
        {
            DebugLog("Enrolment successful!");
            Params->IsEnrolling = false;
        }
    }
    
    UpdateProgressBar(Params->State, 55);
    
    if(DoLogin(Params, ProtectedTemplateFileName, ADFileName) != MSG_Result_NoError)
    {
        IsAuthenticating = false;
        Params->State->AuthenticationResult = AuthenticationResult_Failed;
        DebugLog("Login failed");
        return;
    }
    
    UpdateProgressBar(Params->State, 85);
    
    double Score = DoMatching(ProtectedTemplateFileNameCopy, ProtectedTemplateFileName, ScoreFilePath);
    if(Score == -1.0)
    {
        IsAuthenticating = false;
        Params->State->AuthenticationResult = AuthenticationResult_Failed;
        DebugLog("Matcher failed (score == -1.0)");
        return;
    }
    
    UpdateProgressBar(Params->State,100);
    
    DebugLog("Score: %g\n", Score);
    
    Platform.DeleteFileAt(Params->ISOTemplatePath);
    Platform.DeleteFileAt(Params->ProtectedTemplatePath);
    ZeroMem(Params->ISOTemplatePath, sizeof(Params->ISOTemplatePath));
    
    Params->State->AuthenticationResult = AuthenticationResult_Success;
    IsAuthenticating = false;
    
    TempMinutiaPointCount = MinutiaPointCount;
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(LoadProfileImageFromDisk)
///
/// About:
/// Loads a profile image from disk, almost the same as LoadProfileImage
/// 
/// Parameters:
/// Data - Pointer to a load_profile_image_params structure, 
//         NkImage should be valid and File must be valid
///
/// Return value:
/// NkImage in the load_profile_image_params passed to this function will contain a valid image if success and NULL if failed
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(LoadProfileImageFromDisk)
{
    load_profile_image_params *Params = (load_profile_image_params*)Data;
    if(Params->NkImage)
    {
        *Params->NkImage = nk_image_ptr(LoadTexture(Params->File));
    }
    IsLoadingProfileImage = false;
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(LoadProfileImage)
///
/// About:
/// Loads a profile image by parsing an image stored in memory.
/// 
/// Parameters:
/// Data - Pointer to a load_profile_image_params structure, 
//         NkImage should be valid and ImageData and ImageDataSize must be valid
///
/// Return value:
/// NkImage in the load_profile_image_params passed to this function will contain a valid image if success and NULL if failed
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(LoadProfileImage)
{
    load_profile_image_params *Params = (load_profile_image_params*)Data;
    if(Params->NkImage)
    {
        Assert(Params->ImageData && Params->ImageDataSize);
        *Params->NkImage = nk_image_ptr(LoadTextureFromMemory(Params->ImageData, Params->ImageDataSize));
    }
}

///##################################################################################
/// Function:
/// int TextEditFilter(const nk_text_edit *Edit, nk_rune unicode)
///
/// About:
/// Used by nuklear to filter unwanted keys, we mostly use this to limit input to ASCII because
/// that is currently the only encoding we support.
/// 
/// Parameters:
/// 1st argument is not used
/// unicode - A unicode character (provided by nuklear)
///
/// Return value:
/// The filtered unicode character
/// 
///##################################################################################
internal int 
TextEditFilter(const nk_text_edit *, nk_rune unicode)
{
    int Result = 0;
    Result = (unicode >= 0 && unicode <= 128);
    return(Result);
}

///##################################################################################
/// Function:
/// void DoAboutWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
///
/// About:
/// Shows the about window, this includes version, information about device and sensor.
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
///
/// Return value:
/// None
///
/// Note:
/// This is not a standalone routine, do not call manually
/// 
///##################################################################################
internal void
DoAboutWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
{
    r32 Aspect = 0.0f;
    if(Height != 0) 
    {
        Aspect = (r32)Width/(r32)Height;
    }
    if(nk_begin(Context, "About", nk_rect(((r32)Width-((r32)Width*0.8f)/Aspect)/2.0f,(r32)Height*0.1f,((r32)Width*0.8f)/Aspect,(r32)Height*0.8f), NK_WINDOW_NO_SCROLLBAR))
    {
        nk_layout_space_begin(Context, NK_DYNAMIC, (r32)Height*0.8f, INT_MAX);
        nk_layout_space_push(Context, nk_rect(0.0f,0.0f,1.0f,1.0f));
        nk_image(Context, State->Gradient);
        nk_layout_space_push(Context, nk_rect(0.0f,0,1.0f,0.1f));
        nk_label(Context, "About", NK_TEXT_ALIGN_CENTERED);
        
        nk_layout_space_push(Context, nk_rect(0.0f,0.1f,1.0f,0.9f));
        char Buffer[1024];
        stbsp_sprintf(Buffer, "Version: %s\n\nDevice: %s\n\nSensor: %s", FKB_APP_VERSION, SDL_GetPlatform(), "Not available");
        nk_label(Context, Buffer, NK_TEXT_ALIGN_LEFT);
        
        nk_style_push_font(Context, &State->MPlusBold);
        nk_style_push_float(Context, &State->MPlusBold.height, State->FontSizes[FontSize_Small]);
        nk_layout_space_push(Context, nk_rect(0.3f,0.91f,0.4f,0.05f));
        if(nk_button_label_styled(Context, &LabelButtonStyle, "Close"))
        {
            ShowAboutWindow = false;
        }
        nk_layout_space_push(Context, nk_rect(0.3f,0.8f,0.4f,0.05f));
        if(nk_button_label_styled(Context, &LabelButtonStyle, "Credits"))
        {
            ShowCredits = true;
        }
        nk_style_pop_float(Context);
        nk_style_pop_font(Context);
        
        nk_layout_space_end(Context);
        
        if(ShowCredits)
        {
            struct nk_rect Bounds = nk_layout_space_bounds(Context);
            nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Tiny]);
            if(nk_popup_begin(Context, NK_POPUP_STATIC, "Credits", NK_WINDOW_TITLE, nk_rect((Bounds.w-500.0f)/2.0f,(Bounds.h-500.0f)/2.0f,500.0f,500.0f)))
            {
                nk_layout_row_dynamic(Context, 0.0f, 1);
                nk_label(Context, "stb_image.h - Sean Barrett", NK_TEXT_LEFT);
                nk_label(Context, "stb_rect_pack.h - Sean Barrett", NK_TEXT_LEFT);
                nk_label(Context, "stb_sprintf.h - Sean Barrett", NK_TEXT_LEFT);
                nk_label(Context, "stb_truetype.h - Sean Barrett", NK_TEXT_LEFT);
                nk_label(Context, "nuklear.h - Micha Mettke", NK_TEXT_LEFT);
                nk_label(Context, "Secure Remote Password 6a implementation - Tom Cocagne", NK_TEXT_LEFT);
                nk_label(Context, "OS Cryptographically-Secure Pseudo-Random Number Generator\n - Michael Thomas Greer", NK_TEXT_LEFT);
                nk_label(Context, "Encoder/decoder for binary BCH codes in C (Version 3.1)\n - Robert Morelos-Zaragoza", NK_TEXT_LEFT);
                nk_label(Context, "tiny-AES-c - kokke", NK_TEXT_LEFT);
                nk_label(Context, "SDL2 - Sam Lantinga", NK_TEXT_LEFT);
                nk_label(Context, "SDL2_net - Sam Lantinga and Roy Wood", NK_TEXT_LEFT);
                nk_label(Context, "", NK_TEXT_LEFT);
                nk_label(Context, "This product includes software developed by the OpenSSL Project", NK_TEXT_LEFT);
                nk_label(Context, "    for use in the OpenSSL Toolkit. (http://www.openssl.org/)", NK_TEXT_LEFT);
                
                nk_style_push_font(Context, &State->MPlusBold);
                nk_style_push_float(Context, &State->MPlusBold.height, State->FontSizes[FontSize_Small]);
                if(nk_button_label_styled(Context, &LabelButtonStyle, "Close"))
                {
                    ShowCredits = false;
                    nk_window_set_focus(Context, "About");
                    nk_popup_close(Context);
                }
                nk_style_pop_float(Context);
                nk_style_pop_font(Context);
                
                nk_layout_space_end(Context);
            }
            nk_popup_end(Context);
            nk_style_pop_float(Context);
        }
    }
    nk_end(Context);
}

///##################################################################################
/// Function:
/// void DoProfileImageFileSizeWarning(program_state *State, nk_context *Context, s32x Width, s32x Height)
///
/// About:
/// Shows a warning about the profile image file size (which is limited).
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
///
/// Return value:
/// None
///
/// Note:
/// This is not a standalone routine, do not call manually
/// 
///##################################################################################
internal void
DoProfileImageFileSizeWarning(program_state *State, nk_context *Context, s32x Width, s32x Height)
{
    if(nk_begin_titled(Context, "Warning", "Warning", nk_rect(0.1f*Width, 0.1f*Height, 0.8f*Width, 0.8f*Height), NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_TITLE|NK_WINDOW_BORDER))
    {
        nk_layout_space_begin(Context, NK_DYNAMIC, (r32)Height*0.8f, INT_MAX);
        
        nk_layout_space_push(Context, nk_rect(0.0f,0.1f,1.0f,0.9f));
        char Buffer[1024];
        stbsp_sprintf(Buffer, "Maximum allowed profile image size is %gkb. But the current image is %gkb", (r32)MAX_PROFILE_IMG_SIZE/1024.0f, (r32)CurrentProfileImgSize/1024.0f);
        
        nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Medium]);
        nk_label_wrap(Context, Buffer);
        nk_style_pop_float(Context);
        
        nk_layout_space_push(Context, nk_rect(0.35f,0.81f,0.3f,0.06f));
        if(nk_button_label(Context, "Close"))
        {
            ShowProfileImgFileSizeWarning = false;
        }
        
        nk_layout_space_end(Context);
    }
    nk_end(Context);
}

///##################################################################################
/// Function:
/// void DoOtherOptionsWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
///
/// About:
/// Shows the other options window with the 'Sign out' button and such.
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
///
/// Return value:
/// None
///
/// Note:
/// This is not a standalone routine, do not call manually
/// 
///##################################################################################
internal void
DoOtherOptionsWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
{
    if(nk_begin_titled(Context, "Other Options", "Other Options", nk_rect(0.2f*Width, 0.1f*Height, 0.6f*Width, 0.8f*Height), NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_TITLE|NK_WINDOW_BORDER))
    {
        nk_layout_space_begin(Context, NK_DYNAMIC, (r32)Height*0.8f, INT_MAX);
        
        struct nk_rect Bounds = nk_layout_space_bounds(Context);
        r32 Aspect = Bounds.w/Bounds.h;
        
        r32 SignOutWidth = 0.5f/Aspect;
        nk_layout_space_push(Context, nk_rect((1.0f-SignOutWidth)/2.0f,0.15f,SignOutWidth,0.15f));
        nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Large]);
        if(nk_button_label(Context, "Sign out"))
        {
            Platform.AddWork(State->WorkQueue, DoLogout, State);
            State->Mode = ScreenMode_Start;
            ShowOtherOptionsWindow = false;
        }
        nk_style_pop_float(Context);
        
        r32 OtherButtonsWidth = 0.39f/Aspect;
        nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Small]);
        nk_layout_space_push(Context, nk_rect((1.0f-OtherButtonsWidth)/2.0f,0.405f,OtherButtonsWidth,0.07f));
        if(nk_button_label(Context, "Refresh credentials"))
        {
            ShowOtherOptionsWindow = false;
            ShowConfirmationScreen = true;
            ConfirmationText = RefreshCredText;
            strcpy_s(Username, MAX_USERNAME_LENGTH, State->User.Username);
        }
        
        nk_layout_space_push(Context, nk_rect((1.0f-OtherButtonsWidth)/2.0f,0.6f,OtherButtonsWidth,0.07f));
        if(nk_button_label(Context, "Delete user"))
        {
            ShowOtherOptionsWindow = false;
            ShowConfirmationScreen = true;
            ConfirmationText = DeleteUserText;
        }
        nk_style_pop_float(Context);
        nk_layout_space_push(Context, nk_rect(0.35f,0.81f,0.3f,0.06f));
        if(nk_button_label_styled(Context, &LabelButtonStyle, "Close"))
        {
            ShowOtherOptionsWindow = false;
        }
        
        nk_layout_space_end(Context);
    }
    nk_end(Context);
}

///##################################################################################
/// Function:
/// void DoConfirmationWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
///
/// About:
/// Shows a confirmation window, set ConfirmationText to change the text
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
///
/// Return value:
/// None
///
/// Note:
/// This is not a standalone routine, and should only be called if you know what you are doing
/// 
/// Todo:
/// Factor this out to a separate routine
/// 
///##################################################################################
internal void
DoConfirmationWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
{
    if(nk_begin_titled(Context, "Are you sure?", "Are you sure?", nk_rect(0.2f*Width, 0.3f*Height, 0.6f*Width, 0.4f*Height), NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_TITLE|NK_WINDOW_BORDER))
    {
        nk_layout_space_begin(Context, NK_DYNAMIC, (r32)Height*0.8f, INT_MAX);
        struct nk_rect Bounds = nk_layout_space_bounds(Context);
        r32 Aspect = Bounds.w/Bounds.h;
        r32 ButtonWidth = 0.25f/Aspect;
        nk_layout_space_push(Context, nk_rect(0,0.0f,1.0f,0.28f));
        nk_label_wrap(Context, ConfirmationText);
        nk_layout_space_push(Context, nk_rect(0.5f-ButtonWidth-0.01f,0.28f,ButtonWidth,0.09f));
        if(nk_button_label(Context, "Yes"))
        {
            if(ConfirmationText == DeleteUserText)
            {
                Platform.AddWork(State->WorkQueue, DoDeleteUser, State);
                State->Mode = ScreenMode_Start;
                char Buffer[500];
                u32 UserHash = QuickHash(State->User.Username);
                stbsp_sprintf(Buffer, "ad_%u.dat", UserHash);
                Platform.DeleteFileAt(Buffer);
                stbsp_sprintf(Buffer, "template_%u.dat", UserHash);
                Platform.DeleteFileAt(Buffer);
            }
            else if(ConfirmationText == RefreshCredText)
            {
                State->Mode = ScreenMode_Scan;
                IsRefreshing = true;
            }
            ShowConfirmationScreen = false;
        }
        nk_layout_space_push(Context, nk_rect(0.51f,0.28f,ButtonWidth,0.09f));
        if(nk_button_label(Context, "No"))
        {
            ShowOtherOptionsWindow = true;
            ShowConfirmationScreen = false;
        }
        nk_layout_space_end(Context);
    }
    nk_end(Context);
}

///##################################################################################
/// Function:
/// void DoMainWindowStartScreen(program_state *State, nk_context *Context, s32x Width, s32x Height, r32 Aspect)
///
/// About:
/// This is the start up screen and contains the 'Sign up' and 'Sign in' buttons, including the '?' button that opens the about window
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
/// Aspect - Calculated by dividing width by height of the current window/view.
///
/// Return value:
/// None
/// 
/// Note:
/// Do not call this manually, let DoMainWindow call this.
/// 
///
///##################################################################################
internal void
DoMainWindowStartScreen(program_state *State, nk_context *Context, s32x Width, s32x Height, r32 Aspect)
{
    nk_style_push_font(Context, &State->MPlusBold);
    nk_layout_space_push(Context, nk_rect(1.0f-(0.05f/Aspect),0,0.05f/Aspect,0.05f));
    if(nk_button_label_styled(Context, &LabelButtonStyle, "?"))
    {
        nk_window_set_focus(Context, "About");
        ShowAboutWindow = true;
    }
    nk_style_pop_font(Context);
    
    nk_style_push_font(Context, &State->MPlusRegular);
    nk_layout_space_push(Context, nk_rect(0.0f,0.01f,1.0f,0.4f));
    nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Huge]);
    nk_label(Context, "Welcome", NK_TEXT_CENTERED);
    nk_style_pop_float(Context);
    nk_style_pop_font(Context);
    
    switch(SDL_AtomicGet(&State->Net.ConnectionState))
    {
        case ConnectionState_Disconnected:
        {
            nk_layout_space_push(Context, nk_rect(0.0f,0.175f,1.0f,0.4f));
            nk_label(Context, "Disconnected", NK_TEXT_CENTERED);
        }break;
        case ConnectionState_Connecting:
        {
            nk_layout_space_push(Context, nk_rect(0.0f,0.42f,1.0f,0.1f));
            nk_label(Context, "Connecting...", NK_TEXT_CENTERED);
        }break;
        case ConnectionState_Connected:
        {
            
            nk_layout_space_push(Context, nk_rect(0.0f,0.09f,1.0f,0.4f));
            nk_label(Context, "Please sign in", NK_TEXT_CENTERED);
            
            r32 ButtonWidth = 0.5f/Aspect;
            nk_layout_space_push(Context, nk_rect((1.0f-ButtonWidth)/2.0f,0.45f,ButtonWidth,0.08f));
            nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Large]);
            if(nk_button_label(Context, "Sign in"))
            {
                State->Mode = ScreenMode_Scan;
            }
            nk_style_pop_float(Context);
            nk_layout_space_push(Context, nk_rect(0.35f,0.54f,0.3f,0.04f));
            nk_style_set_font(Context, &State->MPlusBold);
            nk_style_push_float(Context, &State->MPlusBold.height, State->FontSizes[FontSize_Medium]);
            if(nk_button_label_styled(Context, &LabelButtonStyle, "Sign up"))
            {
                State->Mode = ScreenMode_Scan;
                IsEnrolling = true;
            }
            nk_style_pop_float(Context);
        }break;
        case ConnectionState_Error:
        {
            nk_layout_space_push(Context, nk_rect(0.0f,0.175f,1.0f,0.4f));
            nk_label(Context, "Error", NK_TEXT_CENTERED);
        }break;
    };
    nk_style_set_font(Context, &State->MPlusRegular);
    nk_layout_space_end(Context);
    
}

///##################################################################################
/// Function:
/// void DoMainWindowScanScreen(program_state *State, nk_context *Context, s32x Width, s32x Height, r32 Aspect)
///
/// About:
/// This is the 'scan' screen,this is where the login happens.
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
/// Aspect - Calculated by dividing width by height of the current window/view.
///
/// Return value:
/// None
/// 
/// Note:
/// Do not call this manually, let DoMainWindow call this.
/// 
/// Todo:
/// Change the name to 'LoginScreen' instead
///
///##################################################################################
internal void
DoMainWindowScanScreen(program_state *State, nk_context *Context, s32x Width, s32x Height, r32 Aspect)
{
    char *Title = IsRefreshing ? "Update credentials" : IsEnrolling ? "Sign up" : "Sign in";
    char *ProgressText = IsAuthenticating ? "Processing..." : strlen(Username) == 0 ? "Please enter username" : "Waiting for fingerprint";
    nk_layout_space_push(Context, nk_rect(0.0f,0.01f,1.0f,0.1f));
    nk_label(Context, Title, NK_TEXT_CENTERED);
    r32 w = 0.25f/Aspect;
    if(State->AuthenticationResult != AuthenticationResult_Success)
    {
        nk_layout_space_push(Context, nk_rect((1.0f-w)/2+0.01f,0.1f+0.01f,w,0.35f));
        nk_image(Context, State->Gray);
    }
    nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.1f,w,0.35f));
    if(!IsAuthenticating)
    {
        r32 ButtonWidth = 0.5f/Aspect;
        if(State->AuthenticationResult == AuthenticationResult_None)
        {
            nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.64f,w,0.06f));
            if(strlen(Username) && nk_button_label(Context, "Scan"))
            {
                local char BiometricsSourceFile[500] = {0};
                local char *FileExtensions = "JPEG\0*.JPG;\0PNG\0*.PNG;\0GIF\0*.GIF;\0BMP\0*.BMP;\0TGA\0*.TGA;\0PSD\0*.PSD;\0TIF\0*.TIF;\0IST\0*.IST;\0\0";
                if(SDL_AtomicGet(&State->VerifingerLicenceState) != VerifingerLicence_Acquired)
                {
                    FileExtensions = "Iso template\0*.IST;\0\0";
                }
                if(Platform.OpenFileDialog(BiometricsSourceFile, sizeof(BiometricsSourceFile), FileExtensions))
                {
                    local login_params LoginParams = {0};
                    strcpy_s(State->User.Username, MAX_USERNAME_LENGTH, Username);
                    LoginParams.IsEnrolling = IsEnrolling;
                    LoginParams.IsRefreshing = IsRefreshing;
                    LoginParams.State = State;
                    if(SDL_AtomicGet(&State->VerifingerLicenceState) == VerifingerLicence_Acquired && !strstr(BiometricsSourceFile, ".ist"))
                    {
                        strcpy_s(LoginParams.FingerprintScanImagePath, MAX_FILEPATH_LEGNTH, BiometricsSourceFile);
                    }
                    else
                    {
                        strcpy_s(LoginParams.ISOTemplatePath, MAX_FILEPATH_LEGNTH, BiometricsSourceFile);
                    }
                    Platform.AddWork(State->WorkQueue, DoEnrolOrLogin, &LoginParams);
                    IsAuthenticating = true;
                    ZeroMem(Username, sizeof(Username));
                }
            }
            
            nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.9f,w,0.06f));
            if(nk_button_label(Context, "Cancel"))
            {
                State->Mode = ScreenMode_Start;
            }
            if(!IsRefreshing)
            {
                nk_layout_space_push(Context, nk_rect((1.0f-w*1.75f)/2,0.575f,w*1.75f,0.06f));
                nk_edit_string_zero_terminated(Context, NK_EDIT_FIELD, Username, sizeof(Username), TextEditFilter);
            }
        }
        else if(State->AuthenticationResult == AuthenticationResult_Processing)
        {
            ProgressText = "Processing";
        }
        else if(State->AuthenticationResult == AuthenticationResult_Success)
        {
            ProgressText = "Success!";
            nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.57f,w,0.06f));
            if(nk_button_label(Context, "Continue"))
            {
                State->Mode = ScreenMode_Profile;
                State->CurrentScanProgress = 0;
                State->CurrentScan = State->White;
                State->AuthenticationResult = AuthenticationResult_None;
                GotUserInfo = false;
                IsRefreshing = false;
                IsEnrolling = false;
                IsAuthenticating = false,
                MinutiaPointCount = 0;
                FreeTexture((fkb_texture*)FingerprintImage.handle.ptr);
                FingerprintImage.handle.ptr = 0;
                SDL_AtomicSet(&State->User.HasImage, 0);
                if(ProfileImg.handle.ptr)
                {
                    FreeTexture((fkb_texture*)ProfileImg.handle.ptr);
                    ProfileImg.handle.ptr = 0;
                }
                TempMinutiaPointCount = 0;
            }
        }
        else if(State->AuthenticationResult == AuthenticationResult_Failed)
        {
            ProgressText = "Failed!";
            nk_layout_space_push(Context, nk_rect((1.0f - ButtonWidth*0.7f)/2.0f,0.56f,ButtonWidth*0.7f,0.06f));
            if(nk_button_label(Context, "Try again"))
            {
                State->AuthenticationResult = AuthenticationResult_None;
            }
            
            nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.84f,w,0.06f));
            if(nk_button_label(Context, "Cancel"))
            {
                State->Mode = ScreenMode_Start;
                State->AuthenticationResult = AuthenticationResult_None;
            }
            if(FingerprintImage.handle.ptr)
            {
                FreeTexture((fkb_texture*)FingerprintImage.handle.ptr);
                FingerprintImage.handle.ptr = 0;
            }
            MinutiaPointCount = 0;
        }
    }
    if(IsAuthenticating)
    {
        nk_layout_space_push(Context, nk_rect(0.3f,0.65f,0.4f,0.05f));
        nk_progress(Context, (nk_size*)&State->CurrentScanProgress, 100, false);
    }
    nk_layout_space_push(Context, nk_rect(0.0f,0.475f,1.0f,0.1f));
    nk_label(Context, ProgressText, NK_TEXT_CENTERED);
    
    
    if(FingerprintImage.handle.ptr)
    {
        nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.1f,w,0.35f));
        nk_image(Context, FingerprintImage);
    }
    if(MinutiaPointCount && ScanImageWidth && ScanImageHeight)
    {
        for(u32 MinutiaIndex = 0;
            MinutiaIndex < TempMinutiaPointCount;
            ++MinutiaIndex)
        {
            r32 x = (1.0f-w-(0.015f/Aspect))/2 + ((r32)MinutiaePoints[MinutiaIndex].x/(r32)ScanImageWidth)*w;
            r32 y = 0.1f + ((r32)MinutiaePoints[MinutiaIndex].y/(r32)ScanImageHeight)*0.35f - 0.015f/2;
            r32 _w = 0.015f/Aspect;
            r32 h = 0.015f;
            
            nk_layout_space_push(Context, nk_rect(x,y,_w,h));
            nk_image(Context, State->MinutiaPoint);
        }
    }
}

///##################################################################################
/// Function:
/// void DoMainWindowProfileScreen(program_state *State, nk_context *Context, s32x Width, s32x Height, r32 Aspect)
///
/// About:
/// This is the profile page screen, lets users view their profile and change it
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
/// Aspect - Calculated by dividing width by height of the current window/view.
///
/// Return value:
/// None
/// 
/// Note:
/// Do not call this manually, let DoMainWindow call this.
/// 
///##################################################################################
internal void
DoMainWindowProfileScreen(program_state *State, nk_context *Context, s32x Width, s32x Height, r32 Aspect)
{
    if(!GotUserInfo)
    {
        Platform.AddWork(State->WorkQueue, GetUserInfo, State);
        GotUserInfo = true;
    }
    r32 w = 0.4f/Aspect;
    r32 h = 0.4f;
    if(ProfileImg.handle.ptr)
    {
        fkb_texture *Img = (fkb_texture*)ProfileImg.handle.ptr;
        r32 ImgAspect = (r32)Img->Width/(r32)Img->Height;
        
        w *= ImgAspect;
        if(w > 0.4)
        {
            r32 Scale = (0.4f/Aspect)/w;
            w *= Scale;
            h *= Scale;
        }
        if(h > 0.4)
        {
            r32 Scale = 0.4f/h;
            w *= Scale;
            h *= Scale;
        }
        nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.04f+(0.35f-h)/2,w,h));
        nk_image(Context, ProfileImg);
    }
    else
    {
        nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.04f,w,0.35f));
        nk_image(Context, State->Gray);
        
        if(SDL_AtomicGet(&State->User.HasImage) && !IsLoadingProfileImage)
        {
            local load_profile_image_params Params = {0};
            Params.NkImage = &ProfileImg;
            Params.ImageData = State->User.ImageData;
            Params.ImageDataSize = State->User.ImageDataSize;
            Platform.AddWork(State->WorkQueue, LoadProfileImage, &Params);
        }
    }
    
    w = 0.4f/Aspect;
    nk_layout_space_push(Context, nk_rect((1.0f-w)/2,0.428f,w,0.06f));
    nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Tiny]);
    if(nk_button_label(Context, "Set profile picture (Max 500kb)"))
    {
        char File[500] = {0};
        if(Platform.OpenFileDialog(File, sizeof(File), "*.JPG;*.PNG;*.GIF;*.BMP;*.TGA;*.PSD;"))
        {
            strcpy_s(State->User.TempFilePath, MAX_FILEPATH_LEGNTH, File);
            CurrentProfileImgSize = Platform.SizeOfFile(File);
            if(CurrentProfileImgSize > MAX_PROFILE_IMG_SIZE)
            {
                ShowProfileImgFileSizeWarning = true;
                ZeroMem(State->User.TempFilePath, sizeof(State->User.TempFilePath));
            }
            else
            {
                if(ProfileImg.handle.ptr)
                {
                    FreeTexture((fkb_texture*)ProfileImg.handle.ptr);
                    ProfileImg.handle.ptr = 0;
                }
                local load_profile_image_params Params = {0};
                Params.NkImage = &ProfileImg;
                strcpy_s(Params.File, MAX_FILEPATH_LEGNTH, File);
                IsLoadingProfileImage = true;
                Platform.AddWork(State->WorkQueue, LoadProfileImageFromDisk, &Params);
            }
        }
    }
    nk_style_pop_float(Context);
    
    nk_layout_space_push(Context, nk_rect((1.0f-w*1.5f)/2,0.5f,w*1.5f,0.06f));
    nk_label(Context, "Full name", NK_TEXT_LEFT);
    nk_layout_space_push(Context, nk_rect((1.0f-w*1.5f)/2,0.56f,w*1.5f,0.06f));
    nk_edit_string_zero_terminated(Context, NK_EDIT_FIELD, State->User.Fullname, sizeof(State->User.Fullname), TextEditFilter);
    
    nk_layout_space_push(Context, nk_rect((1.0f-w*1.5f)/2,0.62f,w*1.5f,0.06f));
    nk_label(Context, "Bio", NK_TEXT_LEFT);
    nk_layout_space_push(Context, nk_rect((1.0f-w*1.5f)/2,0.68f,w*1.5f,0.18f));
    nk_edit_string_zero_terminated(Context, NK_EDIT_BOX, State->User.Description, sizeof(State->User.Description), TextEditFilter);
    
    r32 ButtonWidth = 0.25f/Aspect;
    
    u32 FieldsToUpdate = 0;
    FieldsToUpdate |= (memcmp(State->User.Fullname, State->UserOrig.Fullname, MAX_FULL_NAME_LENGTH) != 0 ? UPDATE_FULLNAME : 0);
    FieldsToUpdate |= (memcmp(State->User.Description, State->UserOrig.Description, MAX_DESCRIPTION_LENGTH) != 0 ? UPDATE_DESCRIPTION : 0);
    FieldsToUpdate |= (memcmp(State->User.TempFilePath, State->UserOrig.TempFilePath, sizeof(State->User.TempFilePath)) != 0 != 0 ? UPDATE_PROFILE_IMAGE : 0);
    
    if(FieldsToUpdate != 0 && !ShowProfileImgFileSizeWarning)
    {
        nk_layout_space_push(Context, nk_rect((1.0f - ButtonWidth)/2.0f-ButtonWidth/2,0.9f,ButtonWidth,0.06f));
        if(nk_button_label(Context, "Save"))
        {
            State->User.FieldsToUpdate = FieldsToUpdate;
            Platform.AddWork(State->WorkQueue, UploadUserInfo, State);
            State->UserOrig = State->User;
        }
        nk_layout_space_push(Context, nk_rect((1.0f - ButtonWidth)/2.0f+ButtonWidth/2,0.9f,ButtonWidth,0.06f));
        if(nk_button_label(Context, "Revert"))
        {
            State->User = State->UserOrig;
            if(ProfileImg.handle.ptr)
            {
                FreeTexture((fkb_texture*)ProfileImg.handle.ptr);
                ProfileImg.handle.ptr = 0;
            }
        }
    }
    
    struct nk_style_button ButtonStyle = LabelButtonStyle;
    ButtonStyle.text_alignment = NK_TEXT_LEFT;
    nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Tiny]);
    nk_layout_space_push(Context, nk_rect(0.0f,0.0f,0.1f,0.02f));
    if(nk_button_label_styled(Context, &ButtonStyle, "Other options"))
    {
        ShowOtherOptionsWindow = true;
    }
    nk_style_pop_float(Context);
}

///##################################################################################
/// Function:
/// void DoMainWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
///
/// About:
/// This is the main window where almost all the interaction happens
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
///
/// Return value:
/// None
/// 
/// Note:
/// Do not call this manually, let OnGUI call this
/// 
///##################################################################################
internal void
DoMainWindow(program_state *State, nk_context *Context, s32x Width, s32x Height)
{
    if(nk_begin(Context, "Window", nk_rect(0,0,(r32)Width,(r32)Height), NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_NO_INPUT))
    {
        nk_layout_space_begin(Context, NK_DYNAMIC, (r32)Height, INT_MAX);
        nk_layout_space_push(Context, nk_rect(0.0f,0.5f,1.0f,0.5f));
        nk_image(Context, State->Gray);
        struct nk_rect Bounds = nk_layout_space_bounds(Context);
        r32 Aspect = Bounds.w/Bounds.h;
        
        switch(State->Mode)
        {
            case ScreenMode_Start:
            {
                DoMainWindowStartScreen(State, Context, Width, Height, Aspect);
            }break;
            case ScreenMode_Scan:
            {
                DoMainWindowScanScreen(State, Context, Width, Height, Aspect);
            }break;
            case ScreenMode_Profile:
            {
                DoMainWindowProfileScreen(State, Context, Width, Height, Aspect);
            }break;
        }
        if(ShowProfileImgFileSizeWarning ||
           ShowOtherOptionsWindow ||
           ShowAboutWindow ||
           ShowConfirmationScreen)
        {
            nk_layout_space_push(Context, nk_rect(0.0f,0.0f,1.0f,1.0f));
            nk_image(Context, State->Dark);
        }
    }
    nk_end(Context);
}

///##################################################################################
/// Function:
/// void OnGUI(program_state *State, nk_context *Context, s32x Width, s32x Height, renderer *Renderer)
///
/// About:
/// Main GUI function that contains all the logic for the interface.
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
/// Context - A valid pointer to a nk_context
/// Width, Height - Desired width and height of GUI area, this should be set to screen width/height most of the time
/// Renderer - A valid pointer to a renderer structure
///
/// Return value:
/// None
/// 
/// Note:
/// The functions in this function are not self contained and must be called in order.
///
///##################################################################################
internal void
OnGUI(program_state *State, nk_context *Context, s32x Width, s32x Height, renderer *Renderer)
{
    //
    // NOTE(Sander): If a color is undefined we default to magenta (to make it easy to spot)
    //
#define COLOUR_UNDEFINED nk_rgb(255,0,255)
    //
    // NOTE(Sanderkp): Custom defined skin for the GUI, some fields have not been coloured yet
    //                 (Hence COLOUR_UNDEFINED, feel free to define more colours if needed
    //
    local nk_color Colours[NK_COLOR_COUNT] =
    {
        nk_rgb(0,0,0),      //NK_COLOUR_TEXT
        nk_rgb(239,239,240),//NK_COLOUR_WINDOW
        nk_rgb(190,190,190),//NK_COLOUR_HEADER
        nk_rgb(0,0,0),      //NK_COLOUR_BORDER
        nk_rgb(232,232,232),//NK_COLOUR_BUTTON
        nk_rgb(252,252,252),//NK_COLOUR_BUTTON_HOVER
        nk_rgb(215,215,215),//NK_COLOUR_BUTTON_ACTIVE
        COLOUR_UNDEFINED,//NK_COLOUR_TOGGLE
        COLOUR_UNDEFINED,//NK_COLOUR_TOGGLE_HOVER
        COLOUR_UNDEFINED,//NK_COLOUR_TOGGLE_CURSOR
        COLOUR_UNDEFINED,//NK_COLOUR_SELECT
        COLOUR_UNDEFINED,//NK_COLOUR_SELECT_ACTIVE
        nk_rgb(232,232,232),//NK_COLOUR_SLIDER
        nk_rgb(125,125,125),//NK_COLOUR_SLIDER_CURSOR
        COLOUR_UNDEFINED,//NK_COLOUR_SLIDER_CURSOR_HOVER
        COLOUR_UNDEFINED,//NK_COLOUR_SLIDER_CURSOR_ACTIVE
        COLOUR_UNDEFINED,//NK_COLOUR_PROPERTY
        nk_rgb(252,252,252),//NK_COLOUR_EDIT
        COLOUR_UNDEFINED,//NK_COLOUR_EDIT_CURSOR
        COLOUR_UNDEFINED,//NK_COLOUR_COMBO
        COLOUR_UNDEFINED,//NK_COLOUR_CHART
        COLOUR_UNDEFINED,//NK_COLOUR_CHART_COLOUR
        COLOUR_UNDEFINED,//NK_COLOUR_CHART_COLOUR_HIGHLIGHT
        nk_rgb(239,239,240),//NK_COLOUR_SCROLLBAR
        nk_rgb(199,199,199),//NK_COLOUR_SCROLLBAR_CURSOR
        nk_rgb(199,199,199),//NK_COLOUR_SCROLLBAR_CURSOR_HOVER
        nk_rgb(199,199,199),//NK_COLOUR_SCROLLBAR_CURSOR_ACTIVE
        COLOUR_UNDEFINED//NK_COLOUR_TAB_HEADER
    };
    
    //
    // NOTE(Sander): Set the custom colour scheme
    //
    nk_style_from_table(Context, Colours);
    //
    // NOTE(Sander): Set the initial font size as 'regular', these must be popped later
    //
    nk_style_push_float(Context, &State->MPlusRegular.height, State->FontSizes[FontSize_Regular]);
    nk_style_push_float(Context, &State->MPlusBold.height, State->FontSizes[FontSize_Regular]);
    
    //
    // NOTE(Sander): If we ever lose connection to the server we immediately jump back to start screen
    //
    if(SDL_AtomicGet(&State->Net.ConnectionState) != ConnectionState_Connected)
    {
        State->Mode = ScreenMode_Start;
    }
    
    //
    // NOTE(Sander): Show the about window
    //
    if(ShowAboutWindow)
    {
        DoAboutWindow(State, Context, Width, Height);
    }
    
    
    //
    // NOTE(Sander): Show the profile image file size warning
    //
    if(ShowProfileImgFileSizeWarning)
    {
        DoProfileImageFileSizeWarning(State, Context, Width, Height);
    }
    
    
    //
    // NOTE(Sander): Show the other options window
    //
    if(ShowOtherOptionsWindow)
    {
        DoOtherOptionsWindow(State, Context, Width, Height);
    }
    
    //
    // NOTE(Sander): Show the confirmation screen
    //
    if(ShowConfirmationScreen)
    {
        DoConfirmationWindow(State, Context, Width, Height);
    }
    
    //
    // NOTE(Sander): This is the main window where everything happens
    //
    DoMainWindow(State, Context, Width, Height);
    
    //
    // NOTE(Sander): Pop the font sizes we pushed earlier
    //
    nk_style_pop_float(Context);
    nk_style_pop_float(Context);
}

