///##################################################################################
/// fkb_fingerprint_key_box.cpp
/// 
/// This is the main entry point into the DLL and is responsible for initializing
/// everything when the DLL is first loaded (IE not EVERY time the DLL is loaded
/// but the FIRST time it is loaded). This also processes the nuklear render commands
/// and translates them into commands we understand. This calls the GUI code.
/// 
/// 
///##################################################################################

///##################################################################################
/// Function:
/// PLATFORM_APP_EXIT(AppExit)
///
/// About:
/// Called by the host when the app exists, this is where we release the verifinger licence and close all the sockets.
/// 
/// Parameters:
/// State - A valid pointer to a program_state structure
///
/// Return value:
/// None
///
/// Note:
/// Must be set by the DLL at initalization time.
/// 
///##################################################################################
internal
PLATFORM_APP_EXIT(AppExit)
{
    if(SDL_AtomicGet(&State->VerifingerLicenceState) == VerifingerLicence_Acquired)
    {
        ReleaseNeuroTechLicence(VerifingerLicence);
    }
    if(State->Net.Socket)
    {
        DebugLog("Closing socket...\n");
        SDLNet_TCP_Close(State->Net.Socket);
    }
}

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(GetVerifingerLicence)
///
/// About:
/// This tries to get the verifinger licence. Because this is a potentially slow process we do this on another thread.
/// 
/// Parameters:
/// Data - Pointer to verifinger licence state (type SDL_atomic_t)
///
/// Return value:
/// None
///
/// Note:
/// Called at initalization time
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(GetVerifingerLicence)
{
    SDL_atomic_t *LicenceState = (SDL_atomic_t*)Data;
    DebugLog("Checking verifinger licence\n");
    if(CheckNeuroTechLicence(VerifingerLicence))
    {
        SDL_AtomicSet(LicenceState, VerifingerLicence_Acquired);
        DebugLog("Licence OK\n");
    }
    else
    {
        SDL_AtomicSet(LicenceState, VerifingerLicence_Unavailable);
        DebugLog("Licence Not Available!\n");
    }
}

//
// NOTE(Sanderkp): Prevent C++ name mangling
//
extern "C"
{
    //
    // NOTE(Sanderkp): This uses the exported macro which means it is available to other translation units
    //
    exported UPDATE_AND_RENDER(UpdateAndRender)
    {
        //
        // IMPORTANT(Sanderkp): The platform structure must be set every frame if hot reloader is enabled
        //
        Platform = *PlatformAPI;
        //
        // NOTE(Sanderkp): This is where initialization happens
        //
        if(!State->Initialized)
        {
            //
            // NOTE(Sanderkp): Create BCH object, defined in fkb_secret_release.cpp
            //
            BCH = new bch();
            BCH->Init(255, 207, 6);
            rng = csprng_create();
            if(!rng)
            {
                DebugLog("Failed to create random number generator!");
            }
            
            SDL_AtomicSet(&State->VerifingerLicenceState, VerifingerLicence_Checking);
            Platform.AddWork(State->WorkQueue, GetVerifingerLicence, &State->VerifingerLicenceState);
            
            State->MPlusRegular.width = FontCalculateWidth;
            State->MPlusBold.width = FontCalculateWidth;
            
            local load_font_params MPlusRegularParams = {"mplus-2p-regular.ttf", 40.0f, 2048, 2048, &State->MPlusRegular};
            Platform.AddWork(State->WorkQueue, LoadFontForNKThreaded, &MPlusRegularParams);
            
            local load_font_params MMPlusBoldParams = {"mplus-2p-bold.ttf", 40.0f, 2048, 2048, &State->MPlusBold};
            Platform.AddWork(State->WorkQueue, LoadFontForNKThreaded, &MMPlusBoldParams);
            
            State->White = nk_image_ptr(LoadTexture("white.psd"));
            State->CurrentScan = State->White;
            State->Gray = nk_image_ptr(LoadTexture("gray.psd"));
            State->Gradient = nk_image_ptr(LoadTexture("gradient.psd"));
            State->Dark = nk_image_ptr(LoadTexture("dark.psd"));
            State->MinutiaPoint = nk_image_ptr(LoadTexture("minutia_point.psd"));
            
            Platform.AddWork(State->WorkQueue, ConnectToServer, &State->Net);
            
            PlatformAPI->AppExit = AppExit;
            
            State->User.ImageData = Platform.Allocate(MAX_PROFILE_IMG_SIZE);
            
            State->Initialized = true;
        }
        //
        // NOTE(Sanderkp): Set up font scaling such that they scale with window height (looks better on high DPI screens)
        //
        r32 ScaleFactor = Clamp((Renderer->Height/720.0f), 0.001f, 100.0f);
        State->FontSizes[FontSize_Huge] = 90.0f*ScaleFactor;
        State->FontSizes[FontSize_Large] = 55.0f*ScaleFactor;
        State->FontSizes[FontSize_Regular] = 40.0f*ScaleFactor;
        State->FontSizes[FontSize_Medium] = 35.0f*ScaleFactor;
        State->FontSizes[FontSize_Small] = 30.0f*ScaleFactor;
        State->FontSizes[FontSize_Tiny] = 20.0f*ScaleFactor;
        
        //
        // NOTE(Sanderkp): This is where all the GUI code happens
        //
        OnGUI(State, Context,Renderer->Width, Renderer->Height, Renderer);
        
        //
        // NOTE(Sanderkp): Process nuklear draw commands
        //
        const nk_command *NKCommand = 0;
        nk_foreach(NKCommand, Context) 
        {
            switch(NKCommand->type) 
            {
                case NK_COMMAND_RECT:
                {
                    nk_command_rect *Rect = (nk_command_rect*)NKCommand;
                    PushImage(Renderer, 0);
                    PushColour(Renderer, Rect->color.r, Rect->color.g,Rect->color.b,Rect->color.a);
                    PushRectOutline(Renderer, Rect->x, Rect->y, Rect->w, Rect->h);
                }break;
                case NK_COMMAND_RECT_FILLED:
                {
                    nk_command_rect_filled *FilledRect = (nk_command_rect_filled*)NKCommand;
                    PushImage(Renderer, 0);
                    PushColour(Renderer, FilledRect->color.r, FilledRect->color.g,FilledRect->color.b,FilledRect->color.a);
                    PushRect(Renderer, FilledRect->x, FilledRect->y, FilledRect->w, FilledRect->h);
                }break;
                case NK_COMMAND_SCISSOR:
                {
                    nk_command_scissor *ClipRect = (nk_command_scissor*)NKCommand;
                    PushImage(Renderer, 0);
                    PushClipRect(Renderer, ClipRect->x, ClipRect->y,ClipRect->w,ClipRect->h);
                }break;
                case NK_COMMAND_TRIANGLE_FILLED:
                {
                    nk_command_triangle_filled *FilledTriangle = (nk_command_triangle_filled*)NKCommand;
                    PushImage(Renderer, 0);
                    PushColour(Renderer, FilledTriangle->color.r, FilledTriangle->color.g,FilledTriangle->color.b,FilledTriangle->color.a);
                    PushRect(Renderer, FilledTriangle->a.x, FilledTriangle->a.y, 4,4);
                    PushRect(Renderer, FilledTriangle->b.x, FilledTriangle->b.y, 4,4);
                    PushRect(Renderer, FilledTriangle->c.x, FilledTriangle->c.y, 4,4);
                }break;
                case NK_COMMAND_TEXT:
                {
                    nk_command_text *Text = (nk_command_text*)NKCommand;
                    PushColour(Renderer, Text->foreground.r, Text->foreground.g,Text->foreground.b,Text->foreground.a);
                    PushText(Renderer, Text->string, Text->x, Text->y, Text->height,(fkb_font*)Text->font->userdata.ptr);
                }break;
                case NK_COMMAND_IMAGE:
                {
                    nk_command_image *Img = (nk_command_image*)NKCommand;
                    PushColour(Renderer, Img->col.r, Img->col.g,Img->col.b,Img->col.a);
                    PushImage(Renderer, (fkb_texture*)Img->img.handle.ptr);
                    if(nk_image_is_subimage((const struct nk_image*)&Img->img))
                    {
                        PushRectUV(Renderer, Img->x, Img->y, Img->w,Img->h,Img->img.region[0],Img->img.region[1],Img->img.region[2],Img->img.region[3]);
                    }
                    else
                    {
                        PushRect(Renderer, Img->x, Img->y, Img->w,Img->h);
                    }
                }break;
                case NK_COMMAND_CIRCLE_FILLED:
                {
                    nk_command_circle_filled *FilledCircle = (nk_command_circle_filled*)NKCommand;
                    PushImage(Renderer, 0);
                    PushColour(Renderer, FilledCircle->color.r, FilledCircle->color.g,FilledCircle->color.b,FilledCircle->color.a);
                    PushRect(Renderer, FilledCircle->x, FilledCircle->y, FilledCircle->w,FilledCircle->h);
                }break;
                case NK_COMMAND_LINE:
                {
                    nk_command_line *Line = (nk_command_line*)NKCommand;
                    PushImage(Renderer, 0);
                    PushColour(Renderer, Line->color.r, Line->color.g,Line->color.b,Line->color.a);
                    PushLine(Renderer, Line->begin.x, Line->begin.y, Line->end.x, Line->end.y);
                }break;
                InvalidDefaultCase;
            }
        }
    }
}