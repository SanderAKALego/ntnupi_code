///##################################################################################
/// shared.cpp
/// 
/// This file contains all the #includes shared between the host (main exe file that
/// loads the DLL if hot reloading is enabled) and DLL.
/// 
/// 
/// 
///##################################################################################
#ifndef SHARED_CPP
#define STB_IMAGE_IMPLEMENTATION
#define STB_SPRINTF_IMPLEMENTATION
#define STB_RECT_PACK_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#define STBTT_STATIC
#define NK_IMPLEMENTATION
#define NK_PRIVATE
#define NK_INCLUDE_DEFAULT_ALLOCATOR

#define NK_ASSERT(expr) if(!(expr)) { *(int *)0 =0; }

//-----Shared between Exe and DLL ------

// NOTE(Sanderkp): Disable warnings  for libraries
#pragma warning(push, 0)
#include "string.h"
#include "SDL.h"
#include "SDL_Net.h"
#include "nuklear.h"
#include "stb_image.h"
#include "stb_sprintf.h"
#include "stb_rect_pack.h"
#include "stb_truetype.h"
#pragma warning(pop)

#pragma warning(push, 4)
//
// NOTE(Sanderkp): Disable some warnings that or not 100% needed, comment out the lines to re-enable them
//
#pragma warning(disable : 4820) // NOTE(Sanderkp): Padding warnings.
#pragma warning(disable : 4505) // NOTE(Sanderkp): Disabled warnings about function not being used

#include "fkb_platform.h"
#include "fkb_debug.h"
#include "fkb_renderer.h"
#include "fkb_net.h"
#include "fkb_fingerprint_key_box.h"
#pragma warning(pop)
#define SHARED_CPP
#endif