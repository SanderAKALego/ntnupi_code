///##################################################################################
/// dll.cpp
/// 
/// Contains all the #includes for the DLL
/// 
/// 
///##################################################################################

#include <tiny_aes/aes.c>
#include <duthomhas/csprng.h>
#include <stdlib.h>
#include "srp.h"
#include "srp.c"
#include "bch.cpp"
#include "fkb_secret_releaser.cpp"
#include "fkb_verifinger.cpp"
#include "fkb_rpc.cpp"
#include "fkb_gui.cpp"
#include "fkb_fingerprint_key_box.cpp"

#if HOT_RELOADER_ENABLED
#include "csprng.c"
#endif
