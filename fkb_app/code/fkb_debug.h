///##################################################################################
/// fkb_debug.h
/// 
/// This file was supposed to contain a debug system but due to time constrains
/// we never got to it. So there is only a small overload for printf here that
/// prints out the file and line number.
/// 
/// 
///##################################################################################

#if defined FKB_DEBUG
#define DebugLog(fmt,...) printf("%s:%d|", __FILE__,__LINE__); printf(fmt, __VA_ARGS__);printf("\n");
#else
#define DebugLog(fmt,...) 
#endif