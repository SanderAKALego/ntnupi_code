///##################################################################################
/// fkb_hotreload.cpp
/// 
/// This file contains the implementation of the hot reloader. The hot reloader is
/// supposed monitor the build directory for changes and reload the DLL if a change
/// occured. (Such as when recompiling). Sadly the hot reloader no longer works
/// because nuklear is dependant on data stored on the stack (which gets nuked when
/// recompiling), as such hot reloading is disabled until a fix for this is found.
/// 
/// 
///##################################################################################

//
// NOTE(Sanderkp): Handle to dll file
//
global void *HotreloaderDllHandle;

///##################################################################################
/// Function:
/// void UnloadDLL(void)
///
/// About:
/// Unloads the application DLL.
/// 
/// Parameters:
/// None
///
/// Return value:
/// None
///
/// Note:
/// Not used if hot reloading is disabled
/// 
///##################################################################################
internal void
UnloadDLL()
{
    if(HotreloaderDllHandle)
    {
        DllUpdateAndRender = 0;
        SDL_UnloadObject(HotreloaderDllHandle);
        HotreloaderDllHandle = 0;
        DebugLog("DLL unloaded");
    }
    else
    {
        DebugLog("ERROR: Tried to unload invalid DLL");
    }
}

///##################################################################################
/// Function:
/// void LoadDLL(char *File)
///
/// About:
/// Tries to load the dll file specified.
/// 
/// Parameters:
/// File - Location of dll file
///
/// Return value:
/// None
///
/// Note:
/// Not used if hot reloading is disabled
/// 
///##################################################################################
internal void
LoadDLL(char *File)
{
    HotreloaderDllHandle = SDL_LoadObject(File);
    if(!HotreloaderDllHandle)
    {
        DebugLog("%s", SDL_GetError());
        return;
    }
    DllUpdateAndRender = (update_and_render*)SDL_LoadFunction(HotreloaderDllHandle, "UpdateAndRender");
    if(DllUpdateAndRender)
    {
        SDL_Event Event = {0};
        Event.type = ForceUpdateEvent;
        SDL_PushEvent(&Event);
        DebugLog("DLL loaded");
    }
}

//
// NOTE(Sanderkp): Hotreloading is only supported on windows for now because we can monitor directories
//                 One could enable hot reloading on other platforms as well by emulating this.
//
#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

//
// NOTE(Sanderkp): Temp error buffer used by Win32_GetError
//
global char _tempErrorBuffer[4096];

#pragma warning(push)

#pragma warning(disable : 4505) // NOTE(Sanderkp): Disabled warning about function not being used

internal char*
Win32_GetError()
{
    DWORD LastError = GetLastError();
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,0,LastError,0,_tempErrorBuffer,ArrayCount(_tempErrorBuffer),0);
    return(_tempErrorBuffer);
}

#pragma warning(pop)

///##################################################################################
/// Function:
/// DWORD HotreloadFunc(void *lpParameter)
///
/// About:
/// Thread that runs and monitors the directory of the application. If the dll changes this function will try and reload the dll.
/// 
/// Parameters:
/// lpParameter - Directory to look for changes in
///
/// Return value:
/// Always zero
///
/// Note:
/// Not used if hot reloading is disabled
/// 
///##################################################################################
internal DWORD
HotreloadFunc(void *lpParameter)
{
    //
    // NOTE(Sanderkp): Set up paths and file names
    //
    char DllDir[1024];
    char TempDllDir[1024];
    char *Directory = (char*)lpParameter;
    char *DllName = "fkb_app_demo.dll";
    strcpy_s(DllDir, sizeof(DllDir), Directory);
    if(strlen(Directory))
    {
        strcat_s(DllDir, sizeof(DllDir), "\\");
    }
    strcat_s(DllDir, sizeof(DllDir), DllName);
    strcpy_s(TempDllDir, sizeof(DllDir), DllDir);
    strcat_s(TempDllDir, sizeof(DllDir), "_temp");
    if(CopyFile(DllDir,TempDllDir,FALSE))
    {
        LoadDLL(TempDllDir);
    }
    else
    {
        DebugLog("%s", Win32_GetError());
    }
    //
    // NOTE(Sanderkp): Start monitoring directory
    //
    HANDLE ChangeHandle = FindFirstChangeNotification(Directory, FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE);
    if(ChangeHandle != INVALID_HANDLE_VALUE)
    {
        DebugLog("Hotreloader initialized");
        FILETIME LastDllWriteTime = {0};
        //
        // NOTE(Sanderkp): Look for changes
        //
        while(true)
        {
            //
            // NOTE(Sanderkp): Block until a change is detected
            //
            if(WaitForSingleObject(ChangeHandle, INFINITE) == WAIT_OBJECT_0)
            {
                //
                // NOTE(Sanderkp): Get a handle to the DLL file
                //
                HANDLE DllFileHandle = CreateFile(DllDir, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
                if(DllFileHandle != INVALID_HANDLE_VALUE)
                {
                    FILETIME DllLastWriteTime;
                    if(GetFileTime(DllFileHandle,0,0, &DllLastWriteTime))
                    {
                        //
                        // NOTE(Sanderkp): Compare the file times to see if the DLL has been written to lately
                        //                 Following recommandations according to:
                        //                 https://msdn.microsoft.com/en-us/library/windows/desktop/ms724284(v=vs.85).aspx
                        //
                        ULARGE_INTEGER Last = {0};
                        ULARGE_INTEGER Recent = {0};
                        Last.LowPart = LastDllWriteTime.dwLowDateTime;
                        Last.HighPart = LastDllWriteTime.dwHighDateTime;
                        Recent.LowPart = DllLastWriteTime.dwLowDateTime;
                        Recent.HighPart = DllLastWriteTime.dwHighDateTime;
                        if(Recent.QuadPart > Last.QuadPart)
                        {
                            //
                            // NOTE(Sanderkp): The dll has changed so we unload the dll we have, we then make a copy of it
                            //                 called fkb_demo_app.dll_temp and then we load the temp DLL. This way we can
                            //                 still recompile the DLL without having it be locked.
                            //
                            UnloadDLL();
                            if(CopyFile(DllDir,TempDllDir,FALSE))
                            {
                                LoadDLL(TempDllDir);
                            }
                            else
                            {
                                DebugLog("%s", Win32_GetError());
                            }
                            LastDllWriteTime = DllLastWriteTime;
                        }
                        else
                        {
                            // NOTE(Sanderkp): Did nit change
                        }
                    }
                    else
                    {
                        DebugLog("%s", Win32_GetError());
                    }
                    CloseHandle(DllFileHandle);
                }
                else
                {
                    DebugLog("%s", Win32_GetError());
                }
                if(FindNextChangeNotification(ChangeHandle) == FALSE)
                {
                    DebugLog("%s", Win32_GetError());
                }
            }
            else
            {
                DebugLog("%s", Win32_GetError());
                break;
            }
        }
    }
    else
    {
        DebugLog("%s", Win32_GetError());
    }
    return(0);
}

///##################################################################################
/// Function:
/// void InitializeHotreloader(char *Directory)
///
/// About:
/// Spins up a thread which runs the hot reloader
/// 
/// Parameters:
/// Directory - Directory to monitor
///
/// Return value:
/// None
///
/// Note:
/// Disabled if hot reloading is not used
/// 
///##################################################################################
internal void
InitializeHotreloader(char *Directory)
{
    if(!CreateThread(0,0,HotreloadFunc,Directory,0,0))
    {
        DebugLog("%s", Win32_GetError());
    }
}

#else

//
// NOTE(Sanderkp): Stub for other platform, implement it however you want
//
internal void
InitializeHotreloader(char *Directory){}

#endif
