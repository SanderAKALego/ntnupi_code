///##################################################################################
/// exe.cpp
/// 
/// This file contains all the neccessary #includes for the exe/host
/// 
/// 
///##################################################################################

#include "fkb_main.h"
#include "SDL_net.h"
#include "fkb_file_handling.cpp"
#include "fkb_renderer.cpp"
#include "fkb_work_queue.cpp"
#if HOT_RELOADER_ENABLED
#include "fkb_hotreload.cpp"
#endif
#include "fkb_main.cpp"

#include "csprng.c"