///##################################################################################
/// fkb_platform.h
/// 
/// This file defines the platform layer, every subsequent port must adhere to this.
/// In here all the types we use are typedefed and most of the macros we use.
/// What you need to get used to:
///    -We use mostly u32/s32 instead of int, there is no specific reason for this
///     unless specified, it's mostly jsut a habit.
///    -b32 same as bool but 32 bits, we use this (in conjunction with b8, b16) to
///     know exactly how big a bool is.
///    -We use internal on every function, internal is defined as static and the
///     reason we do this is because 'static' means multiple things in C/C++ and
///     that makes it hard to search for. When static is defined as internal,
///     global or local it makes it a lot easier to search for. 
///    -Why static functions? Because it speeds up compile time a little bit.
///    -s32x, u32x, b32x are types where 'I don't care what size it is', again
///     this is very similiar to regular u32/s32/b32, we don't care what size it is
///     but we try to keep everything 32 bits either way just for consistency
/// 
///##################################################################################

#ifndef FKB_PLATFORM_H
#include <stdint.h>

///##################################################################################
/// NOTE(Sanderkp): Many of these macros are inspired/ripped from Handmade Hero (Handmade Hero licence permits this)
/// See https://handmadehero.org/
///
/// and https://github.com/HandmadeHero/cpp (NOTE: Requires preordering the game)
///
///##################################################################################

//
// IMPORTANT(Sanderkp): All strings are ASCII and null-terminated!
// IMPORTANT(Sanderkp): All strings are ASCII and null-terminated!
// IMPORTANT(Sanderkp): All strings are ASCII and null-terminated!
// IMPORTANT(Sanderkp): All strings are ASCII and null-terminated!
//

//
// NOTE(Sanderkp): Keyword 'static' is redefined multiple times because it means different things in C/C++
//
#define internal static   // NOTE(Sanderkp): Internal means local to the current C++ translation unit
#define global static     // NOTE(Sanderkp): Global means a global variable which is cleared to zero at initialization time
#define local static      // NOTE(Sanderkp): Local is used for local persistent values
#define exported          // NOTE(Sanderkp): Exported means the function or variable is accessable across translation units

#ifndef __cplusplus
#define true     1
#define false    0
#endif

///##################################################################################
/// Macro:
/// ArrayCount(Array)
///
/// About:
/// Used to count the number of elements in an array.
/// 
/// Parameters:
/// An array to get the count for,
///
/// Return value:
/// The number of elements in the array.
///
/// Note:
/// The array must have fixed size (IE NOT a dynamic array).
/// 
///##################################################################################
#define ArrayCount(Array) (sizeof(Array) / sizeof(Array[0]))

///##################################################################################
/// Macro:
/// ArrayIndex(Element, ArrayStart)
///
/// About:
/// Used to get the index of an element in an array.
/// 
/// Parameters:
/// Element - Pointer to the element
/// ArrayStart - Pointer to the beginning of the array
///
/// Return value:
/// Index in the array. No attempt is made at checking if it falls within the bounds of the array.
///
/// Note:
/// The array must be fixed (same as ArrayCount). Its up the the user of this macro to make sure the element is inside array.
/// 
///##################################################################################
#define ArrayIndex(Element, ArrayStart) ((Element - ArrayStart))

//
// NOTE(Sanderkp): Clear macros
//
#define ZeroMem(Base, Size) (memset(Base, 0, Size))
#define ZeroStruct(Struct) (memset(&Struct, 0, sizeof(Struct)))       // NOTE(Sanderkp): You must pass the struct as a value (IE NOT a pointer)
#define ZeroPointer(Pointer) (memset(Pointer, 0, sizeof(*Pointer)))   // NOTE(Sanderkp): Clear a struct by passing the pointer to it

//
// NOTE(Sanderkp): Various useful macros
//
#define Min(A, B) (((A) < (B)) ? (A) : (B))
#define Max(A, B) (((A) > (B)) ? (A) : (B))
#define Clamp(x,xmin,xmax)  ((x) < (xmin) ? (xmin) : (x) > (xmax) ? (xmax) : (x))
#define Repeat(val, min, max) (((val) >= 0) ? ((val) % max + min) : (max - Abs((val)) % max))
#define Abs(Value) ((Value < 0) ? -(Value) : (Value))
#define Square(Value) ((Value)*(Value))

//
// NOTE(Sanderkp): Useful macros for allocating fixed sizes
//
#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)


//
// NOTE(Sanderkp): Compile with FKB_DEBUG defined to enable assertions, these should be disabled for release builds
//
#if defined FKB_DEBUG
#define Assert(Expression) if(!(Expression)) { *(int *)0 =0; }
#else
#define Assert(e)
#endif

//
// NOTE(Sanderkp): Additional assertions
//
#define InvalidDefaultCase default: Assert(0)
#define NotImplemented Assert(!"Not Implemented!")

//
// NOTE(Sanderkp): Useful math macros
//
#define Pi32 3.14159265358979323846f
#define Epsilon32 1.19209290e-7f
#define Tau32 2*Pi32
#define DegToRad(Angle) ((Angle)*(Pi32/180.0f))
#define RadToDeg(Angle) ((Angle)*(180.0f/Pi32))

//
// NOTE(Sanderkp): Compare two floating point values with epsilon
//
#define Approximate(Value, CompareValue) ((CompareValue) >= (Value) - Epsilon32 && (CompareValue) <= (Value) + Epsilon32)

//
// NOTE(Sanderkp): Get the offset of a member in a struct
//
#define OffsetOf(type, member) (umm)&(((type*)0)->member)

//
// NOTE(Sanderkp): Returns 1 or -1 depending on sign
//
#define SignOf(value) (((value) < 0) ? -1 : 1)

//
// NOTE(Sanderkp): Alignment macros, useful for aligning memory
//
#define AlignPow2(Value, Alignment) ((Value + ((Alignment) - 1)) & ~((Alignment) - 1))
#define Align4(Value) ((Value + 3) & ~3)
#define Align8(Value) ((Value + 7) & ~7)
#define Align16(Value) ((Value + 15) & ~15)

//
// NOTE(Sanderkp): Define types
//
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef int8_t   b8;
typedef int16_t  b16;
typedef int32_t  b32;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float    r32;
typedef double   r64;

//
// NOTE(Sanderkp): X-variant signify that the type must be ATLEAST ZZ big but it can also be larger
//
typedef b32 b32x;
typedef u32 u32x;
typedef s32 s32x;

//
// NOTE(Sanderkp): Used when dealing with memory sizes. 
//                 These are as big as the memory addresses on the given platform (8-bytes for 64 bit, 4-bytes for 32 bit etc)
//
typedef size_t memory_index;
typedef uintptr_t umm;
typedef intptr_t smm;

//
// NOTE(Sanderkp): Predefining some structures
//
typedef struct renderer *Renderer;
typedef struct program_state *State;
typedef struct work_queue *WorkQueue;

typedef struct fkb_read_file
{
    umm Size;
    void *Content;
}fkb_read_file;
///##################################################################################
///                              FAQ
///
///Q:What's going on with the #defines and the typedefs here?
///A:To retain type safety when assigned pointers we have to typedef the functions, however typedefing function pointers
///   in C/C++ is rather painful so we use the #defines to make life easier.
///
///   Example of what's happening when the macro is expanded:
///
///    Before expansion:
///        #define SAMPLE_FUNCTION(name) int name(char *Sample)
///        typedef SAMPLE_FUNCTION(sample_function);
///
///    After:
///        typedef int sample_function(char *Sample);
///
///    We can now store a pointer of sample_function with:
///        sample_function *SampleFunction;
///
///##################################################################################

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(name) void name(void *Data)
///
/// About:
/// Work to be done on another thread. It is up to the caller to provide a valid data pointer and ensure thread safety.
/// 
/// Parameters:
/// Data - Pointer to a user defined structure, can be null.
///
/// Return value:
/// None
///
/// Note:
/// Define this macro with a given Name to create a function suitable for adding to the work queue
/// 
///##################################################################################
#define WORK_QUEUE_FUNC(name) void name(void *Data)
typedef WORK_QUEUE_FUNC(work_queue_func);

//
// NOTE(Sanderkp): Shorthand for allocating a struct
//
#define AllocStruct(type) (type*)Platform.Allocate(sizeof(type))
//
// NOTE(Sanderkp): Shorthand for allocating an array
//
#define AllocArray(type, count) (type*)Platform.Allocate(sizeof(type)*count)

///##################################################################################
/// Function:
/// PLATFORM_ALLOCATE(name) void *name(umm Size)
///
/// About:
/// Allocates a block of memory with the given size.
/// 
/// Parameters:
/// Size - The amount to allocate, passing zero size returns NULL
///
/// Return value:
/// An allocated block atleast as big as Size, NULL if fail or if Size is zero
///
/// Note:
/// Memory allocated should be freed by callign Free(). 
/// 
///##################################################################################
#define PLATFORM_ALLOCATE(name) void *name(umm Size)
typedef PLATFORM_ALLOCATE(platform_allocate);

///##################################################################################
/// Function:
/// PLATFORM_FREE(name) void name(void *Memory)
///
/// About:
/// Frees a block allocated by PLATFORM_ALLOCATE
/// 
/// Parameters:
/// Memory - Pointer to the located block, can be NULL
///
/// Return value:
/// None
///
/// Note:
/// Only free memory allocated using PLATFORM_ALLOCATE
/// 
///##################################################################################
#define PLATFORM_FREE(name) void name(void *Memory)
typedef PLATFORM_FREE(platform_free);

///##################################################################################
/// Function:
/// PLATFORM_FREE_ENTIRE_FILE(name) void name(fkb_read_file File)
///
/// About:
/// Allocates a buffer large enough to hold the file and reads the entire content
/// into the buffer. Not suited for text files since it's not null terminated.
/// 
/// Parameters:
/// A null-terminated ASCII string representing the filepath to the file.
///
/// Return value:
/// Returns a fkb_file_read structure containing the buffer and size, see fkb_platform.h
///
/// Note:
/// Use FreeEntireFile(...) to release memory allocated by the buffer
/// 
///##################################################################################
#define PLATFORM_READ_ENTIRE_FILE(name) fkb_read_file name(char *Path)
typedef PLATFORM_READ_ENTIRE_FILE(platform_read_entire_file);


///##################################################################################
/// Function:
/// PLATFORM_FREE_ENTIRE_FILE(name) void name(fkb_read_file File)
///
/// About:
/// Releases memory allocated by ReadEntireFile. 
/// 
/// Parameters:
/// A fkb_file_read structure (see fkb_platform.h), the content may be null but invalid pointers will crash.
///
/// Return value:
/// None
///
/// Note:
/// Use this function to free memory allocated by ReadEntireFile, do NOT free the memory manually.
/// 
///##################################################################################
#define PLATFORM_FREE_ENTIRE_FILE(name) void name(fkb_read_file File)
typedef PLATFORM_FREE_ENTIRE_FILE(platform_free_entire_file);

///##################################################################################
/// Function:
/// PLATFORM_ADD_WORK(name) void name(work_queue *WorkQueue, work_queue_func *Func, void *Data)
///
/// About:
/// Adds work to the work queue.
/// 
/// Parameters:
/// WorkQueue - A WorkQueue created by calling CreateWorkQueue, can NOT be NULL
/// Func - Pointer to a WORK_QUEUE_FUNC function to run, can NOT be NULL
/// Data - Data passed to Func, can be NULL
///
/// Return value:
/// None
///
/// Note:
/// There is currently no way to wait for a given piece of work to finish.
/// For more info see fkb_work_queue.cpp
/// 
///##################################################################################
#define PLATFORM_ADD_WORK(name) void name(work_queue *WorkQueue, work_queue_func *Func, void *Data)
typedef PLATFORM_ADD_WORK(platform_add_work);

///##################################################################################
/// Function:
/// PLATFORM_COMPLETE_ALL_WORK(name) void name(work_queue *WorkQueue)
///
/// About:
/// Blocks the thread until all work is completed.
/// 
/// Parameters:
/// WorkQueue - A valid WorkQueue created using CreateWorkQueue
///
/// Return value:
/// None
///
/// Note:
/// This blocks the calling thread until everything is finished, should be used with care.
/// For more info see fkb_work_queue.cpp
/// 
///##################################################################################
#define PLATFORM_COMPLETE_ALL_WORK(name) void name(work_queue *WorkQueue)
typedef PLATFORM_COMPLETE_ALL_WORK(platform_complete_all_work);

///##################################################################################
/// Function:
/// PLATFORM_UPDATE_SCREEN(name) void name(void)
///
/// About:
/// Forces the update loop to run once.
/// 
/// Parameters:
/// None
///
/// Return value:
/// None
///
/// Note:
/// This is thread safe.
/// This adds an event to the application's event queue and thus forces the loop to run once.
/// Useful for animating or triggering things with timers.
/// 
///##################################################################################
#define PLATFORM_UPDATE_SCREEN(name) void name(void)
typedef PLATFORM_UPDATE_SCREEN(platform_update_screen);

///##################################################################################
/// Function:
/// PLATFORM_APP_EXIT(name) void name(program_state *State)
///
/// About:
/// Runs when the app is exiting, this function should not take a long time to run
/// 
/// Parameters:
/// State - Pointer to the program state
///
/// Return value:
/// None
///
/// Note:
/// Used for closing sockets and other cleanup routines.
/// 
///##################################################################################
#define PLATFORM_APP_EXIT(name) void name(program_state *State)
typedef PLATFORM_APP_EXIT(platform_app_exit);

///##################################################################################
/// Function:
/// PLATFORM_OPEN_FILE_DIALOG(name) b32 name(char *File, u32 MaxFile, char *Extensions)
///
/// About:
/// Opens a platform specific file dialog
/// 
/// Parameters:
/// File - An array big enough to fit the resulting file path
/// MaxFile - The maximum file path, this should be the size of File
/// Extensions - PLATFORM SPECIFIC string describing which extensions to handle
///
/// Return value:
/// True if the user pressed OK
/// False if user aborted
///
/// Note:
/// WARNING the extensions parameter is PLATFORM SPECIFIC at the moment. If you are using this
/// on another platform you need to wrap the calling code in #ifdef. This was done due to time restrictions.
/// 
///##################################################################################
#define PLATFORM_OPEN_FILE_DIALOG(name) b32 name(char *File, u32 MaxFile, char *Extensions)
typedef PLATFORM_OPEN_FILE_DIALOG(platform_open_file_dialog);

///##################################################################################
/// Function:
/// PLATFORM_SIZE_OF_FILE(name) s64 name(char *Path)
///
/// About:
/// Get the size of a file.
/// 
/// Parameters:
/// Path - Path to the file, can be relative or absolute, can NOT be NULL
///
/// Return value:
/// Positive (> 0) if success, Negative (< 0) if fail
/// 
///##################################################################################
#define PLATFORM_SIZE_OF_FILE(name) s64 name(char *Path)
typedef PLATFORM_SIZE_OF_FILE(platform_size_of_file);

///##################################################################################
/// Function:
/// PLATFORM_RUN_AND_WAIT_FOR_PROCESS(name) b32 name(char *Executable, char *Arguments)
///
/// About:
/// Creates and waits for a process.
/// 
/// Parameters:
/// Executable - Path to executable
/// Arguments - String containing arguments
///
/// Return value:
/// True if the process was created successfully and ran, false if error.
///
/// Note:
/// This creates an instance of the process located at Executable and blocks the calling thread until done.
/// 
///##################################################################################
#define PLATFORM_RUN_AND_WAIT_FOR_PROCESS(name) b32 name(char *Executable, char *Arguments)
typedef PLATFORM_RUN_AND_WAIT_FOR_PROCESS(platform_run_and_wait_for_process);

typedef void file_handle;

//
// NOTE(Sanderkp): File creation mode
//
enum PlatformCreateFileMode
{
    //
    // NOTE(Sanderkp): This ALWAYS creates a new file, erases the original file if exists
    //
    FileMode_Create,
    //
    // NOTE(Sanderkp): This ALWAYS opens a file, the file must exist
    //
    FileMode_Open,
};

///##################################################################################
/// Function:
/// PLATFORM_OPEN_FILE(name) file_handle *name(char *FilePath, u32 CreationMode)
///
/// About:
/// Opens a file for reading and writing. 
/// 
/// Parameters:
/// FilePath - Path to file, must be valid (IE NOT NULL)
/// CreationMode - See enum PlatformCreateFileMode
///
/// Return value:
/// A pointer to a valid file_handle structure if successful or NULL if fail.
///
/// Note:
/// Close the file_handle using PLATFORM_CLOSE_FILE
/// Read from the file using PLATFORM_READ
/// Write using PLATFORM_WRITE
/// 
///##################################################################################
#define PLATFORM_OPEN_FILE(name) file_handle *name(char *FilePath, u32 CreationMode)
typedef PLATFORM_OPEN_FILE(platform_open_file);

///##################################################################################
/// Function:
/// PLATFORM_OPEN_FILE(name) file_handle *name(char *FilePath, u32 CreationMode)
///
/// About:
/// Opens a file for reading and writing. 
/// 
/// Parameters:
/// FilePath - Path to file, must be valid (IE NOT NULL)
/// CreationMode - See enum PlatformCreateFileMode
///
/// Return value:
/// A pointer to a valid file_handle structure if successful or NULL if fail.
///
/// Note:
/// Close the filhandle using PLATFORM_CLOSE_FILE, these handles are not thread safe
/// 
///##################################################################################
#define PLATFORM_CLOSE_FILE(name) void name(file_handle *FileHandle)
typedef PLATFORM_CLOSE_FILE(platform_close_file);

///##################################################################################
/// Function:
/// PLATFORM_WRITE(name) umm name(void *Buffer, umm Size, file_handle *FileHandle)
///
/// About:
/// Write to a file opened with PLATFORM_OPEN_FILE
/// 
/// Parameters:
/// Buffer - Buffer to write to the file
/// Size - Size of buffer
/// file_handle - A valid file_handle obtained from PLATFORM_OPEN_FILE, this must be valid.
///
/// Return value:
/// Size if success, a number less than Size if fail.
///
/// Note:
/// This advances the file pointer used for reading as well.
/// 
///##################################################################################
#define PLATFORM_WRITE(name) umm name(void *Buffer, umm Size, file_handle *FileHandle)
typedef PLATFORM_WRITE(platform_write);

///##################################################################################
/// Function:
/// PLATFORM_READ(name) umm name(void *Buffer, umm Size, file_handle *FileHandle)
///
/// About:
/// Reads from a file opened with PLATFORM_OPEN_FILE.
/// 
/// Parameters:
/// Buffer - Destination buffer
/// Size - Size of Destination buffer
/// FileHandle - A valid file_handle from PLATFORM_OPEN_FILE, this must be valid.
///
/// Return value:
/// Number of objects read, or zero if end of file or fail.
///
/// Note:
/// This advances the file pointer used for writing as well.
/// 
///##################################################################################
#define PLATFORM_READ(name) umm name(void *Buffer, umm Size, file_handle *FileHandle)
typedef PLATFORM_READ(platform_read);

///##################################################################################
/// Function:
/// PLATFORM_GET_CURRENT_FILE_POSITION(name) smm name(file_handle *FileHandle)
///
/// About:
/// Gets the current position of the file pointer (read and write).
/// 
/// Parameters:
/// FileHandle - A valid file_handle from PLATFORM_OPEN_FILE
///
/// Return value:
/// Current offset in the file or -1 if fail.
///
/// Note:
/// Use PLATFORM_SET_CURRENT_FILE_POSITION to set the offset.
/// 
///##################################################################################
#define PLATFORM_GET_CURRENT_FILE_POSITION(name) smm name(file_handle *FileHandle)
typedef PLATFORM_GET_CURRENT_FILE_POSITION(platform_get_current_file_position);

//
// NOTE(Sanderkp): Relative positions for PLATFORM_SET_CURRENT_FILE_POSITION
//
enum PlatformSetFilePositionEnum
{
    FilePosition_RelativeToStart,
    FilePosition_RelativeToCurrent,
    FilePosition_RelativeToEnd,
};

///##################################################################################
/// Function:
/// PLATFORM_SET_CURRENT_FILE_POSITION(name) smm name(file_handle *FileHandle, smm Position, u32 RelativeTo)
///
/// About:
/// Sets the current offset into the file.
/// 
/// Parameters:
/// FileHandle - A valid file_handle from PLATFORM_OPEN_FILE
/// Position - Offset from relative position, can be negative
/// RelativeTo - See enum PlatformSetFilePositionEnum
///
/// Return value:
/// The new offset into the file, or -1 if fail.
///
/// Note:
/// Use PLATFORM_GET_CURRENT_FILE_POSITION to get the offset, or use the return value
/// 
///##################################################################################
#define PLATFORM_SET_CURRENT_FILE_POSITION(name) smm name(file_handle *FileHandle, smm Position, u32 RelativeTo)
typedef PLATFORM_SET_CURRENT_FILE_POSITION(platform_set_current_file_position);

///##################################################################################
/// Function:
/// PLATFORM_COPY_FILE_TO(name) b32 name(char *From, char *To)
///
/// About:
/// Copies a file on the local filesystem. 
/// 
/// Parameters:
/// From - Filepath to copy from
/// To - New filepath to copy to
///
/// Return value:
/// True if success, false if fail
///
/// Note:
/// The filepaths must include the filenames, supports relative and absolute filepaths
/// 
///##################################################################################
#define PLATFORM_COPY_FILE_TO(name) b32 name(char *From, char *To)
typedef PLATFORM_COPY_FILE_TO(platform_copy_file_to);

///##################################################################################
/// Function:
/// PLATFORM_DELETE_FILE_AT(name) b32 name(char *FilePath)
///
/// About:
/// Deletes the file located the specified file path.
/// 
/// Parameters:
/// FilePath - A null-terminated ASCII string representing the path to the file.
///
/// Return value:
/// True if deletion was successful, false if error.
/// 
///##################################################################################
#define PLATFORM_DELETE_FILE_AT(name) b32 name(char *FilePath)
typedef PLATFORM_DELETE_FILE_AT(platform_delete_file_at);

//
// NOTE(Sanderkp): This defines the Platform API, all the function pointers must be set by the host at initialization time.
//                 These must be valid the first time the DLL is called.
//
typedef struct platform
{
    // NOTE(Sanderkp): Allocation
    platform_allocate *Allocate;
    platform_free *Free;
    
    // NOTE(Sanderkp): File API
    platform_read_entire_file *ReadEntireFile;
    platform_free_entire_file *FreeEntireFile;
    platform_size_of_file *SizeOfFile;
    platform_open_file *OpenFile;
    platform_close_file *CloseFile;
    platform_write *Write;
    platform_write *Read;
    platform_get_current_file_position *GetCurrentFilePosition;
    platform_set_current_file_position *SetCurrentFilePosition;
    platform_copy_file_to *CopyFileTo;
    platform_open_file_dialog *OpenFileDialog;
    platform_delete_file_at *DeleteFileAt;
    
    // NOTE(Sanderkp): Work queue
    platform_add_work *AddWork;
    platform_complete_all_work *CompleteAllWork;
    
    // NOTE(Sanderkp): Misc Platform 
    platform_update_screen *UpdateScreen;
    platform_run_and_wait_for_process *RunAndWaitForProcess;
    
    // NOTE(Sanderkp): This is set by the upper layer (DLL) and is called by the host when the program exits
    platform_app_exit *AppExit;
}platform;

//
// NOTE(Sanderkp): Global variable used by the DLL, this must be set every frame to be valid
//
global platform Platform;

///##################################################################################
/// Function:
/// UPDATE_AND_RENDER(name) void name(program_state *State, platform *PlatformAPI, renderer *Renderer, nk_context *Context)
///
/// About:
/// This is the main entry point into the DLL.
/// 
/// Parameters:
/// State - Pointer to a program_state structure, must be valid (IE NOT NULL)
/// PlatformAPI - Pointer to a platform structure, must be valid (IE NOT NULL)
/// Renderer - Pointer to a renderer structure, must be valid (IE NOT NULL)
/// Context - Pointer to a nk_context structure, must be valid (IE NOT NULL)
///
/// Return value:
/// None
///
/// Note:
/// This function must be exported by the DLL and loaded by the host.
/// Skipped when compiling the standalone secret relaser
///##################################################################################
#ifndef SECRET_RELEASER_STANDALONE
#define UPDATE_AND_RENDER(name) void name(program_state *State, platform *PlatformAPI, renderer *Renderer, nk_context *Context)
typedef UPDATE_AND_RENDER(update_and_render);
//
// NOTE(Sanderkp): Global variable used by the host.
//
global update_and_render *DllUpdateAndRender;
#endif

#define FKB_PLATFORM_H
#endif

