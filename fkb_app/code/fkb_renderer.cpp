///##################################################################################
/// fkb_renderer.cpp
/// 
/// This contains the actual implementation of the renderer, meaning it runs the
/// render commands, uploading textures to the GPU, Pushing quads and so on.
/// The renderer builds on top of the SDL renderer.
/// 
/// 
///##################################################################################

///##################################################################################
/// Function:
/// void RunRenderCommands(renderer *Renderer)
///
/// About:
/// Main render loop
/// 
/// Parameters:
/// Renderer - A valid pointer to a renderer struct
///
/// Return value:
/// None
///
/// 
///##################################################################################
internal void
RunRenderCommands(renderer *Renderer)
{
    Assert(Renderer->SDL);
    {
        //
        // NOTE(Sanderkp): Reset Clip rect
        //
        SDL_Rect Rect = {0};
        Rect.x = 0;
        Rect.y = 0;
        Rect.w = Renderer->Width;
        Rect.h = Renderer->Height;
        SDL_RenderSetClipRect(Renderer->SDL, &Rect);
    }
    SDL_SetRenderDrawBlendMode(Renderer->SDL, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(Renderer->SDL,50,50,50,50);
    SDL_RenderClear(Renderer->SDL);
    
    //
    // NOTE(Sanderkp): Enable anisotropic filtering
    //
    if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2"))
    {
        DebugLog("%s", SDL_GetError());
        Assert(0);
    }
    
    //
    // NOTE(Sanderkp): The renderer is basically a stack*, *(It's not technically a stack)
    //                 you push calls on to the top and then they are peeled from the bottom. 
    //                 For colour calls and texture calls we store that texture/colour was set
    //                 until another one is set.Everything is retrieved in the same order they were pushed on.
    //                 Some examples:
    //                    PushColour(...) <- This sets the colour for all subsequent calls to PushQuad etc
    //                    PushTexture(...) <- This sets the texture for all subsequent calls to PushQuad etc
    //                    PushQuad(...) <- Pushes a quad onto the stack
    //
    render_command *Command = (render_command*)Renderer->Commands;
    fkb_texture *CurrentTexture = 0;
    u8 CurR = 255, CurG = 255, CurB = 255, CurA = 255;
    for(u32 CommandIndex = 0;
        CommandIndex < Renderer->CommandCount;
        ++CommandIndex)
    
    {
        umm Size = SizeOfRenderCommand(Command);
        switch(Command->Type)
        {
            case RenderCommand_Colour:
            {
                command_colour *Colour = (command_colour*)Command->Command;
                SDL_SetRenderDrawColor(Renderer->SDL, Colour->r,Colour->g,Colour->b,Colour->a);
                CurR = Colour->r;
                CurG = Colour->g;
                CurB = Colour->b;
                CurA = Colour->a;
            }break;
            case RenderCommand_Image:
            {
                //
                // NOTE(Sanderkp): Textures are special because they are loaded from disk and surfaces are created in
                //                 the DLL but they are not uploaded to the GPU until they reach this code,
                //                 the reason for this is to minimize the footprint of the renderer in the platform_api.
                //                 What happens when a texture is pushed on is that we check if the texture has been uploaded,
                //                 and if it has not, then we upload it and free the surface before we bind it.
                //
                command_image *TexCmd = (command_image*)Command->Command;
                CurrentTexture = TexCmd->Texture;
                if(CurrentTexture)
                {
                    if(CurrentTexture->Texture == (SDL_Texture*)-1)
                    {
                        Assert(0);
                    }
                    if(!CurrentTexture->Texture && CurrentTexture->Surface)
                    {
                        CurrentTexture->Texture = SDL_CreateTextureFromSurface(Renderer->SDL, CurrentTexture->Surface);
                        if(CurrentTexture->Texture == (SDL_Texture*)-1)
                        {
                            Assert(0);
                        }
                        SDL_FreeSurface(CurrentTexture->Surface);
                        CurrentTexture->Surface = 0;
                        if(CurrentTexture->Pixels)
                        {
                            if(CurrentTexture->Flags & TextureFlag_Stb_Image)
                            {
                                stbi_image_free(CurrentTexture->Pixels);
                            }
                            else
                            {
                                Platform.Free(CurrentTexture->Pixels);
                            }
                            CurrentTexture->Pixels = 0;
                        }
                    }
                    if(CurrentTexture->Texture == (SDL_Texture*)-1)
                    {
                        Assert(0);
                    }
                    if(CurrentTexture->Texture)
                    {
                        if(SDL_SetTextureColorMod(CurrentTexture->Texture,CurR,CurG,CurB) < 0)
                        {
                            //TODO: Logging...
                        }
                        if(SDL_SetTextureAlphaMod(CurrentTexture->Texture, CurA)  < 0)
                        {
                            //TODO: Logging...
                        }
                    }
                }
            }break;
            case RenderCommand_Rect:
            case RenderCommand_Rect_Outline:
            {
                command_rect *Quad = (command_rect*)Command->Command;
                SDL_Rect Rect = {Quad->x, Quad->y, Quad->w, Quad->h };
                if(CurrentTexture && CurrentTexture->Texture && Command->Type == RenderCommand_Rect)
                {
                    SDL_RenderCopy(Renderer->SDL, CurrentTexture->Texture, 0, &Rect);
                }
                else
                {
                    if(Command->Type == RenderCommand_Rect_Outline)
                    {
                        SDL_RenderDrawRect(Renderer->SDL, &Rect);
                    }
                    else
                    {
                        SDL_RenderFillRect(Renderer->SDL, &Rect);
                    }
                }
            }break;
            case RenderCommand_Rect_UV:
            {
                command_rect_uv *Quad = (command_rect_uv*)Command->Command;
                SDL_Rect Dest = {Quad->x, Quad->y, Quad->w, Quad->h};
                SDL_Rect Src = {Quad->s, Quad->t, Quad->r, Quad->q};
                if(CurrentTexture && CurrentTexture->Texture)
                {
                    SDL_RenderCopy(Renderer->SDL, CurrentTexture->Texture, &Src, &Dest);
                }
                else
                {
                    SDL_RenderFillRect(Renderer->SDL, &Dest);
                }
            }break;
            case RenderCommand_Clip_Rect:
            {
                command_rect *Rect = (command_rect*)Command->Command;
                SDL_Rect R = {0};
                if(Rect->x == 0 &&
                   Rect->y == 0 &&
                   Rect->w == 0 &&
                   Rect->h == 0)
                {
                    R.x = 0;
                    R.y = 0;
                    R.w = Renderer->Width;
                    R.h = Renderer->Height;
                }
                else
                {
                    R.x = Rect->x;
                    R.y = Rect->y;
                    R.w = Rect->w;
                    R.h = Rect->h;
                }
                SDL_RenderSetClipRect(Renderer->SDL, &R);
                if(!SDL_RenderIsClipEnabled(Renderer->SDL))
                {
                    DebugLog("%s", SDL_GetError());
                    Assert(0);
                }
            }break;
            case RenderCommand_Line:
            {
                command_line *Line = (command_line*)Command->Command;
                SDL_RenderDrawLine(Renderer->SDL,Line->FromX,Line->FromY,Line->ToX,Line->ToY);
            }break;
            InvalidDefaultCase;
        }
        Command = (render_command*)((u8*)Command + Size);
    }
    Renderer->CommandCount = 0;
    Renderer->CommandsAt = Renderer->Commands;
    SDL_RenderPresent(Renderer->SDL);
}

