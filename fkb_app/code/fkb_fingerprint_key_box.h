///##################################################################################
/// fkb_fingerprint_key_box.h
/// 
/// Contains various enums, structs and functions used throughout the DLL
/// 
/// 
///##################################################################################

//
// NOTE(Sanderkp): App version, displayed in the About section in the app
//
#define FKB_APP_VERSION "Build ("__DATE__")"

//
// TODO(Sanderkp): Enum representing the various screen modes.
//
enum ScreenMode
{
    //
    // NOTE(Sanderkp): Initial screen, this is the default screen the app starts up in and has the "Sign in" and "Sign Up" buttons
    //
    ScreenMode_Start,
    //
    // NOTE(Sanderkp): This is where users perform their logins and signups, a picture in the middle shows the current fingerprint
    //
    ScreenMode_Scan,
    //
    // NOTE(Sanderkp): Profile page for the user, not strictly needed but a fun way to show everything working
    //
    ScreenMode_Profile,
};

//
// NOTE(Sanderkp): Result from signing up or signing in
//
enum AuthenticationResults
{
    //
    // NOTE(Sanderkp): Authentication has not been performed yet
    //
    AuthenticationResult_None,
    //
    // NOTE(Sanderkp): Currently processing sign in/up request
    //
    AuthenticationResult_Processing,
    //
    // NOTE(Sanderkp): The user was sucessfully signed in/up
    //
    AuthenticationResult_Success,
    //
    // NOTE(Sanderkp): An error happened or the user was rejected
    //
    AuthenticationResult_Failed,
};

//
// NOTE(Sanderkp): Different font sizes, these are currently set to scale with the window dimensions (See fkb_fingerprint_key_box.cpp)
//
enum FontSizes
{
    FontSize_Huge,     //Default: 90
    FontSize_Large,    //Default: 55
    FontSize_Regular,  //Default: 40
    FontSize_Medium,   //Default: 35
    FontSize_Small,    //Default: 30
    FontSize_Tiny,     //Default: 20
    FontSize_Total,    //Count enum
};

//
// NOTE(Sanderkp): States of verifinger licence
//
enum VerifingerLicenceState
{
    VerifingerLicence_Unavailable,
    VerifingerLicence_Checking,
    VerifingerLicence_Acquired,
};

//
// NOTE(Sanderkp): This is the main data structure of the application
//
struct program_state
{
    //
    // NOTE(Sanderkp): Set to true after initalization
    //
    b32 Initialized;
    
    //
    // NOTE(Sanderkp): State of verifinger licence (See enum VerifingerLicenceState)
    //
    SDL_atomic_t VerifingerLicenceState;
    
    //
    // NOTE(Sanderkp): Networking state, See fkb_net.h
    //
    net_state Net;
    
    //
    // NOTE(Sanderkp): This is the programs basic work queue
    //
    work_queue *WorkQueue;
    
    //
    // NOTE(Sanderkp): We store various font sizes in here, they are recalculated each frame so that they are scaled with the window dimensions
    //
    r32 FontSizes[FontSize_Total];
    
    //
    // NOTE(Sanderkp): Various assets
    //
    struct nk_image Gray;             // NOTE(Sanderkp): Basic gray image, used as shadows
    struct nk_image Gradient;         // NOTE(Sanderkp): Gradient used in about window
    struct nk_image White;            // NOTE(Sanderkp): White image used in fingerprint display when empty
    struct nk_image Dark;             // NOTE(Sanderkp): Darker gray with alpha, used when another window is covering the main one
    struct nk_image MinutiaPoint;     // NOTE(Sanderkp): Small minutia point circle, used in the scanning display
    struct nk_image CurrentScan;      // NOTE(Sanderkp): The current fingerprint scan
    
    //
    // NOTE(Sanderkp): The current scan progress, 0-100. Marked as volatile because it's being updated on one thread and read from another
    //
    volatile nk_size CurrentScanProgress;
    //
    // NOTE(Sanderkp): The current ScreenMode, See enum ScreenMode. Also marked as volatile because its being read and modified 
    //                 by different threads
    //
    volatile u32 Mode;
    //
    // NOTE(Sanderkp): Result from authentication, read nad modified by multiple threads as well. See enum AuthenticationResults
    //
    volatile u32 AuthenticationResult;
    
    //
    // NOTE(Sanderkp): Default font, must be present. This may be used as a fallback if the other fonts don't work
    //
    struct nk_user_font DefaultFont;
    //
    // NOTE(Sanderkp): Regular font, must be present
    //
    struct nk_user_font MPlusRegular;
    //
    // NOTE(Sanderkp): Bold font, must be present
    //
    struct nk_user_font MPlusBold;
    
    //
    // NOTE(Sanderkp): Holds user information, this is mostly for the profile page and partially for the SRP
    //
    user User;
    //
    // NOTE(Sanderkp): Copy of user, used when editing the profile page
    //
    user UserOrig;
};

///##################################################################################
/// Function:
///nk_user_font LoadFontForNK(char *Path, r32 FontHeight, u32 TextureWidth, u32 TextureHeight)
///
/// About:
/// Loads a font for use with nuklear
/// 
/// Parameters:
/// Path - Path to ttf file
/// FontHeight - Height of font in pixels, however most fonts dont extend all the way (this if from descent to ascent, see stb_truetype.h for more info)
/// TextureWidth - Width of generated texture, higher is smoother but more memory intesive
/// TextureHeight - Height of generated texture, higher is smoother but more memory intesive
///
/// Return value:
/// nk_user_font structure that can be used in the GUI, this structure is empty if loading failed.
///
/// Note:
/// Ttf support only
/// 
///##################################################################################
internal nk_user_font
LoadFontForNK(char *Path, r32 FontHeight, u32 TextureWidth, u32 TextureHeight)
{
    nk_user_font Font = {0};
    Font.userdata.ptr = LoadFont(Path, FontHeight, TextureWidth, TextureHeight, 127);
    Font.width = FontCalculateWidth;
    Font.height = FontHeight;
    return(Font);
}

//
// NOTE(Sanderkp): Structure contatining the parameters to LoadFontForNK
//
struct load_font_params
{
    char *Path;
    r32 FontHeight;
    u32 TextureWidth;
    u32 TextureHeight;
    struct nk_user_font *Dest;
};

///##################################################################################
/// Function:
/// WORK_QUEUE_FUNC(LoadFontForNKThreaded)
///
/// About:
/// Same as LoadFontForNK but can be run on the work queue.
/// 
/// Parameters:
/// Data - Pointer to load_font_params containing parameters for LoadFontForNK
///
/// Return value:
/// None, you must either wait on or check font data structure itself if loading has finished
/// 
///##################################################################################
internal
WORK_QUEUE_FUNC(LoadFontForNKThreaded)
{
    load_font_params *Params = (load_font_params*)Data;
    Params->Dest->width = FontCalculateWidth;
    *Params->Dest = LoadFontForNK(Params->Path, Params->FontHeight, Params->TextureWidth, Params->TextureHeight);
}