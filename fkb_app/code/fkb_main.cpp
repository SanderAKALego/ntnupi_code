///##################################################################################
/// fkb_main.cpp
/// 
/// Main entry point into the application. This file contains the 
/// functions responsible for main window creation, renderer creation,
/// timer initialisation, input processing and a few platform specific portions.
/// 
/// 
///##################################################################################

///##################################################################################
/// Global: GlobalPerformanceFrequency
///
///
/// About:
/// Used for acurate timings
/// 
/// Note:
/// See SDL_GetPerformanceFrequency() (https://wiki.libsdl.org/SDL_GetPerformanceFrequency)
/// 
///##################################################################################
global Uint64 GlobalPerformanceFrequency;

///##################################################################################
/// Function:
/// r32 GetSecondsElapsed(Uint64 Start, Uint64 End)
///
/// About:
/// Gets the number of seconds elapsed between start and end. Accuracy is dependant on the underlying platform.
/// 
/// Parameters:
/// Start - Start of time measurement period (result of SDL_GetPerformanceCounter())
/// End   - End  of time measurement period (result of SDL_GetPerformanceCounter())
///
/// Return value:
/// Seconds between Start and End
///
/// Note:
/// See SDL_GetPerformanceCounter() (https://wiki.libsdl.org/SDL_GetPerformanceCounter)
/// 
///##################################################################################
internal r32
GetSecondsElapsed(Uint64 Start, Uint64 End)
{
    r32 Result = (r32)(End - Start)/(r32)GlobalPerformanceFrequency;
    return(Result);
}

///##################################################################################
/// Function:
/// void* NKAllocator(nk_handle, void *, nk_size Size)
///
/// About:
/// Allocator wrapper for nuklear
/// 
/// Parameters:
/// Size - Size of allocation
///
/// Return value:
/// A block of memory or NULL if failed.
///
/// Note:
/// Argument 1 and 2 is not used
/// 
///##################################################################################
internal void*
NKAllocator(nk_handle, void *, nk_size Size)
{
    return(Allocate(Size));
}

///##################################################################################
/// Function:
/// void NKFree(nk_handle, void *Old)
///
/// About:
/// Free wrapper for nuklear
/// 
/// Parameters:
/// Memory block to be freed, must be allocated by NKAllocator
///
/// Return value:
/// None
///
/// Note:
/// Argument 1 is not used
/// 
///##################################################################################
internal void
NKFree(nk_handle, void *Old)
{
    Free(Old);
}

///##################################################################################
/// Function:
/// PLATFORM_UPDATE_SCREEN(UpdateScreen)
///
/// About:
/// Implementation of UpdateScreen specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_UPDATE_SCREEN in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_UPDATE_SCREEN(UpdateScreen)
{
    SDL_Event Event = {0};
    Event.type = ForceUpdateEvent;
    SDL_PushEvent(&Event);
}

#ifdef _WIN32

///##################################################################################
/// Function:
/// PLATFORM_RUN_AND_WAIT_FOR_PROCESS(RunAndWaitForProcess)
///
/// About:
/// Implementation of RunAndWaitForProcess specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_RUN_AND_WAIT_FOR_PROCESS in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_RUN_AND_WAIT_FOR_PROCESS(RunAndWaitForProcess)
{
    STARTUPINFO StartupInfo = {sizeof(PROCESS_INFORMATION)};
    PROCESS_INFORMATION ProcessInformation = {0};
    if(!CreateProcess(Executable, Arguments, 0, 0, FALSE, NORMAL_PRIORITY_CLASS,0,0, &StartupInfo,&ProcessInformation))
    {
        return(false);
    }
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);
    CloseHandle(ProcessInformation.hProcess);
    CloseHandle(ProcessInformation.hThread);
    return(true);
}
#else

///##################################################################################
/// Function:
/// PLATFORM_RUN_AND_WAIT_FOR_PROCESS(RunAndWaitForProcess)
///
/// About:
/// Implementation of RunAndWaitForProcess specified in fkb_platform.h.
///
/// Note:
/// See PLATFORM_RUN_AND_WAIT_FOR_PROCESS in fkb_platform.h
/// 
///##################################################################################
internal
PLATFORM_RUN_AND_WAIT_FOR_PROCESS(RunAndWaitForProcess)
{
    return(0);
}

#endif

///
/// Entry point to application
///
int main(int argc, char **argv)
{
    //
    // NOTE(Sanderkp): Platform initialization, see fkb_platform.h for more info.
    //
    Platform.Allocate = Allocate;
    Platform.Free = Free;
    Platform.ReadEntireFile = ReadEntireFile;
    Platform.FreeEntireFile = FreeEntireFile;
    Platform.AddWork = AddWork;
    Platform.CompleteAllWork = CompleteAllWork;
    Platform.UpdateScreen = UpdateScreen;
    Platform.OpenFileDialog = OpenFileDialog;
    Platform.SizeOfFile = SizeOfFile;
    Platform.RunAndWaitForProcess = RunAndWaitForProcess;
    Platform.OpenFile = OpenFile;
    Platform.CloseFile = CloseFile;
    Platform.Write = Write;
    Platform.Read = Read;
    Platform.GetCurrentFilePosition = GetCurrentFilePosition;
    Platform.SetCurrentFilePosition = SetCurrentFilePosition;
    Platform.CopyFileTo = CopyFileTo;
    Platform.DeleteFileAt = DeleteFileAt;
    
    renderer Renderer = {0};
    Renderer.Width = 1280;
    Renderer.Height = 720;
    if(!SDL_Init(SDL_INIT_VIDEO))
    {
        if(SDLNet_Init() != -1) 
        {
            GlobalPerformanceFrequency = SDL_GetPerformanceFrequency();
            SDL_Window *Window = SDL_CreateWindow("FingerprintKeyBox Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Renderer.Width, Renderer.Height, SDL_WINDOW_RESIZABLE);
            if(Window)
            {
                Renderer.SDL = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);//SDL_RENDERER_SOFTWARE);//
                if(Renderer.SDL)
                {
                    program_state State = {0};
                    nk_context Context = {0};
                    nk_allocator Allocator = {0};
                    Uint64 FrameFlipTime = SDL_GetPerformanceCounter();
                    r32 dt = 1.0f/60.0f;
                    
                    //
                    // NOTE(Sanderkp): Warning this MUST exist, otherwise crash!
                    //
                    Renderer.FallbackFont = LoadFallbackFont("font.png", 7, 9);
                    State.DefaultFont = LoadFontForNK("mplus-2p-regular.ttf", 12.0f, 1024, 1024);
                    
                    SDL_RendererInfo RendererInfo = {0};
                    SDL_GetRendererInfo(Renderer.SDL, &RendererInfo);
                    
                    //
                    // NOTE(Sanderkp): Registering user event (UpdateScreen event)
                    //
                    ForceUpdateEvent = SDL_RegisterEvents(1);
                    if(ForceUpdateEvent == (Uint32)-1)
                    {
                        DebugLog("SDL_RegisterEvents failed: %s", SDL_GetError());
                    }
                    
                    //
                    // NOTE(Sanderkp): Hotloader/DLL loading initalization
                    //
                    char *DLLPath = "";
                    if(argc != 2)
                    {
                        DebugLog("Path to DLL not set, using defaults");
                    }
                    else
                    {
                        DLLPath = argv[1];
                    }
                    DebugLog("DLL path: %s", DLLPath);
#if HOT_RELOADER_ENABLED
                    InitializeHotreloader(DLLPath);
#endif
                    
                    //
                    // NOTE(Sanderkp): SDL documentation is sparse on SDL_GetCPUCount, assume it doesn't return a negative value
                    //
                    Assert(SDL_GetCPUCount() >= 0);
                    State.WorkQueue = CreateWorkQueue((u32)SDL_GetCPUCount());
                    
                    //
                    // NOTE(Sanderkp): Nuklear initalization, Allocator must be set first
                    //
                    Allocator.alloc = NKAllocator;
                    Allocator.free = NKFree;
                    if(!nk_init(&Context, &Allocator, &State.DefaultFont))
                    {
                        DebugLog("nk_init_default failed!");
                        Assert(0);
                    }
                    
                    //
                    // NOTE(Sanderkp): Program loop starts here
                    //
                    b32 GlobalRunning = true;
                    while(GlobalRunning)
                    {
                        //
                        // NOTE(Sanderkp): Processing input
                        //
                        SDL_Event Event;
                        nk_input_begin(&Context);
                        if(SDL_WaitEvent(&Event))
                        {
                            switch(Event.type)
                            {
                                case SDL_USEREVENT:
                                {
                                    //
                                    // NOTE(Sanderkp): Triggered when UpdateScreen is called
                                    //
                                    if(Event.user.type == ForceUpdateEvent)
                                    {
                                        //...
                                    }
                                }break;
                                case SDL_QUIT:
                                {
                                    GlobalRunning = false;
                                }break;
                                case SDL_MOUSEMOTION:
                                {
                                    nk_input_motion(&Context, Event.motion.x, Event.motion.y);
                                }break;
                                case SDL_MOUSEWHEEL:
                                {
                                    struct nk_vec2 Value = {(r32)Event.wheel.x, (r32)Event.wheel.y};
                                    nk_input_scroll(&Context, Value);
                                }break;
                                case SDL_TEXTINPUT:
                                {
                                    char *Scan = Event.text.text;
                                    s32x Off = 0;
                                    s32x InputLength = (s32x)strlen(Event.text.text);
                                    u32 Length = (u32)nk_utf_len(Event.text.text, InputLength);
                                    for(u32 RuneIndex = 0;
                                        RuneIndex < Length;
                                        ++RuneIndex)
                                    {
                                        nk_rune Rune = {0};
                                        Off += nk_utf_decode(Scan + Off, &Rune, InputLength);
                                        nk_input_unicode(&Context, Rune);
                                    }
                                }break;
                                case SDL_MOUSEBUTTONDOWN:
                                case SDL_MOUSEBUTTONUP:
                                {
                                    b32 IsDown = Event.button.state == SDL_PRESSED;
                                    b32 DoubleClick = Event.button.clicks == 2;
                                    if(Event.button.button == SDL_BUTTON_LEFT)
                                    {
                                        nk_input_button(&Context, NK_BUTTON_LEFT, Event.button.x, Event.button.y, IsDown);
                                    }
                                    else if(Event.button.button == SDL_BUTTON_MIDDLE)
                                    {
                                        nk_input_button(&Context, NK_BUTTON_MIDDLE, Event.button.x, Event.button.y, IsDown);
                                    }
                                    else if(Event.button.button == SDL_BUTTON_RIGHT)
                                    {
                                        nk_input_button(&Context, NK_BUTTON_RIGHT, Event.button.x, Event.button.y, IsDown);
                                    }
                                    if(DoubleClick)
                                    {
                                        nk_input_button(&Context, NK_BUTTON_DOUBLE, Event.button.x, Event.button.y, IsDown);
                                    }
                                }break;
                                case SDL_WINDOWEVENT:
                                {
                                    if(Event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED ||
                                       Event.window.event == SDL_WINDOWEVENT_RESIZED)
                                    {
                                        Renderer.Width = Event.window.data1;
                                        if(Event.window.data2 == 0)
                                        {
                                            Renderer.Height = 1;
                                        }
                                        else
                                        {
                                            Renderer.Height = Event.window.data2;
                                        }
                                    }
                                }break;
                                case SDL_KEYDOWN:
                                case SDL_KEYUP:
                                {
                                    bool IsDown = (Event.key.state == SDL_PRESSED);
                                    bool WasDown = false;
                                    if(Event.key.state == SDL_RELEASED)
                                    {
                                        WasDown = true;
                                    }
                                    else if(Event.key.repeat != 0)
                                    {
                                        WasDown = true;
                                    }
                                    
                                    //
                                    // NOTE(Sanderkp): Keybindings, maps SDLK_* to nuklear key events
                                    //
                                    local u32 Keys[] =
                                    {
                                        SDLK_LSHIFT, NK_KEY_SHIFT,
                                        SDLK_RSHIFT, NK_KEY_SHIFT,
                                        SDLK_LCTRL, NK_KEY_CTRL,
                                        SDLK_RCTRL, NK_KEY_CTRL,
                                        SDLK_DELETE, NK_KEY_DEL,
                                        SDLK_RETURN, NK_KEY_ENTER,
                                        SDLK_TAB, NK_KEY_TAB,
                                        SDLK_BACKSPACE,NK_KEY_BACKSPACE,
                                        SDLK_UP,NK_KEY_UP,
                                        SDLK_DOWN,NK_KEY_DOWN,
                                        SDLK_LEFT,NK_KEY_LEFT,
                                        SDLK_RIGHT,NK_KEY_RIGHT,
                                        SDLK_HOME,NK_KEY_TEXT_LINE_START,
                                        SDLK_END,NK_KEY_TEXT_LINE_END,
                                        SDLK_HOME,NK_KEY_TEXT_START,
                                        SDLK_END,NK_KEY_TEXT_END,
                                        
                                        SDLK_z,NK_KEY_TEXT_UNDO,// NOTE(Sanderkp): Ctrl-z
                                        SDLK_y,NK_KEY_TEXT_REDO,// NOTE(Sanderkp): Ctrl-y
                                        SDLK_a,NK_KEY_TEXT_SELECT_ALL,// NOTE(Sanderkp): Ctrl-a
                                        SDLK_LEFT,NK_KEY_TEXT_WORD_LEFT,// NOTE(Sanderkp): Ctrl-left
                                        SDLK_RIGHT,NK_KEY_TEXT_WORD_RIGHT,// NOTE(Sanderkp): Ctrl-right
                                        SDLK_HOME,NK_KEY_SCROLL_START,
                                        SDLK_END,NK_KEY_SCROLL_END,
                                        SDLK_DOWN,NK_KEY_SCROLL_DOWN,
                                        SDLK_UP,NK_KEY_SCROLL_UP,
                                    };
                                    
                                    for(u32 KeyIndex = 0;
                                        KeyIndex < ArrayCount(Keys)/2;
                                        ++KeyIndex)
                                    {
                                        u32 SDLKey = Keys[KeyIndex*2+0];
                                        nk_keys NKKey = (nk_keys)Keys[KeyIndex*2+1];
                                        if((u32)Event.key.keysym.sym == SDLKey)
                                        {
                                            if(NKKey == NK_KEY_TEXT_UNDO       ||
                                               NKKey == NK_KEY_TEXT_REDO       ||
                                               NKKey == NK_KEY_TEXT_SELECT_ALL ||
                                               NKKey == NK_KEY_TEXT_WORD_LEFT  ||
                                               NKKey == NK_KEY_TEXT_WORD_RIGHT)
                                            {
                                                const Uint8 *KeyboardState = SDL_GetKeyboardState(0);
                                                IsDown = (KeyboardState[SDL_SCANCODE_LCTRL] || KeyboardState[SDL_SCANCODE_RCTRL]);
                                            }
                                            nk_input_key(&Context, NKKey, IsDown);
                                        }
                                    }
                                    
                                    if(IsDown != WasDown)
                                    {
                                        if(Event.key.keysym.sym == SDLK_ESCAPE)
                                        {
                                            GlobalRunning = false;
                                        }
                                    }
                                }
                            }
                        }
                        nk_input_end(&Context);
#if HOT_RELOADER_ENABLED
                        if(DllUpdateAndRender)
                        {
                            DllUpdateAndRender(&State, &Platform, &Renderer, &Context);
                        }
#else
                        UpdateAndRender(&State, &Platform, &Renderer, &Context);
#endif
                        PushColourf(&Renderer, 1.0f,1.0f,1.0f,1.0f);
                        
                        char Buffer[256];
#if defined FKB_DEBUG
                        r32 FPS = 0.0f;
                        if(dt != 0.0f)
                        {
                            FPS = 1.0f/dt;
                        }
                        stbsp_sprintf(Buffer, "Renderer: %s dt: %0.2f ms FPS: %0.2f", RendererInfo.name, dt*1000.0f, FPS);
                        PushColour(&Renderer, 0,0,0,255);
                        PushFallbackText(&Renderer, Buffer, 0, 0, &Renderer.FallbackFont);
#endif
#if HOT_RELOADER_ENABLED
                        if(!DllUpdateAndRender)
                        {
                            stbsp_sprintf(Buffer, "Failed to load main DLL");
                            PushColour(&Renderer, 255,255,255,255);
                            PushFallbackText(&Renderer, Buffer, (Renderer.Width-7*(s32)strlen(Buffer))/2, (Renderer.Height-9)/2, &Renderer.FallbackFont);
                        }
#endif
                        nk_clear(&Context);
                        
                        RunRenderCommands(&Renderer);
                        
                        dt = GetSecondsElapsed(FrameFlipTime, SDL_GetPerformanceCounter());
                        FrameFlipTime = SDL_GetPerformanceCounter();
                    }
                    nk_free(&Context);
                    if(Platform.AppExit)
                    {
                        Platform.AppExit(&State);
                    }
                }
            }
            SDLNet_Quit();
        }
        SDL_Quit();
    }
    return(0);
}