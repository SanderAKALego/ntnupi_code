///##################################################################################
/// fkb_verifinger.cpp
/// 
/// File that holds the code responsible for dealing with verifinger.
/// (Verifying the licence, extracting minutiae and so on)
/// 
/// 
///##################################################################################
#include <NLicensing.h>
#include <NImages.h>
#include <NFExtractor.h>

//
// NOTE(Sanderkp): Verifinger server address and port
//
#define LicenceServerAddress "/local"
#define LicenceServerPort "5000"

//
// NOTE(Sanderkp): Licence to request from the verifinger local service
//
#define VerifingerLicence "SingleComputerLicense:VFExtractor"

//
// NOTE(Sanderkp): Defines a single minutia, more info can be stored here such as type and quality
//
struct minutia_point
{
    s32 x, y;
};

///##################################################################################
/// Function:
/// int CheckNeuroTechLicence(NChar *Licence)
///
/// About:
/// Checks the verifinger licence.
/// 
/// Parameters:
/// Licence - Licence to check, this should be set to the VerifingerLicence macro, see Verifinger documentation for other possible licences
///
/// Return value:
/// True if a licence was available or false if:
///  1. Failed to connect to service
///  2. No available licence
///
/// Note:
/// Licence must be release by ReleaseNeuroTechLicence after use.
/// 
///##################################################################################
internal b32
CheckNeuroTechLicence(NChar *Licence)
{
    NBool Available = {0};
    NResult Result = NLicenseObtain(LicenceServerAddress, LicenceServerPort, Licence, &Available);
    if(NFailed(Result))
    {
        return(false);
    }
    if(!Available)
    {
        return(false);
    }
    return(true);
}

///##################################################################################
/// Function:
/// b32 ReleaseNeuroTechLicence(NChar *Licence)
///
/// About:
/// Releases the verifinger licence acquired by CheckNeuroTechLicence
/// 
/// Parameters:
/// Licence - Licence to release, this should be set to the VerifingerLicence macro, see Verifinger documentation for other possible licences
///
/// Return value:
/// True if no error, false if failed.
///
///##################################################################################
internal b32
ReleaseNeuroTechLicence(NChar *Licence)
{
    NResult Result = NLicenseRelease(Licence);
    if(NFailed(Result))
    {
        return(false);
    }
    return(true);
}

///##################################################################################
/// Function:
/// NFMinutia *ExtractMinutiaeFromImage(char *ImageFile, int *MinutiaCount, int *ImageWidth, int *ImageHeight, int *ImageResolution)
///
/// About:
/// Runs the verifinger extractor on an image from disk.
/// 
/// Parameters:
/// ImageFile - Path to image, supported types are: TIF, JPG, PNG, GIF, BMP, CAN NOT be NULL
/// MinutiaCount - Pointer to an integer to store the number of minutiae, CAN NOT be NULL
/// ImageWidth - Pointer to an integer to store the width of the image, can be NULL
/// ImageHeight - Pointer to an integer to store the height of the image, can be NULL
/// ImageResolution - Pointer to an integer to store the resolution of the image, can be NULL
///
/// Return value:
/// Pointer to a block of NFMinutia if success, call free to release memory or NULL if fail
/// 
///##################################################################################
internal NFMinutia*
ExtractMinutiaeFromImage(char *ImageFile, int *MinutiaCount, int *ImageWidth, int *ImageHeight, int *ImageResolution)
{
    
    //
    // NOTE(Sanderkp): Initialization
    //
    *ImageWidth = 0;
    *ImageHeight = 0;
    *ImageResolution = 0;
    
    HNImage Image = 0;
    NFloat HorizontalResolution = 0.0;
    NFloat VerticalResolution = 0.0;
    NResult Result = NImageCreateFromFile(ImageFile, 0, &Image);
    if(NFailed(Result))
    {
        return(0);
    }
    
    Result = NImageGetHorzResolution(Image, &HorizontalResolution);
    if(NFailed(Result))
    {
        NObjectFree(Image);
        return(0);
    }
    
    Result = NImageGetVertResolution(Image, &VerticalResolution);
    if(NFailed(Result))
    {
        NObjectFree(Image);
        return(0);
    }
    
    if(VerticalResolution < 250)
    {
        printf("vertical resolution is less than 250 (resolution %f), forcing 500 resolution\n", VerticalResolution);
        VerticalResolution = 500;
    }
    
    if(HorizontalResolution < 250)
    {
        printf("horizontal resolution is less than 250 (resolution %f), forcing 500 resolution\n", HorizontalResolution);
        HorizontalResolution = 500;
    }
    
    //
    // NOTE(Sanderkp): Create grayscale image
    //
    HNImage GrayscaleImage = 0;
    Result = NImageCreateFromImageEx(npfGrayscale, 0, HorizontalResolution, VerticalResolution, Image, &GrayscaleImage);
    NObjectFree(Image);
    if(NFailed(Result))
    {
        return(0);
    }
    
    //
    // NOTE(Sanderkp): Create extractor
    //
    HNFExtractor Extractor = 0;
    Result = NfeCreate(&Extractor);
    if(NFailed(Result))
    {
        return(0);
    }
    
    //
    // NOTE(Sanderkp): Run extractor
    //
    HNFRecord Record = 0;
    NfeExtractionStatus ExtractionStatus = {};
    Result = NfeExtract(Extractor, GrayscaleImage, nfpUnknown, nfitLiveScanPlain, &ExtractionStatus, &Record);
    NObjectFree(Extractor);
    if(NFailed(Result))
    {
        return(0);
    }
    
    //
    // NOTE(Sanderkp): Check status
    //
    if(ExtractionStatus != nfeesTemplateCreated)
    {
        const NChar *reason;
        switch (ExtractionStatus)
        {
            case nfeesTooFewMinutiae:
            reason = N_T("too few minutiae");
            break;
            case nfeesQualityCheckFailed:
            reason = N_T("quality check failed");
            break;
            case nfeesMatchingFailed:
            reason = N_T("matching failed");
            default:
            reason = N_T("quality check failed");
            break;
        }
        printf(N_T("\rextraction status indicates failure, reason: %s\n"), reason);
        return(0);
    }
    
    //
    // NOTE(Sanderkp): Get minutiae
    //
    Result = NFRecordGetMinutiaCount(Record, MinutiaCount);
    if(NFailed(Result))
    {
        return(0);
    }
    NFMinutia *ExtractedMinutiae = (NFMinutia*)calloc(*MinutiaCount, sizeof(NFMinutia));
    if(!ExtractedMinutiae)
    {
        return(0);
    }
    Result = NFRecordGetMinutiae(Record, ExtractedMinutiae);
    if(NFailed(Result))
    {
        return(0);
    }
    
    NUShort _Width = 0, _Height = 0;
    if(NFailed(NFRecordGetWidth(Record, &_Width)))
    {
        return(0);
    }
    if(NFailed(NFRecordGetHeight(Record, &_Height)))
    {
        return(0);
    }
    
    if(ImageWidth)
    {
        *ImageWidth = _Width;
    }
    if(ImageHeight)
    {
        *ImageHeight = _Height;
    }
    if(ImageResolution)
    {
        *ImageResolution = 500;
    }
    
    //
    // NOTE(Sanderkp): Free memory
    //
    NObjectFree(GrayscaleImage);
    NObjectFree(Record);
    GrayscaleImage = 0;
    Record = 0;
    return(ExtractedMinutiae);
}

///##################################################################################
/// Function:
/// unsigned char ToISOType(NFMinutiaType Type)
///
/// About:
/// Converts verifinger minutiae type to ISO/IEC 19794-2:2005 type.
/// 
/// Parameters:
/// Type - A verifinger specific minuta type
///
/// Return value:
/// A ISO/IEC 19794-2:2005 compliant type value in all cases
///
///##################################################################################
internal unsigned char
ToISOType(NFMinutiaType Type)
{
    char Result = 0x00;
    switch(Type)
    {
        case nfmtUnknown:
        {
            Result = 0x00;
        }break;
        case nfmtEnd:
        {
            Result = 0x01;
        }break;
        case nfmtBifurcation:
        {
            Result = 0x02;
        }break;
        case nfmtOther:
        {
            Result = 0x00;
        }break;
    }
    return(Result);
}

//
// NOTE(Sanderkp): Macro for reversing a short (16 bit) value
//
#define REVERSE_SHORT(n) ((unsigned short) (((n & 0xFF) << 8) | \
((n & 0xFF00) >> 8)))
//
// NOTE(Sanderkp): Macro for reversing a long (32 bit) value
//
#define REVERSE_LONG(n) ((unsigned long) (((n & 0xFF) << 24) | \
((n & 0xFF00) << 8) | \
((n & 0xFF0000) >> 8) | \
((n & 0xFF000000) >> 24)))

///##################################################################################
/// Function:
/// int WriteISOTemplate(char *FilePath, NFMinutia *Minutiae, int MinutiaCount, int ImageWidth, int ImageHeight, int ImageResolution)
///
/// About:
/// Writes an ISO/IEC 19794-2:2005 compliant template to disk.
/// 
/// Parameters:
/// FilePath - Name and path of template to store, CAN NOT be NULL
/// Minutiae - Pointer to a block of minutiae, CAN NOT be NULL
/// MinutiaCount - Number of minutiae
/// ImageWidth - Width of fingerprint image, required by ISO/IEC 19794-2:2005
/// ImageHeight - Height of fingerprint image, required by ISO/IEC 19794-2:2005
/// ImageResolution - Resolution of fingerprint image, required by ISO/IEC 19794-2:2005
///
/// Return value:
/// True if success, false if fails. The function may fail if the appplication has insufficient write access to disk.
///
/// Note:
/// Please see ISO/IEC 19794-2:2005 for a complete description of the file format
/// 
///##################################################################################
internal b32
WriteISOTemplate(char *FilePath, NFMinutia *Minutiae, int MinutiaCount, int ImageWidth, int ImageHeight, int ImageResolution)
{
    file_handle *FileHandle = Platform.OpenFile(FilePath, FileMode_Create);
    if(!FileHandle)
    {
        return(false);
    }
    char ReservedByte = 0, NotUsedByte = 0;
    //Record
    char FormatIdentifier[] = "FMR";
    Platform.Write(FormatIdentifier, sizeof(FormatIdentifier), FileHandle);
    char VersionNumber[] = "020";
    Platform.Write(VersionNumber, sizeof(VersionNumber), FileHandle);
    int RecordLength = 0;
    Platform.Write(&RecordLength, sizeof(int), FileHandle);
    short NotUsed = 0;
    Platform.Write(&NotUsed, sizeof(short), FileHandle);
    short ImgSizeX = REVERSE_SHORT(ImageWidth), ImgSizeY = REVERSE_SHORT(ImageHeight);
    short ImgResX = REVERSE_SHORT(ImageResolution), ImgResY = REVERSE_SHORT(ImageResolution);
    Platform.Write(&ImgSizeX, sizeof(short), FileHandle);
    Platform.Write(&ImgSizeY, sizeof(short), FileHandle);
    Platform.Write(&ImgResX, sizeof(short), FileHandle);
    Platform.Write(&ImgResY, sizeof(short), FileHandle);
    char FingerViews = 1;
    Platform.Write(&FingerViews, sizeof(char), FileHandle);
    Platform.Write(&ReservedByte, sizeof(char), FileHandle);
    //Views
    Platform.Write(&NotUsedByte, sizeof(char), FileHandle);
    Platform.Write(&NotUsedByte, sizeof(char), FileHandle);
    Platform.Write(&NotUsedByte, sizeof(char), FileHandle);
    char _MinutiaeCount = MinutiaCount;
    Platform.Write(&_MinutiaeCount, sizeof(char), FileHandle);
    for(int MinutiaIndex = 0; MinutiaIndex < MinutiaCount; ++MinutiaIndex)
    {
        NFMinutia *Minutia = Minutiae + MinutiaIndex;
        //Minutia
        short x = Minutia->X;
        short y = Minutia->Y;
        char Angle = Minutia->Angle;
        char Quality = Minutia->Quality;
        
        x |= (ToISOType(Minutia->Type) << 14);
        x = REVERSE_SHORT(x);
        y = REVERSE_SHORT(y);
        
        Platform.Write(&x, sizeof(short), FileHandle);
        Platform.Write(&y, sizeof(short), FileHandle);
        Platform.Write(&Angle, sizeof(char), FileHandle);
        Platform.Write(&Quality, sizeof(char), FileHandle);
    }
    RecordLength = REVERSE_LONG(Platform.GetCurrentFilePosition(FileHandle));
    Platform.SetCurrentFilePosition(FileHandle, 8, SEEK_SET);
    Platform.Write(&RecordLength, sizeof(int), FileHandle);
    Platform.CloseFile(FileHandle);
    return(true);
}

///##################################################################################
/// Function:
/// b32 ExtractMinutiaeAndWriteISOTemplate(char *FingerprintScanImagePath, char *ISOTemplatePath, u32 MaxISOTemplatePath, int *ImgWidthOut, 
//                                         int *ImgHeightOut, minutia_point *MinutiaePoints, u32 *MinutiaePointCount, u32 MaxMinutiaePoints)
///
/// About:
/// Shorthand function that combines ExtractMinutiaeFromImage and WriteISOTemplate. Also returns the minutiae points.
/// 
/// Parameters:
/// FingerprintScanImagePath - Path to fingerprint image, CAN NOT be NULL
/// ISOTemplatePath - Buffer to receive path to template, if NULL ISO template will not be written
/// MaxISOTemplatePath - Size of iso template file path buffer
/// ImgWidthOut - An integer to store the width of image
/// ImgHeightOut - An integer to store the height of image
/// MinutiaePoints - Buffer to store the minutia points
/// MinutiaePointCount - An integer to store the minutia point count
/// MaxMinutiaePoints - Max number of minutia that can be stored in MinutiaePoints
///
/// Return value:
/// True if success, false if fail.
///
/// Note:
/// If ISOTemplatePath is NULL no template is written to disk but you can still extract the minutiae
/// 
///##################################################################################
internal b32
ExtractMinutiaeAndWriteISOTemplate(char *FingerprintScanImagePath, char *ISOTemplatePath, u32 MaxISOTemplatePath, int *ImgWidthOut, int *ImgHeightOut, minutia_point *MinutiaePoints, u32 *MinutiaePointCount, u32 MaxMinutiaePoints)
{
    if(!FingerprintScanImagePath && strlen(ISOTemplatePath))
    {
        return(true);
    }
    
    int MinutiaCount = 0, ImageWidth = 0, ImageHeight = 0, ImageResolution = 0;
    NFMinutia *Minutiae = ExtractMinutiaeFromImage(FingerprintScanImagePath, &MinutiaCount, &ImageWidth, &ImageHeight, &ImageResolution);
    if(!Minutiae)
    {
        return(false);
    }
    if(MinutiaePoints)
    {
        for(u32 MinutiaIndex = 0;
            MinutiaIndex < (u32)(Min((u32)MinutiaCount, (u32)MaxMinutiaePoints));
            ++MinutiaIndex)
        {
            MinutiaePoints[MinutiaIndex] = {Minutiae[MinutiaIndex].X,Minutiae[MinutiaIndex].Y};
        }
        if(MinutiaePointCount)
        {
            *MinutiaePointCount = Min((u32)MinutiaCount, (u32)MaxMinutiaePoints);
        }
        if(ImgWidthOut)
        {
            *ImgWidthOut = ImageWidth;
        }
        if(ImgHeightOut)
        {
            *ImgHeightOut = ImageHeight;
        }
    }
    b32 Success = false;
    if(ISOTemplatePath)
    {
        char *TempISOPath = "template.ist";
        if(WriteISOTemplate(TempISOPath, Minutiae, MinutiaCount, ImageWidth, ImageHeight, ImageResolution))
        {
            Success = true;
        }
        free(Minutiae);
        if(!Success)
        {
            return(false);
        }
        
        if(strlen(TempISOPath) < MaxISOTemplatePath)
        {
            strcpy_s(ISOTemplatePath, MaxISOTemplatePath, TempISOPath);
        }
        else
        {
            return(false);
        }
    }
    else
    {
        free(Minutiae);
    }
    return(true);
}
