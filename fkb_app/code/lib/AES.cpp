// #include "stdafx.h"
#include <iostream>
#include "AES.h"


using namespace std; 

/////////////////////////////////





////////////////////////////////




void AES_encryption(unsigned char* msg, unsigned char* key)
{
	unsigned char state[16];		//state of the msg
	unsigned char expandedKey[176];	//unsigned char expandedKey[176];
	
	for(int i=0; i< 16; i++)
	{
		state[i] = msg[i];
	}
	int numberOfRounds = 9;		//Depending on which AES you preform! 9 is for AES128				
	
	keyExpansion(key, expandedKey); //expanding key
	AddRoundKey(state, expandedKey);		//Whitning		Gonna be different keys here
	for (int i = 0; i < numberOfRounds; i++)
	{
		subBytes(state); 
		shiftRows(state);
		mixColums(state);
		AddRoundKey(state, expandedKey + (16 * (i + 1)));
	}
	//Final round
	subBytes(state);
	shiftRows(state);
	mixColums(state);
	AddRoundKey(state, expandedKey + 160);

	//copy over the msg with the encrypted
	for (int i = 0; i < 16; i++)
	{
		msg[i] = state[i]; 
	}

	
}

void keyExpansionCore(unsigned char* in, unsigned char i)
{
	//rotating left
	unsigned int* q = (unsigned int*)in; 
	*q = (*q >> 8) | ((*q & 0xff) << 24); 

	/*unsigned char t = in[0];
	in[0] = in[1];
	in[1] = in[2];
	in[2] = in[3];
	in[3] = t;
	*/
	//S-box four bytes

	in[0] = s_box[in[0]]; in[1] = s_box[in[1]];
	in[2] = s_box[in[2]]; in[3] = s_box[in[3]];

	//Rcon
	in[0] ^= rcon[i]; //xor with rcon 
}

void keyExpansion(unsigned char* inputKey, unsigned char* expandedKeys) 
{	//taking 16 bytes and expanding it to 176 bytes
	//variabels used
	int bytesGenerated = 16,	//There has been 16 bytes generated so far
		rconIteration = 1;		//Start iteration at 1
	unsigned char temp[4];	//Storage for core

//Using the original key that are 16 bytes
	for (int i = 0; i < 16; i++)
	{
		expandedKeys[i] = inputKey[i]; 
	}
	while (bytesGenerated < 176)		//we still got bytes to generate 
	{
		for (int i = 0; i < 4; i++)		//Reading in 4 bytes for the core
		{
			temp[i] = expandedKeys[i + bytesGenerated - 4]; 
		}
		if (bytesGenerated % 16 == 0)	//preforming core for each 16 bytes
			keyExpansionCore(temp, rconIteration++);

		for (unsigned char a = 0; a < 4; a++)
		{
			expandedKeys[bytesGenerated] = expandedKeys[bytesGenerated - 16] ^ temp[a];
			bytesGenerated++; 
		}

	}
}

void subBytes(unsigned char* state) 
{
	for (int i = 0; i < 16; i++)
	{
		state[i] = s_box[state[i]];
	}
}

void shiftRows(unsigned char* state) 
{
	unsigned char temp[16];
	temp[0] = state[0];
	temp[1] = state[5];
	temp[2] = state[10];
	temp[3] = state[15];

	temp[4] = state[4];
	temp[5] = state[9];
	temp[6] = state[14];
	temp[7] = state[3];

	temp[8] = state[8];
	temp[9] = state[13];
	temp[10] = state[2];
	temp[11] = state[7];

	temp[12] = state[12];
	temp[13] = state[1];
	temp[14] = state[6];
	temp[15] = state[11];

	for (int i = 0; i < 16; i++)
	{
		state[i] = temp[i]; 
	}
}

void mixColums(unsigned char* state) 
{	
	//Dot products
	unsigned char temp[16]; 
	temp[0] = (unsigned char)(mul2[state[0]] ^ mul3[state[1]] ^ state[2] ^ state[3]);
	temp[1] = (unsigned char)(state[0] ^ mul2[state[1]] ^ mul3[state[2]] ^ state[3]);
	temp[2] = (unsigned char)(state[0] ^ state[1] ^ mul2[state[2]] ^ mul3[state[3]]);
	temp[3] = (unsigned char)(mul3[state[0]] ^ state[1] ^ state[2] ^ mul2[state[3]]);

	temp[4] = (unsigned char)(mul2[state[4]] ^ mul3[state[5]] ^ state[6] ^ state[7]);
	temp[5] = (unsigned char)(state[4] ^ mul2[state[5]] ^ mul3[state[6]] ^ state[7]);
	temp[6] = (unsigned char)(state[4] ^ state[5] ^ mul2[state[6]] ^ mul3[state[7]]);
	temp[7] = (unsigned char)(mul3[state[4]] ^ state[5] ^ state[6] ^ mul2[state[7]]);

	temp[8] = (unsigned char)(mul2[state[8]] ^ mul3[state[9]] ^ state[10] ^ state[11]);
	temp[9] = (unsigned char)(state[8] ^ mul2[state[9]] ^ mul3[state[10]] ^ state[11]);
	temp[10] = (unsigned char)(state[8] ^ state[9] ^ mul2[state[10]] ^ mul3[state[11]]);
	temp[11] = (unsigned char)(mul3[state[8]] ^ state[9] ^ state[10] ^ mul2[state[11]]);

	temp[12] = (unsigned char)(mul2[state[12]] ^ mul3[state[13]] ^ state[14] ^ state[15]);
	temp[13] = (unsigned char)(state[12] ^ mul2[state[13]] ^ mul3[state[14]] ^ state[15]);
	temp[14] = (unsigned char)(state[12] ^ state[13] ^ mul2[state[14]] ^ mul3[state[15]]);
	temp[15] = (unsigned char)(mul3[state[12]] ^ state[13] ^ state[14] ^ mul2[state[15]]);
	for (int i = 0; i < 16; i++)
	{
		state[i] = temp[i]; 
	}
}

void AddRoundKey(unsigned char* state, unsigned char* roundKey) 
{
	for (int i = 0; i < 16; i++)
	{
		state[i] ^= roundKey[i]; 
	}
}





