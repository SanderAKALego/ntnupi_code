/*
* File:    bch3.c
* Title:   Encoder/decoder for binary BCH codes in C (Version 3.1)
* Author:  Robert Morelos-Zaragoza
* Date:    August 1994
* Revised: June 13, 1997
*
* ===============  Encoder/Decoder for binary BCH codes in C =================
*
* Version 1:   Original program. The user provides the generator polynomial
*              of the code (cumbersome!).
* Version 2:   Computes the generator polynomial of the code.
* Version 3:   No need to input the coefficients of a primitive polynomial of
*              degree m, used to construct the Galois Field GF(2**m). The
*              program now works for any binary BCH code of length such that:
*              2**(m-1) - 1 < length <= 2**m - 1
*
* Note:        You may have to change the size of the arrays to make it work.
*
* The encoding and decoding methods used in this program are based on the
* book "Error Control Coding: Fundamentals and Applications", by Lin and
* Costello, Prentice Hall, 1983.
*
* Thanks to Patrick Boyle (pboyle@era.com) for his observation that 'bch2.c'
* did not work for lengths other than 2**m-1 which led to this new version.
* Portions of this program are from 'rs.c', a Reed-Solomon encoder/decoder
* in C, written by Simon Rockliff (simon@augean.ua.oz.au) on 21/9/89. The
* previous version of the BCH encoder/decoder in C, 'bch2.c', was written by
* Robert Morelos-Zaragoza (robert@spectra.eng.hawaii.edu) on 5/19/92.
*
* NOTE:
*          The author is not responsible for any malfunctioning of
*          this program, nor for any damage caused by it. Please include the
*          original program along with these comments in any redistribution.
*
*  For more information, suggestions, or other ideas on implementing error
*  correcting codes, please contact me at:
*
*                           Robert Morelos-Zaragoza
*                           5120 Woodway, Suite 7036
*                           Houston, Texas 77056
*
*                    email: r.morelos-zaragoza@ieee.org
*
* COPYRIGHT NOTICE: This computer program is free for non-commercial purposes.
* You may implement this program for any non-commercial application. You may
* also implement this program for commercial purposes, provided that you
* obtain my written permission. Any modification of this program is covered
* by this copyright.
*
* == Copyright (c) 1994-7,  Robert Morelos-Zaragoza. All rights reserved.  ==
*
* m = order of the Galois field GF(2**m)
* n = 2**m - 1 = size of the multiplicative group of GF(2**m)
* length = length of the BCH code
* t = error correcting capability (max. no. of errors the code corrects)
* d = 2*t + 1 = designed min. distance = no. of consecutive roots of g(x) + 1
* k = n - deg(g(x)) = dimension (no. of information bits/codeword) of the code
* p[] = coefficients of a primitive polynomial used to generate GF(2**m)
* g[] = coefficients of the generator polynomial, g(x)
* alpha_to [] = log table of GF(2**m)
* index_of[] = antilog table of GF(2**m)
* data[] = information bits = coefficients of data polynomial, i(x)
* bb[] = coefficients of redundancy polynomial x^(length-k) i(x) modulo g(x)
* numerr = number of errors
* errpos[] = error positions
* recd[] = coefficients of the received polynomial
* decerror = number of decoding errors (in _message_ positions)
*
*/

#include <math.h>
#include <stdio.h>

struct bch
{
    int iM = 10;
    int iLength = 1023;
    int iK = 121;
    int iT = 171;
    int iN = 1023;
    int array_p[21];
    int alpha_to[1023];
    int index_of[1023];
    int array_g[1000];  //0---902 used
    
    void Init(int OutputLength, int KeyLength, int Tolerance)
    {
        iLength = OutputLength;
        iN = OutputLength;
        iK = KeyLength;
        iT = Tolerance;
        Read_primitive_polynomial(iM, iLength, array_p);
        Generate_GF(alpha_to, index_of, iM, iN, array_p);
        Generator_Polynomial(iM, iN, iK, iLength, iT, alpha_to, index_of, array_g);
    }
    void Encode2(u8 *Output, u8 *Input)
    {
        int array_KeyS[128];
        int array_redundacy[1000];
        int array_encoded_Code[1023];
        for(u32 i = 0; i < (u32)iLength; ++i)
        {
            array_KeyS[i] = (Input[i / 8] & (1 << (i % 8))) ? 1 : 0;
        }
        
        BCH_Encoder(iLength, iK, array_redundacy, array_KeyS, array_g);
        for (int i = 0; i < iLength - iK; i++)
            array_encoded_Code[i] = array_redundacy[i];
        for (int i = 0; i < iK; i++)
            array_encoded_Code[i + iLength - iK] = array_KeyS[i];
        
        for(int i = 0; i < iLength; i++)
        {
            Output[i / 8] |= ((array_encoded_Code[i]) << (i % 8));
        }
    }
    bool Decode2(u8 *Output, u8 *Input)
    {
        int array_ProbeSampleXOR[1023];
        for(u32 i = 0; i < (u32)iLength; ++i)
        {
            array_ProbeSampleXOR[i] = (Input[i / 8] & (1 << (i % 8))) ? 1 : 0;
        }
        
        BCH_Decoder(iN,iLength,iT,array_ProbeSampleXOR, alpha_to, index_of);
        int iCheck = 0;
        int array_revodered_key[128];
        for(int i = iLength - iK; i < iLength; i++)
        {
            /*if(array_KeyS[i - iLength + iK] != array_ProbeSampleXOR[i])
            {
                iCheck++;
            }*/
            array_revodered_key[i - iLength + iK] = array_ProbeSampleXOR[i];
        }
        for(int i = 0; i < iLength; i++)
        {
            Output[i / 8] |= ((array_revodered_key[i]) << (i % 8));
        }
        int i = 0;
        return(true);
    }
    
    void Read_primitive_polynomial(int m, int length, int p[21])
    {
        //int p[21];
        
        int n;
        
        int			i, ninf;
        
        /*printf("bch3: An encoder/decoder for binary BCH codes\n");
     printf("Copyright (c) 1994-7. Robert Morelos-Zaragoza.\n");
     printf("This program is free, please read first the copyright notice.\n");
     printf("\nFirst, enter a value of m such that the code length is\n");
     printf("2**(m-1) - 1 < length <= 2**m - 1\n\n");*/
        do {
            /*printf("Enter m (between 2 and 20): ");
         scanf_s("%d", &m);*/
        } while ( !(m>1) || !(m<21) );
        for (i=1; i<m; i++)
            p[i] = 0;
        p[0] = p[m] = 1;
        if (m == 2)			p[1] = 1;
        else if (m == 3)	p[1] = 1;
        else if (m == 4)	p[1] = 1;
        else if (m == 5)	p[2] = 1;
        else if (m == 6)	p[1] = 1;
        else if (m == 7)	p[1] = 1;
        else if (m == 8)	p[4] = p[5] = p[6] = 1;
        else if (m == 9)	p[4] = 1;
        else if (m == 10)	p[3] = 1;
        else if (m == 11)	p[2] = 1;
        else if (m == 12)	p[3] = p[4] = p[7] = 1;
        else if (m == 13)	p[1] = p[3] = p[4] = 1;
        else if (m == 14)	p[1] = p[11] = p[12] = 1;
        else if (m == 15)	p[1] = 1;
        else if (m == 16)	p[2] = p[3] = p[5] = 1;
        else if (m == 17)	p[3] = 1;
        else if (m == 18)	p[7] = 1;
        else if (m == 19)	p[1] = p[5] = p[6] = 1;
        else if (m == 20)	p[3] = 1;
        //printf("p(x) = ");
        n = 1;
        for (i = 0; i <= m; i++) {
            n *= 2;
            //printf("%1d", p[i]);
        }
        //printf("\n");
        n = n / 2 - 1;
        ninf = (n + 1) / 2 - 1;
        //do  {
        //	/*printf("Enter code length (%d < length <= %d): ", ninf, n);
        //	scanf_s("%d", &length);*/
        //} while ( !((length <= n)&&(length>ninf)) );
    }
    
    
    void Generate_GF(int alpha_to[1048576], int index_of[1048576], int m, int n, int p[21])
    {
        register int    i, mask;
        
        mask = 1;
        alpha_to[m] = 0;
        for (i = 0; i < m; i++) {
            alpha_to[i] = mask;
            index_of[alpha_to[i]] = i;
            if (p[i] != 0)
                alpha_to[m] ^= mask;
            mask <<= 1;
        }
        index_of[alpha_to[m]] = m;
        mask >>= 1;
        for (i = m + 1; i < n; i++) {
            if (alpha_to[i - 1] >= mask)
                alpha_to[i] = alpha_to[m] ^ ((alpha_to[i - 1] ^ mask) << 1);
            else
                alpha_to[i] = alpha_to[i - 1] << 1;
            index_of[alpha_to[i]] = i;
        }
        index_of[0] = -1;
    }
    
    void Generator_Polynomial(int m, int n, int k, int length, int t, int alpha_to[1048576], int index_of[1048576], int g[548576])
    {
        
        int d;
        
        register int	ii, jj, ll, kaux;
        register int	test, aux, nocycles, root, noterms, rdncy;
        int             cycle[1024][21], size[1024], min[1024], zeros[1024];
        
        /* Generate cycle sets modulo n, n = 2**m - 1 */
        cycle[0][0] = 0;
        size[0] = 1;
        cycle[1][0] = 1;
        size[1] = 1;
        jj = 1;			/* cycle set index */
        if (m > 9)  {
            /*printf("Computing cycle sets modulo %d\n", n);
      printf("(This may take some time)...\n");*/
        }
        do {
            /* Generate the jj-th cycle set */
            ii = 0;
            do {
                ii++;
                cycle[jj][ii] = (cycle[jj][ii - 1] * 2) % n;
                size[jj]++;
                aux = (cycle[jj][ii] * 2) % n;
            } while (aux != cycle[jj][0]);
            /* Next cycle set representative */
            ll = 0;
            do {
                ll++;
                test = 0;
                for (ii = 1; ((ii <= jj) && (!test)); ii++)	
                    /* Examine previous cycle sets */
                    for (kaux = 0; ((kaux < size[ii]) && (!test)); kaux++)
                    if (ll == cycle[ii][kaux])
                    test = 1;
            } while ((test) && (ll < (n - 1)));
            if (!(test)) {
                jj++;	/* next cycle set index */
                cycle[jj][0] = ll;
                size[jj] = 1;
            }
        } while (ll < (n - 1));
        nocycles = jj;		/* number of cycle sets modulo n */
        
        //printf("Enter the error correcting capability, t: ");
        //scanf_s("%d", &t);
        
        d = 2 * t + 1;
        
        /* Search for roots 1, 2, ..., d-1 in cycle sets */
        kaux = 0;
        rdncy = 0;
        for (ii = 1; ii <= nocycles; ii++) {
            min[kaux] = 0;
            test = 0;
            for (jj = 0; ((jj < size[ii]) && (!test)); jj++)
                for (root = 1; ((root < d) && (!test)); root++)
                if (root == cycle[ii][jj])  {
                test = 1;
                min[kaux] = ii;
            }
            if (min[kaux]) {
                rdncy += size[min[kaux]];
                kaux++;
            }
        }
        noterms = kaux;
        kaux = 1;
        for (ii = 0; ii < noterms; ii++)
            for (jj = 0; jj < size[min[ii]]; jj++) {
            zeros[kaux] = cycle[min[ii]][jj];
            kaux++;
        }
        
        k = length - rdncy;
        
        if (k<0)
        {
            ////printf("Parameters invalid!\n");
            //exit(0);
        }
        
        //printf("This is a (%d, %d, %d) binary BCH code\n", length, k, d);
        
        /* Compute the generator polynomial */
        g[0] = alpha_to[zeros[1]];
        g[1] = 1;		/* g(x) = (X + zeros[1]) initially */
        for (ii = 2; ii <= rdncy; ii++) {
            g[ii] = 1;
            for (jj = ii - 1; jj > 0; jj--)
                if (g[jj] != 0)
                g[jj] = g[jj - 1] ^ alpha_to[(index_of[g[jj]] + zeros[ii]) % n];
            else
                g[jj] = g[jj - 1];
            g[0] = alpha_to[(index_of[g[0]] + zeros[ii]) % n];
        }
        //printf("Generator polynomial:\ng(x) = ");
        //for (ii = 0; ii <= rdncy; ii++) 
        //{
        //  printf("%d", g[ii]);
        //  if (ii && ((ii % 50) == 0))
        //    //printf("\n");
        //}
        //printf("\n");
    }
    
    
    void BCH_Encoder(int length, int k, int bb[1000], int data[500], int g[1000])
    {
        register int    i, j;
        register int    feedback;
        
        for (i = 0; i < length - k; i++)
            bb[i] = 0;
        for (i = k - 1; i >= 0; i--) {
            feedback = data[i] ^ bb[length - k - 1];
            if (feedback != 0) {
                for (j = length - k - 1; j > 0; j--)
                    if (g[j] != 0)
                    bb[j] = bb[j - 1] ^ feedback;
                else
                    bb[j] = bb[j - 1];
                bb[0] = g[0] && feedback;
            } else {
                for (j = length - k - 1; j > 0; j--)
                    bb[j] = bb[j - 1];
                bb[0] = 0;
            }
        }
    }
    
    void BCH_Decoder(int n, int length, int t, int recd[1023], int alpha_to[1023],  int index_of[1023])
        //void GUC_BCH_Class::BCH_Decoder()
    {
        //	int n = 1023;
        //	int length=1023;
        //	int t=171;
        //	int recd[1023];
        //	int alpha_to[1023];
        //	int index_of[1023];
        //
        
        int iTemp = index_of[1023];
        int iTempSecond = alpha_to[1023];
        
        register int    i, j, u, q, t2, count = 0, syn_error = 0;
        int elp[350][350];
        //int             elp[1026][1024], d[1026], l[1026], u_lu[1026], s[1025];
        int d[1026], l[1026], u_lu[1026], s[1025];
        int             root[200], loc[200], reg[201];
        
        t2 = 2 * t;
        
        /* first form the syndromes */
        //printf("S(x) = ");
        for (i = 1; i <= t2; i++) 
        {
            s[i] = 0;
            for (j = 0; j < length; j++)
            { 
                if (recd[j] != 0)
                    s[i] ^= alpha_to[(i * j) % n];
            }
            if (s[i] != 0)
                syn_error = 1; /* set error flag if non-zero syndrome */
            
            //*
            // * Note:    If the code is used only for ERROR DETECTION, then
            // *          exit program here indicating the presence of errors.
            // */
            /* convert syndrome from polynomial form to index form  */
            s[i] = index_of[s[i]];
            //printf("%3d ", s[i]);
        }
        //printf("\n");
        
        if (syn_error) {	/* if there are errors, try to correct them */
            /*
       * Compute the error location polynomial via the Berlekamp
       * iterative algorithm. Following the terminology of Lin and
       * Costello's book :   d[u] is the 'mu'th discrepancy, where
       * u='mu'+1 and 'mu' (the Greek letter!) is the step number
       * ranging from -1 to 2*t (see L&C),  l[u] is the degree of
       * the elp at that step, and u_l[u] is the difference between
       * the step number and the degree of the elp. 
       */
            /* initialise table entries */
            d[0] = 0;			/* index form */
            d[1] = s[1];		/* index form */
            elp[0][0] = 0;		/* index form */
            elp[1][0] = 1;		/* polynomial form */
            for (i = 1; i < t2; i++) {
                elp[0][i] = -1;	/* index form */
                elp[1][i] = 0;	/* polynomial form */
            }
            l[0] = 0;
            l[1] = 0;
            u_lu[0] = -1;
            u_lu[1] = 0;
            u = 0;
            
            do {
                u++;
                if (u == 131)
                {
                    int iStopDebug = 0;
                }
                if (d[u] == -1) {
                    l[u + 1] = l[u];
                    for (i = 0; i <= l[u]; i++) {
                        elp[u + 1][i] = elp[u][i];
                        elp[u][i] = index_of[elp[u][i]];
                    }
                } else
                    /*
* search for words with greatest u_lu[q] for
* which d[q]!=0 
*/
                {
                    q = u - 1;
                    while ((d[q] == -1) && (q > 0))
                        q--;
                    /* have found first non-zero d[q]  */
                    if (q > 0) {
                        j = q;
                        do {
                            j--;
                            if ((d[j] != -1) && (u_lu[q] < u_lu[j]))
                                q = j;
                        } while (j > 0);
                    }
                    
                    /*
         * have now found q such that d[u]!=0 and
         * u_lu[q] is maximum 
         */
                    /* store degree of new elp polynomial */
                    if (l[u] > l[q] + u - q)
                        l[u + 1] = l[u];
                    else
                        l[u + 1] = l[q] + u - q;
                    
                    /* form new elp(x) */
                    for (i = 0; i < t2; i++)
                    {
                        elp[u + 1][i] = 0;
                    }
                    for (i = 0; i <= l[q]; i++)
                    {
                        if (elp[q][i] != -1)
                        {
                            elp[u + 1][i + u - q] = alpha_to[(d[u] + n - d[q] + elp[q][i]) % n];
                        }
                    }
                    for (i = 0; i <= l[u]; i++) 
                    {
                        elp[u + 1][i] ^= elp[u][i];
                        elp[u][i] = index_of[elp[u][i]];
                    }
                }
                u_lu[u + 1] = u - l[u + 1];
                
                /* form (u+1)th discrepancy */
                if (u < t2) 
                {	
                    /* no discrepancy computed on last iteration */
                    if (s[u + 1] != -1)
                    {
                        d[u + 1] = alpha_to[s[u + 1]];
                    }
                    else
                    {
                        d[u + 1] = 0;
                    }
                    for (i = 1; i <= l[u + 1]; i++)
                    {
                        if ((s[u + 1 - i] != -1) && (elp[u + 1][i] != 0))
                        {
                            int iTemp_one = s[u + 1 - i];
                            int iTemp_Two = elp[u + 1][i];
                            int iTemp_Three = index_of[elp[u + 1][i]];
                            int iTemp_Four = s[u + 1 - i] + index_of[elp[u + 1][i]];
                            int iTemp_Five = (s[u + 1 - i] + index_of[elp[u + 1][i]]) % n;
                            int iTemp_Six = alpha_to[(s[u + 1 - i] + index_of[elp[u + 1][i]]) % n];
                            
                            
                            
                            
                            d[u + 1] ^= alpha_to[(s[u + 1 - i] + index_of[elp[u + 1][i]]) % n];
                            
                            
                            int iTempSeven = d[u + 1];
                            
                            int iTempStop = 0;
                            
                            /*printf("Characters: %c %c \n", 'a', 65);
          printf("index value: d%\n", i);
          printf("d values: d%\n", d[u + 1]);
          printf("d%", iTemp_one);
          printf("d%", iTemp_Two);
          printf("d%", iTemp_Three);
          printf("d%", iTemp_Four);
          printf("d%", iTemp_Five);
          printf("d%", iTemp_Six);*/
                            
                        }
                    }
                    /* put d[u+1] into index form */
                    d[u + 1] = index_of[d[u + 1]];	
                }
            } while ((u < t2) && (l[u + 1] <= t));
            
            u++;
            if (l[u] <= t) 
            {/* Can correct errors */
                /* put elp into index form */
                for (i = 0; i <= l[u]; i++)
                {
                    elp[u][i] = index_of[elp[u][i]];
                }
                
                /*printf("sigma(x) = ");
       for (i = 0; i <= l[u]; i++)
        printf("%3d ", elp[u][i]);
       printf("\n");*/
                //printf("Roots: ");
                
                /* Chien search: find roots of the error location polynomial */
                for (i = 1; i <= l[u]; i++)
                {
                    reg[i] = elp[u][i];
                }
                count = 0;
                for (i = 1; i <= n; i++)
                {
                    q = 1;
                    for (j = 1; j <= l[u]; j++)
                    {
                        if (reg[j] != -1)
                        {
                            reg[j] = (reg[j] + j) % n;
                            q ^= alpha_to[reg[j]];
                        }
                    }
                    if (!q) 
                    {	/* store root and error
                                                                                                                                           * location number indices */
                        root[count] = i;
                        loc[count] = n - i;
                        count++;
                        //printf("%3d ", n - i);
                    }
                }
                //printf("\n");
                if (count == l[u])	
                    /* no. roots = degree of elp hence <= t errors */
                    for (i = 0; i < l[u]; i++)
                    recd[loc[i]] ^= 1;
                else{	/* elp has degree >t hence cannot solve */
                    //printf("Incomplete decoding: errors detected\n");
                }
            }
        }
    }
};