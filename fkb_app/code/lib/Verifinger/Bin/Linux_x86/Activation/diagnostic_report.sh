#!/bin/bash

if [ "${UID}" != "0" ]; then
	echo "-------------------------------------------------------------------"
	echo "  WARNING: Please run this application with superuser privileges."
	echo "-------------------------------------------------------------------"
	SUPERUSER="no"
fi

if [ "`uname -m`" = "x86_64" ]; then 
	PLATFORM="Linux_x86_64"
else
	PLATFORM="Linux_x86"
fi

TMP_DIR="/tmp/`basename $0 .sh`/"
BIN_DIR="${TMP_DIR}Bin/${PLATFORM}/"



SCRIPT_DIR="`dirname "$0"`"

if [ "${SCRIPT_DIR:0:1}" != "/" ]; then
	SCRIPT_DIR="${PWD}/${SCRIPT_DIR}"
fi

SCRIPT_DIR="`cd ${SCRIPT_DIR}; pwd`/"


if [ "$1" == "" ]; then
	OUTFILE="${SCRIPT_DIR}/`basename $0 .sh`.log"
else
	OUTFILE="$1"
fi

COMPONENTS_DIR="${SCRIPT_DIR}../../../Lib/${PLATFORM}/"

if [ -d "${COMPONENTS_DIR}" ]; then
	COMPONENTS_DIR="`cd ${COMPONENTS_DIR}; pwd`/"
else
	COMPONENTS_DIR=""
fi

function log_message()
{
	echo "$1" >> ${OUTFILE};
}

function init_diagnostic()
{
	echo "================================= Diagnostic report =================================" > ${OUTFILE};
	echo "Time: $(date)" >> ${OUTFILE};
	echo "" >> ${OUTFILE};
	echo "Genarating diagnostic report...";
}

function gunzip_tools()
{
	mkdir -p ${TMP_DIR}
	SKIP=$[`cat -t $0 | grep -nx END_OF_SCRIPT | sed 's/:.*.$//g'` + 1];
	tail -n +${SKIP} $0 | gzip -cd 2> /dev/null | tar xvf - -C ${TMP_DIR} &> /dev/null;
}

function check_platform()
{
	if [ ! -d ${BIN_DIR} ]; then
		echo "This tool is built for $(ls $(dirname ${BIN_DIR}))" >&2;
		echo "" >&2;
		echo "Please make sure you running it on correct platform." >&2;
		return 1;
	fi

	return 0;
}

function end_diagnostic()
{
	echo "";
	echo "Diganostic report is generated and saved to:"
	echo "   '${OUTFILE}'";
	echo "";
	echo "Please send file '`basename ${OUTFILE}`' with problem description to:";
	echo "   support@neurotechnology.com";
	echo "   linux@neurotechnology.com";
	echo "";
	echo "Thank you for using our products";
}

function clean_up_diagnostic()
{
	rm -rf ${TMP_DIR}
}

function linux_info()
{
	log_message "============ Linux info =============================================================";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Uname:";
       	log_message "`uname -a`";
	log_message "";
	DIST_RELEASE="`ls /etc/*-release 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/*_release 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/*-version 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/*_version 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/release 2> /dev/null`"
	log_message "-------------------------------------------------------------------------------------";
	log_message "Linux distribution:";
	echo "${DIST_RELEASE}" | while read dist_release; do 
		log_message "${dist_release}: `cat ${dist_release}`";
	done;
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Pre-login message:";
	log_message "/etc/issue:";
	log_message "`cat -v /etc/issue`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Linux kernel headers version:";
	log_message "/usr/include/linux/version.h:"
	log_message "`cat /usr/include/linux/version.h`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Linux kernel modules:";
	log_message "`cat /proc/modules`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "File systems supported by Linux kernel:";
	log_message "`cat /proc/filesystems`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Enviroment variables";
	log_message "`env`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	if [ -x `which gcc` ]; then
		log_message "GNU gcc version:";
		log_message "`gcc --version 2>&1`";
		log_message "`gcc -v 2>&1`";
	else
		log_message "gcc: not found";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "GNU glibc version: `${BIN_DIR}glibc-version 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "GNU glibc++ version:";
	log_message "`strings /usr/lib/libstdc++.so | grep 'GLIBCXX_[[:digit:]]'`";
	log_message "`strings /usr/lib/libstdc++.so | grep 'CXXABI_[[:digit:]]'`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "libusb version: `libusb-config --version 2>&1`";
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}


function hw_info()
{
	log_message "============ Harware info ===========================================================";
	log_message "-------------------------------------------------------------------------------------";
	log_message "CPU info:";
	log_message "/proc/cpuinfo:";
	log_message "`cat /proc/cpuinfo 2>&1`";
	log_message "";
	log_message "dmidecode -t processor";
	log_message "`${BIN_DIR}dmidecode -t processor 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Memory info:";
	log_message "`cat /proc/meminfo 2>&1`";
	log_message "";
	log_message "dmidecode -t 6,16";
	log_message "`${BIN_DIR}dmidecode -t 6,16 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "HDD info:";
	if [ -f "/proc/partitions" ]; then
		log_message "/proc/partitions:";
		log_message "`cat /proc/partitions`";
		log_message "";
		HD_DEV=$(cat /proc/partitions | sed -n -e '/\([sh]d\)\{1\}[[:alpha:]]$/ s/^.*...[^[:alpha:]]//p')
		for dev_file in ${HD_DEV}; do
			HDPARM_ERROR=$(/sbin/hdparm -I /dev/${dev_file} 2>&1 >/dev/null);
			log_message "-------------------";
			if [ "${HDPARM_ERROR}" = "" ]; then
				log_message "$(/sbin/hdparm -I /dev/${dev_file} | head -n 7 | sed -n -e '/[^[:blank:]]/p')";
			else	
				log_message "/dev/${dev_file}:";
				log_message "vendor:       `cat /sys/block/${dev_file}/device/vendor 2> /dev/null`";
				log_message "model:        `cat /sys/block/${dev_file}/device/model 2> /dev/null`";
				log_message "serial:       `cat /sys/block/${dev_file}/device/serial 2> /dev/null`";
				if [ "`echo "${dev_file}" | sed -n -e '/^h.*/p'`" != "" ]; then
					log_message "firmware rev: `cat /sys/block/${dev_file}/device/firmware 2> /dev/null`";
				else
					log_message "firmware rev: `cat /sys/block/${dev_file}/device/rev 2> /dev/null`";
				fi
			fi
			log_message "";
		done;
	fi
	log_message "-------------------------------------------------------------------------------------";
	log_message "PCI devices:";
	log_message "lspci:";
	log_message "`/usr/sbin/lspci 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "USB devices:";
	if [ -f "/proc/bus/usb/devices" ]; then
		log_message "/proc/bus/usb/devices:";
		log_message "`cat /proc/bus/usb/devices`";
	else
		log_message "ERROR: usbfs is not mounted";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Network info:";
	log_message "";
	log_message "--------------------";
	log_message "Network interfaces:";
	log_message "$(/sbin/ifconfig -a 2>&1)";
	log_message "";
	log_message "--------------------";
	log_message "IP routing table:";
	log_message "$(/sbin/route -n 2>&1)";
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}

function sdk_info()
{
	log_message "============ SDK info =============================================================";
	log_message "";
	if [ "${SUPERUSER}" != "no" ]; then
		ldconfig
	fi
	if [ "${COMPONENTS_DIR}" != "" -a -d "${COMPONENTS_DIR}" ]; then
		log_message "Components' directory: ${COMPONENTS_DIR}";
		log_message "";
		log_message "Components:";
		ls ${COMPONENTS_DIR}lib*.so | 		while read comp_file; do
			comp_filename="$(basename ${comp_file})";
			COMP_LIBS_INSYS="$(ldconfig -p | sed -n -e "/${comp_filename}/ s/^.*=> //p")";
			COMP_INFO_FUNC="$(echo ${comp_filename} | sed -e 's/^lib//' -e 's/[.]so$//')GetInfoA";
			if [ "${COMP_LIBS_INSYS}" != "" ]; then
				echo "${COMP_LIBS_INSYS}" |
				while read sys_comp_file; do
					log_message "$(if ! (${BIN_DIR}component-version ${sys_comp_file} ${COMP_INFO_FUNC} 2>/dev/null); then echo "ERROR: can't get info about component ${sys_comp_file}"; fi)";
				done
			fi
			log_message "$(if !(LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${COMPONENTS_DIR} ${BIN_DIR}component-version ${comp_filename} ${COMP_INFO_FUNC} 2>/dev/null); then echo "ERROR: can't get info about component ${comp_filename}"; fi)";
		done
	else
		log_message "Can't find components' directory";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------"
	log_message "Licensing config file NLicenses.cfg:";
	log_message "$(cat ${SCRIPT_DIR}../NLicenses.cfg)";
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}

function prot_info()
{
	PGD_PID="`ps -eo pid,comm= | awk '{if ($0~/pgd$/) { print $1 } }'`"
	PGD_UID="`ps n -eo user,comm= | awk '{if ($0~/pgd$/) { print $1 } }'`"


	log_message "============ PGD info ==============================================================="
	log_message ""
	log_message "-------------------------------------------------------------------------------------"
	if [ "${PGD_PID}" = "" ]; then
		echo "----------------------------------------------------"
		echo "  WARNING: pgd is not running."
		echo "  Please start pgd and run this application again."
		echo "----------------------------------------------------"
		log_message "PGD is not running"
		log_message "-------------------------------------------------------------------------------------"
		log_message ""
		log_message "=====================================================================================";
		log_message "";
		return
	fi
	log_message "PGD is running"
	log_message "procps:"
	PGD_PS="`ps -p ${PGD_PID} u`"
	log_message "${PGD_PS}"

	if [ "${PGD_UID}" = "0" -a "${SUPERUSER}" = "no" ]; then
		echo "------------------------------------------------------"
		echo "  WARNING: pgd was started was superuser privileges."
		echo "           Can't collect information about pgd."
		echo "           Please restart this application with"
		echo "           superuser privileges."
		echo "------------------------------------------------------"
		log_message "PGD was started with superuser privileges. Can't collect information about pgd."
		log_message "-------------------------------------------------------------------------------------"
		log_message ""
		log_message "=====================================================================================";
		log_message "";
		return
	fi

	if [ "${SUPERUSER}" = "no" ]; then
		if [ "${PGD_UID}" != "${UID}" ]; then
			echo "--------------------------------------------------"
			echo "  WARNING: pgd was started with different user"
			echo "           privileges. Can't collect information"
			echo "           about pgd."
			echo "           Please restart this application with"
			echo "           superuser privileges."
			echo "--------------------------------------------------"
			log_message "PGD was started with different user privileges. Can't collect information about pgd."
			log_message "-------------------------------------------------------------------------------------"
			log_message ""
			log_message "=====================================================================================";
			log_message "";
			return
		fi
	fi

	PGD_CWD="`readlink /proc/${PGD_PID}/cwd`"
	if [ "${PGD_CWD}" != "" ]; then
		PGD_CWD="${PGD_CWD}/"
	fi

	log_message "Path to pgd: `readlink /proc/${PGD_PID}/exe`"
	log_message "Path to cwd: ${PGD_CWD}"


	PGD_LOG_FILE="`cat /proc/${PGD_PID}/cmdline | awk -F'\0' '{ for(i=2;i<NF;i++){ if ($i=="-l") { print $(i+1) } } }'`"
	if [ "${PGD_LOG_FILE}" = "" ]; then
		PGD_LOG_FILE="/tmp/pgd.log"
	else
		if [ "${PGD_LOG_FILE:0:1}" != "/" ]; then
			PGD_LOG_FILE="${PGD_CWD}${PGD_LOG_FILE}"
		fi
	fi

	PGD_CONF_FILE="`cat /proc/${PGD_PID}/cmdline | awk -F'\0' '{ for(i=2;i<NF;i++){ if ($i=="-c") { print $(i+1) } } }'`"
	if [ "${PGD_CONF_FILE}" = "" ]; then
		PGD_CONF_FILE="${PGD_CWD}pgd.conf"
	else
		if [ "${PGD_CONF_FILE:0:1}" != "/" ]; then
			PGD_CONF_FILE="${PGD_CWD}${PGD_CONF_FILE}"
		fi
	fi

	log_message "-------------------------------------------------------------------------------------";
	log_message "PGD log file: ${PGD_LOG_FILE}"
	log_message "PGD log:";
	PGD_LOG="`cat ${PGD_LOG_FILE}`"
	log_message "${PGD_LOG}";
	log_message "-------------------------------------------------------------------------------------";
	log_message "PGD confif file: ${PGD_CONF_FILE}";
	log_message "PGD config:";
	if [ -f "${PGD_CONF_FILE}" ]; then
		PGD_CONF="`cat ${PGD_CONF_FILE}`";
		log_message "${PGD_CONF}";
	else
		log_message "PGD configuration file not found";
		PGD_CONF="";
	fi
	log_message "-------------------------------------------------------------------------------------";
	log_message "";
	PGD_LICENCEUSAGELOG_FILE="`echo "${PGD_CONF}" | grep -v '^#' | grep -i LicenceUsageLogFile | sed -n 's/^.*.=//g; s/^\ //p'`";
	if [ "${PGD_LICENCEUSAGELOG_FILE}" = "" ]; then
		PGD_LICENCEUSAGELOG_FILE="${PGD_CWD}LicenceUsage.log";
	else
		if [ "${PGD_LICENCEUSAGELOG_FILE:0:1}" != "/" ]; then
			PGD_LICENCEUSAGELOG_FILE="${PGD_CWD}${PGD_LICENCEUSAGELOG_FILE}"
		fi
	fi
	log_message "PGD Licence Usage Log file: ${PGD_LICENCEUSAGELOG_FILE}";
	log_message "";
	log_message "PGD Licence Usage Log:";
	if [ -f "${PGD_LICENCEUSAGELOG_FILE}" ]; then
		log_message "`cat ${PGD_LICENCEUSAGELOG_FILE}`";
	else
		log_message "license log file not found";
	fi
	log_message "-------------------------------------------------------------------------------------";
	log_message "";
	log_message "PGD licenses:"
	if [ "${PGD_CONF}" != "" ]; then
		PGD_LICS="`echo "${PGD_CONF}" | grep -v '^#' | grep -i licence | grep -v LicenceUsageLogFile | sed -n 's/^.*.=//g; s/^\ //p'`";
		PGD_LICS+=" `echo "${PGD_CONF}" | grep -v '^#' | grep -i license | sed -n 's/^.*.=//g; s/^\ //p'`";
		echo "${PGD_LICS}" | 		while read lic_file; do
			if [ "`echo "${lic_file}" | sed -n '/^\//p'`" != "" ]; then
				LIC_FILE="${lic_file}";
			else
				LIC_FILE="${PGD_CWD}/${lic_file}";
			fi
			if [ -f ${LIC_FILE} ]; then
				log_message "License file: ${LIC_FILE}";
				log_message "`cat "${LIC_FILE}"`";
				log_message "";
			fi
		done
	else
		log_message "PGD licenses not found";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "";
	log_message "Computer ID:"
	if [ -x "./id_gen" ]; then
		echo "B042-8724-A90E-B69E-42DE-C112-A191-A696" > ${TMP_DIR}sn.txt;
		./id_gen ${TMP_DIR}sn.txt ${TMP_DIR}id.txt &> /dev/null;
		log_message "`cat ${TMP_DIR}id.txt`";
	else
		log_message "id_gen not found"
	fi
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}

function trial_info() {
	return
}

# main

gunzip_tools;

if ! check_platform; then
	clean_up_diagnostic;
	exit 1;
fi

init_diagnostic;

linux_info;

hw_info;

sdk_info;

prot_info;

trial_info;

clean_up_diagnostic;

end_diagnostic;

exit 0;

END_OF_SCRIPT
� �v�M �[tTՙ�o�$C���b}�D�#�L��`���s�̼dF'3����t�t3M��]��=���ZE���F�**g7K=��n[����ҳ��ј����yLV`{����}��}���������"����kVm-ƮY����n�\���ܳf��A���UG���_5Bq�SUҜ��q-�A��§��W�BW�rV-�/���񯩩�:�W�b�,Nl�l�]wYf�ŏ������y�}��h$�����`$|	����wU׹��?�]���}�����?ָ�EQ2�B����6���q,�MT�O��k�d�G1�v��p�1�!� X!�����6�	�'p��� /[�
X~R��4�Y�A���8��$�O�A��x��t(�
X�h+N��\}!�W���U!���
g<�a�ż��^���h���xc HrE{���"�瀐��n,�B�rԭ�ǻ �B���
������P�W���/�����0��9���B��q,�/8ވmI
���#�ު0��R�UN���C\�>��Xg�C�P$�A��F<K7x��Z�q]�5���'Ok[$����{<��ja�C	u$��}o��Dc���B�_�!�tҲ1�5�m
�ĳ����=��� ep�m�`�,^�da���Y��s�J�?�f��=l>(da��Z�����۬@���*�t�.Oc��
�x�86�v(�,�|0�1N&Hχ�m�Dr`��c�\��D+�&�$�a�M����]�<�1�H}�E���'��wH��4���<�X�t%� ������Ě�vj�b�Q �r��b�Y`�(��kN��k����b�q ��S�c��G)��#~�blI�N��)��#�P�z������8qz��{���g�}��o�)0���~h�����ց�����0�:�����p���wz����}�J$z�GS:�m��
 }lF�ŝ��eG����2��~�� �7c޶U`ZzP��O�nt$U8:`���mN���"�u$�� �㖃�ԷD�Op�%�U؀ZͲ������k��'WV���`S��ǁ*�s�&�kH��K?:5��i�֡z�y�Y�Wۊ:_�L��V1�;��om�
��G�]A��%*�^����Ő!}�����)ݝ��ݤ��!vW�ݙbw�ݝ'�]aO��3z�B�)�O�=�̻&�!��"����j��W�}p}�wx���S���i��6���)����i��;9������Ԫ�.�����S�p�݆c=h맆�S��H%	�}f~m����R�ýy~�_S
�������c���c������o���D�#i��gI��z7�'��χ���O��µz� �˻���6��z�SǄ>I���d�i}�+����:�ޘ����Ď�l�}�t⿄��)r}�^G� ��M��`���#[z��
��3�]�;�{�z.�j/��X�����UM7�Q+�j=<b������`�%��$�>���#]���`��k�����ijK0��ao��n�5��"����Y4*����EbsTx 6GB���45�ՖH"�<4����y��t���R����;�?@�'�����JE
�,C}�tB�MNt�����[��ҵ�O��u�O�.��k�<��!.|\=@�>� �	!:l��q)χ�">P`�GVB:T-�yOC��`ϡ��&=k�mY
a��!|�7!<�^��.��n�)�����A�Yu;]�������:=�bZH�Ƶ��鋴�7Ly\3j\7��9/cNg<�c���8A��bQ��9,\2C��g��sUJ�1�l'��Ε�hP�hHGɠ1:um��  R��սĩ<-1X����#`&:�,z���yۂ>( ��&��l�,�60�/f.��µ�k��Y
[3�6��s>j)L'����� ȇ��N�[.х�4���|��w�i�W!����$+�������s�f��е�����,�k���·��J+�����o"���4�O�ZY}�v��^'���s���%o���x]q������,s�=$�U _�M|����>�g���	��p�=����܍Ę4 l�H��d�o_�A���İg��]�����+�_�(|OK|����R�.�V�v}�����[I�E��0�@�����������W�S�X�����^>�U=6w{J$��_���H�4��-(�mr.�����V4�/�������ܳf��u�����+r�����`���X���M%7\r��ȈA��Ѵ���Xa�d��>���#�1�_���/�F�}K\b�k>/G��<�H�O�ñ��	�������'���흆O��$O�S�?�n4���f�yZ5�9�Ğ���u�/�L�x��Ÿ��B�?{���p���8=��t|{�m�c~��>����)�}�/����Ÿ�� "eC����ԇ��-}�߅�e� ���$�߅%��.,Y�wa�<d�T;�����?�(Ց�?u�4�?��9�S{�$���>��H���.���>u�
Ө*�Դ�c�85
M���?f]�C@�.���_�P�
J�ۈ����8U��*�?�_������0��*�I��pSB��/��`C��$�������q�`��ocv��3�����-��$��p�����|���ۘ~x��q��v��m��� ޶�g��-�o���K!���0��J�����9�6	۵��z�Ti�o��RE��ֺ���|�vk�)�_����>��UN����!l�`z���u�r��܋�Ϊ��_�ƫ�3A��B-U�1�l"U�H�V�M@�g�0u�+�5Ι�:�?��=�i2��O�n�.hR��6�(�=�e�ky��٭�
�	�&��[�tU�W1)���Q�P�,s1�n��2��-��v��|�R�)�hۻM��_���=X�wh�����Z��	�Z���K�c�����-��`�RWP)!�y%J�bu\�tL����8�~+�Ok�xhC��;�v��V�u�[�R猝;vɄ�Z��Md�*�fI����n�A����q�pMZ�1�	��N{�'���(3���*�Z'��הO]�֢�����s�&��r��
�K���b�"cD�E���:�c���yk�[[��֔���z7�@�g���>�39�%�]�z��z�d�d�����T��9�p�*��x �U\�H�U�?��x�5��U�f۠eґ�NuVv�D��on�~V�o���:�Zs���yíZ\�h�_�d0��ɰ67Ǵ����{l�_ב���*�?.Tf?�[�������k'��q�B?.��
�+��[L?.��,�~W��]I?.�;,L���7�E}����f?nH�C��(�g��&��O�mdؒۏ���`��YY�̇�k���d`�u�ć��ߞۏ�Ę���ngg��|OH|���k��O}�~	�G���W��v߮Q��'�!�w�r�ÏK�3v�1�q_�������}M�C�E�c:��o@��	|;�r�q�!�m޽9���H���r�q���7���:q������ *%,h��~	|+-��Y�l>L�9����|/�h��'|�����s�<h�~�j���F�oK�&�@	^ngz;�)bc�wv2!R����q�$4?㚞���;3�	ܒ�l��3��Na�Z�wnG�S|8�٨��1L^��e���7������av�=�����`����lwٕ�%��`z�.cg[�%�'0;M�g0�����L^�;%�I&<ل�5��Lx�	_o�_6�LX5��yb#H��t�2�9&��Yk�-0^��踿��P`<pܕ�%tۛ���a���ї�?A��O�'�!>.���o�|<�S*�����Q��G���g���8Ww����H��]&��}��R,���b�C�E�{�@1�b����i����΃%���X&�	��?zA�v!��b��RX7��G���X������k���,�|/��� ���vN�����-�zQ��g8]�柷����9]���4�w����RH~x�D?a������}�F}�!U��P�ۄ��98�*�M���!�(�o��.�7��=�m�-- ��iXyϽ�eK�Vz<�e���g�B�����u5.��u��#��P���PS��Ml"�D��mm�E�w/2�pǽ�j� ,L�R}���7�����h���Z� Eb�E�n�hav�6��E�Y�입�y�㎦ƕ��.k�d��-QO`#�+��aH�k�r�$Bs�|4���&S�Lf������軟s�B�	Zd���z���6��)+U�f��nv�k�y�@"���ya��m��zq���T�8u�'q����ۂ~��k��z����w�r׺]����u�\W��_�k���x6�:�?`������A~�O���֡.`z�?�A�a�cx��8j��٢v�?jը��x<���#4b|~����j�|�eˑV��ϱ�7�nh���P�뇗�-��z��|^��<��<��\�R��p����&i#������?�d��>����Ώ�&��g�i��y�.�)Z����G<�8_%t������;8���].��9�%��p��q��-�je�w��9�ɱ���9^a��^̷����<��.zU⿈��{)P}�p��d$
O�H���%�m�(i��8i��|��$��1s�I��E|���V(���D�C��ao��av�2>Ȋ����k��c/�����V����J@ZH��/eOӤ&�ʀ�������+���3)%�L���f�cN߹��Ȼ��W{G�d6>v�$�m��C/`�� �0�1�E��0��1�E~cX�G0�~c�l1�M�}��&bοb��q���'0�ä�6��æpcظ�b��0ƸI@��ac�a����3gh~��g����Z��ݐ3]�~�?ҕ�b��\>C�= �������`��gh�g�oư��oư��oư��oư��oư�o�ҕؓ<|��b���C�U��gؠS�c�U|�ܨĞ�+�~Ô�����S�=���G �K�O1�D`'m?�8"�ݴ������S�#x���b��A�~�q����#ƹ��Qt�C����l�T~��F����}7�y�
(-sި@����y#�K獎�c�|�����~Z���¢W��m��5v���=C.��1�U��@m�6�,ǋ:Ύy��?+�����-z��cp�[� ??���Y������C#GV��2A�rɏ��E��rt���vc	�Y��^�a̒g7�m�!�����hX2D*���3��rg篟�<�u(�#��l{�޶ᚑC��i���,�	Qg���g���%�ѭ�]��n3苲髶�'O�v��3^@X��@I��:2y��u��#�g�\S�I��܊JF{rP��\�9��4tGj�0~���Nⷐ�8�T�>�^��Z�v�|��m�m��ގ��=��\WaK�y�C��?������)��Q������U��b�:O���'�כ��j��h��m����F�1�Ed��@ɏ>��>s��G���'r��G���� x���}���oя� �g@��.S�O��S�~�h����"����4<Us�L�|���L=ț�2��?�yxTE�7~;�&�\vPQ/
N	$!�%K��D� .�6I���t�^�8��$@c�q�q�qe�W6@Ee��#���,���9U��y������proU��:u��9��[}��	�P�/��?P���3���<ϚPO�$a�9��M�rR�N_��Hy��T	���IS��[�3[K�k仙�+b�l�!����?�����L�RK��r����[܏�$B�)�W"�l��Ȯi��#�){j'�@_�����ѲjOŅ�F�,�dǉ��Б��!�IY�[X��߱�T�ѷO��5��M��C����H�����N��lO}�|ѐ$���	"vP+�i��IN)8!�o���Q{�W��[C�jO������l�Q�}���B޺=����쌞^�	���	�8�U�F�
�=.�S@1��1i�1��l�_�qu�|�{�������e]p\��������J�?��.��Ǿ��cQw�Kxg��y\ꀹyjU�o�
��ǈ���O����-�h�1I,uy}���X��
op9����ҟ%����y7�����{�W赇��5-v�_�.O�#�O��pp"6|�f�q%����ݮ�k��vS�=N�ߌ@����d�^H8Fl`��cq���u���w�f��'����d
D��g�[���pAC�Sؿ�έuM�N)���������Ό?��~�ZO^��P4{��]_��[�uY�
�����t<��s4�	x����d<I��3�D<����SÓ�K<��=��Ey/<c�3{<c�Cs<m$+�2������Ï�^����a�!h��G�?�#MS�A/�?��ڿ���ZO�����Z%��.�S���5ƨhq�z�i��Q���e��(9���q4e�!ݸԮ��:re�@Ym���*�!:�oG���-�A�kk�E�'���h�;��tdWd��sE�dm��cC�D��2��){���������Mi퉜O�;ڍ0}�J��-v�Z�U�,8�4\m]A}��������\�����̷�NF��fDcQ虼S��m�-��к<��Pǒ�f����͡�S�[샺�~�-�s�����-�/��N�?�����CHD��F\�ФmĖ���>uY����O���7�wє(���gR� � p���S��\d����d��n�"����6a$-�'�u�6~�E֊}]�-l��/���G�u���}ߎ�1'Ի�,�a��5�60���MQ�6�/
8Bj�܉26��6�W���Tm���h�a���c�F\�պ�s$��Z��\��&?Ji,�q�D��D\�o�u�� 5���lp�5�Ćg!�"|�a�0m��1_ܚ�P|�~O����ڞ*^I��"��7�0v�����#q{F�j��Vׁ1��Q�B�i�T^��@j��ʯ�X�����Ʊ��u�2�>�`b�JFr�ց!>L�I�"Q���RU��gh�P���1�Z�RYw��̇r99[���K��FZ��_����F�!���Z�d������ڛ��3�(ȋ�*�j���j]����7�ɰz׭(�����X��nk�Q���� )�.i9B]�Л�]L�e���y`�k��<+�,��2�ⲯ&=b�Ґ
'���>]]�$��M�7��5A� iz���$8�	��Kg�s͂}��u�٘q��:�/ا�x#}�]]�;�$�x?d�1���=K�;��Ȧ�'�_^O[лf�>��ڦ4f�`����8���9��X�1Ę2�(X�g���wWH�]��I�".R_��y]�i��agc޾H��h~>�Z�{�f�K�w�k�a�;���:����a	��/��}�_ΈH��ᢆ�Y>�i���,��Yn����PTk�hp�)�e���9�����Z�����v;&��))��-��g�ۅWt��]����0�ZfҐ�?r���j]_&&������<��V]x��t5��=|�֏ݜC�p��3F����XG`��S��)dzI�y!ӆ�\_����������실�;{���Q�(-[��j�o�k_���͛vn�J��$\� �܊p�;V��s�"���RQ�QIKkeB��Е ն�����/�����:G$ZI5fХ8��R�EޤNn��-%I��u��b�!������~:g�!"s�S]�H7�4�`"�����`�����_�"K��E�2p����!M=�#}xH�F����Q[S4�v���^J�]�g	��6�Ma�*�R^�Q魇J\#\/NW,G�}DG����C�E�;�P��h��u��N3��h'�{�қ��9����	��e	]���E�W,!?��7-�,�tfS�}^guYsC��{��\ŧ?��[F@7uᅧE�&���I���8J/u�S��Y��V^{��&�>k<Ss(��[S?ma�R��o�N�A�~��̕,�z���)҆7�T���Q_n�m�=f�zgtD��!aU��^���V��G�=@%~�R�r�5��s?���YB�F�WԼC1sˠ�{��ů\Ɇ1�'z��ʖ�+TdCU�y�Bԕ�<�!_������?�qq0J�s�Dێ1gZ��A���ދ2Ξ�.|�����?1�Eg�s��潫.��`=�=|�p��(H�������N$��C��b!d�D��'*O��:��'jT�J�m�q�G��D�'C�8�FgdA]�Uó��ft�mJ����X����I�D�e&�I��X>	��'Am\_f���Xח+���G}v�r�u��`]OuR붦c��{H�t��>��^@��s:!�	;�#'R�O�3�~�ua��p�Z��"�W��ԅ���B��3�לB�caG�q��8~Ԫ.�a�A?�;�����{��#��/���#���n�6��2K$��8b!��D�E�c%�iq
!�'!�!��b�ג�[:^�6�K�k�<�I:�KI3��T伯���1�\&���<�7s�I9\_3��H]��q�<��O<&���6�|���yު��$��w&����˔�_���V�g��G�d�����tG>Y�O�?��������9���2Mh�?X�J�c�$-�[�~�A�r�d<6p�O~�Q.���N]X�!_R��iz/��'H}6Њp�v�(dEP���?��}r}���c�/����Q�K؄�"�����f��Sք�®�¯~2�*�dv��b
�x�;�W��EP�iF#Vme=�}D6��j.�3�H���#z#O�/�_1��dˈ�$PA�Ʈ|A1��W���8$W��LZ�;,�ٯX�&"�J�ѵ��q6����?M�.�)~�k�sG)f�+x�@����[F����KV�2*�D�V}�
��^}���8�½��d�9�%�M�ȗ4#j�v1�1{�
sxG�h��O���!gu���%�[/K٣K���k�,�n��()P����]�*���nT��GFD:X����q��L��[���e7�{�~Bġ�L@������9S��!�����k�I'�/	o�����P���M����#NQ���tA��sH�֑�Ŧ�������8&��B<�e������YY�5��f���ÿ_����*^]֧��8��-�"u��������Y���S������u�pQ�(��R�)�i!q%��Pr�?��e�k���6�#��#[��ٶn��˶�o_�Z)^��5�){Z�V�ƠmM�#!�W:B=��>����f>͛k;�.�R��6��x��h�J������Ad_���p�\�`b1b�Jǳ�3���7xyOCd�ؤ��.���[�`	����R���1r�f�b�Z?�6P� ��Jm���eT++�o?���/�z��ު��BO=lu�����o��-b�:j�9�:ÎU�"��i%�u�c%5�v_�x�rNP\)�s��7YD��	-����,#�R��6G<��ZC;����wR#�}��zO�´�����צ^��s�~��~n؄�q�)�U�(�����.z�{!��L����YE�T�[$��1&���o�����@W��J��S��$�`���#�єH�����n��\o�Ȧ��L2A=:4Af�i���h̩�#E�&f�=�}o| ��,��~��j�tD�U/ٵ�I湓(d/�� 
I�����yJ�[*�jg���E�����#yi�6D��QE\�"u1{���W|w�F�XG��{�<�������)p��ܪ�U��?��D���?�>M���)�j��^Z�a�?�P79%{�)+���~Tא��,%>`����%�c��.�82!��p={�!�����G���D����'����Sl��pG�'�?3��[��ڡ�rl]l��z%��"��:[����G�5��(����5C�ȩJ��-�rTC
rէ�����?&C@]Ve�?��u�o��u���>�%k~�1�VcÚ������--ټ�%T�t����*.�����aR��UQ-��Ͻ����?aU�V�/��P��՘�J��:BT�Ef����F��Cl�܋�y�[e��[�t}^�k�>�v���,N���G�}"�d,��5����y&&oG��k��j�k?�.B\�S�M�E65��h�Z�*]�cX�0,h��	���>>r�1>S�#��S~o��<�S��X{��-=%�9����w�R����S]X�d�����wK6�RJD��݆/�[���A��l����Ж��xמ���X�넹ޏbn'�v3�࣢ԛvJ�R�ծ�LS�^����s���ߥr�w��'�6�~l��n��׽�{��n�E9u�7w�cv��t�.��@h��]���خ�]�uk����:_���"벵<b�h]P^�e�y�.s)�Fy�/��x�G]x$�TF/Z�u���5�b몺S��3��*�%�5ڒ-�s�.S=����J�Od���ڦ���a�l��ط~�♯G�^P�
Gɡ;��fm<P�=��,>*�&H��R�����eK�.�\*��qd\m�byܔ�q�3�9���h�[����՗���F՜�.��obm�פߪ.�u�3ͫ2P�Z7۰�-4�7�ḳ:���SߘVf��{��N�T!�<M�a^Q�?%c}'a��c��T�֝�� ��9�1V䱝L�J���i�wf4B�n�DQ��&�n{����uxmsΩx�)X�?6W<����vt�V\�bv1���E�$?_��?�DC.����ǒ,٣ͻ���-ˋ�ѵ<�ޓz�{[f��x��"������$��*&�}jR߅�o��ی��V2�D;Ic����3B�f��0�o�Ğ�Ϥ���#~G�I���IR�!������g���d�����,N��}�'x�sG���+�	�/�nt=[.}j��ݤ<Ǟ����fy�,*��AXϽ�N#b�vᏽt���L5Ύ�	�����^f��r͇��T��"V�S]ć&)c�Mٽ�iԦ��>��OF��5��˞'��_3P�92hޖ~�����t��.<�}��~A����Z>1��;�u���ns�o����&/`2%n�&��߾:K����?ۭ�0{�4�������Vձq�̐��V��c?I=�d�d];E���qG�I-��M2�ڞ��t�o�8ՙs���b�a�O�V���a�p�Yн�����N2Y	QU�oG�Eh_l���M�в��֭����o�[��%�t�VFs����e(�l9���"�U�����ԅ;�/���,��r�[�b�Ę�c6dsev���b=;5f�đ�,�Ϭf��H��].�"���C�@��[���P��в:�r
���{��G��G��r�VO��Y�U�<�M�Fk~�Ppb󾳬�:m3b/�Z�+�8uQ�b�_o��}(Ϛ�����GJ�$�?#3v݇b#/u�d��j�&ǐ�.�PSg�&��9��n�v��\���$�8�{��n�	���x�����u���/����F�|h���	OW�N7M���(�x2�����j�ԕ��m��n�1]��Q-&��>�D��������J��5tXq��u��LE֊��z�ފ���j����� 5���uyU���\�(a�eֹ�H�9j]���������c��(?ݢ��O���-B���,�6���}h��08j�]��#�A��a����t���#�B���T��[�Lt�����������-������������EV�/�/�O�蟅���}����M���}���G�i����|�}��l���E}V�/�玽������>Ie4����"�h��S�����P�e�Y��	�;���ޓ�	+��_9Y0��&:la�������t�c�1�䃔�<Q՗Ӿ�֢�)�S��Z��i��)2��g���AZB8̽�����Q:I�"6sc�t�{��
s���Z���lf��S�>���Ht����~��u����F����[k���)bZJSR���*j	��<u�}��;�5��8M����f�K���\������?5�U��&���|z���i���M���*�����t��ݢ�K���I��-�;�<��Mb���'������㜹��#Դ�7�����d�1���l�Z�Yj�K���'���ɴɽh��/�Ė��n>'��f��轍��g'K��-��H[ѽ>��%��+��4.oЉ����`�w�i.tI��g�S���冀��=zJ��l��|��J��brs�niBܹ�$|�>}��d��7��6*�F^�Tp������-[r���$�r����ê�� ���`�ua,����A���[�t�ֶ�vm���0�/	�ۭ��A�bXwq�I����~W���D��#�(�y���ıWߕg07�i������
����ړ��6K`�3�֮�4F�;ݛ�tﯹf�u�����I�\"U���l���m�v��uc�t_��5K����o�?�Ȳe �YV<��a��0�C1T�Dн���Lf�3�-&o���t�|�q�dC�Y�G�������Ò�o����Lu/�AV�d���3�pN���r�3����ubg�>���m��xZI_�B��B2�^���u�K��պ�����*���ga���2E�r?y��|!�s䘔�+/�߭瘭ݭ�^�1c�y�\�c�71�Ǵ���CzX1�]k
���r�'}�v�^����>>���Q�om;���6��Z���n�o�������=$��Y������*	�f��[+���h���=-��f��k���f��W �4�Y������4"�g����o���5���A���&����蘆��)�A���P?�$�����a�|`��h�4 s���T�^��J�#.q��y\<hݶ���n"��6��Oћ�$��W򚄝}���A=�}�zu��aEXA�M�ӊ���&���V�ӄ����+BR7�����E�� \ e�R ���K/7@��ֈ�3[e[����[�����[k]t-^�/���.���1�[�����%!yt���ڍ�w�v��oYm��YM�j��z3��������{F�J��kI�s�����Zth���\ג�%u��.\��!��
��ji���b��o��M��#�9���ղw�����i�����4o�t���wR/��]Ou{nX��>W5��nPGm�ԵΚ+�jݫ	b�}埐��Z{�t籵0h+oC���3�%~m$W?���j#�q�;&�:�Ø�]]�/ZN��~O��6[_�j������c�[��Ŵ�K���w�R�Rgh�ҘUo *�X��jH�R_ ���W���Ɣ���O�--Y�Y���b����@j��Ci��|���-ZrɇgQ/)܁"���g�o87['7��{|�Sx��&
�qoss CV?4A,;�eg;�t��B���?$�v}�egg��}�P��:��d���]�>^�Ɏ}�GBZ�}IZ
�
����PBV�)-�?�2�X�I٣.���XV�R��V�ͼgx+��{�XJ�i)e��JI�ĲR���]�����5t1X�;�5t^����;����}�!��ٲ���4�\[�DH�)�*dl��/ќ"51f���u�bh�¯�=˭��b���i��Y��[e�Q����#�����3��ZŪJ�u�+V}�oo���i|t�s�=��:ϣ}���Ʃm��ʇ�������>��B=e+��޷.�p���~,��f��M�� �ǘӫ!�a��}�ʽu%��Zw��V}�96z������o����#Tr�a�J>�B����#�����]+��9�SĬ�LY\���6���Oh{Vp�}N�_8�BX��D#� D�t�E�$��+ĉ�S���G(7r��y�[K�yVH*	Hv���s��7n�lO��1�?�q��{Ǡ��y�z�vr9h�p��I����od�9�4s�Ճ}s��� �F��Na�^n���l�n�T��D_p̻X׹�>�y	�_��F%����]g�N�c��Ww+�Yg�ݗK�r�n���j\g��\&��,x�ħc
:juq�M�,a���I0J5�v�$��.c�%��#���Seم~f g��ηL���M�~%�����M��H�ytc�2˒��7%a�*���{J��z�L��;-��>�?u���	i��&m�o��l�7��r%|�9+%���@�W(�Q椔�J����ˋ��vRbms�^���x;���<�[E�p�]3:&<X����%��O.�]��9��#�".EB˽|Z������@��<b:��}o�3�s6���ה��7�ʦ�������~�2Ng�y���%�L���n��D]=7	�<��d��qSx�!*6������ߤq9�m��~ì5�9n�^�G��'I����b��E6���ߦ��N������!:Q���$�N�?w�ٺ�_��.����\;�î��%eܛ�}�͔��R��_+ʯK\L�
1iZcU;��O�'�����j���PΕ������r��
w5G1>:�PZz��=G�Z�&�.є�fk~y����s��.|�Tk�=-t������Tj�i�%�{��ÿ��^	�67 ]B���>cb�X��(����
�b`��]s��8���Og�a��c��D��+!l)4ؖR�^�cĔ=�ޯ�j���L=�#7�U��9�[[R���?��,�2/�gro��,�Px�b�H��w�eb��E��t#;`�#��
pʎ�ǯ��Tל\��/&�ꢝ-b���=����uǿ(+O��#���l?��׏B�����ʱ���Vv���s)e��&k=1�G$���".�0�(�0_?4�.}b������N�Q��)�;꤬9?�p-�_feW��7/�Y�*�/r���/��ג�	e���BX�/�r<�-���V^��eE,�ޤ�/Q�9����z)��B������4�]O�y�>D�5�ev^�E��DEH/���RD�)�k$��2�a��6��Ǘ�Ɣ�D�2~6��E��ץ�N1w����)�I	���}�^�*=$v��|��5/�:�s%{{�Ej�,����QD���Gk6�O�����R8�[�M]4n���[����cS���]!}$�bA�`�������{AC��-+u��\�������R�侐��p���d	1��lN�݈o����`�7d��'����)u�NI�ٙ=.���?�A��^ ���6��9�s�����	ᷨ���S)כ#�s��/�
��ԅu�}�jݦ�Z[�jN���K�G��+}nU��;�!�����\Iw��l<��Z������ӽ��KQ	?����vyu)M�#�";�[�͇���گ�ړj�}����&�_G��ݘ�M�̽����ح\�����S�SWѧ��!U,�ї��#����S�n�&Xx-�Z�5����'bԻ��W]uR�E�>Ev �����O��ݾ���,_�ښ���:��Ѷ5b��eT򮾞j�c��8n~��PgZy@ЦD�%5���C����k����A#�w%�Z'|��u].=�n������x��?�lO�ջ�E0�X�r]Ѹo��n,����%�~�¨�s��}�\���l�o����g-��Dɖ"��T֎��`�+��[���������*��[	�N�51|�^���<�X@^�?"����Fh%�9�-:J�U�㯴��sϹ�5?�]����戼_�J����?��|� *���7����y$wǖ"�6b��?�~B�hܵ�]ˇ��~z�%s�����#3����O�]UŇ�e������%�b��7V5fu[sf�t�ud%��p����#k�Ѓ���8����׌jvj�S\CV?u������>3��(S�]����vT]���J�ܛ��:uYV�!7_HʢU[�M����C���PɿM=}Z�[�������7d�S6G.Jޢ+�#��᰾ngw�,���ա��1j}�ʍM�����\/�"����F���MK�]X��:j����F�RJ����Bc�+��
v���,�DKC�k�Pw��W�ikfQ>4~�3&���4T]�[C�>0�Ք�)�S�Q焠�t�$���1��S�V1����A^S=��3gԺl�}�ZG7�=A���6Z�	�>��.8BA*]nԛ���Bń���*��PY���_OХFT��g�j������=8�����;){�UN�L��	h�-(��=K�xh���ô�� �[R�K��e�IY���YM�S]w?ùI���x)ݭ������mFw%RweR�����~#�2��.�ZA^���x���j]��h��z�-^��ǡ�3�]���SGZ�{Z�����\O���5^Tsڶ;�z�����O�#��O2�� 8lМF�+h�4�c]���$�{1B%��f>X����l8^0�0�:5d9#Gk�Vo��p�Wv�Q�Q����tg>��z�$va!���4�m����Q�ݳ����
v#���z0��)r[��G[k]�wk�Ԫ��҇�f.]��RN4���D?��ӟ������Ռ�7�������4\(%R|_�4Q+~��ǿ��!��yk[� �(t�|��;�O���I�Ⱥ��p��e��_^�dKI���^�f��!���ծ@���'ݘ�l�Iv���O���~�FF7g��xmﳍ�jA�ꥯ8��7~[ #r�g��)G���}u_?I��Ň?ѯ ���HW'���AA��_`ldWc1g �k�����)dP���DƌL�QR���7.VQ�S���j]W���蕵'F�[O�[�+���7]S�0����֧߮Ʋ���x}�:/]��y������>����)�D�4�ا�q�]��]�_{"��]k#_"����UHj�}=�+��]��3�Z�Q&"�)Yv/;O�i�Eb>��Q��H�e4�cm���D��.�$Q�iB���W�o��ZG?�����:]G��C�!��3��u�@�8r��E��u�=�ր�e��CB�t-�,���ԟ�}�i����h����گ�����5�;Ewf����ԷFX��r�bWɯ�Z�䖁��z��5���9���"[�}o5]��/�3��+3u��	��z���춼��9���2�b�P׆q�9�����4e{����	j�HR�T;~+�����_.;�t�_��Y�-j_�L�}�����(|�皟?N��Oe-��_?8(���Y�,��J�k��o���_C��{�95�莈���5��=�m��l����/z�V��=��B/�<CO^1��NH32�|����0�]�X#����n������K������i���4ծO7�1��o���˔�mj�(���ԺS(<}Kx'�P�ݏќ�YSm�.��FR:�ض��yVm�*�;�����v��QN��9J'n��%��q�s&�(wrv8U�>Ew�r�@�r��XW����?}�e�(���|oq>��8`�Wq��v��-��]Ի� �
f��wp�e���i�թ�f��x�M����L�!�]�Ů��$�hl��_;E�M�Ó�����v_\S�~o3��2E]|�>x�:����h�Nw���l�%���Dd��,�S�f��VZ�k�{%��
7�Q���m��ڔ��m�� ���gްVjX��+A%��|������-ϴ���X]�XB��ȱ1at�c�ʹ�(�nLt��EĄ���~�$U����y	�3���C��7����7~Ls�DJS�v��&2�m�ǝ>-��@����i����G�ŋlT�2�9�b�|�;+P��+la�~�)���%��}�{`�d�j�u����p���%�[0+5��H�&?«���UU��hOf�������GH��]�^���P*麵�k���O��ѐY�0��}��:��Nn�y��W�ҋ	P��st&]C3	�N`���s+�H����^ �TNEO|�(�%���i\�D�wMn_7��.�Nh�%����|��DLcUk��P䡵��)x׻1�7&��}似I��m~V^t�C(��V�V�N���F�i��Ll�S��>f���O�Ԇ��Y����G�j۟`���>�}�=do������3��yϺ{��^J:�� W��n3�L�����P~��P�b���Oђ�J=�@�^�e�3�L+��ֲ�=�l� =m�a���'�^�h9���
o)����y�1'S��������M񇴂����SJ!��~�	o>�2(8(�p'gr�lO�@��YE��饔����!w&f��HH����Ѳ�>��$���#����a
j�1z�AA-�]R��A���uLS��BO(�*g�*O��̋'yܥ�����j��{lg��S�Sg�Q
�� ��xP�b�2(�Y��Nh%�*VnfN~!�R�T��2%~��W����[�zh*�=I�ѣGe��<��X��Ʉ������{�g,g}u�DB~��Ԭrw�]{�!oIp�����ի�o�r�;�i�TXy*�M�s��e(7�����K�%!m��R�^�j�ڔp�,��8'{���Ow��$�����*}r�w���[Q��ĖL�;Pj�$#�"�l���&��������z�:��&��,�e=(^��2�8G/"����!��
�� 1�᪪�jKBFe%8!�zJ�o�#�e��<�ʱ��B�$�wvyh,�!����J<皖_�q���Rp`P"���<����S�񅨞�.�Z"D�:��"�X[[��zh���}:9Dd��!99Z^�%#/[��W�}�Ey�Y0�
"�p�r0�P�M2�_rφ�ƻ��_uӲ*KI�����R +�3B��_� K��L��}U""V5;���s47�~��Jn
"S�F�"rx����Sw d�J0�cr^�Sa�zTQy �C���T�դ�B?�Z^3%ɴ!ԗo���	��K��@���y�7����u�S��<��`��d҇#��쯄��G������#<*ú£}g��4"Ӫ��˭=	��pW�ծ�x�}F��C�]J�K���g+Z�dU��ď�K���F&ؙ�|�Xޭ2�L�3�&�ɯ�X<$3DGW z"ꭄ��4J���'X'7MD��~�0�F�M���V��r�xA�\(
}�J��6�'F��.f���RY�fX�����a��v� ^d��cq��Eg��7;T������
���R7�
���C�'�d� �(	x�L"Hm�ٰ�ڠRK[��2��0�h�P$���o6���n��0ZЖ��k�h	]�N�a�����%K���M��8(8� Δ������nۙ`I!�`%T}�i�Dj�|�rr_Β|KҭI	j.!3`�=z��B�,����\#=`hcX� �gfg}`b���j��gy=.�Y�2���'���j�Y�؂�꠷�-WJ���0 AO�!�g��Ɛ�R��^2��p�JD���8�T��u�/�C6���R�_��~<QCk
�6�Z�Mp�p4��p^�8�Y�Q4>�`����g�"����O�G�p�aj9��$?T+������ ��f���KS=AE��X�乫���tLR�4->,�G�Ǧ2�е)�Y/�ε��oW��e�\���S�bXóDs�WQe���$��^3���Y8�B�2á6F�L�2�!�U[�bم�*�T�����:TJ=�0�l�5���]�Y�B��3LL�8b[�&KjC��uЏ��⡒�R�l��
C����Z�}����r��ސ1o!/��;"��!�T(wP0��#�a$M�7���6l�pc��D��3�F��O)ON�N��tL����^�~��e/c��̤��,�ɷi�0�u�O�9!�����+!$h?��)~��V�,��B�.�a�Ǭ��
�|� ��Z~���I�9�偪J��S��)ɐ ]&U���	<��;R����<zs2��p(�_F��Rtw�?�q�T�u'O�r|�Б��''H8�q��kL@d���	M�*�%41թU7�3^�i�TV�}� ���d���rEi�$��@�<�t���~���-6����d�ȱmk�)��Զ��H���WNj�-RX~��g�?(E+,�Ca^f�>gX�ޚ�J� sч�4gc��@
ǧO�3Ё�o���=�?O�/��(�e��*��S��ٳQ��0����oH�<;s�v��
���O��U	]	
a(Q>.�+�?�sA��(�_���I
* \x8��1�SU�!m�cs|n���!(bk9��P"Sz���)YE��+:+7d��ܠ�?�ud�&Z�rhgeX�g�JO��:t�r�VZ�-������a���2}�e���K0*ʰ*���^�rCa�J�g6t��Z�Ֆ�����L�v͸B���мA-(�)�+��MLAVN�p4P�%d�e�dn�:���Q�g�`,@L�����`���?�4$R�W��{Znf
��E�!΅{�!�R?E�%O��B��fR{zhY�b�|ϕݦ�)�,� +i�h%#��G�3�
�pk�'��ENj~�&��&w���r����c�:H+��0��֤���"-�VX�ʺB!K��w3�
� Xv2)�St���o����zA���VE(�%񋭰�r��B�(�L���z�Q��1#�Ѩ�<h$]Hd��
�"��\���iҕ��鹯,-N�����r�f�*3�2�ʄ)"��*�����ee�;�#�eAݑ5U�i�8ь~c�ljoZ�/��Q���!�[�_j3��'�A�n��Sex*[e���k��yf�E��̳X���L�����}0��r)�]X ��A���p�KK����(���<7�/Ȗ䳑���Kr%%"���� frJQa��H��AVVi%�bz��ܐ%�]R��y48�%s���S��]Z�w"�ێ>�@=E=�+��@���2�����wVnR�'�y�Dkl��*"��5�%j4QYR���W�,g�L��_�ୀdy`�2����\�N
�Zf��Il���|�ڜ	�b�".� �h�9J����pR�#U>*9�
391�?�}&-u�x�@��ȑ���Q���i�uLZ��:|�x�,��12�@ʝ�1jT�%4B�D���B�]YYBV�5%u��55�Ó4NsJ��󷍚��ƹh�md�"��14Y��:a��W�6)��<(�G�Q���YϚE���ER

����<LBWWv�R�=5#O�n�E,�s��X�N�<AL�1,��[��@C!1=6�JV
�U�
,�"�_	����A%#(���yነ���E �<�f�
8������St%�1��Jr+�%���	ኊjs�1�;�K�a43�pb�~?&���)��W�C�Y��ԛ��-	���'Cn�ܳ3�RGh� x��2����)r��|�c�^���B�F�
{�UZ�U��k�Ι��dj'$M��U�6��Z"|�$˒���
eKͫk�Wl؊�l��Pᯪ��d��}"PF�nՆ;��MJI�8>u$��
�%�{ܥU嘂A�eN�2=�0�pXaFQ�F7����<:uD
�ág����	�r$-����,(��&ʕU�6��]LV�0���FX܌�a��RMrb��4p쬉#~#1Ò�ndiJ^N��v����b���dO5�t,@�(( �� �#���)��YGh9�P� deϢ!nJ+�Q�t��#.,9%f�K�~AQ)���u�'4���^���lXVj�񖦿��'-[VP��ympA]���<�MM�M>h~ �Y��'F��~,����ޠGG���$�I�:s�S�G�|�0�F�'�B�MXav���O�� �)L����3o�$%�Gf[),'�����fklä���O��S�͡�*q���KL~�ǧO�0�
��$e)M�k�~L�x[�Y	�Iuљ����g��D^S��Y�.\(i*�4�4v�d�_�
�֔6�G���a.��Y���`�#��2#���Σ͢�fP_b�S�0b��Df�#�O�9����O�#�
��2@8��l
1�`2r���-g\t����I��ꡮ�5Q�y�E#�f$|�f��ٚ���h���$AG�I��פ8��Z�2�	j��5E!1HGy*�U��JcRfw�9��>}P���A���8C%tT
1���R�1���|��T�)㺵�K�B��LIZ�$��A�\��:RL�p������
�qr�����Z�S�d�{��l�\��c��(���_�&���lr��_s	���U�u+.W��|�#��g'�ߩ�=;{*A*��M�Lb���(�
�ZJ�(FjE�ҧ=�RR�E*(�iV�ӽ�RP�m6��Rӧ�MW��d� �lKE����ߋ��L(ݥ��b��
m,�Tbꧏ`n�Ò�yޒ��yR��*�r�#e�(ٙ��UA���ʜ��JcK��t&#��t0�A������<m�M�.�A̅�͎�`�]���hހY௨��}O�K���>�ì��AN���εUV��8�n�^���`P3��[�MÅ��L��	n� 3	L���r�"�Y�2��W�z ��e�l}��s��
#��wW+檬B�����	p�h�F*��ed���"t
�a�d�)��LRJ��@�Z);��a���&���R/�0�I�MaO��X����z����`E���[I�%z�yb���f��*/[�rs ��-��"m�s�b�)�Y�)�dNPl��$U��Y ;�b�<JI<���E�Bȅ��KH��'�fa�}�Y����95���?c�gt2�I�����Yzva�(��e�H�2J��;"et!f�������L*�:iԘf9��ǔ�Tw%=h����Q&gj���z�0TM�&�EZb&Y[z�,�5��	���f�*�̐f>*��j�쒏���3�b�gk��`��H�������z������t@�-h�0���㫏�kl?Z�2,C�<z$�������4�w�(=�E�bѣ�y8�\��tF�x��rY�
:'%k3<�0f�)J�u��&�P&����T�3\����]��d�p�>�
��mdr�n	\&�Ҥ ��"0��4jD��QQU���Ԕ�i��H�=E��;��Bf�T	m8��r�W�G�����鯩�k��:�xMI&d�=O�� cj�{%�[B�*��"psD�-!�5�&�#����;�|zO����7����L�ς�*Y�p��ъ�F�N!WY6�c�L�`��QrBnI�j�)�TV�0A����'�c��AK[��YrF�Ԝ�,��F�v3�,F��\qT���bMCP�����?�
�~�0f�Xtt!u�jy�����6k��`CE�6��7�K���>&Y���L�xG�LV�c�'�N��e^��+ޛovCS��Ě��J�����U�|�fJ���*�H��À�g�e%�!�"������l�%�+��p��*����|❔f�n��m�JvXDM���6dE�h��,���4"��6
�B��(l�$Gv|.f(-�,�n�Vo�2iY����-P���_�cך��$\U͎��G׆vf���3=T?dWb=���h�Ș�*�w�@�Z;ئ%
�v>����uV���_����;����s.������Y��K����gGi����t��
��H�a,_��C��;{�s��j�TUq���Ak�T�#g'&化|����,*��������_����L�4?��cZP@�X�5��-������o�c9�9V?d8��@�(�,|]O��7ί'�ݺ��
��~��p�6�MT��8[Qm��3��x5�!b���g���G�!��ݾ6g��x�Y��)��Iɳ�^4���E����I������ąI��?�O�!Ԏ?~Џ�$��4C��%�H�o��v�������Qw�/�r�6k�Nf�o�Yv`5�a|���l��kڜvjWO{�ۗ!��2�M�t�XhS)����[8����(���&Ҏz;4��_�ӿ��?�1OG�ϕ��)��h7���U�D^����SB�!U�8���TŬ�8�$�E�X�Y:R/�rv��r�C4g3S&����R���ὄ>���H�j��Q)lm�y+�9q� �E����P"�M?��s	�a�+�3z��`Uz��fwF���d�:���u���z��H�~���U�>t�V���Pʕ���׫�M�E�d�����僕�����Y��ʪ��:��a�G&k�3E������q6=�^��o�Pd����T��=���� @� [�:�����(U�#t�>��o��*i�d4@9�*�(No�Ča�~�_��Vk5K2�de�h�z��R�`��8���U��(�6h�!� $������ӷ�:*Z)rfӱ4�g�T����3(�k�.-nц7�:�^4�Q��JŹ(�*�L((�'T���dWa:oIʾ�[E��+��%rѰ���htXE�n��K�s��3Fk�Yy3�&M�y��������|^��B�� ���W4����y���e�hE���w !ZH�!�+/��J���]�0*g'��Е4|��8�oVut��p
�a�H����w#�)L�ɰq�t�N(�Br'�������@���(��+���-fPg�CMe�^ҹa�5���(ҁ��P��i Mes��fYz� h��oRQ��8	�$��猄h��x\��%��[1��\�4�i{�@K��G)�ZI8���#������65n-'�S�I��A�f��o���L򇒪��A��E��[��1�����s�'��rX�6�����}EHrl)}VB�m�a����8c�����ߪ�r\��V�:�c��*��h�!�*������+V\��ł��1��G��7�0�C�h�.��+�%�����M�Oò�UP�T�]������el�/��V��L��D��ݥ���=I�7����ve�na�3}�Gr��3�Y��U�����{߶;��vg�G���؝�<iw?gw�?kw��������9��3����({����Kv�W�N�
����vg��3q����r�3�#�s�v�ڝ�uv���΃�۝?���U����`wښ���_�;"�;¯ ��ٝ�o�;s������o@_�ӑ5vg��p���C�G�E?��� V 6�� ��8� F&fn<��R��%<������<�p&"p#�N��+=��8
��:����p<�����p��۾-�G������ O ^,l�#��x��l7ˡ|� �2�� ���z8���/�� �5�mQ�[�kll���|gw�G��~�>(�s8c{������9���~�G�}�Ps��\(Ż���90��I�O@�
����6�����I�~�� �p<��1�h�B�y� ��I��F?NF?H�q�6���^�!�I�ٞ����N祀+� V n7 ���x�=ǿ�\�?�g�����(��eo��.�x^}��	@�_ ��i&�}��i�	��}H�P�܃�#�'� � �l l|����t8{.$;�})��H��p*�&�n�{ீ�M�]�� _9�2ӂ�#��oƧ�}, �����+��bO�3�<@!����|Y��=�^!��|q/ ��Xo���q�J�Q��8�q�m�N0p9 PX�q}3�� w�l�O��qO� 4��|8�Щ�Ù ��0��O��=p9 P�n�	�7��(�_���Z<?� �G� �����w�#�p3�N���5��������2���s8=S�� �� <x��+��!�!�����c����gW@�����f�d�} �W�	xN��yz���3�<	�������G�N�]H�.��N��/_�|8
8!����T3�� �_S��	�n<x	��3��� �� �:u? {��+	<�^ B������?�i�+��o�qn�-�{ �^���	�[�)@\w�&ʾ�;/'���p^����³�;�5��1���u�]��<[�a_\=��&��Fڭ��� �^4��}*�>A��� tC��g۲~"]rp/$� �{r��� �P��\ON���e�9���j@`.``��?=���� ������������'�[�zr}x���%�b�x@/�	�L�{�z��aI�܂�:�]�L�=��W k �Z '�a��Џ���ܛ��1xN�����(�
x3 s !@�πG����W�ܕ���C>M���lB����1} �}���������K���Я�7x uO6f \Xx���>�s�	��/��_�b����פ�:��K�u�_��m}��oD�^�_�� <x	�&���������~��/���x?ܗ��Ox������ـ 7�똗���:�i ���w?����y�o � ���؆������|8��8����H$�r�綧q*�f ��P(��  <x	�`������W�i���[8�H;���P �P��?�'�=��ŀ � ^�|��7��ҏNZ���.����w��~H��v�l=~2���j�L�����߅� O^�l����/��ѿ����g�����p`�J@�00��W_ �Հ[��|��w��b��ߟ.Ǽ�,��k�� � k[ {� ~��gv\��(�!@-��# ��m��2�O5m��Eѱ���nr���&]�ɱ��{�􉭿�@���;���ݡ����G+�lѭ�R:��������4��37��Q(�TT)7����©PJÕU�,�?�4��h$�پ0���I��Gї�*��OD\�~a��]i���� _�O���p�[��Bl��Z�m)݌6(5b�u��n��2)䞭��C`֚D$1���E�Z��5K��Ie��a�̌5H9��k_I5�� Q����Ue��Zb|
��Ӣn>�f�b�.p%n�6ˣ`�fs�)N�?ЁQ9��7-)I/�vK���v@e�>XG]��%a:���lyH���<LB��,�~U�y1�u�E9�S
gvű��%����8�&��4�G���<�"��!��S�W��j���(��
���������
w�*��(5D�.�������4�h��/�����8A����brn����S���:�Ԩȣͥ;���7
�G|�ВBCD?]S�U�+�}'A��B(����[Z�g�u~K��c��E�#�x4�h0FQ�<`w������Xvs�|[��rjt�Qx��r��Lw�_̄��mmZ��4mX9�J
��+�Ӟ�]Yf���	?�e��u`7� |����3��%��"�/�7SX��I�Nf� ����I
����g���b���SX*o8���\�ʛIa������[Ha���k���T�S��[Ia�����FϿ���Q
w3Îl����{�;�E�I�@��"��D<{ᙌg<����</�s"��y5��T�i�%�}�֘έ���VG�n�[�Z����$voH��M��2�9��j֯5�t~{� uJ]ɓ�Y^FV�Õ�ױ�,�fOB��"ݪE���:h��K�k���c�����(��Js�J*�A:�a$;Z�.��zi���Cn����2�Ҵ	�
~�0����+B�(����,���ft,K��/;+�>z�?���I�h����R�_hw��g�S�I�c<�y���$M����g��뭗C��?z=d�M�^����v'������'��Lk�?� �O���p�����z��[|h˭�� �^�|�����"��B�P��)�? |�[w�X��9�E&���މYYc�ĉS�kiC�Wjr�Ȕ��Zb�(�f7�``0���Ԕ������r�9�? ���Fc��y�$�CXz�H��K���E�=%���u�xT~�>�ſ�v��3�pCN��P��wC8�ϯ)L�lq��C��ROUp�w���<eÂtN}h�2��_��ګ�:lv�wV	���:t�БCK�)�f���I���#'+	��Q�(�R���Rl5���^�����-�4��Os(=.��"�1�)*.��GSd���)�K���BNG�v��\E�Q��,��=z��@�u`�#�*]����������:�$����'�5��{��>����ں+�:u����:�wr�$�c�n�֩��ڳKl���خ]�\�ū����;���]rz^���T���J@�����-�{b6۩��்�&�w �pmuifb�!��mRl���tql?�k\|�u16t	+����V���6t�fh�v����G��K�N)���.����v���Æ];`� F'Ii�^��qpqY����::�\���[t�L:I/����H?���	bIf��Ku�3;Xή��H�������>��N㖻R�OH����붔�V�VWҵ�C1��r��NBW)C1�����a�6C��B����>d��P@:�*�g��H��_�*BT�iү-C I~:����P�U���:D��ǒ ��]�-A���K�9g�V����;��8�+�ߵw��q�y��!i�PX��0Ne�N줆b����+IY�۬7��h)m*��~�H%
�ڊ���RW@0�������B@AU+
E��=w����8;�9���{���;�r�^���s�rBoIc��E߈�Ls]iW�����JU�O����T۔����_���7}oJ����AWP~����1F��u��A7fك���'&���ؙ�s�!���[�:�OUZ��v[������)�`�[�B��2����Rx㥍��|��B*�v>�2Z�t��
)>��Ӻ2�O�4�4�R������nt��3����k��FW������-�7,V��g��{�0�`�a買]-�g�}9>����~p���:�.z�����5���tcЍ�A����n��>�y%��L<��#d�(��Y��}ء����������:��~e�7���/�{D����IG�z[���9���O3�k�0�m���:��:��|ӧ;	�,ֶ�t�@�1M���ڗ��1Mx5>�3����.e]�y��B����辸�:��!.�&<�F���kh�)���/�}�zu4wjx�m4^
y�Rչ�bnsY�@�bU�4�)V�S��W�^-�'���M.�Z���l�4�(�Rz���լZf�{�U��]>W2�o��I>~�f5���<O���%O���(��/�|�e�������.�#%�b��K||����x��/��E>���Şv|Z��:_��z�!��q�k��N㫮� ���1����m����i
o���9��|�7k���$>7X�����>G���	��:?����~.	x�!��=��<Z^����H�>�Q�h<��+�5��b!y�N<��>����5w�2�	n��I�ӫf�B��G�NKO��m�/f�"p�J�]<��KOc�H2^�zSGao`{�8
�c�҃2�IC�=&��B�,����O���װ�w�>��>�����	��S�K0d���:��/��a?����G�M������*^��]ࣖ�n�L�= �����?�|+��8x��oS|L�[� �P�/�1_�`q� 8�|=x��< n�7�0�.�/g~<�3?�73�	����xE������M~��%�4�~�����`N�G���׀'��|o ?�@0|��I~�����IS�����o��GౌI�S�c���;��v]���&M���%���o��Ń���J_��.ʳ��<��f}|0n�#�ǝ	�:�$Z�]O�;���7p-� ��v��^���=�C�~���A�$�N�/pu��/��Ip[ʤ����/R�R&}��=
�e~��~֔����ςK��d��dM�-OfM�/�sƞ /�x%���������p-�WO�\(�	\�����|�oJd���S��&��(�9s�C�a��)>p��'��MxO�G�2�����WH����:�g������,�������JՈ�<ļ<3�]	��}x���F�r�=��-�xZ�7���ت��WX��켝6�?J�3�ܞ6����`��ד�M]?��c���O�5γ> ����+����of{xtm(�������&=�`�ݴR�0�~:K�x�e���w�Ϸ��~_1/�쏃/���b�O���L�=W�_$N����m��@�����|:��˵>�U�6P�9��ߨ\t��K�r�J��L~�|���U(�U�?���C~��Y�?ʼ����	��2o\��޼��ū����O�W���U�7�˘���#�/��~S���q���j������6������9�����-w��4KٿM�W�W-�WlV����Z���	\�d�1�?�� �"����9�~���&�����Je���ϧ���Wj�?/ �~��
���o r�OV���r|-���k>�ֱ~�of>��`^��;�o��<������}+�.��������+���j_��t�*��'�?����-c������	�_�|sx�U��G�Q���f��}�
>��e/sx���5ה���u�^��#�Z+�'|v���v���x�8��q��8}����,~���Ζ��G�C�헆�}}�q�;�_累���8��kvA=>��I��1Q�G,IϠ����ȯ��|y��_Q;=����:��״��5������.Qdz߄���Y�6�N�Cֱ��ib�㮴��)��_$��$�`y��sh���wu�8�KsE�R�#'T��r)ޣ�<Iǳ-��i�+�c")��N��2R�f:��Z��6��I�Yy-8ำ5�T$�ٜ�s�����\ԎA>�o˓��T���Ra�C=]h������#�J�����%�Z~r�*a*��B�|�����V.	�a.&�;ԚON��jr
��h�w"Ij�����J:�J<���6�͇^�A֜Z�Ðs$��!����<��r����T�jS�ե�����9���=Ur�	�hG��O��f�c(�gm����줗�������+�˂�yòA�d�BGSQ��&��闫��0-_:�q;�D.��&1��|Ǹ�F�TB�r�з���q,������q:z�����/�6 �����8r��5/۔\��j��#��j"����}Z�f��_��&E�|[4b�%�OǢ!��1�i~3v�i�V�~���҂^�j��NM��v�2	w���=�sNA2M��r���XL�gh7��څ�H�Y+2����(��{Z&��{q��t*iw�tN+:�)�p�^�����>��g����'S�k������y�cF&j���,���FEI�J��u"Ґ�}�Y+�$�S�f������`dM��W?0���<�x�J&���RA�܂b����%CY�+� �L4㽈���+���R�� ��)MS��ө�(�a4�;j�S@����`�%�Q�2IZ���n"mL�Q�x�]cu�qp����?�`Α#�A��1=A���gg��z)�	��j�ᚦ��6Û!WN�=��#sr�lMk��UM���5k6��p:�V��v�2u&
�{����������x�?$�G@hzk�-t���֝�(s��'���>�*K>��4��~���	�!�ˁ�%3�q��Q,Q�>K��i��P���������Z�Hi�9��]�U�<���Cm��-�ި��A=҄��gȠL��t�JZ~ۀ�#��ʳX���9hBbj�[��|�I(jwq���|�5S�/c�yS�o/Q�F��9ԚЁ���SRW�x��Jpz�$4�t6A�N���Z�5�E���vG�ϛ�N��5���y�o�Qh��t�{�SvP@hq�Z����0x+Ϫ g�G����@^�!����'ܚ�l3��6��l3��6��l3����_sT� @ 