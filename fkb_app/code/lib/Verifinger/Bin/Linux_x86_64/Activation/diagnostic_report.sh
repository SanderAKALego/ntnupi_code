#!/bin/bash

if [ "${UID}" != "0" ]; then
	echo "-------------------------------------------------------------------"
	echo "  WARNING: Please run this application with superuser privileges."
	echo "-------------------------------------------------------------------"
	SUPERUSER="no"
fi

if [ "`uname -m`" = "x86_64" ]; then 
	PLATFORM="Linux_x86_64"
else
	PLATFORM="Linux_x86"
fi

TMP_DIR="/tmp/`basename $0 .sh`/"
BIN_DIR="${TMP_DIR}Bin/${PLATFORM}/"



SCRIPT_DIR="`dirname "$0"`"

if [ "${SCRIPT_DIR:0:1}" != "/" ]; then
	SCRIPT_DIR="${PWD}/${SCRIPT_DIR}"
fi

SCRIPT_DIR="`cd ${SCRIPT_DIR}; pwd`/"


if [ "$1" == "" ]; then
	OUTFILE="${SCRIPT_DIR}/`basename $0 .sh`.log"
else
	OUTFILE="$1"
fi

COMPONENTS_DIR="${SCRIPT_DIR}../../../Lib/${PLATFORM}/"

if [ -d "${COMPONENTS_DIR}" ]; then
	COMPONENTS_DIR="`cd ${COMPONENTS_DIR}; pwd`/"
else
	COMPONENTS_DIR=""
fi

function log_message()
{
	echo "$1" >> ${OUTFILE};
}

function init_diagnostic()
{
	echo "================================= Diagnostic report =================================" > ${OUTFILE};
	echo "Time: $(date)" >> ${OUTFILE};
	echo "" >> ${OUTFILE};
	echo "Genarating diagnostic report...";
}

function gunzip_tools()
{
	mkdir -p ${TMP_DIR}
	SKIP=$[`cat -t $0 | grep -nx END_OF_SCRIPT | sed 's/:.*.$//g'` + 1];
	tail -n +${SKIP} $0 | gzip -cd 2> /dev/null | tar xvf - -C ${TMP_DIR} &> /dev/null;
}

function check_platform()
{
	if [ ! -d ${BIN_DIR} ]; then
		echo "This tool is built for $(ls $(dirname ${BIN_DIR}))" >&2;
		echo "" >&2;
		echo "Please make sure you running it on correct platform." >&2;
		return 1;
	fi

	return 0;
}

function end_diagnostic()
{
	echo "";
	echo "Diganostic report is generated and saved to:"
	echo "   '${OUTFILE}'";
	echo "";
	echo "Please send file '`basename ${OUTFILE}`' with problem description to:";
	echo "   support@neurotechnology.com";
	echo "   linux@neurotechnology.com";
	echo "";
	echo "Thank you for using our products";
}

function clean_up_diagnostic()
{
	rm -rf ${TMP_DIR}
}

function linux_info()
{
	log_message "============ Linux info =============================================================";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Uname:";
       	log_message "`uname -a`";
	log_message "";
	DIST_RELEASE="`ls /etc/*-release 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/*_release 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/*-version 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/*_version 2> /dev/null`"
	DIST_RELEASE+=" `ls /etc/release 2> /dev/null`"
	log_message "-------------------------------------------------------------------------------------";
	log_message "Linux distribution:";
	echo "${DIST_RELEASE}" | while read dist_release; do 
		log_message "${dist_release}: `cat ${dist_release}`";
	done;
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Pre-login message:";
	log_message "/etc/issue:";
	log_message "`cat -v /etc/issue`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Linux kernel headers version:";
	log_message "/usr/include/linux/version.h:"
	log_message "`cat /usr/include/linux/version.h`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Linux kernel modules:";
	log_message "`cat /proc/modules`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "File systems supported by Linux kernel:";
	log_message "`cat /proc/filesystems`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Enviroment variables";
	log_message "`env`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	if [ -x `which gcc` ]; then
		log_message "GNU gcc version:";
		log_message "`gcc --version 2>&1`";
		log_message "`gcc -v 2>&1`";
	else
		log_message "gcc: not found";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "GNU glibc version: `${BIN_DIR}glibc-version 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "GNU glibc++ version:";
	log_message "`strings /usr/lib/libstdc++.so | grep 'GLIBCXX_[[:digit:]]'`";
	log_message "`strings /usr/lib/libstdc++.so | grep 'CXXABI_[[:digit:]]'`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "libusb version: `libusb-config --version 2>&1`";
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}


function hw_info()
{
	log_message "============ Harware info ===========================================================";
	log_message "-------------------------------------------------------------------------------------";
	log_message "CPU info:";
	log_message "/proc/cpuinfo:";
	log_message "`cat /proc/cpuinfo 2>&1`";
	log_message "";
	log_message "dmidecode -t processor";
	log_message "`${BIN_DIR}dmidecode -t processor 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Memory info:";
	log_message "`cat /proc/meminfo 2>&1`";
	log_message "";
	log_message "dmidecode -t 6,16";
	log_message "`${BIN_DIR}dmidecode -t 6,16 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "HDD info:";
	if [ -f "/proc/partitions" ]; then
		log_message "/proc/partitions:";
		log_message "`cat /proc/partitions`";
		log_message "";
		HD_DEV=$(cat /proc/partitions | sed -n -e '/\([sh]d\)\{1\}[[:alpha:]]$/ s/^.*...[^[:alpha:]]//p')
		for dev_file in ${HD_DEV}; do
			HDPARM_ERROR=$(/sbin/hdparm -I /dev/${dev_file} 2>&1 >/dev/null);
			log_message "-------------------";
			if [ "${HDPARM_ERROR}" = "" ]; then
				log_message "$(/sbin/hdparm -I /dev/${dev_file} | head -n 7 | sed -n -e '/[^[:blank:]]/p')";
			else	
				log_message "/dev/${dev_file}:";
				log_message "vendor:       `cat /sys/block/${dev_file}/device/vendor 2> /dev/null`";
				log_message "model:        `cat /sys/block/${dev_file}/device/model 2> /dev/null`";
				log_message "serial:       `cat /sys/block/${dev_file}/device/serial 2> /dev/null`";
				if [ "`echo "${dev_file}" | sed -n -e '/^h.*/p'`" != "" ]; then
					log_message "firmware rev: `cat /sys/block/${dev_file}/device/firmware 2> /dev/null`";
				else
					log_message "firmware rev: `cat /sys/block/${dev_file}/device/rev 2> /dev/null`";
				fi
			fi
			log_message "";
		done;
	fi
	log_message "-------------------------------------------------------------------------------------";
	log_message "PCI devices:";
	log_message "lspci:";
	log_message "`/usr/sbin/lspci 2>&1`";
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "USB devices:";
	if [ -f "/proc/bus/usb/devices" ]; then
		log_message "/proc/bus/usb/devices:";
		log_message "`cat /proc/bus/usb/devices`";
	else
		log_message "ERROR: usbfs is not mounted";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "Network info:";
	log_message "";
	log_message "--------------------";
	log_message "Network interfaces:";
	log_message "$(/sbin/ifconfig -a 2>&1)";
	log_message "";
	log_message "--------------------";
	log_message "IP routing table:";
	log_message "$(/sbin/route -n 2>&1)";
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}

function sdk_info()
{
	log_message "============ SDK info =============================================================";
	log_message "";
	if [ "${SUPERUSER}" != "no" ]; then
		ldconfig
	fi
	if [ "${COMPONENTS_DIR}" != "" -a -d "${COMPONENTS_DIR}" ]; then
		log_message "Components' directory: ${COMPONENTS_DIR}";
		log_message "";
		log_message "Components:";
		ls ${COMPONENTS_DIR}lib*.so | 		while read comp_file; do
			comp_filename="$(basename ${comp_file})";
			COMP_LIBS_INSYS="$(ldconfig -p | sed -n -e "/${comp_filename}/ s/^.*=> //p")";
			COMP_INFO_FUNC="$(echo ${comp_filename} | sed -e 's/^lib//' -e 's/[.]so$//')GetInfoA";
			if [ "${COMP_LIBS_INSYS}" != "" ]; then
				echo "${COMP_LIBS_INSYS}" |
				while read sys_comp_file; do
					log_message "$(if ! (${BIN_DIR}component-version ${sys_comp_file} ${COMP_INFO_FUNC} 2>/dev/null); then echo "ERROR: can't get info about component ${sys_comp_file}"; fi)";
				done
			fi
			log_message "$(if !(LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${COMPONENTS_DIR} ${BIN_DIR}component-version ${comp_filename} ${COMP_INFO_FUNC} 2>/dev/null); then echo "ERROR: can't get info about component ${comp_filename}"; fi)";
		done
	else
		log_message "Can't find components' directory";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------"
	log_message "Licensing config file NLicenses.cfg:";
	log_message "$(cat ${SCRIPT_DIR}../NLicenses.cfg)";
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}

function prot_info()
{
	PGD_PID="`ps -eo pid,comm= | awk '{if ($0~/pgd$/) { print $1 } }'`"
	PGD_UID="`ps n -eo user,comm= | awk '{if ($0~/pgd$/) { print $1 } }'`"


	log_message "============ PGD info ==============================================================="
	log_message ""
	log_message "-------------------------------------------------------------------------------------"
	if [ "${PGD_PID}" = "" ]; then
		echo "----------------------------------------------------"
		echo "  WARNING: pgd is not running."
		echo "  Please start pgd and run this application again."
		echo "----------------------------------------------------"
		log_message "PGD is not running"
		log_message "-------------------------------------------------------------------------------------"
		log_message ""
		log_message "=====================================================================================";
		log_message "";
		return
	fi
	log_message "PGD is running"
	log_message "procps:"
	PGD_PS="`ps -p ${PGD_PID} u`"
	log_message "${PGD_PS}"

	if [ "${PGD_UID}" = "0" -a "${SUPERUSER}" = "no" ]; then
		echo "------------------------------------------------------"
		echo "  WARNING: pgd was started was superuser privileges."
		echo "           Can't collect information about pgd."
		echo "           Please restart this application with"
		echo "           superuser privileges."
		echo "------------------------------------------------------"
		log_message "PGD was started with superuser privileges. Can't collect information about pgd."
		log_message "-------------------------------------------------------------------------------------"
		log_message ""
		log_message "=====================================================================================";
		log_message "";
		return
	fi

	if [ "${SUPERUSER}" = "no" ]; then
		if [ "${PGD_UID}" != "${UID}" ]; then
			echo "--------------------------------------------------"
			echo "  WARNING: pgd was started with different user"
			echo "           privileges. Can't collect information"
			echo "           about pgd."
			echo "           Please restart this application with"
			echo "           superuser privileges."
			echo "--------------------------------------------------"
			log_message "PGD was started with different user privileges. Can't collect information about pgd."
			log_message "-------------------------------------------------------------------------------------"
			log_message ""
			log_message "=====================================================================================";
			log_message "";
			return
		fi
	fi

	PGD_CWD="`readlink /proc/${PGD_PID}/cwd`"
	if [ "${PGD_CWD}" != "" ]; then
		PGD_CWD="${PGD_CWD}/"
	fi

	log_message "Path to pgd: `readlink /proc/${PGD_PID}/exe`"
	log_message "Path to cwd: ${PGD_CWD}"


	PGD_LOG_FILE="`cat /proc/${PGD_PID}/cmdline | awk -F'\0' '{ for(i=2;i<NF;i++){ if ($i=="-l") { print $(i+1) } } }'`"
	if [ "${PGD_LOG_FILE}" = "" ]; then
		PGD_LOG_FILE="/tmp/pgd.log"
	else
		if [ "${PGD_LOG_FILE:0:1}" != "/" ]; then
			PGD_LOG_FILE="${PGD_CWD}${PGD_LOG_FILE}"
		fi
	fi

	PGD_CONF_FILE="`cat /proc/${PGD_PID}/cmdline | awk -F'\0' '{ for(i=2;i<NF;i++){ if ($i=="-c") { print $(i+1) } } }'`"
	if [ "${PGD_CONF_FILE}" = "" ]; then
		PGD_CONF_FILE="${PGD_CWD}pgd.conf"
	else
		if [ "${PGD_CONF_FILE:0:1}" != "/" ]; then
			PGD_CONF_FILE="${PGD_CWD}${PGD_CONF_FILE}"
		fi
	fi

	log_message "-------------------------------------------------------------------------------------";
	log_message "PGD log file: ${PGD_LOG_FILE}"
	log_message "PGD log:";
	PGD_LOG="`cat ${PGD_LOG_FILE}`"
	log_message "${PGD_LOG}";
	log_message "-------------------------------------------------------------------------------------";
	log_message "PGD confif file: ${PGD_CONF_FILE}";
	log_message "PGD config:";
	if [ -f "${PGD_CONF_FILE}" ]; then
		PGD_CONF="`cat ${PGD_CONF_FILE}`";
		log_message "${PGD_CONF}";
	else
		log_message "PGD configuration file not found";
		PGD_CONF="";
	fi
	log_message "-------------------------------------------------------------------------------------";
	log_message "";
	PGD_LICENCEUSAGELOG_FILE="`echo "${PGD_CONF}" | grep -v '^#' | grep -i LicenceUsageLogFile | sed -n 's/^.*.=//g; s/^\ //p'`";
	if [ "${PGD_LICENCEUSAGELOG_FILE}" = "" ]; then
		PGD_LICENCEUSAGELOG_FILE="${PGD_CWD}LicenceUsage.log";
	else
		if [ "${PGD_LICENCEUSAGELOG_FILE:0:1}" != "/" ]; then
			PGD_LICENCEUSAGELOG_FILE="${PGD_CWD}${PGD_LICENCEUSAGELOG_FILE}"
		fi
	fi
	log_message "PGD Licence Usage Log file: ${PGD_LICENCEUSAGELOG_FILE}";
	log_message "";
	log_message "PGD Licence Usage Log:";
	if [ -f "${PGD_LICENCEUSAGELOG_FILE}" ]; then
		log_message "`cat ${PGD_LICENCEUSAGELOG_FILE}`";
	else
		log_message "license log file not found";
	fi
	log_message "-------------------------------------------------------------------------------------";
	log_message "";
	log_message "PGD licenses:"
	if [ "${PGD_CONF}" != "" ]; then
		PGD_LICS="`echo "${PGD_CONF}" | grep -v '^#' | grep -i licence | grep -v LicenceUsageLogFile | sed -n 's/^.*.=//g; s/^\ //p'`";
		PGD_LICS+=" `echo "${PGD_CONF}" | grep -v '^#' | grep -i license | sed -n 's/^.*.=//g; s/^\ //p'`";
		echo "${PGD_LICS}" | 		while read lic_file; do
			if [ "`echo "${lic_file}" | sed -n '/^\//p'`" != "" ]; then
				LIC_FILE="${lic_file}";
			else
				LIC_FILE="${PGD_CWD}/${lic_file}";
			fi
			if [ -f ${LIC_FILE} ]; then
				log_message "License file: ${LIC_FILE}";
				log_message "`cat "${LIC_FILE}"`";
				log_message "";
			fi
		done
	else
		log_message "PGD licenses not found";
	fi
	log_message "";
	log_message "-------------------------------------------------------------------------------------";
	log_message "";
	log_message "Computer ID:"
	if [ -x "./id_gen" ]; then
		echo "B042-8724-A90E-B69E-42DE-C112-A191-A696" > ${TMP_DIR}sn.txt;
		./id_gen ${TMP_DIR}sn.txt ${TMP_DIR}id.txt &> /dev/null;
		log_message "`cat ${TMP_DIR}id.txt`";
	else
		log_message "id_gen not found"
	fi
	log_message "";
	log_message "=====================================================================================";
	log_message "";
}

function trial_info() {
	return
}

# main

gunzip_tools;

if ! check_platform; then
	clean_up_diagnostic;
	exit 1;
fi

init_diagnostic;

linux_info;

hw_info;

sdk_info;

prot_info;

trial_info;

clean_up_diagnostic;

end_diagnostic;

exit 0;

END_OF_SCRIPT
� F��M �\|Tՙ?�$�d�Wێ�,h�$H�t�C.%��E�L2w�����<`�54��F~��u�֭��v��ʺ-���3���!�v��וee�(�X�-������̹'��@�[�|������=��;��*r�C5jk�z�V3�3>���S[K�jk��H��3���j/|�I�����EZ��@L�nT�&rj4v1
tq��jQg���s�����O������:É�/Y_竛�ù���y��O��b��o�tuG�j8^I�"�	�<����yu�r�/X0������x�����^�t��b��Vr.=��x/�g^�K�I��Mf�	�wr^���4&�i���Ǯe�{u�2�9�ԡ��WG�'%ęӃ��E|��fm�r�X�zV�kF�f.�tl�T?;~.��.��B�B9� ��=�oe��+��:z�]#��J�&����U��V�̇�!�*�PY7���kX��
P�e��c�=x�~,��b�)�⸼���ρe�H?�X��dr氉~.x�c	oE���B�H��YW�ɒ|�/Hx?�?��G�N��:���?�4*��Ζ�8�[$�Y�����C�	Nh�BZB�n5LI[(S)�m�"�}����X\�.�c15F|���H�3c��$� �:ҝ��u��$��ǃ$�Q`)N����q����8M�0�.g�,kZ�h���]�f˺���I��`a����J��,�������xk.~�X�w��B�|<�8�	��$?V!�	�X>���|����T����H��Op���"���,�E�^�'����(ȼKdV��f~ih��l�;�$[q�~�Ӑ� �T��fi�8<4��!�M3�f�k�C��f���C�b��C��3�Y�I�{�4�P��n�?<4�p��>�LY����ԫw(�cJ�[#�k�H�z�r`o��#�$SG%�uV����$ʶtܚ=Ī�/����4}�NI�;�-�:�%�X
��Un�?�>�����[/9R�rp_0��>��ޅ��i
�����4�Ƀ
]0���l���A!KV��)�o�;�X�7y�'��3Jk�W���73?f@�Kxv!��LR�̝Z�5?5@��v�+i�P>%*QR��g��/�R��ʖ���z���m�������qA����S�K�����SI�thx��"*�츭��of�m-��N��= �e��r}�����"��"eYBŧQ����th�_�8��j;�fДz;���P���]0:��W>�RO�����Հl�ߧ�͠���#�|��^�No��P|�cl�}^�
�,4Z�3N(�dB�9���<_6��S�X�nv��h����R�̉����s���|��Sv8��dwT~�U�5�ޓ6e��,������ڟ%�O�$>�^�p��Ҵh6�]�&SE3��e�L���s�v����S=�)�ו�٩Jﾂ�מH�|�퍷���S���#���
ڽ'��i0`��|��>'}6���a�����!�3��qݚ�eK�sU�\t����op5t��W0n����lzrѐ��B���91ƹ��!��w��M��W�?�Q������uq,���h4��EW��H�5�"6���]�H"�:)�˵��s�.X61�"�*Wp�����ٶ�aO]=4��Q�<�;)MR:���4��/�&���b�<��ߢ������%Ȓ��#���0�V�ڷ�V��'�g5Jo6�w�F>i,qn��.)�5��������C��gcIYc�ҿ/��e1��<XL��|�w��e����E��R�U��zԦ�|���x��})_�ʐN�������?�A�ۑ��3�`y9~خ�|m�gU�&��i�G��D�,�|�߈�+��� �}uHoB��A��nG��� ݃T^��ŋ�sͥǊ�\��w��������Ժ�vGըR�1�*��%jk�?�d<�5��>ռ���XG,��[��n��h7q��Qu7.Z^��w�?�A܁�a8;0�w{8���"���8���Ww(i�3�;�&�w�24.���ĭv��Q:Y�:�<G�m�H4F3�ȝmQ������f��/-m-����A���s0��\���8���^��H<��3D���.I�.�p�s�^��"�ϱ\�@��`�>�?�h�;��\��c`�	�|>>�*�y�뿀hs����F, o?^^�Da���|��$_~+џ� ����>���>�������]���A.P|� �I��$o���׿��r��J�N�_��6����.I����$�ð��CL���u=.�%�3�??
�+�?*����E����4�I^��~I?��I���8��y3��/����KүG�����;I_A}e����C�&�y;&�Ox�E}^�J�s��H����YI��N�~�8��$��k������<����D����_��9M�H��eŲ��3~�@�b�O;��Η��?5���y�������s1����G/������g��?Mx@k�S���fě'6�h߇�����DyN��`�R�>���A\h�u��r�+��~��nӶ��s���}?fZ�ȿs���r�"�~�����Rg�d�2*��Ds}�S��]�k~>Y�@<g��CĚ���eam/�7Zy����?��1�\�c���w^�R������ߠ?2���%�Ox���ع?xџ�]�OxԮ�'��]�Ox�.���i���	����\f�h��b?���V����֎��k����nd��i5��wO����ݕ�,u�/�T"��*�ڗy�멗����}�,��Sp��:1*Z�5��V��e�M��7�2�qKgz����0�b�&go?�Tg?�*���p<�EE�>ym8ȇ(}���O����4{���	K{���N�{��v�C���NʟonUp��v�$��fv�2�.ڍ��.�7pgku���I�fv�C�?���Ͽ�vQ��vQ�.�.��/V����b;҇�~��mO�����E������X������hb�}9�(ϟ����.?�rh`�UZxzP�?�[&�y9ܓ/�E,��H����X@�U��N��6Ľ�TuD��*�6Ѽ���ǿ�<w�;�T��Qe<��bTU[,�BW�W����,=�dE�Ί�e�}����u�+�M��-�0%�`%��rJ>�%�L!�	S,�Xl�*
��ng� ����Z��XN��ov���a�����B+4�4�؜7\~CS��ĕؘ�`�Q˥,��F�-��(��/!8���I�YV&VU�dE�����YVZ�;[��j��=���|!��;+W:� �J-��c�����{Z�_�\��5���G��v5�Y���Z[��F���j2ǝK�M>���J���4rf?������'�4|?׍�c���in$z?���O6���_F�~��ڎ���o�YK$?��D}�I�yh#z?�O�>�����K����C����f~�MD�����b��� �T�oA�l1��瀻$}>5�Qp@�0�n�+��0��x~��??�����_$���_����+�/��e�G$����0I^?�%z[{�Χ����wK�-��r���H�ݨ�=�X^�#z?M�ި���h�yQʟۉ

Ϝ?�/K��<�D}�8��K�ͨߌ���i�����;L�/��� ƾ�?���m}~�NK�<��D?�������b�?俉�e�&��Q��q�B��T�R�^v~.+a���q좟����(��F�I���'����k�[s����o=nϭ�zܑ[o��C;���4v��uO���3=^�[��������'��=^�[/�xIn�㥹�]�;s�������%�yV�O�͟z|����F�|���3r������xYn��㗎�4|�	>��w���	�Y�s&�kω�|0�9������&8�۰�\NH���&����'�3|�8`��q����~��Ŀ��3I�Կ?�r:�r~�x�����c,�z�<E��>��N�Lԧ_a1��Ǣ��+=_f���b��g�?�Z�>_�&��M��?h����>�#&�6��_�
�O��$�t��<�߰�"~�����g������#�a����+(O�_�G�+�>����7Q>��l@�o��6����d�c��X���/���/�wK�kО�<��U���ǖ�g&���S�Mp�o��d�5��*�~�nc���y10��C!����X<��}��kW��5-_�����bq��?;�q5஽��Z�d	D|��H�?�c6�?�$��%�����3Xz˒|���iu��Ksd��Sm˥z^,���?P�%tK����<��}�hԿ٧�z@k7�zF�YִjQc�o�M7�Y�ַ�qQ�R�^�kJ�q�W��
�"�8R�,-4+��^Rgv/�J������D}���3G=�xQY��З�P�L���UT:��%b*4��ot?@Ji�?�<����W��3��E������xjkjj��_u�՟������W3ޏ����:�����ǈ������'�5��n,G�M���;�B�](�酺�u���;����0N����^��p��� 7 ?��3�v���3���s��i���`/�r�]S��f#S�z�����m6Ƨ3-<@���z��x?*�?�ǈ?A��O ���r'M��x���[��y^��a�N��g�B���܎�*R��%� ҅�t�����$|R�^�\����4cy�8UN�����/���V	��9,��#}Z¹\�����p���D�����������t+�		�oD��#n']�p���)#]jW[�fҭ����f8�F��?�N� �d$�1�d�"A��]�J�&�D��ngں��k!h2�?/D��|b�=H�Ri������+�\���3�G�S���ߣ�!��9�~�S\�o�|���e|s������m��.��I�+�#�-�^�+�/��g����n�%��{7p��[�K|@�E{Z����G>U�	�4?,��]��>C�3>S�G\�m�p���x��.�K���t�dj�0�|C�^�W��^��!~�x�^헆��j�C�{�w��ն�����!�{:V��ӱbH���EC��t\7$��#[Q<�a/���j�_<�a�gCh���K���3�.UVCfxdx+�NVƟ:B�)����9��3�৲�3���X�?�tVƿ�Vƿ�LV����N���G�>�Ke��{�{I�Q�>�w��>����GV��}�ێ����1���Կ�G�y��}$ޞ�M���=�>��ާ��~G�[��c�yi��S����ԏz����,�{���W����o�����zG,�{��<�}Z��S;e&瓬RKr6K�g�����z������O�x
g)�q�~/TL)]���b�	4�	y��Ok��@����S�����Z����0�:�t�����k��^�"Z����{����o����b�=_����|[��|�jqo>��!�P,O�P�����#��y���x%5�:����k�EfO�t�u�u2��#x�L��l���5�sܒ%=���9;ߤ臥W���K�pl���å�w��sݏN#��[�W��+���?'�ǯ*}������d��X�?��iY�v�_5V��C��l����_y҃���_I#�����w��(?	�?<����X�m��Hb2�x�JPe���R�ҧF(�[Pۖ���� "�K)��{½��"O:5:8���{ߟ�  Q(�� K>H�Fi����r[�l�0��EB��t�������&�Qٿ�MH��r#��2�U�1(�$H0h:�M C�Q����$�m/$�zb�u�u�u�Q"�	;Ȏ �"��m7������so��$:�������ן����9Uuj;u�n�۪�C��X�X�������٪��?�2��x�;�Zټ�.�nf�6�e�A�&��[�5�bysy�9
��<ߕ�+�*J9�u~�w��t����R��f��9��sy�뜱i�Y�`�|�)o�B�����KQ��������T���鬡���7 ǳ�]��.<�_ m�z��p��<�<��/���!ר��&��0�u!�M,ξS��N���H�w{������B.I^�$����,��;����?��^;ʏ@��fO<��XV��9@;���B�+:��?m�]���>P�}Bkp�ۡ58���`�nj������Z�s$/���r7������*�+��a>�:��s������o=y!�3i��������������b��#z�1^"w/d/�٬�s�$c�u�Ʀ�������8�������4��,(�w���s<dؾQf��b��b_�=�ʯ;��q�g�C�ϝ3Oo3@;�����i�N{eX�I�+�D��ou�N9<���ƺ��+Ƴ��_��>ްUl�[����Ŗ	"rR|M��b�T���z��@�h��bB_�t�>�ɀ����1�`4��(�8�ހр�ގ'��^P�γ'|���~��}�yl�5��}�Mu%y���bt:��+V��T4�0��G��;6���(���:�)G�HvI{�2�oi�3�#��y�u��x���{��wΚ�ix�׉-g��(��}������R�{t�i�Ŕj!%%(c
$Ӿ�ukl%�o.�yÒ���^��=����a`��@O��j��eR�(��3��3�z�����S
��w�p��z����C���yן76M�va/˪�fl�.|���[w������ք	���}o;XX��[�'�`V�O =-�}���C��I�T�{�z�����4��is]ͺ�5u��y��M׌��j����=)������y��~Y�n���W�
4zF��+!M����[�:#����)_����{Nz���zi;U��%:,�09c����[�G�KŖ��b=�%�u�u�`8'@D���SwI|hk����k���c�k��]mfX��yaa�X�����Ԑ� �f,8t�W|\��<��5�'��7��?�����;���,�{���~��7�A�0��bQ=���0��g���=ߧ��π�Z|�n�Y]�'��Ų�ĵő��R�Y��~D%�$����r�elr?�y�{�x�yYn�o,,�����TRZ�ǒ������+7#��Ky�:�����~�?8�!ݽ�߯�/tSy/�y>*��l��Ը��AV]K��	��W|j�'?>�=�>� �^d&�X+�J�|f������ﺃ��X�_d���C�z���G�M�͕y�����(.���6i����@c�26���Bn����VF�	3z����@jC�ʜ�rO}�:�v[�`mf8��Z����+V���9Ulltj���=��k@NЉOl��%6.vӠg�Tw"�տv��@���5|�n�+�b� �+L*B\�U��W�e���/�-��d��5?�c�յ�[��ՠs��n?j���2ֵ�C�F�m�Xvvb��Mc���p�k�����N�J)����`�����i�	�U�"Li�{;�8P�M������Ѯ�Ѡ�M��W���U�*��W�|d�6�H���|��4��G{�y��Yg��>e{�+뺊w�������3 p�W��Ư�(�p�0M�TZ`��Wl��.�3K\�8u�bP���^/�ov��b=��Tl�E��NҐ�	1���ƫ��oq5/��ڣUl�+}�fy$���=�u>򍭐�� �s���?���CL�'x�+�a�Ll�����Z�%���ʸ�}�\H�D� �|�%q����+?xQy+�!�>�e�ĳ�@@�R���Z��F��sqќׁ��������~��C����bK�1�3�^_�������;o:Wz�9��bo�T���:V�V%e�e�9���j�C��΂��=�t���/6��J_alJ{f� �Z8jN��M�7��(?o�����|�]��H�#������B��B�{�q�gH�̥�d��Q����S�;d)����b��{���d}|-+HAt
4��?H	��w`)v�Ū1�7����/��Z��z\�ů�1�St�G�����:�fh���:�AH��/o�`~��}����S������,+a?y��ވ�hR�W�L�m�f� 3։g��&���p������
�9,�o���<t��y�gmp �ŕ�f�d��,YB̺;@0/��qc�#!��aNq,'�'�.f5,?ێ�=��YÉ-���3���D�K�4�ބ��ٮv�>b��Kl�D�C�ctX��SZ4��5�c�ng*���_��):�9����w03�z&����]��#@�s��XwYt�%����B]����>x�zڄov	��8��3�ɢ�4.�2)(3��<ɓ���V��=��gsv�lV�ߞa�k��Me��}ί~���^�v|y����0��r�o��Z�9�Rz朳�s� �7���~�����bH�����>,jA�sw���v�|H{����h���]�uU]9�;uǣRZ�m�R��b������1.���?K�齢��69���;��xL{���"_�6'�XP��;��ڀl���9;J>���~�#9;Ƨb���B���2'e&ӏ�����t�Z]���`����oˬ	E�U�dV�80r�6q#��V�2�-�aqK� �GH0"`�`-
N�@��)�lv���O\��Q�W0�lYU;ZlL�i��{.UZY>���\�á�Z��ftwo5��'<��W�&�;�}��[��%ѵ�����P����+��S�]o?X?�kOk�Aͅ`����#��8��J^�Y^��:� q�u,�6X�m�
mOAo���� u����>�	���U�X��w����=��{����V�^=
�r�h��Ep���O1AX�Ă|L&,�G�b�.&Bf�X�B����/������x��Sj=�������g5���r!�8���1���՝���ݳ�M�{�V�s�>~���/>�
�)G|85�
���Ө��|�$�m"���<�ˋ��K�}�E������b��n<��)<���oB|]mf����j4����ل� �9�3B>d��������1z�y����Q}�Ob�|�C���y�3���g��Luy4H����]�5�pR�������츅eV�pN|����P@�o�٠��0x�lփ��be��_�������=θ�0��r����9l�����p���Rrxs��Mn�B�a����w��C�둛�E Gh��x(94?H�ůh�z����֢��C����\�fή��C�F|G��x�t��B���������ȩ�J�MI���Q���޻��l���r��k����A�; ���hى�`0�
�!�ɿ��i76Gd��a,=)�Z��X�}yuV`�u�{�7B�ƌv�����Q���W�����Jw�w���J�	������pI4LP��3ֹĕ߈+[Wc��iU��� Xw9�(N®�O|G�AT�yz���	6��f��>��%�>d���a|�r�	6���<���26������f��p��h��/G@%��z9��2�l�+Oj8�^��{�=[ꎇ�!���q��Ak_�$�������4������t��������0�q;dϖ��0�ӎՃ6��;�Y_�,�$�����v��#z����T�:\�G���[���y�k�
k�y��mS��̆�ݸl5z[]�zrDam�u4g�?��a���E�T�}7�x����<W�j�͸��5Z��$��p�޲A��zc�WG�^<{Qsv�{X�̇f7�7�_^g����bgZ��1���k���|-��?F\��fu�;�>���ȏ�*L���d�;0��a�[1��� �S�ր ��\�c����Hiǰ	����h���[��&��$�?��{�"w:\�}K78d�gJh8��ɣ�q9Y�-�lu�����_�]�a}5�a���?�
c��c��q���g̅���xR�e(���svg���-�݅��1��o9�!��N��������Wv(e����X��
��a^���V��� |��[��}w�us�����3!�f֒��`�Q%��,w�j9oC���~���s_����gU������B�ᕁ��h�(�Ӹ�|����Xx�홙��]�py� ��n���=Lُ�M ��;���clW�4�9�A��HH�}����jG��_2�-B^�����R����UHߩ���SZ�"��J��^���ux�՟+uw-�b��q0ny���	�^͹&��0��ro��ƽR�\5c�����WcAL~�S�@M?��$�{�{;ߴ}^w�WۉX��'ȉ�9B8����|�3#�x�Ώ�e~��<H��9��J����+<����u�;����}o�r�qT��'�œg{R��T�Q�&���㊊�>AhA%���U�p�M[��F>}�u�L�#l`���HM��ꄠ��ꈒ�m��ӯ�?"6�ƣ��p]���ԹU>��6@��K���I}�$U�&�-\�ە�m�s�!�[G���-6V���H%���C֫�H������8����0N��H��a6Y�V&�f}^NRQ�l��8�V���V��a�����e�3N��g��;���^�_a�/��nzk{����>
w�����o_坄�����%,A�Uӱls�������q�a*X�s�Wd�~QwB�[���VK9ĳv�ȱ�l�<4�7dߍ����R���p��(u�l�}~\V�W�u��jo�`�K��T�d�xo%�Z�>|�_ΐ~k�;q9f�w+O��CJ�^(�b��pE}k��;�Wˋ�	8���j����y++��G�2���������}�7��͔"�{��� ������G���?�o�{t���g�x_�Nw���g���vHU5Vl�C���������ş~���~�v�iZ���?��QP���t�A�����t�w/�娇h^b�+j���L�Q>��^h>��r2O�w����9#:����?f�� ��3N~Lӯ�X��9�?2��S�����0}{O�y�;nP��۫N��_��A�g��O4u���k��0ǉ�J��=�Zq��j�4�{���Ø�0/ɇ�.�
����؏�/٪�}���,r��@���'~�NF�w�;�X���L���%f�f���R��W.S3��Wrv#g��7��^�}�5�=������!X�{������cԸ������Z_�)6f�
Y��\���r#�n����f��FCO��ߡ������9퓔V��ۀ���$ͷ5�Q��"{С��u�߯5��T��+5�\G���l����+�sW�mo��r�U�U ����?��y���é�N	����_;fƟ���fsoy�>��Gܘ�ݳ��T*�=�m��Vw,�|�����v�<#d;��x�(�jQ=G
�H�Us�ŵ�h:?F>�YКzO8&3\��r,��r�&�Q6EvKܸ���z����5���������Z\t�5�����ﭩ���c<�1��x�ï�����>��s������Z?�V����^���qw'�]W�I��ky��`�>������V�9Wq�5\x�>�or�����x�.o�2��|G�*�<s��Mk� �>,���������C��cUד{q4^��"��;v��E�oڧَk<~�GD������0���ٽ�+�6k×��ʝ��O��%��iHg��ţ��o����z�����r�`��ӽ�*dK*;��[���e"R�j���G�o/���Z�EȞ���{ho�u�]��J����}�&�����	ZȰ�{D��z�f�{��g���Q�X�w�-���v8��Dv��
�6b#�d%�����=������u�v<u~���A�ܱ�y���t:
���ǡ8���`��˫a�n��pO|���p9	Ws�\x�"�r~��\2�$S���*��@�..9�$/\�$7sI�"���p��k%�٭��p�u��G܍�q�n��<�X��\q$h~޽+0?_�/�v��+d~f���������E�s�G�f�w���v���S��]����]����J>?_v���O����kw)��uW`~��E�s���û��4?'���]A�����������N����qo����(��gy���_<��]�s��j~�Nu�h���Pwbh�F'Z�+����L�V�%��'�%�������=����4/ɱ�����;-ѶC���X�)���Z�;�T��ă;�%P3��h�{vPK<���j�q;�-Q�i�%��Cո�ɗ�2#�%�����-���+������OgGZ��s;��5O�M�-.:;@uP�z�οnn���A��v��2��ϐ��U^����Z��ƅ��:��
l��b��|l/o�v��-p����[q�1g�v���%��A��8Uw��f.��>��1���T�L���0x�`�.���!۵��^�a��0S׸T�>2}rG��A�ν����r�n�0�Y��=�L���cT���W�_����*ߍ���������?���ncM�w��m�6ͨ�|8�ք�%4�F�^9�C~�7s+yJ���{w�~y��7#�FO��DT��᥸���_G(i��l��a%�˨fI�b\1���������j�(��#p�z�ȭ�m�֜�r��x�"�?Y��qOy}|��-���y/�hW��j��_q�9�%�l~�+{\`��𽉢��1 �X��8c _H��V���V<���t*�G�n�n=$
�����TY���-!���w���-!�o,��T��-�G�y�@6b��*�֮���S۩��l��wz�o�Ӣ�X��PW���������x�n��7��I�n^:����׹�������;��?������wѾ��Ա9��ȿM���2Z'����CF+�ɚѸd�����u4�J~�e3��e�Y��%fݱ�>A�KE���M�i����I^#6Ne��U��[�* r�Q�������j��������٬�;��������h��Ɩ�=�'���m��ح��W����m
��/�U��M!�sOPŎ����d�oҮWRX1��OD�,��w��B�C�N���>r'Ԇ�w*���K�B�=���J�b-.ݨf'6�k?N���Y��́b���|9���>]�MW�z�U�޹Q�!{62��HM�P�t�";T�}���W�-�ۑ�?��Z�N���'p�n3�t���x4l� ��1�{�f}<�_Y d�I����j�=�XZ�"#A��O�&�z��M�}�N���[ i�5G�[�abKVxz�X�[6!�[��~ga��.��^_������7�+�O�������hv.b��nЮ����ק����6�^堊.�V��,���}��nπMAc���!}3uWРxv=��^���H�b��᎓{�l�g����^������0�>~����2�-��x���0<s�ϼ0>>�Ǯ"�s/�������-�H��O�쯽�[�_�
��}ftsN���u���?�F�rcQ�]�~|,
L��ܩ�RΌ��KC�'��fF�a��A���ҏԓ1��/a`�'E��Πݸ���X�>��U��O��/ {g߽����3*� ��sv&g�T��!���+ξJ�Ge?����Y�����cQx4����ф��#������xҾ.�}d``����Q��6��w��9���Е��=��Z�i��'>��k������u[��P�v.jf���^��>]�:����Υ~R�t�XU�݀67����m����G������ʷn��s�*"��G�W����$6�a����q���M\�?�D�
9�����|''��2��|�i��������X���K@�U�V���^�5�8{.Í؆V�1"�.h����Y���~�����r?���{�{��^�_U�:	RS�����H0Kd�W��Ȧ��Op���*�
d��\7K�Zy����^���Kݿy����Z�rW�}[�-��H��mA����v�l��ֆ���ۂ�M�Z����;C&�aۂ:џֲ݈�h\�n,�c��*6��4뎵!�/�K�V)X��j���]�>�3܃�"��'_����1zA��9m͉�vbj�R�y_������?�1��8����_<"�:�g|�>�ۂ��2\���@� ]�#���[�v;*?P��3���8W�.�¾�t%��l�%�ǋ�x���A��������fԎ?+�_��4�Jl콃����a�� �ͻ���o5���/�![޻���fKP�[��C�l�\�kB|�Û��SΚ_v�� �t���TvR��/A}���$��BV_���$"��|Ο�����Cf�Q4�}}� ���R|M���qҁ�Bd�����ٵ�X��b����aT{�E�Q�M{��W�*�y�1>��V�g�ز�\�&G�������e�^���t3�3x���_m�s��i�kOQL�����y������j����}�M�~�ɡ)Ԇ��ZM����`h�uP0����0��2���s�N�'�jW3��@D�������5���������t|+�E�Ӏ�7���۬�-��}��Q�>|�&�y��W3�?�3�ư鰜
���'��X�^�����.�[I'߃DV�'f:��9mҦ��d: ��@2a�~��d�f�麟�.�Fy�/��l���m�~� vl�(_S�<�լ@b�8<�{'�����+'a��xL�}N�	x�ݻ����DH\�����}�w9;�����W�Y@ݯV��'E�WH�@��v��G�0��{o�=��}El�7����{�@�{�<[���Е��-ث=���.<�������K���ѻ�D��6��B�g���8~�b`�A����<`�.l�_��/6�*��O�Z->�����W4s�CT�H��K�{��=mmg��|xn���(���=�mr_��LLL@]��a!�>[�SY�WB�E~���}y��ޏI�?�H����C���zO�c��c.:�Iϟ��oWN�W~�ْ������r��Tי%>����&��rw{��sPl�U��ޗ��anA�Z/�G��S@!zyN��X�e�t��ʧ[���#�3K[q�{O;��2h�+}ТX�L��@Wy9y���r���ɏ u��؃-�3;�|��,��)~Zoz���S��Ø�S)d !wSf�)��[p���q@�w��Ë�Ձ���L7�@b���c�>e.;��_ �c'K6�Ä��v 6��4|�NE�\�s��"�2.{[�W�}�俢g��3��_>\��KfGg�,6�e�^^�f���0�o�f[U���nC�Ǩ�3��c��10��	����o~�=��L�O���a'�AyM�T?�t��yu�q*�%�&[ߡ[��j����V\����2����ˁ��.@�#Қ?
4\-�}D���c����%#4o-��!C��8	���֙z�(��_q�;|�`(�v�c�_{�r�>H{���ğ�e�e�W�J����/�f��k��8�6��\������TąM��5һ�K����D&&?���Q�w�����i��X���־ݻ�K�F�_��f$�G���Ip�Gb�=9�1~�c��wo����3�i� Ms`}�赋�5l�}gؚ��w����>
�謁�"P��=�K]����K�?�O��F���N��|�[LŖ9�F�%��>����ڔV|:����k��Õd�ې�>�9'�;n.Xg|�ۏ��M߾�޵�����`��k��H}3���ͥ��gE�F���q�� �ydqe�u�J�UMi;�&��*�� 6��/[���'��ix��*�m�N���%E���F���u��	SAxsd�#��r��P��>��{�@^�-�E�����k7.����[���zb��sR�/f�^{;�:-����b=��q�x�6��M0r_�j�b�/� ��R���M9U+y�
\�\E���Q�3���x���/�����b���\ӠϚℵK�+yo{h)�GbMi^��#�Q�1%�Lu�?���M�� }�c����Z����	F�A�c��G侐R�a���#���!�>��_�Zo���y+�wN��p'�����o��gᇀ�/_���y�xk�"u1D�������+��ް�=�=���6����G�H4j�?{�.�)��Wc�/h����-����j�6�φ;����wz_��~4z�B���c(��8v�@���wY����7���rJ4��ϟ�������/ֿ2�C��[���ݻ��h�3������;�����[a��a��g���������/��ފY���y�X�H������}�c�Ģm�ּ�����NW�D�q����@�|ϙ�˘��%/��ڝ����Y��}��MK@��5Po����
}?RS�a�M�j|	%d=}�ob16=^��zM��A�����'I>�&�)�R+t8����W�@z�czW��������^�A�޿kӋ������IO7u5S�ǯ���9���#�n��0�	�cw��j�x����/v�=��z�_N}�ޏì8n���u������u/f>�^PE�oJ�	Pl����������X^�=��m�"�M+�Zd�l��d�HVW����P]�����A34E~�u�N�~F:�.n6V�-�7`A<�5#���j��Kbo��|tv�ȿ �����&���p_�MIO!��W� ��z���s\L��r�2ּ��免�nģ�?Ÿ?��U��F�;����h����Ƿ�<�hb}V�x���EX�&}�fl"~K����-gJl��:T\���~�6�	����-XL�%ƃ��|X�^U��F��ؒf������B�=I��1�CL��ˁ��i��F'ֺv��h)���`��{���~� �z��?Y��u��Jֳwό�$W���Wb�-�M&�PqF���mND�H�[w�o]g_�A�3;� ����-��5���s����4�W��������5�9��>�����Ey�EY���<JDi�����$5�8��ZL��L_�=���#�:3�.W�g��8K�_
�2JMŎ��MI���E��U!y�P�t\�t}PJ��������A�k|��&i?Z��R�5�
�溎;6���$�c���M�~��D�lљ{��N�<�S�'s�{�����m�a�b�_Y7uG`���G�C����hX���Ee�F����"�S���m��%d}����F;��Y�����xAt.�=����o2ݻ�*_o\��b�5L*'��������a�DVᤨ[���m�a�kvE
���{r���f�'��F��[/�Lo��J���JH���-�7�[n$�[n�ɕ�K�����G蚋�3v�+�{���u���K!a�|��A?�{��+S��=�W�/n|�͆�ʾW����%�{Y}�����Ӏ��Jx]��*ޝ�jʂ��>���}�bK��9�0.
W��Ux9!���z�?h����qS�ECd��q�'r8|�kr���5<���%�Ӎ�Vc�&��)��{��}�^�%���؏���٠[�{��{�.a���ߑ���wz�y���<���ׯ�	J��g�#?-��\��x�f /�$�<ʋ��_�v��<�!�<2{A`A��y��=�W��T��^E��g>��͌76�N�)��
ڞ��#���Օ�S�7~}��)"���D~�+>1�i^|2����0�v�vi�j�բ}��vo�u��.�߂r�b��/���`?���׎�ב��O���C��J }%�m	�I��9��`6����Eƶ�^��i~���K~W?���!��������Y�s^»��z�Π�,��y�ym?�r]�r������J�[��2κ�a/���='���_��=���x�h ���ڣ�`�{!��5��������9���?aB�0�UivR�u��6�*H�5	����_�F�*�t�)�R��B������MtK�+"��Z)Hy�٦*K#L�fP�aV��δ���4��
�ٖ2s�$�Q����g���ޙ�%�f��Y�fV�Ioq�E��Y�.S&�mn`$�4YgI96��\�جÀSd�Vtˁ\�L)�)�J+!{P�� U�3)2�\,s��K-��1�i4����d-��&�i\,�b� ɩ� �\����7$8o`	�Xa��$�	��dg�f�M*�����&�L��b����$� ��*
M�?�9���++s���q��GOKF)��e�6KE���x��u����T���u@4���0�B[�.K�s����l�m�l�#� �kqT�19�]YEs�.sup
LVw9��v���@���K]�DS��zdk��&��g�b%%y�q�3�4˜�K�s����%����jK�\��jɶ�e���N�K*6U(q�&�
K�o+eBA�zp:-N��dd?%�T-kA�R	�R�EId�l.Կ�h�B��1En��jn�Fe5Ԅ�Vd.u;,..�V4�d(�f9��p�fKE�kn��M��K�^kҤr���cs�A$8a@�L��L2T���V�㦮*%H��MW
]��.m�֌C�i���:�d!�cqC��˓

�	���sUV٬���Xp���]�� ��*S6�[��T]6�P�EDʩbU�=����9n�2	�:*���UP�eé�^b�+�b�
�]���>����S��JL�P�(hS���+�r�P�0y]r����2"W:�|h�
��6�\�P.��ZH]3#ɬ!�/k�T`vU�ʔ�=�怲A�70���)5Յ��*�l2��1�;�}k�H=� -�I0��̒�v��Bd�`l�������.Ĩ��lXU�d�A�+���
�����*iҝЩJ��6H��Yo>�8cY��J�ͪՕY{ֿ��L���=
�v2 �.j��~O:jjx��@�:m�2
S�y�HG�MZ������Vjf����Y*u:`�ʨR���c�;.K7&]���<�ɣ�5�Q"��'v�F���U�	N4��ᕮ̄�(é=��Z	AP��a��@�I3�:7K	eAe�b�[*ܼ��Bai~�J��1�d�pC	B�5-�Z��	Lj{a�1��0PM/�b���0U�@�с��:���M �!)��KS_�^`�R�!�X����-�f�Rȹ��`�C4�Aw��\C	g�Tk���3�g�P�ŶY�ੌ��j��c 7�V.UA$8��a�P��ģ���ʹNK)Zn�t�[�08�!C6��RC�Դ�j�5Ө��R�m)qN���L��1�6��TKT?չ ?<Q�j*�m�H�5�pT�⼨4fY�I8>p�0Y���-�)�l��aB��hU&�j9VNu=����Z��j� m5�<��\C�lvڪ�,)!(N��n�G�h��p$����56�u��8�ZXs�Z�Z�]D�l�����5ˢΆ=��O	۱��ݖ*W��
�+�~��f1��U�S����i�&�قl�����Z>��\��CinC���(�$�_S�/d다�%�ҥY?f���ァ�6�YhG�}qWie��B*�U�����%+ٕ"M�J��j�K]����%���Q1Q��B�67���ͳ5���s���2g	M���k��q��qv6�Ėc��B�H�Y�.��,Pe�bt�9PoSLUn��-Vm��	�΋m�����(�&�Rl��6\�V)�96[���(�+���l@���< em.Mr��8�f�kr��Z(6W�q�}HQ]�*-����R���*3/�C��	�ܮ$[9.�ˠ��m.�̤/�˶<�Lh�2�'���{�� ��ք�)��&��H��ۆ��W�t�T�mV `��e76�pg����Kq�a�����w@-��L�4��2�V
�h��9��)�v��Цhݯ��p��`�Gj3�NY`tp�
��`��FN��$U3)6}������4�A08VZ*�!?#,+��|����CW�i�CY�Y�-vX** Q��`�6�F��ձ�V�2Z0�*��K�+AFP����b� # ���\ڎbp���p�����+�$&��U�"h�95�jbU�p��^N�)�xr��]�A[��{}A��htٮ�d�Y��F��g��6W�#2�%�'�Y6�+�#�i�e��܂�T��vX�k	<u��E���`�$ȵKn��:��7Qo�v�P���,Nɩ�SC^Q�0'/� 5�(�s
r�t�B��	�`����<��4��栅.�\������aQ�^	2�����Dж���"�DP�"s��ޛ�B]��m1�*�=�>$����0')#]����HIQ�T�+<��B�"/uR7
�%��&w��3Rʨ������q ��nE0+sRI�4)W**4��&�L�M�&ZG��8���&��h�ͱ�L�Z���p#��F��%��aÚ�*mN��DR��gS��$u���P(��H�\�~N]ʮ�N!(��l���u"#����W�lNAorWT�5cY���Lh�`!"3P�<{�_d*7SM+�X�;�M65�8�����9`�q3|���҆�f߲�P�b^��v
�a�y(xa*�Je�̔&�c���e���P�,�fzVv���i�*!��%Q�&_W�&�/*Ԅ��5�i�)q���rC�h��s3[�lK^<�y&_�WFap
�Ys�j@�).ʑ�cK`��R)��f�00n��TZi��%pKgA�R��o�
��\�� /;�B1�3Aɧ��644�V���0���np�\��3WZbCd7��[��#��%*OrZm6;L˺1��D���X��g�a6.�F�|a4���HHD��!eϕ`�"0��>��\�,4�T�1��Jg	�	����!U569�+-Sǡ@��ѩ��K��cR��cӕ˱i��ˌ����岺p�nL�*�?A7vlJP(MHg]���J�6��(}/SR����90:�Is`�X�	�sl��\��J3�6z(��]�l%��H���F��o���[�ۄ�m��P�Quǣ�R�{�L��]D��[��WP �PtW�~�P���+��OeK�����Q����'���0,��7-��@� 1;6S
-vs��k)�m����Ω	hK�]�rY�n\8���p-��nD�-XRZ���"�̮$���iFwK�uWU��%�[��1ݒٴN,�߆#L�6��{��p%1�f�ZJ�8Aء&]&X�볓R�$X ��ܬR�-=C
\u�I))�=:w�a�a�E9Ey<w}�D��r�	�Zca�1/_�6��D�5В�$%��(����γ�Q��\��7�U6�}������'��.���a~nR����c8��Rb��Tf��%X��9�F(��,��$����@�l*��S�R�q���˝�%�A�M2��p�,,�B2��R
�Pr
G"��FmZ�H�����̔1���zd��Sąfd<� OOWw����Y<t�y.�H
��_BWr�Wρ�G�J�Naa�9k)/8�>i��!�e,y��N�I\�ɑ�s�Yl�Fel��ͮ96鍭����Ȝ�d�j4]q۲
sV$��.���ά�S�Ҥ����/��=e�*֏��ɛlq�Il��g��4f���]O�a0��1��@zZ�o�̣L;�#Ԥ�*����tc e|��+��Hɳ�-Ub���΃�-�&U!>.޻�9Iv���(��f+-�`]��ے@��Ԩ������2��w��x��8�<Sbg�m��En�פ�Ju	i��g�x
;O2�&��)!3�T�[��\Ԡi(����`V��o3��Q��y��x��/w�}ǉ0b&+��;���I�8�S;� @ ��r
K�r� n��/�1��ES����o�no�rƅ��*�&e����DQf�d�l�,�H�M,\Y�*$�+�ſk$�U�H�ߓ�g��5�]���TɅX�!)`:�(O�ɮ�U�.��}�$���2������&��T��_��K�Nµ �J�4e��P^P7B���H�4f��|��!I��*�S����5�V��8�o��3q��
��J�]�����_�˧�hR���1&�K(	~�$=��E����\�)��w�yނ�P8y8�0�Q�h~Of�z�d�Oe_RnvKGIvh�2�EaB��Z家
�N��̖�dD@C�S k��kh��]bѧPхb��0�%��"L�o��b����2��\oo,`�(���`>���	XJ6�I�W0�qGJt�lX�١���Y.�]b[��x&CW��&`09��j��Z	���qn�'�|p��1��XK%�šmUsq������=E�0%.Z�����AN6��εU��aܷ��t� l�ө3��R���{f֏�\�� V���렓GH �
.�����1�C��RA7qm9K(�:��H��Ms�������B� �?\��	�{٬m5�@3L���%�@�����7�N���z1�+�-f��eX^����0��6;� �
)�r~j��u>v����j�&���K(��N������z��r�G�\·EB�s�I��'Zf��L�]̥��ʍ9\�*W|��/5,��L�@���XW)[H�;����x�W�B~���+�2�+=�R`\d��*]_�2h�i)cR�
��Rҋ`ř>jlz�^0���f����{{�L6U#��1�b�%ܙS$%�,M�R�k.n�dK��8�J`��rܳ)*�˥��z��6̀�f>*�3sq��>[�*����>[��3���q��V�l387a����-+,|��u��㻏�K�~�0q�NHON_��{�;��2�XF��c��p�80�3r�%��1W�8[�f
�J1E( c�F�;ӄ;��_R*~��1;<b��������+�Q/�1�)�����-��hM }x%P3V���,��&�-��$����^�h�pJiB���Qp]�m�_����P0�L\�\�Q/S�Q̞�QT���#�Ɂ]b���v����0q��%
�,��.��@�'�wMM^'�ur
�Neߣ��m&�X!��v��RqA���Q2�����#`,��f���<�Ɋ��7��Sd��cg({�)�pOb��e@Z��d]�伢�t�=H�Ƃ&����Z^J0��(���gh�Æ.�l�
]����RA���4�
hTf)50X��"�UZ��`�Fe$F�K��d��_�x�a+��~����&�5|Ӣ�M���!�dG!t�a=R�-6�������MiL`E�l[	�!XE��l%9M�٭�\��	�W��:D��)�h�`�kb�*U3+�ɺ<} ��hn~5�`�T�7
��:�4�D�a��g�a��bXe��� Hp�%�"k������L1�-�MA<v-�JK�����]�n�J���;�#b��
�~Fi��tɊw��)����Bs�*v�MJLp���ֱ3�>���fK���I5�Ņf.S��	A��'ͼ�1yT��?;�����+�+b'$�:��<��ϲ���}�p��n��4�	�I�V:�^9;9����CϤ9Gs�b\��L��_�9��lޞ�?	G�]�@?e�7p�����Cg�I�������p��^�X��3�L�r��ɩw�~^.�rkN{�#�;~BY�ϏN�T��T����4�%?��SNH*�&twG0�<?��8�����ᄣʧb�
(�������Ժ9:�F�ÿP0F{X���$x��t4����p;As|X�	�v����Њ���{�|��"~�-8�.'������Jf���?�ܹ��PN�)�n��*8&���(���?��- 	e,S�Z�ߑ����$C���裹3�5+�>?/GO{�CB�S���'!f��KxW8�,���:mv��v)g:%:���N4�(X�Ǟ|�������.���m+�t���K�I�2�d��u)>bc���Q&�Ke4ow�J9wN�6�.ށ*�R�Q��S|��c�W���ծ$@�w#Ymo�Z�L�)8���Z�r\*�60����;v���=����T*g�� �\�)<i)eD*�lA�D�-eT�0�ww�m�J�Lӿ]H��v��Qc��;��4����]���x��o��+��0;/uDz��_�CN��D�FҔ
��W�4K��?�C�B��~V�� �� ��c�fr&�FV۬6i6���h�MW�+̓~g�Kelc��/��3�3�@�����b��$��\ܣ��m����M�
<c�ڕ�\�g^��E�]���7�qd�3�.�f�L9��w��'��DJ̭��m�$>��x�'�J�]�����R"wJ�Y�A'U� T�Ã2��a"Bs6Hi�i�RbNA��t�8��=���)qWBb�J� �V��x2*ƧY�bp0ӭՍ韣=�dr@73�xPHѯ`�P�f-��#f�0)��ha�!i�)q�-i��X7E�~Ի�N�ͭ�:�_V��>��EFP(?7�.zԣ�`���@�V[��bn�@�4l@vȪ<���x�sv���~��W��a�%	.��
�"9qwА�3R*�X\,%�]I.��%
tT뽸@JT��qjAgN�F�)OF�$K�n�SӺH�T/������O�&���G�r�@0�\Iv<Qf�ƀt�=ц���!c�=��φ��4�J~���B!-�{AbH<�]�9���%?��KR�����e�o��	f+�$����M���K��,���g͖9�ǅ~�Ϫ=���7 X����A�	jo��'x�>��$�Ԇwtj����W�]��'p�D��1Ԍ������$��H@y�~s����|Q�Aڡ�,gWs4V:����9��t'�%9�r9���0��C�8��q7r4�s�K��w�~�Ḍp�M_���9چ�>I�%�5��io,��^G�8��O��qw*�\J��p�mT�;8��$��`�'���I�B������|���Rs9���7���s��Cx��;��8�H8��@x������U��	;^�x��C��q�R�݄��8f�.'<H(��c2�%J��O䃜?�p$a������M�����P&|����+���!�D�L���S�3��>�����!/r�5�Ss��||EE��$�C�O"l#\N��8���.�/B��_�����|=.�����9���}9B�����H���)�e8��f�SH��'9��GѕrJ�8��0�c�k�.#�x�لo�9^}?ǷHn����	�7��s�:?��	�g*�����(�Ä��g7��,!��<~�Q�"����zS}!��0�0�c|��ğI� ᓄv�[B��WQ�؋��o'~��8~N�d
O#���)��P�[pz���$ƽD�� �%���Sx7�D�W*�6�굎�O�L��s�'�����:�{�P�)�1�Q��^"��؏�F���z�I��V�v�wO�+z>@����y��C�I�V�}��	��ʅ<|5�Ѕ����K 9���x=ů&�ل���ף����q�=!�<F��	�/�o=������Jx?�Ʉ�~Ix�߬�<Qs����x=ŏ'�d´��<��Ih"�C�!�����
�'�F���W�{��v�HU�q �P��c�	��	�%|�ҿ�c��s���e��	�v�]�{��A��&�2���|G���O��Е�R�(�?��K�-�[�^"��1���p:a����P��G)�'��-�	�~Jx��{������J�I�����1D�"��p2�L��⇎��Hn.�|�}��<L��	�~��_!\K�C�Ox��(�I³�߄�ٿ*+�\����p���������L8�p���|�Oh�d�\a(����P���g��&�;���	�.}98~h��9F!��p8a*a6�����*�U�_a�+g.~%8���9�0�O��>��	�o!��'\E��� ������C8�0�p��{Op�v
T��*�F�?G�H��L�ا}>L�#�^I����'�V���	�!X>���ϓ�j���&<C���>"�F�d������O���B������s7��{�2�J�*B�?�O�0�S������/�?����H�E�W	� |����G�6a+�&B��Nὄ	?!<N�ߗD��0��D��i�QX�/��pa�#ݧ����B�{+	��ϣ~�P�_{�������UDo%�փ�'�~��a��#L L#�%���o�ge}fn��M���x=���{��U��kw~�C�=���WtP��	
٧i�׉x��Hx-a�m��}4X��U������'�$�_����Äv��KW>A���?�'�ӓ~��#}	� ��0��v����u�u�Gh�O��K��|?鵞����nO�o%����~Jx���ׄ?�-���?� �����?�=�'.	��PX��0��3Cү��o�����������f�q�����	?#��#zᏄQ����C�q�W^Gx�M�I��3u������.&l�}�ϣ$��S�����%¿�M��:�]���!�@�'������K���Na
�x�<©�e������Q:	!|��E�7������t�#�@����9�K���s����N�a$a᭄:�	�e>=}
��t�)�LB��^m��"
?F�.�ӄ%�;�����(�^���^"�~���'���B?�������D�?�p��7wQ�����>H�@��ӄ�_\��(�w	W�o����?�S�����:��x	~N(~Ix��&�_�L�h
!��0�p<��Y�wV�'\B�<�
B/�v	��L�;��e����C|�b�����(i_y�-866 _Yg+UE�ѕw�j8V�m�U��-A��)7v ,V(Wy�s�k�{�Pf����%�4Wم���K�Ze�j�0�bs&�foW���<��S�Tfr�:vV�y��B�����jUNI����j�_!�ݖ2a&���' � �����<t�xL��T!��*�sR�X�j@I]	��P)ە�I��Y<!(� Y�A����Z:��~� JvQ����΃�wT��Z�YeSN§:��&u�R�U�i���Q�(t+���3)��_R���(A<���*�e�v�� !OsMi�����x����x������I��x�TX�7ib��X%I6(���� Rn^�A�� B9`���u�@b����r�������*1!`����8�U����_��\�ޜծ�� �����i>�x�}6���i�ln�ݍ�����p��I�� ��L��,��w��O,`�jŪ<�HI��J��UhP��{	>�SN�`����APIm�^�oM|�1cPW�n̒�T+F���������,|��g�����6��s_H:(���z/�ws�j�0!��<�r����J�{��O��������ꞯ�͘�ݧ�&��J�Ԯ��J�����(����7)����J�{H�@����I���oW�Q��~�~+���??���+���*��ۣ�{�ϧ�{����߫�P��E����
�U�=�7R����A����T�GP��!�W*��_�����Q�_Lo��/�A��?�G�׉���P�
���Q
o��1
��I
��i
Ja��~����`�����G�����������c�o1�ϓ1�s${��������'��+���dT����~E��$cP m
�ȇ�_z�.��P3��R�w×J��鋧Ǵ��~��ZZes�S���#w]��)�.�l�\'`:Fy��I��I��*��w(��Lym��􃞱PYB5_��c�B5�m��mE��׭k����-N����r�������	(I�����V� ��\
xp�܂�F���0`'�0���D�ŀY�� �v��G���k �!p��pH��/ю/�0P4��� �X�
p)�W V�=1>�1�N@����`%`2`b�0z�� ���[!�!�����v!q>��p`�B�p1�
���[P�a�0��{Q��p9`a���� ����_�pٕ�d��,��ą���$�ӯ��8�K�j��G�2�o������:��>Am7�y����CB�o�}�F~�Y��:��rV���ߡ����B�k���}Я	�g���B��@��M:U@��^���YBr�9�g=3����};���t�Į�1}�_Bc��}8З=1���n������%�Bw�0�k=<����k����*��J��8�U� q���І�Ȓ,�?rϱ��ĀI�+�'Y���ZRp�z�wn�	1s\/�aZ��thKҔ�6@����c�pCʕ+��2e(�M�ܤ��_����I��Ho
�Wv������_����j�#�5��U�_��c_��.l��s�y�g<}^�t��~�j��C�9����
���:�F�s_p�MϷг��>>n�UGO�?����{�x{�!�(�p�il�n��l��D����Gͳ~*�>o\D���9>{��^��ݛ����3[������F!���_f�/
��3���P������r�D�g�[��"7շ/�>l��w����Ao�&�o�w}�7�����M/�Y��qÃ�/�7����[�?6*�Mĥ�b�,��vO/�Ζ�������^��1�0�x�7�߽��_���n�X�c�U֔M9��N��r|���b+���x+������7n�B�:���+*����=[+l����&;�|��|�q���.�k�����?3\���x i�:� �_���="�x���a_��ҝ^�A�̒
~ȟ��?_��c�N�o��O{�>uO������оz��=���x�xxXD���@�e4=mF<�dɘ|�A�_�i��Vizl8ڨ��4ܣ��z�m�+Әq�a�)����R�!Tn4�^�8���p�4M�㈛��s"^X����r9�������N��_D�������#�.W����7"��]��ߋ�A�_F|�_D��_">�ػ��oDl�Y�goﵾ��7�j_�/�W	�`0��O��j�jyj��4����9/r'�0�����}�f�Ssa7��C�iГ_!?H~��f�^���"y�?�ɯ����+�華�O�?���u�t��1����tf�� 6���`�s�tG�?cf:C������!���G��B�c�4�Im(N�ӕ�:���e������]�5�ei�e�1�?
OdS��I�ew$'�cWI�t������X�6�%.ϲ5ˮZ�RV�+ 3}�̖�Ѹ\��m�]�-������տl2Mnj4�y��^��C�y��Ҟ�;�..n�T����B��;j���ʗ14I=ͳܵ�tCG`�4�ߠ.�iwms7*���g�������O#��FR���cu`�\����Yq���P�P�n��{_�<WL�{6�o����j�8gTqq��G�K~�HY���=��� �U>S���_��Gx��ˉ�\�?���Hd����D9���e�+T�''��Dv,��򏒭�8Y2�����S�6;!�m*PR�����H<��wkT�:�@��h�Ao~g�tK+�ׂ- �Ao�� S��<�������Hw15�u�Ǽ�&�Gq��?�O� ��O��@h^M��?SsZ�y죘�_k��6�L͑)M����Xm�)$!��?��4Q����g~����O� hV��v�s	9��&���Y��Q���Q��O�Sf�kЖ�u~�3���O�� :���?���3O���|Q��������X����?���[^���5�w��|��\8����*m����������?0�������f�SuG��\7~j?4$~��G�� ��?w��`��su7�z�W��7����L{^F��׃�2�}F�V��*���:nҾU�?�����l������_2n��nWm�E8���u��������r�W��������ފ`�Z�y<(��z�o�ߦ:��4+Mo��x���A��j�ng��q�3~�xCy\��Fg���%5�y���P_V�4���t|�3N��
g���fg\��g��q�3����㭎���6���%�������Kk�����O�w:~���t|��t���;:��
S���k���,��߫�_Q��:��
�����z��\�X<X����L��f�=	����?"m�n'ߔxu;9*��v���v��H~┼^�|F��v�v���[��?�ʰg�K�?k�$���u�W�j�+�R�č�X�^�]��}u���ߊu+Ԃ���x��m�[(�c�R8���!' 9�M�U�8��wH�a��z�s��?V8�?�CΜ!�խ��!��������Џ�Q���-�g�Ux7��@/�K�@�x�zR��(�?n�o�@�բ��
�yN�y�� }ܠ�;���}���S����wB���Ay��<����1�σ���~�Ǐ��� �ˆ=㠟�W�m�?�3�~�/����o@/��W�G�[��[�����_)_�\|o��W5@>p����7����S���e���'����"�7�������-�����$�����|�.�gP��S8������)����h�^���0�3mz9\܇ukw�ި�Ρ�Q{��S��o�"���~�X�C��aЋ��"���}J��I�SO�r^��=^�/_}z��-A�{\��'�,A�\�p7&�뀋��"�o�<�!g�	�[T��������v�S����E�S�!�O�z�D�}����#�G~��%z��� �������p�/GH�Ӻ��(�����������3���S;y��yF���d�1��<�c�˖�|�O_��[�����8�ྒྷz��H�I�|z��S;"�g���d'p�pv\�'	���5|�.��N�!z��ǨN�z� 9���?G�B��9{����. ?�y��w(_�����#�^��e�}GǛ�3��ǀ�?�p�]	|ᄮw�9������3|
8�{��:�����p?���	�_�{F���d��rx����8������~�
���j�o߄��S���� ��� �؄��D�����M���5����y�$�^>��i�6���pZ�M�=��_3�'��:nz� �7�മ��'>�M���o����6�כ��z.��_���L�����b����b����������B~���mu����%i��{|���$��ϧ5������9Ћ�m"��s�ͽ��ϭ��~!���y�m�S�s�z��׻B�?����
�N��>��Kt����r���-���T�2�������ї����0��b�y A���� �m��|?v���~��7��,�E��Y�?��*'?kVz���rS����{����,����z����^��!�1n�}���"�����C�X�(­h'� ���s���~�����{~\�����)���A��:���)?gȡߥ-m��ϠV���?ޢ��9E�5�a���3�8=��j�[~�J�{�A�
���7�{`��[D~���[�m�2|ːO��N�^�#�%��6��
;.��H�?�'e$����d�����=�͍�R�X�Q(�FF�|���y�v{p`�N��>-���I��c��?��X^D]�$��G3��DƖk �Di����R6��l���,�7n�i����,5�H}OV"9?��y��v!+~�n;'���'�⏿S�,�+�܈-~s.釓)[.�P�D!m��W��BQ��%~�.�_lC ���S�����nI�~�m'����-��d҉=iV��Cјd�ί�+��Cye&{"�� O �$�vV��Y-e�OVqͩkI˩�}r�̹��P�_�-N�*���7NZEtIB:�.��%�M�Z��T�	C!I�G�Ci����v&�N�ct�	#UF���Q%��|�	O���!��!�.�b��Ԫ{*�Hr�Dro[ˤZ�ȓ��ay�%�[�� �<�DRԉ�yLm��$�z׌i7
{�S�wU�Q?���KwW�	qZ.�v�C;UA�}d�*�ŋ��;Xq�1r��t溰8���+��զJ����b8�V����8כ� �9�.	�^�M'e��Ϭ2�;n$̆)[B&%�]Ld��Ǽg��ݹ'GXV8�pg4PѪ;��.a�A��sO��`���1���f��e8�JY
a��a����"E	u9D;�Ln��/�A6R�d�)���U?ױ
�x-8IWg'���ۈ�&8��[��󤋉1.�+4���*f���G��`�r��m���x����R:+���ױcP$�+�!	���u�r�U���[�W�>x���H=w_#i��.�w�{ar9� /�<���RC��/�MW�(IK�"���]��;��v����+L�?��3�QnF�7*w�:���
#-KQ7xnԈ�FQ�n�烩����,/�'��V{v�n'�Hc��
B��!�)"���\n��|Kn-㤔�����9�@����w�q lGS摞��t6�)+;Ո����L`�R�7e>�����{n���1�#�9��5%�m1髤��eSϠ}ˍ7�ؼ��ٳip���)N^�I��d6oY���z�?�pR1�QD|��o�>��o����N�*J�ħf��}���+�!m�#�#��<���6�B1��w5PmId�6T̕2j^4����+x9�l�Z��#cr�1^���Q�#��kM�vU#\�_�.U��	�a��My�)�_�je.�j�Å�Z��a�Y��b4rđ}���r�!��R�OLD��fyE�eǒ��F�Ɠ5����k�e�Q���Y^��4�t�B)�Ǌ�����:n�K���|�I�]�ı�U�Ǜei�n�z�%ao�co�Q���V��:�;�7[@U��OT���b�.���bx���Ttf� h 