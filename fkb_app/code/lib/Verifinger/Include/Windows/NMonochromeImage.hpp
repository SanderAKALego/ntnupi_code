#ifndef N_MONOCHROME_IMAGE_HPP_INCLUDED
#define N_MONOCHROME_IMAGE_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <NMonochromeImage.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(NMonochromeImage) : public N_CLASS(NImage)
{
public:
	explicit N_CLASS(NMonochromeImage)(HNImage handle, bool ownsHandle = true)
		: N_CLASS(NImage)(handle, N_NATIVE_TYPE_OF(NMonochromeImage), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	bool GetPixel(NUInt x, NUInt y)
	{
		NBool value;
		NCheck(NMonochromeImageGetPixel(GetHandle(), x, y, &value));
		return value != 0;
	}

	void SetPixel(NUInt x, NUInt y, bool value)
	{
		NCheck(NMonochromeImageSetPixel(GetHandle(), x, y, value));
	}

	N_DECLARE_OBJECT_TYPE(NMonochromeImage)
};

}}

#endif // !N_MONOCHROME_IMAGE_HPP_INCLUDED
