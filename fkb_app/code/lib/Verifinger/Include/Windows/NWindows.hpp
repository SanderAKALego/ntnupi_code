#ifndef N_WINDOWS_HPP_INCLUDED
#define N_WINDOWS_HPP_INCLUDED

#include <NTypes.hpp>
#ifdef N_WINDOWS
	#include <NWindows.h>
	#ifdef N_FRAMEWORK_WX
		#include <wx/msw/winundef.h>
	#endif
#endif

#endif // !N_WINDOWS_HPP_INCLUDED
