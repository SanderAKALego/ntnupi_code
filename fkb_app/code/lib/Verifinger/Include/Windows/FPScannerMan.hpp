#ifndef FP_SCANNER_MAN_HPP_INCLUDED
#define FP_SCANNER_MAN_HPP_INCLUDED

#include <FPScanner.hpp>
#include <NDeviceManager.hpp>
namespace Neurotec { namespace DeviceManager
{
#include <FPScannerMan.h>
}}

namespace Neurotec { namespace DeviceManager
{

class N_CLASS(FPScannerMan)
{
private:
	static N_CLASS(FPScanner) * FromHandle(HNObject handle)
	{
		return new N_CLASS(FPScanner)(handle);
	}

public:
	typedef void (* ScannerCallback)(N_CLASS(FPScanner) * pScanner, void * pParam);

	class FPScannerCollection : public ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(FPScanner)>
	{
	private:
		FPScannerCollection()
			: ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(FPScanner)>(FPScannerManGetScannerCount, FPScannerManGetScanner, FromHandle, true)
		{
		}

		N_CLASS(FPScanner) * OnAdded(HFPScanner hScanner)
		{
			return this->AddItem(hScanner);
		}

		N_CLASS(FPScanner) * OnRemoved(HFPScanner hScanner)
		{
			return this->RemoveItemNoDelete(IndexOf(hScanner));
		}

	public:
		N_CLASS(FPScanner) * Get(NInt index) const // repeat it here because the following memebers hide it
		{
			return ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(FPScanner)>::Get(index);
		}

		N_CLASS(FPScanner) * Get(HNObject handle) const // repeat it here because the following memebers hide it
		{
			return ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(FPScanner)>::Get(handle);
		}

		N_CLASS(FPScanner) * Get(const NChar * szId) const
		{
			HFPScanner handle;
			NCheck(FPScannerManGetScannerById(szId, &handle));
			NInt count = GetCount();
			for (NInt i = 0; i < count; i++)
			{
				if (items[i]->GetHandle() == handle)
				{
					return items[i];
				}
			}
			NThrowException(N_T("Unknown FPScanner handle"));
		}

		N_CLASS(FPScanner) * Get(const NString & id) const
		{
			return Get(NGetConstStringBuffer(id));
		}

		N_CLASS(FPScanner) * operator[](NInt index) const // repeat it here because the following memebers hide it
		{
			return Get(index);
		}

		N_CLASS(FPScanner) * operator[](HNObject handle) const // repeat it here because the following memebers hide it
		{
			return Get(handle);
		}

		N_CLASS(FPScanner) * operator[](const NChar * szId) const
		{
			return Get(szId);
		}

		N_CLASS(FPScanner) * operator[](const NString & id) const
		{
			return Get(id);
		}

		friend class N_CLASS(FPScannerMan);
	};

private:
	static NInt initCount;
	static std::auto_ptr<FPScannerCollection> scanners;
	static NEvent1<N_CLASS(FPScanner) *> scannerAddedEvent;
	static NEvent1<N_CLASS(FPScanner) *> scannerRemovedEvent;

	static void N_API OnScannerAdded(HFPScanner hScanner, void *)
	{
		N_CLASS(FPScanner) * pScanner = scanners->OnAdded(hScanner);
		scannerAddedEvent(pScanner);
	}

	static void N_API OnScannerRemoved(HFPScanner hScanner, void *)
	{
		std::auto_ptr<N_CLASS(FPScanner)> scanner(scanners->OnRemoved(hScanner));
		scannerRemovedEvent(scanner.get());
	}

	N_CLASS(FPScannerMan)()
	{
	}

	N_CLASS(FPScannerMan)(const N_CLASS(FPScannerMan) &)
	{
	}

public:
	static void GetAvailableModules(NChar * * pSzValue)
	{
		NCheck(FPScannerManGetAvailableModules(pSzValue));
	}

	static NString GetAvailableModules()
	{
		NChar * szValue;
		GetAvailableModules(&szValue);
		NAutoFree value(szValue);
		return NString(szValue);
	}

	static void Initialize()
	{
		Initialize(NULL);
	}

	static void Initialize(const NChar * szModules)
	{
		NCheck(FPScannerManInitializeWithModules(szModules));
		try
		{
			if(initCount++ == 0)
			{
				scanners = std::auto_ptr<FPScannerCollection>(new FPScannerCollection());
				FPScannerManSetScannerAddedCallbackEx(OnScannerAdded, NULL);
				FPScannerManSetScannerRemovedCallbackEx(OnScannerRemoved, NULL);
			}
		}
		catch(...)
		{
			Uninitialize();
			throw;
		}
	}

	static void Initialize(const NString & modules)
	{
		Initialize(NGetConstStringBuffer(modules));
	}

	static void Uninitialize()
	{
		if(initCount != 0)
		{
			if(--initCount == 0)
			{
				FPScannerManSetScannerAddedCallbackEx(NULL, NULL);
				FPScannerManSetScannerRemovedCallbackEx(NULL, NULL);
				scanners.reset();
			}
		}
		FPScannerManUninitialize();
	}

	static FPScannerCollection * GetScanners()
	{
		return scanners.get();
	}

	static void AddScannerAddedCallback(ScannerCallback pCallback, void * pParam)
	{
		scannerAddedEvent.Add(pCallback, pParam);
	}

	static void RemoveScannerAddedCallback(ScannerCallback pCallback, void * pParam)
	{
		scannerAddedEvent.Remove(pCallback, pParam);
	}

	static void AddScannerRemovedCallback(ScannerCallback pCallback, void * pParam)
	{
		scannerRemovedEvent.Add(pCallback, pParam);
	}

	static void RemoveScannerRemovedCallback(ScannerCallback pCallback, void * pParam)
	{
		scannerRemovedEvent.Remove(pCallback, pParam);
	}
};

}}

#endif // !FP_SCANNER_MAN_HPP_INCLUDED
