#ifndef N_STREAM_H_INCLUDED
#define N_STREAM_H_INCLUDED

#include <NObject.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef enum NFileAccess_
{
	nfaRead = 1,
	nfaWrite = 2,
	nfaReadWrite = nfaRead | nfaWrite
} NFileAccess;

typedef enum NByteOrder_
{
	nboLittleEndian = 0,
	nboBigEndian = 1,
#ifdef N_BIG_ENDIAN
	nboSystem = nboBigEndian
#else
	nboSystem = nboLittleEndian
#endif
} NByteOrder;

#define NIsReverseByteOrder(byteOrder) ((byteOrder) != nboSystem)

DECLARE_N_OBJECT_HANDLE(NStream)

typedef enum NSeekOrigin_
{
	nsoBegin = 0,
	nsoCurrent = 1,
	nsoEnd = 2
} NSeekOrigin;

NResult N_API NStreamGetNull(HNStream * pValue);

NResult N_API NStreamIsOpened(HNStream hStream, NBool * pValue);
NResult N_API NStreamCanRead(HNStream hStream, NBool * pValue);
NResult N_API NStreamCanWrite(HNStream hStream, NBool * pValue);
NResult N_API NStreamCanSeek(HNStream hStream, NBool * pValue);
NResult N_API NStreamClose(HNStream hStream);
NResult N_API NStreamFlush(HNStream hStream);
NResult N_API NStreamGetLength(HNStream hStream, NPosType * pValue);
NResult N_API NStreamSetLength(HNStream hStream, NPosType value);
NResult N_API NStreamGetPosition(HNStream hStream, NPosType * pValue);
NResult N_API NStreamSetPosition(HNStream hStream, NPosType value);
NResult N_API NStreamSeek(HNStream hStream, NPosType offset, NSeekOrigin origin);
NResult N_API NStreamReadByte(HNStream hStream, NInt * pValue);
NResult N_API NStreamRead(HNStream hStream, void * pBuffer, NSizeType count, NSizeType * pCountRead);
NResult N_API NStreamWriteByte(HNStream hStream, NByte value);
NResult N_API NStreamWrite(HNStream hStream, const void * pBuffer, NSizeType count);

#ifdef N_CPP
}
#endif

#endif // !N_STREAM_H_INCLUDED
