#ifndef N_MATCHER_PARAMS_HPP_INCLUDED
#define N_MATCHER_PARAMS_HPP_INCLUDED

#include <NParameters.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NMatcherParams.h>
}}

namespace Neurotec { namespace Biometrics
{
#undef NM_PART_NONE
#undef NM_PART_FINGERS
#undef NM_PART_FACES
#undef NM_PART_IRISES
#undef NM_PART_PALMS

#undef NMP_MATCHING_THRESHOLD
#undef NMP_FUSION_TYPE
#undef NMP_FACES_MATCHING_THRESHOLD
#undef NMP_IRISES_MATCHING_THRESHOLD

const NUShort NM_PART_NONE =  0;
const NUShort NM_PART_FINGERS = 10;
const NUShort NM_PART_FACES = 20;
const NUShort NM_PART_IRISES = 30;
const NUShort NM_PART_PALMS = 40;

const NUShort NMP_MATCHING_THRESHOLD = 100;
const NUShort NMP_FUSION_TYPE = 200;
const NUShort NMP_FACES_MATCHING_THRESHOLD = 210;
const NUShort NMP_IRISES_MATCHING_THRESHOLD = 220;
}}

#endif // !N_MATCHER_PARAMS_HPP_INCLUDED
