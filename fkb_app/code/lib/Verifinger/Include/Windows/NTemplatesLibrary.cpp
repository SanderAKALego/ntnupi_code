#ifndef N_TEMPLATES_LIBRARY_CPP_INCLUDED
#define N_TEMPLATES_LIBRARY_CPP_INCLUDED

#include <NCoreLibrary.cpp>

#include <NFRecord.hpp>
#include <NLRecord.hpp>
#include <NERecord.hpp>
#include <NFTemplate.hpp>
#include <NLTemplate.hpp>
#include <NETemplate.hpp>
#include <NTemplate.hpp>
#include <NTemplates.hpp>

namespace Neurotec { namespace Biometrics
{

N_IMPLEMENT_OBJECT_TYPE(NFRecord, NObject)
N_IMPLEMENT_OBJECT_TYPE(NLRecord, NObject)
N_IMPLEMENT_OBJECT_TYPE(NERecord, NObject)
N_IMPLEMENT_OBJECT_TYPE(NFTemplate, NObject)
N_IMPLEMENT_OBJECT_TYPE(NLTemplate, NObject)
N_IMPLEMENT_OBJECT_TYPE(NETemplate, NObject)
N_IMPLEMENT_OBJECT_TYPE(NTemplate, NObject)

}}

#endif // !N_TEMPLATES_LIBRARY_CPP_INCLUDED
