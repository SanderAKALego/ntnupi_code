#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include <NImage.hpp>
#include <CameraParams.hpp>
namespace Neurotec { namespace DeviceManager
{
using Neurotec::Images::HNImage;
#include <Camera.h>
}}

namespace Neurotec { namespace DeviceManager
{

class N_CLASS(CameraMan);

class N_CLASS(Camera) : public N_CLASS(NObject)
{
private:
	N_CLASS(Camera)(HCamera handle)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(Camera), false)
	{
	}

public:
	NInt GetId(NChar * pValue)
	{
		return NCheck(CameraGetId(GetHandle(), pValue));
	}

	NString GetId()
	{
		NString value;
		return NGetString(value, CameraGetId, this);
	}

	NInt GetVideoFormats(CameraVideoFormat * arVideoFormats)
	{
		return NCheck(CameraGetVideoFormats(GetHandle(), arVideoFormats));
	}

	CameraVideoFormat GetVideoFormat()
	{
		CameraVideoFormat value;
		NCheck(CameraGetVideoFormat(GetHandle(), &value));
		return value;
	}

	void SetVideoFormat(const CameraVideoFormat & value)
	{
		NCheck(CameraSetVideoFormat(GetHandle(), &value));
	}

	bool IsCapturing()
	{
		NBool value;
		NCheck(CameraIsCapturing(GetHandle(), &value));
		return value != 0;
	}

	void StartCapturing()
	{
		NCheck(CameraStartCapturing(GetHandle()));
	}

	void StopCapturing()
	{
		NCheck(CameraStopCapturing(GetHandle()));
	}

	Neurotec::Images::N_CLASS(NImage) * GetCurrentFrame()
	{
		HNImage handle;
		NCheck(CameraGetCurrentFrame(GetHandle(), &handle));
		try
		{
			return Neurotec::Images::N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	bool GetMirrorHorizontal()
	{
		return GetParameterAsBoolean(CAMERAP_MIRROR_HORIZONTAL);
	}

	void SetMirrorHorizontal(bool value)
	{
		SetParameter(CAMERAP_MIRROR_HORIZONTAL, value);
	}

	bool GetMirrorVertical()
	{
		return GetParameterAsBoolean(CAMERAP_MIRROR_VERTICAL);
	}

	void SetMirrorVertical(bool value)
	{
		SetParameter(CAMERAP_MIRROR_VERTICAL, value);
	}

	bool GetAutomaticSettings()
	{
		return GetParameterAsBoolean(CAMERAP_AUTOMATIC_SETTINGS);
	}

	void SetAutomaticSettings(bool value)
	{
		SetParameter(CAMERAP_AUTOMATIC_SETTINGS, value);
	}

	NUInt GetGain()
	{
		return GetParameterAsUInt32(CAMERAP_GAIN);
	}

	void SetGain(NUInt value)
	{
		SetParameter(CAMERAP_GAIN, value);
	}

	NUInt GetGainMin()
	{
		return GetParameterAsUInt32(CAMERAP_GAIN_MIN);
	}

	NUInt GetGainMax()
	{
		return GetParameterAsUInt32(CAMERAP_GAIN_MAX);
	}

	NUInt GetExposure()
	{
		return GetParameterAsUInt32(CAMERAP_EXPOSURE);
	}

	void SetExposure(NUInt value)
	{
		SetParameter(CAMERAP_EXPOSURE, value);
	}

	NUInt GetExposureMin()
	{
		return GetParameterAsUInt32(CAMERAP_EXPOSURE_MIN);
	}

	NUInt GetExposureMax()
	{
		return GetParameterAsUInt32(CAMERAP_EXPOSURE_MAX);
	}

	NInt GetIpUsername(NChar * pValue)
	{
		return GetParameterAsString(CAMERAP_IP_USERNAME, pValue);
	}

	NString GetIpUsername()
	{
		return GetParameterAsString(CAMERAP_IP_USERNAME);
	}

	void SetIpUsername(const NChar * szValue)
	{
		SetParameter(CAMERAP_IP_USERNAME, szValue);
	}

	void SetIpUsername(const NString & value)
	{
		SetParameter(CAMERAP_IP_USERNAME, value);
	}

	NInt GetIpPassword(NChar * pValue)
	{
		return GetParameterAsString(CAMERAP_IP_PASSWORD, pValue);
	}

	NString GetIpPassword()
	{
		return GetParameterAsString(CAMERAP_IP_PASSWORD);
	}

	void SetIpPassword(const NChar * szValue)
	{
		SetParameter(CAMERAP_IP_PASSWORD, szValue);
	}

	void SetIpPassword(const NString & value)
	{
		SetParameter(CAMERAP_IP_PASSWORD, value);
	}

	NInt GetIpChannelId(NChar * pValue)
	{
		return GetParameterAsString(CAMERAP_IP_CHANNEL_ID, pValue);
	}

	NString GetIpChannelId()
	{
		return GetParameterAsString(CAMERAP_IP_CHANNEL_ID);
	}

	void SetIpChannelId(const NChar * szValue)
	{
		SetParameter(CAMERAP_IP_CHANNEL_ID, szValue);
	}

	void SetIpChannelId(const NString & value)
	{
		SetParameter(CAMERAP_IP_CHANNEL_ID, value);
	}

	NInt GetIpChannelName(NChar * pValue)
	{
		return GetParameterAsString(CAMERAP_IP_CHANNEL_NAME, pValue);
	}

	NString GetIpChannelName()
	{
		return GetParameterAsString(CAMERAP_IP_CHANNEL_NAME);
	}

	void SetIpChannelName(const NChar * szValue)
	{
		SetParameter(CAMERAP_IP_CHANNEL_NAME, szValue);
	}

	void SetIpChannelName(const NString & value)
	{
		SetParameter(CAMERAP_IP_CHANNEL_NAME, value);
	}

	N_DECLARE_OBJECT_TYPE(Camera)

	friend class N_CLASS(CameraMan);
};

}}

#endif // !CAMERA_HPP_INCLUDED
