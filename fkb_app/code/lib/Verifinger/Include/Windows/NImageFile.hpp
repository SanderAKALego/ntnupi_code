#ifndef N_IMAGE_FILE_HPP_INCLUDED
#define N_IMAGE_FILE_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Images
{
using ::Neurotec::IO::HNStream;
using ::Neurotec::IO::NFileAccess;
#include <NImageFile.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(NImage);
class N_CLASS(NImageFormat);

class N_CLASS(NImageFile) : public N_CLASS(NObject)
{
private:
	N_CLASS(NImageFormat) * pImageFormat;

	N_CLASS(NImageFile)(HNImageFile handle, bool claimHandle, N_CLASS(NImageFormat) * pImageFormat);

public:
	static N_CLASS(NImageFile) * FromHandle(HNImageFile handle, bool ownsHandle = true);
	static N_CLASS(NImageFile)* FromFile(const NChar * szFileName, N_CLASS(NImageFormat) *pImageFormat = NULL);

	static N_CLASS(NImageFile)* FromFile(const NString & fileName, N_CLASS(NImageFormat) *pImageFormat = NULL)
	{
		return FromFile(NGetConstStringBuffer(fileName), pImageFormat);
	}

	void Close()
	{
		NCheck(NImageFileClose(GetHandle()));
	}

	N_CLASS(NImage)* ReadImage();

	bool IsOpened()
	{
		NBool value;
		NCheck(NImageFileIsOpened(GetHandle(), &value));
		return value != 0;
	}

	N_CLASS(NImageFormat) * GetFormat()
	{
		return pImageFormat;
	}

	N_DECLARE_OBJECT_TYPE(NImageFile)
};

}}

#include <NImage.hpp>
#include <NImageFormat.hpp>

namespace Neurotec { namespace Images
{

inline N_CLASS(NImageFile)::N_CLASS(NImageFile)(HNImageFile handle, bool claimHandle, N_CLASS(NImageFormat) * pImageFormat)
	: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NImageFile), claimHandle), pImageFormat(pImageFormat)
{
}

inline N_CLASS(NImageFile) * N_CLASS(NImageFile)::FromHandle(HNImageFile handle, bool ownsHandle)
{
	HNImageFormat hImageFormat;
	NCheck(NImageFileGetFormat(handle, &hImageFormat));
	N_CLASS(NImageFormat) * pImageFormat = N_CLASS(NImageFormat)::FromHandle(hImageFormat);
	N_CLASS(NImageFile) * pImageFile = new N_CLASS(NImageFile)(handle, false, pImageFormat);
	if (ownsHandle)
	{
		pImageFile->ClaimHandle();
	}
	return pImageFile;
}

inline N_CLASS(NImageFile) * N_CLASS(NImageFile)::FromFile(const NChar * szFileName, N_CLASS(NImageFormat) * pImageFormat)
{
	HNImageFile handle;
	NCheck(NImageFileCreate(szFileName, pImageFormat ? pImageFormat->GetHandle() : NULL, &handle));
	return N_CLASS(NImageFile)::FromHandle(handle);
}

inline N_CLASS(NImage)* N_CLASS(NImageFile)::ReadImage()
{
	HNImage handle;
	NCheck(NImageFileReadImage(GetHandle(), &handle));
	try
	{
		return N_CLASS(NImage)::FromHandle(handle);
	}
	catch(...)
	{
		NObjectFree(handle);
		throw;
	}
}

}}

#endif // !N_IMAGE_FILE_HPP_INCLUDED
