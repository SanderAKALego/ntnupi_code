#ifndef N_LICENSING_H_INCLUDED
#define N_LICENSING_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef struct NLicenseInfo_
{
	NBool IsObtained;
	NInt DistributorId;
	NInt SerialNumber;
} NLicenseInfo;

#ifndef N_NO_ANSI_FUNC
NResult N_API NLicensingGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NLicensingGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NLicensingGetInfo(NLibraryInfo * pValue);
#endif
#define NLicensingGetInfo N_FUNC_AW(NLicensingGetInfo)

#ifndef N_NO_ANSI_FUNC
NResult N_API NLicenseObtainA(const NAChar * szAddress, const NAChar * szPort, const NAChar * szProducts, NBool * pAvailable);
#endif
#ifndef N_NO_UNICODE
NResult N_API NLicenseObtainW(const NWChar * szAddress, const NWChar * szPort, const NWChar * szProducts, NBool * pAvailable);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NLicenseObtain(const NChar * szAddress, const NChar * szPort, const NChar * szProducts, NBool * pAvailable);
#endif
#define NLicenseObtain N_FUNC_AW(NLicenseObtain)

#ifndef N_NO_ANSI_FUNC
NResult N_API NLicenseReleaseA(const NAChar * szProducts);
#endif
#ifndef N_NO_UNICODE
NResult N_API NLicenseReleaseW(const NWChar * szProducts);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NLicenseRelease(const NChar * szProducts);
#endif
#define NLicenseRelease N_FUNC_AW(NLicenseRelease)

#ifndef N_NO_ANSI_FUNC
NResult N_API NLicenseGetInfoA(const NAChar * szProduct, NLicenseInfo * pLicenseInfo);
#endif
#ifndef N_NO_UNICODE
NResult N_API NLicenseGetInfoW(const NWChar * szProduct, NLicenseInfo * pLicenseInfo);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NLicenseGetInfo(const NChar * szProduct, NLicenseInfo * pLicenseInfo);
#endif
#define NLicenseGetInfo N_FUNC_AW(NLicenseGetInfo)

#ifndef N_NO_ANSI_FUNC
NResult N_API NLicenseIsComponentActivatedA(const NAChar * szName, NBool * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NLicenseIsComponentActivatedW(const NWChar * szName, NBool * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NLicenseIsComponentActivated(const NChar * szName, NBool * pValue);
#endif
#define NLicenseIsComponentActivated N_FUNC_AW(NLicenseIsComponentActivated)

#ifdef N_CPP
}
#endif

#endif /**!doc !N_LICENSING_H_INCLUDED */
