#ifndef N_MATCHERS_HPP_INCLUDED
#define N_MATCHERS_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NMatchers.h>
}}

namespace Neurotec { namespace Biometrics
{

class NMatchers
{
private:
	NMatchers()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NMatchersGetInfo(pValue));
	}
};

}
}

#endif // !N_MATCHERS_HPP_INCLUDED
