#ifndef N_IMAGE_HPP_INCLUDED
#define N_IMAGE_HPP_INCLUDED

#include <NCore.hpp>
#include <NPixelFormat.hpp>
namespace Neurotec { namespace Images
{
using ::Neurotec::IO::HNStream;
using ::Neurotec::IO::NFileAccess;
#include <NImage.h>
}}
#if defined(N_FRAMEWORK_MFC)
	#include <afxstr.h>
	#include <atlimage.h>
#elif defined(N_FRAMEWORK_WX)
	#include <wx/image.h>
#endif

namespace Neurotec { namespace Images
{

class N_CLASS(NImageFormat);

class N_CLASS(NImage) : public N_CLASS(NObject)
{
protected:
	N_CLASS(NImage)(HNImage handle, const N_CLASS(NNativeType) & requiredType, bool claimHandle)
		: N_CLASS(NObject)(handle, requiredType, claimHandle)
	{
	}

public:
	static N_CLASS(NImage)* FromHandle(HNImage handle, bool ownsHandle = true);

	static N_CLASS(NImage)* GetWrapper(NPixelFormat pixelFormat,
		NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
		void * pixels, NBool ownsPixels)
	{
		HNImage handle;
		NCheck(NImageCreateWrapper(pixelFormat, width, height, stride, horzResolution, vertResolution,
			pixels, ownsPixels, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* GetWrapper(NPixelFormat pixelFormat,
		NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
		void * pixels, NUInt left, NUInt top)
	{
		HNImage handle;
		NCheck(NImageCreateWrapperForPart(pixelFormat, width, height, stride, horzResolution, vertResolution,
			pixels, left, top, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* GetWrapper(N_CLASS(NImage) *pSrcImage, NUInt left, NUInt top, NUInt width, NUInt height)
	{
		HNImage handle;
		NCheck(NImageCreateWrapperForImagePart(pSrcImage->GetHandle(), left, top, width, height, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* GetWrapper(N_CLASS(NImage) *pSrcImage, NUInt left, NUInt top, NUInt width, NUInt height,
		NFloat horzResolution, NFloat vertResolution)
	{
		HNImage handle;
		NCheck(NImageCreateWrapperForImagePartEx(pSrcImage->GetHandle(), left, top, width, height,
			horzResolution, vertResolution, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* Create(NPixelFormat pixelFormat,
		NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution)
	{
		HNImage handle;
		NCheck(NImageCreate(pixelFormat, width, height, stride, horzResolution, vertResolution, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromData(NPixelFormat pixelFormat,
		NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
		NSizeType srcStride, const void * srcPixels)
	{
		HNImage handle;
		NCheck(NImageCreateFromData(pixelFormat, width, height, stride, horzResolution, vertResolution,
			srcStride, srcPixels, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromData(NPixelFormat pixelFormat,
		NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
		NSizeType srcStride, const void * srcPixels, NUInt left, NUInt top)
	{
		HNImage handle;
		NCheck(NImageCreateFromDataPart(pixelFormat, width, height, stride, horzResolution, vertResolution,
			srcStride, srcPixels, left, top, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromImage(NPixelFormat pixelFormat, NSizeType stride, N_CLASS(NImage)* pSrcImage)
	{
		HNImage handle;
		NCheck(NImageCreateFromImage(pixelFormat, stride, pSrcImage->GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromImage(NPixelFormat pixelFormat,
		NSizeType stride, NFloat horzResolution, NFloat vertResolution, N_CLASS(NImage)* pSrcImage)
	{
		HNImage handle;
		NCheck(NImageCreateFromImageEx(pixelFormat, stride, horzResolution, vertResolution,
			pSrcImage->GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromImage(NPixelFormat pixelFormat,
		NSizeType stride, N_CLASS(NImage)* pSrcImage,
		NUInt left, NUInt top, NUInt width, NUInt height)
	{
		HNImage handle;
		NCheck(NImageCreateFromImagePart(pixelFormat, stride, pSrcImage->GetHandle(),
			left, top, width, height, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromImage(NPixelFormat pixelFormat,
		NSizeType stride, NFloat horzResolution, NFloat vertResolution, N_CLASS(NImage)* pSrcImage,
		NUInt left, NUInt top, NUInt width, NUInt height)
	{
		HNImage handle;
		NCheck(NImageCreateFromImagePartEx(pixelFormat,
			stride, horzResolution, vertResolution, pSrcImage->GetHandle(),
			left, top, width, height, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* FromFile(const NChar * szFileName, N_CLASS(NImageFormat) *pImageFormat = NULL);
	static N_CLASS(NImage)* FromFile(const NString & fileName, N_CLASS(NImageFormat) *pImageFormat = NULL);

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
	static N_CLASS(NImage)* FromHBitmap(HBITMAP hBitmap);
#endif

#if defined(N_FRAMEWORK_MFC)
	static N_CLASS(NImage) * FromBitmap(CImage * pImage);
#elif defined(N_FRAMEWORK_WX)
	static N_CLASS(NImage) * FromBitmap(const wxImage & image, float horzResolution, float vertResolution);
#endif

	static void Copy(N_CLASS(NImage) * pSrcImage, NUInt left, NUInt top, NUInt width, NUInt height, N_CLASS(NImage) *pDstImage, NUInt dstLeft, NUInt dstTop)
	{
		if (pSrcImage == NULL)
			NThrowArgumentNullException(N_T("pSrcImage"));
		if (pDstImage == NULL)
			NThrowArgumentNullException(N_T("pDstImage"));
		NCheck(NImageCopy(pSrcImage->GetHandle(), left, top, width, height, pDstImage->GetHandle(), dstLeft, dstTop));
	}

	explicit N_CLASS(NImage)(HNImage handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NImage), false)
	{
		if (N_CLASS(NObject)::GetNativeType(handle) != N_NATIVE_TYPE_OF(NImage)) NThrowArgumentException(N_T("handle type is not NImage"), N_T("handle"));
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	void Save(const NChar * szFileName, N_CLASS(NImageFormat) *pImageFormat = NULL);
	void Save(const NString & fileName, N_CLASS(NImageFormat) *pImageFormat = NULL);

	void FlipHorizontally()
	{
		NCheck(NImageFlipHorizontally((HNImage)GetHandle()));
	}

	void FlipVertically()
	{
		NCheck(NImageFlipVertically((HNImage)GetHandle()));
	}

	void FlipDiagonally()
	{
		NCheck(NImageFlipDiagonally((HNImage)GetHandle()));
	}

	N_CLASS(NImage)* RotateFlip(NImageRotateFlipType rotateFlipType)
	{
		HNImage handle;
		NCheck(NImageRotateFlip(GetHandle(), rotateFlipType, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	N_CLASS(NImage)* Crop(NUInt left, NUInt top, NUInt width, NUInt height)
	{
		HNImage handle;
		NCheck(NImageCrop(GetHandle(), left, top, width, height, &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
	HBITMAP ToHBitmap();
#endif

#if defined(N_FRAMEWORK_MFC)
	CImage * ToBitmap();
#elif defined(N_FRAMEWORK_WX)
	wxImage ToBitmap();
#endif

	N_CLASS(NObject) * Clone() const
	{
		HNImage handle;
		NCheck(NImageClone(GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	NPixelFormat GetPixelFormat() const
	{
		NPixelFormat value;
		NCheck(NImageGetPixelFormat(GetHandle(), &value));
		return value;
	}

	NUInt GetWidth() const
	{
		NUInt value;
		NCheck(NImageGetWidth(GetHandle(), &value));
		return value;
	}

	NUInt GetHeight() const
	{
		NUInt value;
		NCheck(NImageGetHeight(GetHandle(), &value));
		return value;
	}

	NSizeType GetStride() const
	{
		NSizeType value;
		NCheck(NImageGetStride(GetHandle(), &value));
		return value;
	}

	NSizeType GetSize() const
	{
		NSizeType value;
		NCheck(NImageGetSize(GetHandle(), &value));
		return value;
	}

	NFloat GetHorzResolution() const
	{
		NFloat value;
		NCheck(NImageGetHorzResolution(GetHandle(), &value));
		return value;
	}

	NFloat GetVertResolution() const
	{
		NFloat value;
		NCheck(NImageGetVertResolution(GetHandle(), &value));
		return value;
	}

	void * GetPixels() const
	{
		void * value;
		NCheck(NImageGetPixels(GetHandle(), &value));
		return value;
	}

	N_DECLARE_OBJECT_TYPE(NImage)
};

}}

#include <NImageFormat.hpp>
#include <NMonochromeImage.hpp>
#include <NGrayscaleImage.hpp>
#include <NRgbImage.hpp>
#include <Bmp.hpp>

namespace Neurotec { namespace Images
{

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
	inline N_CLASS(NImage)* N_CLASS(NImage)::FromHBitmap(HBITMAP hBitmap)
	{
		return N_CLASS(Bmp)::LoadImageFromHBitmap(hBitmap);
	}

	inline HBITMAP N_CLASS(NImage)::ToHBitmap()
	{
		return N_CLASS(Bmp)::SaveImageToHBitmap(this);
	}
#endif

#if defined(N_FRAMEWORK_MFC)
	inline N_CLASS(NImage) * FromBitmap(CImage * pImage)
	{
		return N_CLASS(Bmp)::LoadImageFromBitmap(pImage);
	}

	inline CImage * N_CLASS(NImage)::ToBitmap()
	{
		return N_CLASS(Bmp)::SaveImageToBitmap(this);
	}
#elif defined(N_FRAMEWORK_WX)
	inline N_CLASS(NImage)* N_CLASS(NImage)::FromBitmap(const wxImage & image, float horzResolution, float vertResolution)
	{
		return N_CLASS(Bmp)::LoadImageFromBitmap(image, horzResolution, vertResolution);
	}

	inline wxImage N_CLASS(NImage)::ToBitmap()
	{
		return N_CLASS(Bmp)::SaveImageToBitmap(this);
	}
#endif

inline N_CLASS(NImage)* N_CLASS(NImage)::FromFile(const NChar * szFileName, N_CLASS(NImageFormat) *pImageFormat)
{
	HNImage handle;
	NCheck(NImageCreateFromFile(szFileName, pImageFormat ? pImageFormat->GetHandle() : NULL, &handle));
	try
	{
		return FromHandle(handle);
	}
	catch(...)
	{
		NObjectFree(handle);
		throw;
	}
}

inline N_CLASS(NImage)* N_CLASS(NImage)::FromFile(const NString & fileName, N_CLASS(NImageFormat) *pImageFormat)
{
	return FromFile(NGetConstStringBuffer(fileName), pImageFormat);
}

inline void N_CLASS(NImage)::Save(const NChar * szFileName, N_CLASS(NImageFormat) *pImageFormat)
{
	NCheck(NImageSaveToFile(GetHandle(), szFileName, pImageFormat ? pImageFormat->GetHandle() : NULL));
}

inline void N_CLASS(NImage)::Save(const NString & fileName, N_CLASS(NImageFormat) *pImageFormat)
{
	Save(NGetConstStringBuffer(fileName), pImageFormat);
}

}}

#endif // !N_IMAGE_HPP_INCLUDED
