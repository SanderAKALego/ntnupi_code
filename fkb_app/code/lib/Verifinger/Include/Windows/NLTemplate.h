#ifndef NL_TEMPLATE_H_INCLUDED
#define NL_TEMPLATE_H_INCLUDED

#include <NLRecord.h>

#ifdef N_CPP
extern "C"
{
#endif

#define NLT_MAX_RECORD_COUNT 255

DECLARE_N_OBJECT_HANDLE(NLTemplate)

NResult N_API NLTemplateCalculateSize(NInt recordCount, NSizeType * arRecordSizes, NSizeType * pSize);
NResult N_API NLTemplatePack(NInt recordCount, const void * * arPRecords, NSizeType * arRecordSizes, void * pBuffer, NSizeType bufferSize, NSizeType * pSize);
NResult N_API NLTemplateUnpack(const void * pBuffer, NSizeType bufferSize,
	NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
	NInt * pRecordCount, const void * * arPRecords, NSizeType * arRecordSizes);
NResult N_API NLTemplateCheck(const void * pBuffer, NSizeType bufferSize);

NResult N_API NLTemplateGetRecordCountMem(const void * pBuffer, NSizeType bufferSize, NInt * pValue);

typedef struct NLTemplateInfo_
{
	NByte MajorVersion;
	NByte MinorVersion;
	NUInt Size;
	NByte HeaderSize;
	NInt RecordCount;
	NLRecordInfo * RecordInfos;
} NLTemplateInfo;

void N_API NLTemplateInfoDispose(NLTemplateInfo * pInfo);

#define NLT_PROCESS_FIRST_RECORD_ONLY 0x00000100

NResult N_API NLTemplateCreate(HNLTemplate * pHTemplate);
NResult N_API NLTemplateCreateEx(NUInt flags, HNLTemplate * pHTemplate);
NResult N_API NLTemplateCreateFromMemory(const void * pBuffer, NSizeType bufferSize,
	NUInt flags, NLTemplateInfo * pInfo, HNLTemplate * pHTemplate);

NResult N_API NLTemplateGetRecordCount(HNLTemplate hTemplate, NInt * pValue);
NResult N_API NLTemplateGetRecord(HNLTemplate hTemplate, NInt index, HNLRecord * pValue);
NResult N_API NLTemplateGetRecordCapacity(HNLTemplate hTemplate, NInt * pValue);
NResult N_API NLTemplateSetRecordCapacity(HNLTemplate hTemplate, NInt value);
NResult N_API NLTemplateAddRecord(HNLTemplate hTemplate, NUInt flags, HNLRecord * pHRecord);
NResult N_API NLTemplateAddRecordFromMemory(HNLTemplate hTemplate, const void * pBuffer, NSizeType bufferSize,
	NUInt flags, HNLRecord * pHRecord);
NResult N_API NLTemplateAddRecordCopy(HNLTemplate hTemplate, HNLRecord hSrcRecord, HNLRecord * pHRecord);
NResult N_API NLTemplateRemoveRecord(HNLTemplate hTemplate, NInt index);
NResult N_API NLTemplateClearRecords(HNLTemplate hTemplate);

NResult N_API NLTemplateClone(HNLTemplate hTemplate, HNLTemplate * pHClonedTemplate);
NResult N_API NLTemplateGetSize(HNLTemplate hTemplate, NUInt flags, NSizeType * pSize);
NResult N_API NLTemplateSaveToMemory(HNLTemplate hTemplate, void * pBuffer, NSizeType bufferSize, NUInt flags, NSizeType * pSize);

#define NLTemplateFree(hTemplate) NObjectFree(hTemplate)

#ifdef N_MSVC
	#pragma deprecated("NLTemplateFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !NL_TEMPLATE_H_INCLUDED
