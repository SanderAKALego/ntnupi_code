#ifndef CAMERA_MAN_HPP_INCLUDED
#define CAMERA_MAN_HPP_INCLUDED

#include <Camera.hpp>
#include <NDeviceManager.hpp>
namespace Neurotec { namespace DeviceManager
{
#include <CameraMan.h>
}}

namespace Neurotec { namespace DeviceManager
{

class N_CLASS(CameraMan)
{
private:
	static N_CLASS(Camera) * FromHandle(HNObject handle)
	{
		return new N_CLASS(Camera)(handle);
	}

public:
	class CameraCollection : public ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(Camera)>
	{
	private:
		CameraCollection()
			: ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(Camera)>(CameraManGetCameraCount, CameraManGetCamera, FromHandle, true)
		{
		}

	public:
		N_CLASS(Camera) * Get(NInt index) const // repeat it here because the following memebers hide it
		{
			return ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(Camera)>::Get(index);
		}

		N_CLASS(Camera) * Get(HNObject handle) const // repeat it here because the following memebers hide it
		{
			return ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(Camera)>::Get(handle);
		}

		N_CLASS(Camera) * Get(const NChar * szId) const
		{
			HCamera handle;
			NCheck(CameraManGetCameraById(szId, &handle));
			NInt count = GetCount();
			for (NInt i = 0; i < count; i++)
			{
				if (items[i]->GetHandle() == handle)
				{
					return items[i];
				}
			}
			NThrowException(N_T("Unknown Camera handle"));
		}

		N_CLASS(Camera) * Get(const NString & id) const
		{
			return Get(NGetConstStringBuffer(id));
		}

		N_CLASS(Camera) * operator[](NInt index) const // repeat it here because the following memebers hide it
		{
			return Get(index);
		}

		N_CLASS(Camera) * operator[](HNObject handle) const // repeat it here because the following memebers hide it
		{
			return Get(handle);
		}

		N_CLASS(Camera) * operator[](const NChar * szId) const
		{
			return Get(szId);
		}

		N_CLASS(Camera) * operator[](const NString & id) const
		{
			return Get(id);
		}

		friend class N_CLASS(CameraMan);
	};

private:
	static NInt initCount;
	static std::auto_ptr<CameraCollection> cameras;

	N_CLASS(CameraMan)()
	{
	}

	N_CLASS(CameraMan)(const N_CLASS(CameraMan) &)
	{
	}

public:
	static void Initialize()
	{
		NCheck(CameraManInitialize());
		try
		{
			if(initCount++ == 0)
			{
				cameras = std::auto_ptr<CameraCollection>(new CameraCollection());
			}
		}
		catch(...)
		{
			Uninitialize();
			throw;
		}
	}

	static void Uninitialize()
	{
		if(initCount != 0)
		{
			if(--initCount == 0)
			{
				cameras.reset();
			}
		}
		CameraManUninitialize();
	}

	static CameraCollection * GetCameras()
	{
		return cameras.get();
	}
};

}}

#endif // !CAMERA_MAN_HPP_INCLUDED
