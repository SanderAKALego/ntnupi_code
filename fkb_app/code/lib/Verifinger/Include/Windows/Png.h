#ifndef PNG_H_INCLUDED
#define PNG_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API PngLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API PngLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API PngLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define PngLoadImageFromFile N_FUNC_AW(PngLoadImageFromFile)

NResult N_API PngLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API PngLoadImageFromStream(HNStream hStream, HNImage * pHImage);

#define PNG_DEFAULT_COMPRESSION_LEVEL 6

#ifndef N_NO_ANSI_FUNC
NResult N_API PngSaveImageToFileA(HNImage hImage, NInt compressionLevel, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API PngSaveImageToFileW(HNImage hImage, NInt compressionLevel, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API PngSaveImageToFile(HNImage hImage, NInt compressionLevel, const NChar * szFileName);
#endif
#define PngSaveImageToFile N_FUNC_AW(PngSaveImageToFile)

NResult N_API PngSaveImageToMemory(HNImage hImage, NInt compressionLevel, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API PngSaveImageToStream(HNImage hImage, NInt compressionLevel, HNStream hStream);

#ifdef N_CPP
}
#endif

#endif // !PNG_H_INCLUDED
