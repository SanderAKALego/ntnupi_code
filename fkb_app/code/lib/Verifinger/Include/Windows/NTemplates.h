#ifndef N_TEMPLATES_H_INCLUDED
#define N_TEMPLATES_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef enum NBiometricType_
{
	nbtFacialFeatures = 0x00000002,
	nbtFingerprint = 0x00000008,
	nbtIris = 0x00000010,
	nbtPalmPrint = 0x00020000
} NBiometricType;

#ifndef N_NO_ANSI_FUNC
NResult N_API NTemplatesGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NTemplatesGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NTemplatesGetInfo(NLibraryInfo * pValue);
#endif
#define NTemplatesGetInfo N_FUNC_AW(NTemplatesGetInfo)

#ifdef N_CPP
}
#endif

#endif // !N_TEMPLATES_H_INCLUDED
