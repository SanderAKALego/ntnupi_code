#ifndef NESM_MATCH_DETAILS_HPP_INCLUDED
#define NESM_MATCH_DETAILS_HPP_INCLUDED

#include <NemMatchDetails.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NesmMatchDetails.h>
}}

namespace Neurotec { namespace Biometrics
{

inline void MatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NesmMatchDetails ** ppMatchDetails)
{
	NCheck(NesmMatchDetailsDeserialize(buffer, bufferLength, ppMatchDetails));
}

inline NSizeType MatchDetailsSerialize(NesmMatchDetails * pMatchDetails, void * * pBuffer)
{
	NSizeType bufferLength;
	NCheck(NesmMatchDetailsSerialize(pMatchDetails, pBuffer, &bufferLength));
	return bufferLength;
}

}}

#endif // !NESM_MATCH_DETAILS_HPP_INCLUDED
