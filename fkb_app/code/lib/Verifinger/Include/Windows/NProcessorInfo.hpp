#ifndef N_PROCESSOR_INFO_HPP_INCLUDED
#define N_PROCESSOR_INFO_HPP_INCLUDED

namespace Neurotec
{
#include <NProcessorInfo.h>
}
#include <NCore.hpp>

namespace Neurotec
{
#undef N_PROCESSOR_INFO_MAX_VENDOR_NAME_LENGTH
#undef N_PROCESSOR_INFO_MAX_MODEL_NAME_LENGTH

const NSizeType N_PROCESSOR_INFO_MAX_VENDOR_NAME_LENGTH = 13;
const NSizeType N_PROCESSOR_INFO_MAX_MODEL_NAME_LENGTH =  48;

class N_CLASS(NProcessorInfo)
{
private:
	N_CLASS(NProcessorInfo)() {}

public:
	static void GetModelInfo(NInt * pFamily, NInt * pModel, NInt * pStepping)
	{
		NCheck(NProcessorInfoGetModelInfo(pFamily, pModel, pStepping));
	}

	static NInt GetVendorName(NChar * pValue)
	{
		return NCheck(NProcessorInfoGetVendorName(pValue));
	}

	static NString GetVendorName()
	{
		NString value;
		return NGetString(value, NProcessorInfoGetVendorName);
	}

	static NProcessorVendor GetVendor()
	{
		NProcessorVendor value;
		NCheck(NProcessorInfoGetVendor(&value));
		return value;
	}

	static NInt GetModelName(NChar * pValue)
	{
		return NCheck(NProcessorInfoGetModelName(pValue));
	}

	static NString GetModelName()
	{
		NString value;
		return NGetString(value, NProcessorInfoGetModelName);
	}

	static bool IsMmxSupported()
	{
		return NProcessorInfoIsMmxSupported() != 0;
	}

	static bool Is3DNowSupported()
	{
		return NProcessorInfoIs3DNowSupported() != 0;
	}

	static bool IsSseSupported()
	{
		return NProcessorInfoIsSseSupported() != 0;
	}

	static bool IsSse2Supported()
	{
		return NProcessorInfoIsSse2Supported() != 0;
	}

	static bool IsSse3Supported()
	{
		return NProcessorInfoIsSse3Supported() != 0;
	}

	static bool IsSsse3Supported()
	{
		return NProcessorInfoIsSsse3Supported() != 0;
	}

	static bool IsLZCntSupported()
	{
		return NProcessorInfoIsLZCntSupported() != 0;
	}

	static bool IsPopCntSupported()
	{
		return NProcessorInfoIsPopCntSupported() != 0;
	}

	static bool IsSse41Supported()
	{
		return NProcessorInfoIsSse41Supported() != 0;
	}

	static bool IsSse4aSupported()
	{
		return NProcessorInfoIsSse4aSupported() != 0;
	}

	static bool IsSse5Supported()
	{
		return NProcessorInfoIsSse5Supported() != 0;
	}
};

}

#endif // !N_PROCESSOR_INFO_HPP_INCLUDED
