#ifndef N_IMAGE_FILE_H_INCLUDED
#define N_IMAGE_FILE_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NImageFile)

#ifdef N_CPP
}
#endif

#include <NImageFormat.h>

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFileCreateA(const NAChar * szFileName, HNImageFormat hImageFormat, HNImageFile * pHImageFile);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFileCreateW(const NWChar * szFileName, HNImageFormat hImageFormat, HNImageFile * pHImageFile);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFileCreate(const NChar * szFileName, HNImageFormat hImageFormat, HNImageFile * pHImageFile);
#endif
#define NImageFileCreate N_FUNC_AW(NImageFileCreate)

NResult N_API NImageFileClose(HNImageFile hImageFile);
NResult N_API NImageFileReadImage(HNImageFile hImageFile, HNImage * pHImage);

NResult N_API NImageFileIsOpened(HNImageFile hImageFile, NBool * pValue);
NResult N_API NImageFileGetFormat(HNImageFile hImageFile, HNImageFormat * pValue);

#define NImageFileFree(hImageFile) NObjectFree(hImageFile)

#ifdef N_MSVC
	#pragma deprecated("NImageFileFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !N_IMAGE_FILE_H_INCLUDED
