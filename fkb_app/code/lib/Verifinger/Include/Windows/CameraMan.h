#ifndef CAMERA_MAN_H_INCLUDED
#define CAMERA_MAN_H_INCLUDED

#include "Camera.h"
#include "NDeviceManager.h"

#ifdef N_CPP
extern "C"
{
#endif

NResult N_API CameraManInitialize(void);
void N_API CameraManUninitialize(void);

NResult N_API CameraManGetCameraCount(NInt * pValue);
NResult N_API CameraManGetCamera(NInt index, HCamera * pHCamera);

#ifndef N_NO_ANSI_FUNC
NResult N_API CameraManGetCameraByIdA(const NAChar * szId, HCamera * pHCamera);
#endif
#ifndef N_NO_UNICODE
NResult N_API CameraManGetCameraByIdW(const NWChar * szId, HCamera * pHCamera);
#endif
#ifdef N_DOCUMENTATION
NResult N_API CameraManGetCameraById(const NChar * szId, HCamera * pHCamera);
#endif
#define CameraManGetCameraById N_FUNC_AW(CameraManGetCameraById)

#ifdef N_CPP
}
#endif

#endif // !CAMERA_MAN_H_INCLUDED
