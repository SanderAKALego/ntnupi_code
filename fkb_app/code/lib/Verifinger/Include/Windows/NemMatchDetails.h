#ifndef NEM_MATCH_DETAILS_H_INCLUDED
#define NEM_MATCH_DETAILS_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef struct NemMatchDetails_
{
	NInt Score;
} NemMatchDetails;

NResult N_API NemMatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NemMatchDetails * * ppMatchDetails);
NResult N_API NemMatchDetailsDeserializeFromStream(HNStream hStream, NemMatchDetails * * ppMatchDetails);
void N_API NemMatchDetailsFree(NemMatchDetails * pMatchDetails);
NResult N_API NemMatchDetailsSerialize(NemMatchDetails * pMatchDetails, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API NemMatchDetailsSerializeToStream(NemMatchDetails * pMatchDetails, HNStream hStream);

#ifdef N_CPP
}
#endif

#endif // !NEM_MATCH_DETAILS_H_INCLUDED
