#ifndef JPEG_H_INCLUDED
#define JPEG_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API JpegLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API JpegLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API JpegLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define JpegLoadImageFromFile N_FUNC_AW(JpegLoadImageFromFile)

NResult N_API JpegLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API JpegLoadImageFromStream(HNStream hStream, HNImage * pHImage);

#define JPEG_DEFAULT_QUALITY 75

#ifndef N_NO_ANSI_FUNC
NResult N_API JpegSaveImageToFileA(HNImage hImage, NInt quality, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API JpegSaveImageToFileW(HNImage hImage, NInt quality, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API JpegSaveImageToFile(HNImage hImage, NInt quality, const NChar * szFileName);
#endif
#define JpegSaveImageToFile N_FUNC_AW(JpegSaveImageToFile)

NResult N_API JpegSaveImageToMemory(HNImage hImage, NInt quality, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API JpegSaveImageToStream(HNImage hImage, NInt quality, HNStream hStream);

#ifndef N_NO_ANSI_FUNC
NResult N_API LosslessJpegSaveImageToFileA(HNImage hImage, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API LosslessJpegSaveImageToFileW(HNImage hImage, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API LosslessJpegSaveImageToFile(HNImage hImage, const NChar * szFileName);
#endif
#define LosslessJpegSaveImageToFile N_FUNC_AW(LosslessJpegSaveImageToFile)

NResult N_API LosslessJpegSaveImageToMemory(HNImage hImage, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API LosslessJpegSaveImageToStream(HNImage hImage, HNStream hStream);

#ifdef N_CPP
}
#endif

#endif // !JPEG_H_INCLUDED
