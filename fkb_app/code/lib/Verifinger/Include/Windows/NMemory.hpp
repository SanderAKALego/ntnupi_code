#ifndef N_MEMORY_HPP_INCLUDED
#define N_MEMORY_HPP_INCLUDED

#include <NTypes.hpp>
namespace Neurotec
{
#include <NMemory.h>
}
#include <memory>
#ifdef N_WINDOWS
	#include <NWindows.hpp>
#endif

namespace Neurotec
{

#undef NClear
#undef NAlignedAlloc
#undef NAlignedCAlloc
#undef NAlignedReAlloc

inline NResult NClear(void * pBlock, NSizeType size)
{
	return NFill(pBlock, 0, size);
}

inline NResult NAlignedAlloc(NSizeType size, NSizeType alignment, void * * ppBlock)
{
	return NAlignedOffsetAlloc(size, alignment, 0, ppBlock);
}

inline NResult NAlignedCAlloc(NSizeType size, NSizeType alignment, void * * ppBlock)
{
	return NAlignedOffsetCAlloc(size, alignment, 0, ppBlock);
}

inline NResult NAlignedReAlloc(void * * ppBlock, NSizeType size, NSizeType alignment)
{
	return NAlignedOffsetReAlloc(ppBlock, size, alignment, 0);
}

class NAutoFree
{
private:
	void * ptr;

public:
	explicit NAutoFree(void * ptr = NULL)
		: ptr(ptr)
	{
	}

	NAutoFree(NAutoFree & value)
		: ptr(value.Release())
	{
	}

	~NAutoFree()
	{
		NFree(ptr);
	}

	NAutoFree & operator=(NAutoFree & value)
	{
		Reset(value.Release());
		return (*this);
	}

	void * Get() const
	{
		return ptr;
	}

	void * Release()
	{
		void * ptr = this->ptr;
		this->ptr = NULL;
		return ptr;
	}

	void Reset(void * ptr = NULL)
	{
		if (ptr != this->ptr)
		{
			NFree(this->ptr);
			this->ptr = ptr;
		}
	}
};

#ifdef N_WINDOWS
	class AutoLocalFree
	{
	private:
		void * ptr;

	public:
		explicit AutoLocalFree(void * ptr = NULL)
			: ptr(ptr)
		{
		}

		AutoLocalFree(AutoLocalFree & value)
			: ptr(value.Release())
		{
		}

		~AutoLocalFree()
		{
			LocalFree(ptr);
		}

		AutoLocalFree & operator=(AutoLocalFree & value)
		{
			Reset(value.Release());
			return (*this);
		}

		void * Get() const
		{
			return ptr;
		}

		void * Release()
		{
			void * ptr = this->ptr;
			this->ptr = NULL;
			return ptr;
		}

		void Reset(void * ptr = NULL)
		{
			if (ptr != this->ptr)
			{
				LocalFree(this->ptr);
				this->ptr = ptr;
			}
		}
	};
#endif

NInt NCheck(NResult result);

inline void * NAlloc(NSizeType size)
{
	void * pBlock;
	NCheck(NAlloc(size, &pBlock));
	return pBlock;
}

inline void * NCAlloc(NSizeType size)
{
	void * pBlock;
	NCheck(NCAlloc(size, &pBlock));
	return pBlock;
}

inline void * NReAlloc(void * pBlock, NSizeType size)
{
	NCheck(NReAlloc(&pBlock, size));
	return pBlock;
}

inline void NCopyMemory(void * pDstBlock, const void * pSrcBlock, NSizeType size)
{
	NCheck(NCopy(pDstBlock, pSrcBlock, size));
}

inline void NMoveMemory(void * pDstBlock, const void * pSrcBlock, NSizeType size)
{
	NCheck(NMove(pDstBlock, pSrcBlock, size));
}

inline void NFillMemory(void * pBlock, NByte value, NSizeType size)
{
	NCheck(NFill(pBlock, value, size));
}

inline void NClearMemory(void * pBlock, NSizeType size)
{
	NCheck(NClear(pBlock, size));
}

inline NInt NCompare(const void * pBlock1, const void * pBlock2, NSizeType size)
{
	NInt result;
	NCheck(NCompare(pBlock1, pBlock2, size, &result));
	return result;
}

inline void * NAlignedAlloc(NSizeType size, NSizeType alignment, NSizeType offset = 0)
{
	void * pBlock;
	NCheck(NAlignedOffsetAlloc(size, alignment, offset, &pBlock));
	return pBlock;
}

inline void * NAlignedCAlloc(NSizeType size, NSizeType alignment, NSizeType offset = 0)
{
	void * pBlock;
	NCheck(NAlignedOffsetCAlloc(size, alignment, offset, &pBlock));
	return pBlock;
}

inline void * NAlignedReAlloc(void * pBlock, NSizeType size, NSizeType alignment, NSizeType offset = 0)
{
	NCheck(NAlignedOffsetReAlloc(&pBlock, size, alignment, offset));
	return pBlock;
}

}

#endif // !N_MEMORY_HPP_INCLUDED
