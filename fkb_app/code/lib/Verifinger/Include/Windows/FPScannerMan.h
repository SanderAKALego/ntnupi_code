#ifndef FP_SCANNER_MAN_H_INCLUDED
#define FP_SCANNER_MAN_H_INCLUDED

#include "FPScanner.h"
#include "NDeviceManager.h"

#ifdef N_CPP
extern "C"
{
#endif

typedef void (N_CALLBACK FPScannerManScannerCallbackEx)(HFPScanner hScanner, void * pParam);

#ifndef N_NO_ANSI_FUNC
typedef void (N_CALLBACK FPScannerManScannerCallbackA)(const NAChar * szScannerId, void * pParam);
#endif
#ifndef N_NO_UNICODE
typedef void (N_CALLBACK FPScannerManScannerCallbackW)(const NWChar * szScannerId, void * pParam);
#endif
#define FPScannerManScannerCallback N_CALLBACK_AW(FPScannerManScannerCallback)

#ifndef N_NO_ANSI_FUNC
NResult N_API FPScannerManGetAvailableModulesA(NAChar * * pSzValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API FPScannerManGetAvailableModulesW(NWChar * * pSzValue);
#endif
#define FPScannerManGetAvailableModules N_FUNC_AW(FPScannerManGetAvailableModules)

NResult N_API FPScannerManInitialize(void);
#ifndef N_NO_ANSI_FUNC
NResult N_API FPScannerManInitializeWithModulesA(const NAChar * szModules);
#endif
#ifndef N_NO_UNICODE
NResult N_API FPScannerManInitializeWithModulesW(const NWChar * szModules);
#endif
#define FPScannerManInitializeWithModules N_FUNC_AW(FPScannerManInitializeWithModules)

NResult N_API FPScannerManInitialize(void);
void N_API FPScannerManUninitialize(void);

NResult N_API FPScannerManGetScannerCount(NInt * pValue);
NResult N_API FPScannerManGetScanner(NInt index, HFPScanner * pValue);

#ifndef N_NO_ANSI_FUNC
NResult N_API FPScannerManGetScannerByIdA(const NAChar * szId, HFPScanner * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API FPScannerManGetScannerByIdW(const NWChar * szId, HFPScanner * pValue);
#endif
#define FPScannerManGetScannerById N_FUNC_AW(FPScannerManGetScannerById)

NResult N_API FPScannerManSetScannerAddedCallbackEx(FPScannerManScannerCallbackEx pCallback, void * pParam);

#ifndef N_NO_ANSI_FUNC
N_DEPRECATED("function is deprecated, use FPScannerManSetScannerAddedCallbackEx instead")
NResult N_API FPScannerManSetScannerAddedCallbackA(FPScannerManScannerCallbackA pCallback, void * pParam);
#endif
#ifndef N_NO_UNICODE
N_DEPRECATED("function is deprecated, use FPScannerManSetScannerAddedCallbackEx instead")
NResult N_API FPScannerManSetScannerAddedCallbackW(FPScannerManScannerCallbackW pCallback, void * pParam);
#endif
#define FPScannerManSetScannerAddedCallback N_FUNC_AW(FPScannerManSetScannerAddedCallback)

NResult N_API FPScannerManSetScannerRemovedCallbackEx(FPScannerManScannerCallbackEx pCallback, void * pParam);

#ifndef N_NO_ANSI_FUNC
N_DEPRECATED("function is deprecated, use FPScannerManSetScannerRemovedCallbackEx instead")
NResult N_API FPScannerManSetScannerRemovedCallbackA(FPScannerManScannerCallbackA pCallback, void * pParam);
#endif
#ifndef N_NO_UNICODE
N_DEPRECATED("function is deprecated, use FPScannerManSetScannerRemovedCallbackEx instead")
NResult N_API FPScannerManSetScannerRemovedCallbackW(FPScannerManScannerCallbackW pCallback, void * pParam);
#endif
#define FPScannerManSetScannerRemovedCallback N_FUNC_AW(FPScannerManSetScannerRemovedCallback)

#ifdef N_CPP
}
#endif

#endif // !FP_SCANNER_MAN_H_INCLUDED
