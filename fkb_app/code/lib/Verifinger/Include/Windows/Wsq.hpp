#ifndef WSQ_HPP_INCLUDED
#define WSQ_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <Wsq.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(Wsq)
{
private:
	N_CLASS(Wsq)() {}
	N_CLASS(Wsq)(const N_CLASS(Wsq)&) {}

public:

	static const float DefaultBitRate;

	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(WsqLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(WsqLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NFloat bitRate, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(WsqSaveImageToFile(pImage->GetHandle(), bitRate, szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NFloat bitRate, const NString & fileName)
	{
		SaveImage(pImage, bitRate, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NChar * szFileName)
	{
		NCheck(WsqSaveImageToFile(pImage->GetHandle(), DefaultBitRate, szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NString & fileName)
	{
		SaveImage(pImage, DefaultBitRate, fileName);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, void * * pBuffer, NSizeType * pBufferLength)
	{
		SaveImage(pImage, DefaultBitRate, pBuffer, pBufferLength);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NFloat bitRate, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(WsqSaveImageToMemory(pImage->GetHandle(), bitRate, pBuffer, pBufferLength));
	}
};

const float N_CLASS(Wsq)::DefaultBitRate = 0.75f;

}}

#endif // !WSQ_HPP_INCLUDED
