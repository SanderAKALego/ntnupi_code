#ifndef FP_SCANNER_H_INCLUDED
#define FP_SCANNER_H_INCLUDED

#include <NGrayscaleImage.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(FPScanner)

typedef void (N_CALLBACK FPScannerCallback)(HFPScanner hScanner, void * pParam);
typedef void (N_CALLBACK FPScannerImageScannedCallback)(HFPScanner hScanner, HNGrayscaleImage hImage, void * pParam);

#ifndef N_NO_ANSI_FUNC
NResult N_API FPScannerGetIdA(HFPScanner hScanner, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API FPScannerGetIdW(HFPScanner hScanner, NWChar * pValue);
#endif
#define FPScannerGetId N_FUNC_AW(FPScannerGetId)

NResult N_API FPScannerIsCapturing(HFPScanner hScanner, NBool * pValue);
NResult N_API FPScannerSetIsCapturingChangedCallback(HFPScanner hScanner, FPScannerCallback pCallback, void * pParam);
NResult N_API FPScannerSetFingerPlacedCallback(HFPScanner hScanner, FPScannerCallback pCallback, void * pParam);
NResult N_API FPScannerSetFingerRemovedCallback(HFPScanner hScanner, FPScannerCallback pCallback, void * pParam);
NResult N_API FPScannerSetImageScannedCallback(HFPScanner hScanner, FPScannerImageScannedCallback pCallback, void * pParam);
NResult N_API FPScannerStartCapturing(HFPScanner hScanner);
NResult N_API FPScannerStartCapturingForOneImage(HFPScanner hScanner);
NResult N_API FPScannerStopCapturing(HFPScanner hScanner);

#ifdef N_CPP
}
#endif

#endif // !FP_SCANNER_H_INCLUDED
