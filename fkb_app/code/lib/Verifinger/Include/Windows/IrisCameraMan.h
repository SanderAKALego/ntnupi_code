#ifndef IRIS_CAMERA_MAN_H_INCLUDED
#define IRIS_CAMERA_MAN_H_INCLUDED

#include "IrisCamera.h"
#include "NDeviceManager.h"

#ifdef N_CPP
extern "C"
{
#endif

NResult N_API IrisCameraManInitialize(void);
void N_API IrisCameraManUninitialize(void);

NResult N_API IrisCameraManGetCameraCount(NInt *pValue);
NResult N_API IrisCameraManGetCamera(NInt index, HIrisCamera *pHIrisCamera);

#ifndef N_NO_ANSI_FUNC
NResult N_API IrisCameraManGetCameraByIdA(const NAChar *szId, HIrisCamera *pHIrisCamera);
#endif
#ifndef N_NO_UNICODE
NResult N_API IrisCameraManGetCameraByIdW(const NWChar *szId, HIrisCamera *pHIrisCamera);
#endif
#ifdef N_DOCUMENTATION
NResult N_API IrisCameraManGetCameraById(const NChar *szId, HIrisCamera *pHIrisCamera);
#endif
#define IrisCameraManGetCameraById N_FUNC_AW(IrisCameraManGetCameraById)

#ifdef N_CPP
}
#endif

#endif // !IRIS_CAMERA_MAN_H_INCLUDED
