#ifndef N_DEVICE_MANAGER_LIBRARY_CPP_INCLUDED
#define N_DEVICE_MANAGER_LIBRARY_CPP_INCLUDED

#include <NImagesLibrary.cpp>

#include <NDeviceManager.hpp>
#include <CameraMan.hpp>
#include <Camera.hpp>
#ifdef N_WINDOWS
	#include <IrisCameraMan.hpp>
	#include <IrisCamera.hpp>
	#include <FPScannerMan.hpp>
	#include <FPScanner.hpp>
#endif

namespace Neurotec { namespace DeviceManager
{

NInt N_CLASS(CameraMan)::initCount = 0;
std::auto_ptr<N_CLASS(CameraMan)::CameraCollection> N_CLASS(CameraMan)::cameras;
#ifdef N_WINDOWS
	NInt N_CLASS(IrisCameraMan)::initCount = 0;
	std::auto_ptr<N_CLASS(IrisCameraMan)::IrisCameraCollection> N_CLASS(IrisCameraMan)::cameras;
	NInt N_CLASS(FPScannerMan)::initCount = 0;
	std::auto_ptr<N_CLASS(FPScannerMan)::FPScannerCollection> N_CLASS(FPScannerMan)::scanners;
	NEvent1<N_CLASS(FPScanner) *> N_CLASS(FPScannerMan)::scannerAddedEvent;
	NEvent1<N_CLASS(FPScanner) *> N_CLASS(FPScannerMan)::scannerRemovedEvent;
#endif

N_IMPLEMENT_OBJECT_TYPE(Camera, NObject)
#ifdef N_WINDOWS
	N_IMPLEMENT_OBJECT_TYPE(IrisCamera, NObject)
	N_IMPLEMENT_OBJECT_TYPE(FPScanner, NObject)
#endif

}}

#endif // !N_DEVICE_MANAGER_LIBRARY_CPP_INCLUDED
