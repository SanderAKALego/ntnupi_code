#ifndef N_DEVICE_MANAGER_H_INCLUDED
#define N_DEVICE_MANAGER_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API NDeviceManagerGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NDeviceManagerGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NDeviceManagerGetInfo(NLibraryInfo * pValue);
#endif
#define NDeviceManagerGetInfo N_FUNC_AW(NDeviceManagerGetInfo)

#ifdef N_CPP
}
#endif

#endif // !N_DEVICE_MANAGER_H_INCLUDED
