#ifndef N_IMAGES_H_INCLUDED
#define N_IMAGES_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API NImagesGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImagesGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImagesGetInfo(NLibraryInfo * pValue);
#endif
#define NImagesGetInfo N_FUNC_AW(NImagesGetInfo)

NResult N_API NImagesGetGrayscaleColorWrapperEx(HNImage hImage, const NRgb * pMinColor, const NRgb * pMaxColor, HNImage * pHDstImage);

#ifdef N_CPP
}
#endif

#endif // !N_IMAGES_H_INCLUDED
