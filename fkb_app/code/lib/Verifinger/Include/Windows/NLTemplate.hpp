#ifndef NL_TEMPLATE_HPP_INCLUDED
#define NL_TEMPLATE_HPP_INCLUDED

#include <NLRecord.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NLTemplate.h>
}}

namespace Neurotec { namespace Biometrics
{
#undef NLT_MAX_RECORD_COUNT
#undef NLT_PROCESS_FIRST_RECORD_ONLY

const NInt NLT_MAX_RECORD_COUNT = 255;
const NUInt NLT_PROCESS_FIRST_RECORD_ONLY = 0x00000100;

class N_CLASS(NLTemplate) : public N_CLASS(NObject)
{
public:
	class RecordCollection : public ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NLRecord)>
	{
		RecordCollection(N_CLASS(NLTemplate) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NLRecord)>(pOwner, NLTemplateGetRecordCount, NLTemplateGetRecord, FromHandle,
			NLTemplateGetRecordCapacity, NLTemplateSetRecordCapacity, NLTemplateRemoveRecord, NULL, NLTemplateClearRecords)
		{
		}

		static N_CLASS(NLRecord) * FromHandle(HNLRecord handle)
		{
			return N_CLASS(NLRecord)::FromHandle(handle, false);
		}

		friend class N_CLASS(NLTemplate);

	public:
		N_CLASS(NLRecord)* Add(NUInt flags = 0)
		{
			HNLRecord hRecord;
			NCheck(NLTemplateAddRecord(GetOwner()->GetHandle(), flags, &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NLRecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NLTemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}

		N_CLASS(NLRecord)* Add(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
		{
			HNLRecord hRecord;
			NCheck(NLTemplateAddRecordFromMemory(GetOwner()->GetHandle(), pBuffer, bufferSize, flags, &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NLRecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NLTemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}

		N_CLASS(NLRecord)* AddCopy(N_CLASS(NLRecord)* pSrcRecord)
		{
			HNLRecord hRecord;
			if (pSrcRecord == NULL)
			{
				NThrowArgumentNullException(N_T("pSrcRecord"));
			}

			NCheck(NLTemplateAddRecordCopy(GetOwner()->GetHandle(), pSrcRecord->GetHandle(), &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NLRecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NLTemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}
	};

private:
	static HNLTemplate Create(NUInt flags)
	{
		HNLTemplate handle;
		NCheck(NLTemplateCreateEx(flags, &handle));
		return handle;
	}

	static HNLTemplate Create(const void * pBuffer, NSizeType bufferSize,
		NUInt flags, NLTemplateInfo * pInfo)
	{
		HNLTemplate handle;
		NCheck(NLTemplateCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

	std::auto_ptr<RecordCollection> records;

	void Init()
	{
		records = std::auto_ptr<RecordCollection>(new RecordCollection(this));
	}

public:
	static N_CLASS(NLTemplate)* FromHandle(HNLTemplate handle, bool ownsHandle = true)
	{
		return new N_CLASS(NLTemplate)(handle, ownsHandle);
	}

	static NSizeType CalculateSize(NInt recordCount, NSizeType * arRecordSizes)
	{
		NSizeType value;
		NCheck(NLTemplateCalculateSize(recordCount, arRecordSizes, &value));
		return value;
	}

	static NSizeType Pack(NInt recordCount, const void * * arPRecords, NSizeType * arRecordSizes, void * pBuffer, NSizeType bufferSize)
	{
		NSizeType value;
		NCheck(NLTemplatePack(recordCount, arPRecords, arRecordSizes, pBuffer, bufferSize, &value));
		return value;
	}

	static void Unpack(const void * pBuffer, NSizeType bufferSize,
		NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
		NInt * pRecordCount, const void * * arPRecords, NSizeType * arRecordSizes)
	{
		NCheck(NLTemplateUnpack(pBuffer, bufferSize,
			pMajorVersion, pMinorVersion, pSize, pHeaderSize,
			pRecordCount, arPRecords, arRecordSizes));
	}

	static void Check(const void * pBuffer, NSizeType bufferSize)
	{
		NCheck(NLTemplateCheck(pBuffer, bufferSize));
	}

	static NInt GetRecordCount(const void * pBuffer, NSizeType bufferSize)
	{
		NInt value;
		NCheck(NLTemplateGetRecordCountMem(pBuffer, bufferSize, &value));
		return value;
	}

	explicit N_CLASS(NLTemplate)(HNLTemplate handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NLTemplate), false)
	{
		Init();
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NLTemplate)(NUInt flags = 0)
		: N_CLASS(NObject)(Create(flags), N_NATIVE_TYPE_OF(NLTemplate), true)
	{
		Init();
	}

	N_CLASS(NLTemplate)(const void * pBuffer, NSizeType bufferSize,
		NUInt flags = 0, NLTemplateInfo * pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NLTemplate), true)
	{
		Init();
	}

	RecordCollection * GetRecords() const
	{
		return records.get();
	}

	N_CLASS(NObject) * Clone() const
	{
		HNLTemplate handle;
		NCheck(NLTemplateClone((HNLTemplate)GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NLTemplateGetSize((HNLTemplate)GetHandle(), flags, &value));
		return value;
	}

	NSizeType Save(void * pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NLTemplateSaveToMemory((HNLTemplate)GetHandle(), pBuffer, bufferSize, flags, &value));
		return value;
	}

	N_DECLARE_OBJECT_TYPE(NLTemplate)
};

}}

#endif // !NL_TEMPLATE_HPP_INCLUDED
