#ifndef IRIS_CAMERA_HPP_INCLUDED
#define IRIS_CAMERA_HPP_INCLUDED

#include <NGrayscaleImage.hpp>
#include <IrisCameraParams.hpp>
namespace Neurotec { namespace DeviceManager
{
using Neurotec::Images::HNGrayscaleImage;
#include <IrisCamera.h>
}}

namespace Neurotec { namespace DeviceManager
{

class N_CLASS(IrisCameraMan);
class N_CLASS(IrisCamera);

class N_CLASS(IrisCamera) : public N_CLASS(NObject)
{
private:
	NEvent3<N_CLASS(IrisCamera) *, Neurotec::Images::N_CLASS(NGrayscaleImage) *, Neurotec::Images::N_CLASS(NGrayscaleImage) *> irisesAcquiredEvent;

	static void N_API OnIrisesAcquired(HIrisCamera, HNGrayscaleImage hLeftIris, HNGrayscaleImage hRightIris, void * pParam)
	{
		N_CLASS(IrisCamera) * pCamera = (N_CLASS(IrisCamera) *)pParam;
		if(pCamera->irisesAcquiredEvent)
		{
			std::auto_ptr<Neurotec::Images::N_CLASS(NGrayscaleImage)> leftImage((Neurotec::Images::N_CLASS(NGrayscaleImage) *)(hLeftIris ? Neurotec::Images::N_CLASS(NImage)::FromHandle(hLeftIris, false) : NULL));
			std::auto_ptr<Neurotec::Images::N_CLASS(NGrayscaleImage)> rightImage((Neurotec::Images::N_CLASS(NGrayscaleImage) *)(hRightIris ? Neurotec::Images::N_CLASS(NImage)::FromHandle(hRightIris, false) : NULL));
			pCamera->irisesAcquiredEvent(pCamera, leftImage.get(), rightImage.get());
		}
	}

	N_CLASS(IrisCamera)(HIrisCamera handle)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(IrisCamera), false)
	{
		IrisCameraSetIrisesAcquiredCallback(GetHandle(), OnIrisesAcquired, this);
	}

public:
	typedef void (* IrisesAcquiredCallback)(N_CLASS(IrisCamera) *pIrisCamera, Neurotec::Images::N_CLASS(NGrayscaleImage) *pLeftIris, Neurotec::Images::N_CLASS(NGrayscaleImage) *pRightIris, void *pParam);

	virtual ~N_CLASS(IrisCamera)()
	{
		IrisCameraSetIrisesAcquiredCallback(GetHandle(), NULL, NULL);
	}

	NInt GetId(NChar * pValue)
	{
		return NCheck(IrisCameraGetId(GetHandle(), pValue));
	}

	NString GetId()
	{
		NString value;
		return NGetString(value, IrisCameraGetId, this);
	}

	bool IsCapturing()
	{
		NBool value;
		NCheck(IrisCameraIsCapturing(GetHandle(), &value));
		return value != 0;
	}

	void StartCapturing()
	{
		NCheck(IrisCameraStartCapturing(GetHandle()));
	}

	void StopCapturing()
	{
		NCheck(IrisCameraStopCapturing(GetHandle()));
	}

	void GetImages(Neurotec::Images::N_CLASS(NGrayscaleImage) * * ppLeftIris, Neurotec::Images::N_CLASS(NGrayscaleImage) * * ppRightIris)
	{
		if (!ppLeftIris) NThrowArgumentNullException(N_T("ppLeftIris"));
		if (!ppRightIris) NThrowArgumentNullException(N_T("ppRightIris"));
		*ppLeftIris = NULL;
		*ppRightIris = NULL;
		HNGrayscaleImage hLeftIris, hRightIris;
		NCheck(IrisCameraGetImages(GetHandle(), &hLeftIris, &hRightIris));
		try
		{
			*ppLeftIris = (Neurotec::Images::N_CLASS(NGrayscaleImage) *)(hLeftIris ? Neurotec::Images::N_CLASS(NImage)::FromHandle(hLeftIris) : NULL);
			*ppRightIris = (Neurotec::Images::N_CLASS(NGrayscaleImage) *)(hRightIris ? Neurotec::Images::N_CLASS(NImage)::FromHandle(hRightIris) : NULL);
		}
		catch(...)
		{
			if(!*ppLeftIris) NObjectFree(hLeftIris);
			else delete *ppLeftIris;
			if(!*ppRightIris) NObjectFree(hRightIris);
			else delete *ppRightIris;
			throw;
		}
	}

	void AddIrisesAcquiredCallback(IrisesAcquiredCallback pCallback, void * pParam)
	{
		irisesAcquiredEvent.Add(pCallback, pParam);
	}

	void RemoveIrisesAcquiredCallback(IrisesAcquiredCallback pCallback, void * pParam)
	{
		irisesAcquiredEvent.Remove(pCallback, pParam);
	}

	N_DECLARE_OBJECT_TYPE(IrisCamera)

	friend class N_CLASS(IrisCameraMan);
};

}}

#endif // !IRIS_CAMERA_HPP_INCLUDED
