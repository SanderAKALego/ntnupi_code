#ifndef N_MATCHERS_LIBRARY_CPP_INCLUDED
#define N_MATCHERS_LIBRARY_CPP_INCLUDED

#include <NTemplatesLibrary.cpp>

#include <NEMatcher.hpp>
#include <NesMatcher.hpp>
#include <NFMatcher.hpp>
#include <NfsMatcher.hpp>
#include <NLMatcher.hpp>
#include <NlsMatcher.hpp>
#include <NMatcher.hpp>

#include <NMMatchDetails.hpp>
#include <NemMatchDetails.hpp>
#include <NesmMatchDetails.hpp>
#include <NfmMatchDetails.hpp>
#include <NfsmMatchDetails.hpp>
#include <NlsmMatchDetails.hpp>

namespace Neurotec { namespace Biometrics
{

N_IMPLEMENT_OBJECT_TYPE(NEMatcher, NObject)
N_IMPLEMENT_OBJECT_TYPE(NesMatcher, NObject)
N_IMPLEMENT_OBJECT_TYPE(NFMatcher, NObject)
N_IMPLEMENT_OBJECT_TYPE(NfsMatcher, NObject)
N_IMPLEMENT_OBJECT_TYPE(NLMatcher, NObject)
N_IMPLEMENT_OBJECT_TYPE(NlsMatcher, NObject)
N_IMPLEMENT_OBJECT_TYPE(NMatcher, NObject)

}}

#endif // !N_MATCHERS_LIBRARY_CPP_INCLUDED
