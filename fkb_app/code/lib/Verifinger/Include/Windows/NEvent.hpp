#ifndef N_EVENT_HPP_INCLUDED
#define N_EVENT_HPP_INCLUDED

#include <NTypes.hpp>
#include <list>

namespace Neurotec
{
#define N_EVENT_IMPL(argCount, argsDef, callbackArgTypes, callbackArgs) \
	private:\
		typedef void (* CallbackType)callbackArgTypes;\
		typedef std::pair<CallbackType, void *> ItemType;\
		std::list<ItemType> callbacks;\
	public:\
		NEvent##argCount()\
		{\
		}\
		NEvent##argCount(CallbackType pCallback, void * pParam)\
		{\
			Add(pCallback, pParam);\
		}\
		bool IsEmpty() const\
		{\
			return callbacks.empty();\
		}\
		void Add(CallbackType pCallback, void * pParam)\
		{\
			if (!pCallback) NThrowArgumentNullException(N_T("pCallback"));\
			callbacks.push_back(ItemType(pCallback, pParam));\
		}\
		bool Remove(CallbackType pCallback, void * pParam)\
		{\
			if (!pCallback) NThrowArgumentNullException(N_T("pCallback"));\
			for (typename std::list<ItemType>::reverse_iterator i = callbacks.rbegin(); i != callbacks.rend(); i++)\
			{\
				if (i->first == pCallback && i->second == pParam)\
				{\
					callbacks.erase(--(i.base()));\
					return true;\
				}\
			}\
			return false;\
		}\
		void operator()argsDef const\
		{\
			for (typename std::list<ItemType>::const_iterator i = callbacks.begin(); i != callbacks.end(); i++)\
			{\
				i->first callbackArgs;\
			}\
		}\
		operator bool() const\
		{\
			return !IsEmpty();\
		}\
		bool operator!() const\
		{\
			return IsEmpty();\
		}

	template<typename T1> class NEvent1
	{
		N_EVENT_IMPL(1, (T1 p1), (T1, void *), (p1, i->second))
	};

	template<typename T1, typename T2> class NEvent2
	{
		N_EVENT_IMPL(2, (T1 p1, T2 p2), (T1, T2, void *), (p1, p2, i->second))
	};

	template<typename T1, typename T2, typename T3> class NEvent3
	{
		N_EVENT_IMPL(3, (T1 p1, T2 p2, T3 p3), (T1, T2, T3, void *), (p1, p2, p3, i->second))
	};
}

#endif // !N_EVENT_HPP_INCLUDED
