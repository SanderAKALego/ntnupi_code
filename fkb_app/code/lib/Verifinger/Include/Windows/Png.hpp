#ifndef PNG_HPP_INCLUDED
#define PNG_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <Png.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(Png)
{
private:
	N_CLASS(Png)() {}
	N_CLASS(Png)(const N_CLASS(Png)&) {}

public:

	static const NInt DefaultCompressionLevel = 6;

	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(PngLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(PngLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NChar * szFileName)
	{
		SaveImage(pImage, DefaultCompressionLevel, szFileName);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NString & fileName)
	{
		SaveImage(pImage, DefaultCompressionLevel, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NInt compressionLevel, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(PngSaveImageToFile(pImage->GetHandle(), compressionLevel, szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NInt compressionLevel, const NString & fileName)
	{
		SaveImage(pImage, compressionLevel, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, void * * pBuffer, NSizeType * pBufferLength)
	{
		SaveImage(pImage, DefaultCompressionLevel, pBuffer, pBufferLength);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NInt compressionLevel, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(PngSaveImageToMemory(pImage->GetHandle(), compressionLevel, pBuffer, pBufferLength));
	}
};

}}

#endif // !PNG_HPP_INCLUDED
