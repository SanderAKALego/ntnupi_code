#ifndef N_LICENSING_HPP_INCLUDED
#define N_LICENSING_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Licensing
{
#include <NLicensing.h>
}}

namespace Neurotec { namespace Licensing
{

class N_CLASS(NLicensing)
{
private:
	N_CLASS(NLicensing)()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NLicensingGetInfo(pValue));
	}
};

class N_CLASS(NLicense)
{
private:
	N_CLASS(NLicense)()
	{
	}

public:
	static bool Obtain(const NChar * szAddress, const NChar * szPort, const NChar * szProducts)
	{
		NBool available;
		NCheck(NLicenseObtain(szAddress, szPort, szProducts, &available));
		return available != 0;
	}

	static bool Obtain(const NString & address, const NString & port, const NString & products)
	{
		return Obtain(NGetConstStringBuffer(address), NGetConstStringBuffer(port), NGetConstStringBuffer(products));
	}

	static void Release(const NChar * szProducts)
	{
		NCheck(NLicenseRelease(szProducts));
	}

	static void Release(const NString & products)
	{
		Release(NGetConstStringBuffer(products));
	}

	static void GetInfo(const NChar * szProduct, NLicenseInfo * pLicenseInfo)
	{
		NCheck(NLicenseGetInfo(szProduct, pLicenseInfo));
	}

	static void GetInfo(const NString & product, NLicenseInfo * pLicenseInfo)
	{
		GetInfo(NGetConstStringBuffer(product), pLicenseInfo);
	}

	static bool IsComponentActivated(const NChar * szName)
	{
		NBool value;
		NCheck(NLicenseIsComponentActivated(szName, &value));
		return value != 0;
	}

	static bool IsComponentActivated(const NString & name)
	{
		return IsComponentActivated(NGetConstStringBuffer(name));
	}
};

}}

#endif // !N_LICENSING_HPP_INCLUDED
