#ifndef N_GRAYSCALE_IMAGE_HPP_INCLUDED
#define N_GRAYSCALE_IMAGE_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <NGrayscaleImage.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(NGrayscaleImage) : public N_CLASS(NImage)
{
public:
	explicit N_CLASS(NGrayscaleImage)(HNImage handle, bool ownsHandle = true)
		: N_CLASS(NImage)(handle, N_NATIVE_TYPE_OF(NGrayscaleImage), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	NByte GetPixel(NUInt x, NUInt y)
	{
		NByte value;
		NCheck(NGrayscaleImageGetPixel(GetHandle(), x, y, &value));
		return value;
	}

	void SetPixel(NUInt x, NUInt y, NByte value)
	{
		NCheck(NGrayscaleImageSetPixel(GetHandle(), x, y, value));
	}

	N_DECLARE_OBJECT_TYPE(NGrayscaleImage)
};

}}

#endif // !N_GRAYSCALE_IMAGE_HPP_INCLUDED
