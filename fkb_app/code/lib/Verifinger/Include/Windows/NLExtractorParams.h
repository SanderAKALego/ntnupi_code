#ifndef NL_EXTRACTOR_PARAMS_H_INCLUDED
#define NL_EXTRACTOR_PARAMS_H_INCLUDED

#include <NParameters.h>

#ifdef N_CPP
extern "C"
{
#endif

#define NLEP_MIN_IOD                    10101
#define NLEP_MAX_IOD                    10102
#define NLEP_FACE_CONFIDENCE_THRESHOLD  10103
#define NLEP_FAVOR_LARGEST_FACE         10104
#define NLEP_MAX_ROLL_ANGLE_DEVIATION   10105

#define NLEP_FACE_QUALITY_THRESHOLD  10350
#define NLEP_TEMPLATE_SIZE           10360

#define NLEP_USE_LIVENESS_CHECK        10402
#define NLEP_LIVENESS_THRESHOLD        10403
#define NLEP_MAX_RECORDS_PER_TEMPLATE  10408

typedef enum NleTemplateSize_
{
	nletsSmall = 0,
	nletsMedium = 64,
	nletsLarge = 128,
} NleTemplateSize;

#ifdef N_CPP
}
#endif

#endif // !NL_EXTRACTOR_PARAMS_H_INCLUDED
