#ifndef N_DEVICE_MANAGER_HPP_INCLUDED
#define N_DEVICE_MANAGER_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace DeviceManager
{
#include <NDeviceManager.h>
}}

namespace Neurotec { namespace DeviceManager
{

class NDeviceManager
{
private:
	NDeviceManager()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NDeviceManagerGetInfo(pValue));
	}
};

}}

#endif // !N_DEVICE_MANAGER_HPP_INCLUDED
