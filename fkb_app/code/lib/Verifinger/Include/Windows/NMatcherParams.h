#ifndef N_MATCHER_PARAMS_H_INCLUDED
#define N_MATCHER_PARAMS_H_INCLUDED

#include <NParameters.h>

#ifdef N_CPP
extern "C"
{
#endif

#define NM_PART_NONE     0
#define NM_PART_FINGERS 10
#define NM_PART_FACES   20
#define NM_PART_IRISES  30
#define NM_PART_PALMS   40

#define NMP_MATCHING_THRESHOLD       100

#define NMP_FUSION_TYPE               200
#define NMP_FACES_MATCHING_THRESHOLD  210
#define NMP_IRISES_MATCHING_THRESHOLD 220

typedef enum NMFusionType_
{
	nmftFuseAlways = 0,
	nmftSelectByFaceThenFuse = 1,
	nmftSelectByIrisThenFuse = 2
} NMFusionType;

#ifdef N_CPP
}
#endif

#endif // !N_MATCHER_PARAMS_H_INCLUDED
