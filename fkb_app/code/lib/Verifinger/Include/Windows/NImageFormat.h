#ifndef N_IMAGE_FORMAT_H_INCLUDED
#define N_IMAGE_FORMAT_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NImageFormat)

#ifdef N_CPP
}
#endif

#include <NImage.h>
#include <NImageFile.h>

#ifdef N_CPP
extern "C"
{
#endif

NResult N_API NImageFormatGetFormatCount(NInt * pValue);
NResult N_API NImageFormatGetFormat(NInt index, HNImageFormat * pValue);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatSelectA(const NAChar * szFileName, NFileAccess fileAccess, HNImageFormat * pHImageFormat);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatSelectW(const NWChar * szFileName, NFileAccess fileAccess, HNImageFormat * pHImageFormat);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatSelect(const NChar * szFileName, NFileAccess fileAccess, HNImageFormat * pHImageFormat);
#endif
#define NImageFormatSelect N_FUNC_AW(NImageFormatSelect)

NResult N_API NImageFormatGetBmp(HNImageFormat * pValue);
NResult N_API NImageFormatGetTiff(HNImageFormat * pValue);
NResult N_API NImageFormatGetJpeg(HNImageFormat * pValue);
NResult N_API NImageFormatGetPng(HNImageFormat * pValue);
NResult N_API NImageFormatGetWsq(HNImageFormat * pValue);
NResult N_API NImageFormatGetIHead(HNImageFormat * pValue);
NResult N_API NImageFormatGetJpeg2K(HNImageFormat * pValue);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatOpenFileA(HNImageFormat hImageFormat, const NAChar * szFileName, HNImageFile * pHImageFile);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatOpenFileW(HNImageFormat hImageFormat, const NWChar * szFileName, HNImageFile * pHImageFile);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatOpenFile(HNImageFormat hImageFormat, const NChar * szFileName, HNImageFile * pHImageFile);
#endif
#define NImageFormatOpenFile N_FUNC_AW(NImageFormatOpenFile)

NResult N_API NImageFormatOpenFileFromMemory(HNImageFormat hImageFormat, const void * buffer, NSizeType bufferLength, HNImageFile * pHImageFile);
NResult N_API NImageFormatOpenFileFromStream(HNImageFormat hImageFormat, HNStream hStream, NBool ownsStream, HNImageFile * pHImageFile);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatLoadImageFromFileA(HNImageFormat hImageFormat, const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatLoadImageFromFileW(HNImageFormat hImageFormat, const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatLoadImageFromFile(HNImageFormat hImageFormat, const NChar * szFileName, HNImage * pHImage);
#endif
#define NImageFormatLoadImageFromFile N_FUNC_AW(NImageFormatLoadImageFromFile)

NResult N_API NImageFormatLoadImageFromMemory(HNImageFormat hImageFormat, const void * buffer, NSizeType bufferLength, HNImage * pHImage);

NResult N_API NImageFormatLoadImageFromStream(HNImageFormat hImageFormat, HNStream hStream, HNImage * pHImage);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatSaveImageToFileA(HNImageFormat hImageFormat, HNImage hImage, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatSaveImageToFileW(HNImageFormat hImageFormat, HNImage hImage, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatSaveImageToFile(HNImageFormat hImageFormat, HNImage hImage, const NChar * szFileName);
#endif
#define NImageFormatSaveImageToFile N_FUNC_AW(NImageFormatSaveImageToFile)

NResult N_API NImageFormatSaveImageToMemory(HNImageFormat hImageFormat, HNImage hImage, void * * pBuffer, NSizeType * pBufferLength);

NResult N_API NImageFormatSaveImageToStream(HNImageFormat hImageFormat, HNImage hImage, HNStream hStream);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatSaveImagesToFileA(HNImageFormat hImageFormat, NInt imageCount, HNImage * arHImages, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatSaveImagesToFileW(HNImageFormat hImageFormat, NInt imageCount, HNImage * arHImages, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatSaveImagesToFile(HNImageFormat hImageFormat, NInt imageCount, HNImage * arHImages, const NAChar * szFileName);
#endif
#define NImageFormatSaveImagesToFile N_FUNC_AW(NImageFormatSaveImagesToFile)

NResult N_API NImageFormatSaveImagesToMemory(HNImageFormat hImageFormat, NInt imageCount, HNImage * arHImages, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API NImageFormatSaveImagesToStream(HNImageFormat hImageFormat, NInt imageCount, HNImage * arHImages, HNStream hStream);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatGetNameA(HNImageFormat hImageFormat, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatGetNameW(HNImageFormat hImageFormat, NWChar * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatGetName(HNImageFormat hImageFormat, NChar * pValue);
#endif
#define NImageFormatGetName N_FUNC_AW(NImageFormatGetName)

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatGetDefaultFileExtensionA(HNImageFormat hImageFormat, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatGetDefaultFileExtensionW(HNImageFormat hImageFormat, NWChar * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatGetDefaultFileExtension(HNImageFormat hImageFormat, NChar * pValue);
#endif
#define NImageFormatGetDefaultFileExtension N_FUNC_AW(NImageFormatGetDefaultFileExtension)

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageFormatGetFileFilterA(HNImageFormat hImageFormat, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageFormatGetFileFilterW(HNImageFormat hImageFormat, NWChar * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageFormatGetFileFilter(HNImageFormat hImageFormat, NChar * pValue);
#endif
#define NImageFormatGetFileFilter N_FUNC_AW(NImageFormatGetFileFilter)

NResult N_API NImageFormatCanRead(HNImageFormat hImageFormat, NBool * pValue);
NResult N_API NImageFormatCanWrite(HNImageFormat hImageFormat, NBool * pValue);
NResult N_API NImageFormatCanWriteMultiple(HNImageFormat hImageFormat, NBool * pValue);

#ifdef N_CPP
}
#endif

#endif // !N_IMAGE_FORMAT_H_INCLUDED
