#ifndef N_EXTRACTORS_H_INCLUDED
#define N_EXTRACTORS_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API NExtractorsGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NExtractorsGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NExtractorsGetInfo(NLibraryInfo * pValue);
#endif
#define NExtractorsGetInfo N_FUNC_AW(NExtractorsGetInfo)

#ifdef N_CPP
}
#endif

#endif // !N_EXTRACTORS_H_INCLUDED
