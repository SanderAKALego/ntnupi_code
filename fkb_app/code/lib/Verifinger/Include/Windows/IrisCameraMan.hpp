#ifndef IRIS_CAMERA_MAN_HPP_INCLUDED
#define IRIS_CAMERA_MAN_HPP_INCLUDED

#include <IrisCamera.hpp>
#include <NDeviceManager.hpp>
namespace Neurotec { namespace DeviceManager
{
#include <IrisCameraMan.h>
}}

namespace Neurotec { namespace DeviceManager
{

class N_CLASS(IrisCameraMan)
{
private:
	static N_CLASS(IrisCamera) * FromHandle(HNObject handle)
	{
		return new N_CLASS(IrisCamera)(handle);
	}

public:
	class IrisCameraCollection : public ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(IrisCamera)>
	{
	private:
		IrisCameraCollection()
			: ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(IrisCamera)>(IrisCameraManGetCameraCount, IrisCameraManGetCamera, FromHandle, true)
		{
		}

	public:
		N_CLASS(IrisCamera) * Get(NInt index) const // repeat it here because the following memebers hide it
		{
			return ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(IrisCamera)>::Get(index);
		}

		N_CLASS(IrisCamera) * Get(HNObject handle) const // repeat it here because the following memebers hide it
		{
			return ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(IrisCamera)>::Get(handle);
		}

		N_CLASS(IrisCamera) * Get(const NChar * szId) const
		{
			HIrisCamera handle;
			NCheck(IrisCameraManGetCameraById(szId, &handle));
			NInt count = GetCount();
			for (NInt i = 0; i < count; i++)
			{
				if (items[i]->GetHandle() == handle)
				{
					return items[i];
				}
			}
			NThrowException(N_T("Unknown IrisCamera handle"));
		}

		N_CLASS(IrisCamera) * Get(const NString & id) const
		{
			return Get(NGetConstStringBuffer(id));
		}

		N_CLASS(IrisCamera) * operator[](NInt index) const // repeat it here because the following memebers hide it
		{
			return Get(index);
		}

		N_CLASS(IrisCamera) * operator[](HNObject handle) const // repeat it here because the following memebers hide it
		{
			return Get(handle);
		}

		N_CLASS(IrisCamera) * operator[](const NChar * szId) const
		{
			return Get(szId);
		}

		N_CLASS(IrisCamera) * operator[](const NString & id) const
		{
			return Get(id);
		}

		friend class N_CLASS(IrisCameraMan);
	};

private:
	static NInt initCount;
	static std::auto_ptr<IrisCameraCollection> cameras;

	N_CLASS(IrisCameraMan)()
	{
	}

	N_CLASS(IrisCameraMan)(const N_CLASS(IrisCameraMan) &)
	{
	}

public:
	static void Initialize()
	{
		NCheck(IrisCameraManInitialize());
		try
		{
			if(initCount++ == 0)
			{
				cameras = std::auto_ptr<IrisCameraCollection>(new IrisCameraCollection());
			}
		}
		catch(...)
		{
			Uninitialize();
			throw;
		}
	}

	static void Uninitialize()
	{
		if(initCount != 0)
		{
			if(--initCount == 0)
			{
				cameras.reset();
			}
		}
		IrisCameraManUninitialize();
	}

	static IrisCameraCollection * GetCameras()
	{
		return cameras.get();
	}
};

}}

#endif // !IRIS_CAMERA_MAN_HPP_INCLUDED
