#ifndef WSQ_H_INCLUDED
#define WSQ_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif


#ifndef N_NO_ANSI_FUNC
NResult N_API WsqLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API WsqLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API WsqLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define WsqLoadImageFromFile N_FUNC_AW(WsqLoadImageFromFile)

NResult N_API WsqLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API WsqLoadImageFromStream(HNStream hStream, HNImage * pHImage);


#define WSQ_DEFAULT_BIT_RATE 0.75f

#ifndef N_NO_ANSI_FUNC
NResult N_API WsqSaveImageToFileA(HNImage hImage, NFloat bitRate, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API WsqSaveImageToFileW(HNImage hImage, NFloat bitRate, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API WsqSaveImageToFile(HNImage hImage, NFloat bitRate, const NChar * szFileName);
#endif
#define WsqSaveImageToFile N_FUNC_AW(WsqSaveImageToFile)

NResult N_API WsqSaveImageToMemory(HNImage hImage, NFloat bitRate, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API WsqSaveImageToStream(HNImage hImage, NFloat bitRate, HNStream hStream);

#ifdef N_CPP
}
#endif

#endif // !WSQ_H_INCLUDED
