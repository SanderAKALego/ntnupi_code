#ifndef BMP_HPP_INCLUDED
#define BMP_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <Bmp.h>
}}
#if defined(N_FRAMEWORK_MFC)
	#include <afxstr.h>
	#include <atlimage.h>
#elif defined(N_FRAMEWORK_WX)
	#include <wx/image.h>
#endif

namespace Neurotec { namespace Images
{

class N_CLASS(Bmp)
{
private:
	N_CLASS(Bmp)() {}
	N_CLASS(Bmp)(const N_CLASS(Bmp)&) {}

public:
	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(BmpLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(BmpLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
	static N_CLASS(NImage)* LoadImageFromHBitmap(HBITMAP hbmp)
	{
		HNImage handle;
		NCheck(BmpLoadImageFromHBitmap(hbmp, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}
#endif

#if defined(N_FRAMEWORK_MFC)
	static N_CLASS(NImage) * LoadImageFromBitmap(CImage * pImage)
	{
		return LoadImageFromHBitmap((HBITMAP)*pImage);
	}
#elif defined(N_FRAMEWORK_WX)
	static N_CLASS(NImage) * LoadImageFromBitmap(const wxImage & image, float horzResolution, float vertResolution)
	{
		HNImage handle;
		NCheck(NImageCreateFromData(npfRgb, image.GetWidth(), image.GetHeight(), 0,
			horzResolution, vertResolution, image.GetWidth() * 3, image.GetData(), &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}
#endif

	static void SaveImage(N_CLASS(NImage) * pImage, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(BmpSaveImageToFile(pImage->GetHandle(), szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NString & fileName)
	{
		SaveImage(pImage, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(BmpSaveImageToMemory(pImage->GetHandle(), pBuffer, pBufferLength));
	}

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
	static HBITMAP SaveImageToHBitmap(N_CLASS(NImage)* pImage)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		HBITMAP hHandle;
		NCheck(BmpSaveImageToHBitmap(pImage->GetHandle(), (NHandle *)&hHandle));
		return hHandle;
	}
#endif

#if defined(N_FRAMEWORK_MFC)
	static CImage * SaveImageToBitmap(N_CLASS(NImage) * pImage)
	{
		std::auto_ptr<CImage> image = std::auto_ptr<CImage>(new CImage());
		image.get()->Attach(SaveImageToHBitmap(pImage));
		return image.release();
	}
#elif defined(N_FRAMEWORK_WX)
	static wxImage SaveImageToBitmap(N_CLASS(NImage) * pImage)
	{
		NUInt width = pImage->GetWidth();
		NUInt height = pImage->GetHeight();
		std::auto_ptr<wxNImage> pRgbImage(N_CLASS(NImage)::FromImage(npfRgb, width * 3, pImage));
		NByte *pixels = (NByte*)pRgbImage->GetPixels();
		return wxImage(width, height, pixels, true).Copy();
	}
#endif
};

}}

#endif // !BMP_HPP_INCLUDED
