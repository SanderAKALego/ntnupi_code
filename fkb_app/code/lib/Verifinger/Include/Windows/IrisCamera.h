#ifndef IRIS_CAMERA_H_INCLUDED
#define IRIS_CAMERA_H_INCLUDED

#include <NGrayscaleImage.h>
#include <IrisCameraParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(IrisCamera);

typedef void (N_CALLBACK IrisCameraIrisesAcquiredCallback)(HIrisCamera hIrisCamera, HNGrayscaleImage hLeftIris, HNGrayscaleImage hRightIris, void * pParam);

#ifndef N_NO_ANSI_FUNC
NResult N_API IrisCameraGetIdA(HIrisCamera hIrisCamera, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API IrisCameraGetIdW(HIrisCamera hIrisCamera, NWChar * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API IrisCameraGetId(HIrisCamera hIrisCamera, NChar * pValue);
#endif
#define IrisCameraGetId N_FUNC_AW(IrisCameraGetId)

NResult N_API IrisCameraIsCapturing(HIrisCamera hIrisCamera, NBool * pValue);
NResult N_API IrisCameraStartCapturing(HIrisCamera hIrisCamera);
NResult N_API IrisCameraStopCapturing(HIrisCamera hIrisCamera);
NResult N_API IrisCameraGetImages(HIrisCamera hIrisCamera, HNGrayscaleImage * pHLeftIris, HNGrayscaleImage * pHRightIris);
NResult N_API IrisCameraSetIrisesAcquiredCallback(HIrisCamera hIrisCamera, IrisCameraIrisesAcquiredCallback pCallback, void * pParam);

N_DEPRECATED("type is deprecated, use IrisCameraIrisesAcquiredCallback") typedef IrisCameraIrisesAcquiredCallback IrisCameraCallback;
#define IrisCameraSetCallback IrisCameraSetIrisesAcquiredCallback
#define IrisCameraCopyParameters(hDstIrisCamera, hSrcIrisCamera) NObjectCopyParameters(hDstIrisCamera, hSrcIrisCamera)
#define IrisCameraGetParameterA(hIrisCamera, parameterId, pValue) NTypeGetParameterA(IrisCameraTypeOf(), hIrisCamera, 0, parameterId, pValue)
#define IrisCameraGetParameterW(hIrisCamera, parameterId, pValue) NTypeGetParameterW(IrisCameraTypeOf(), hIrisCamera, 0, parameterId, pValue)
#define IrisCameraGetParameter(hIrisCamera, parameterId, pValue) NTypeGetParameter(IrisCameraTypeOf(), hIrisCamera, 0, parameterId, pValue)
#define IrisCameraSetParameterA(hIrisCamera, parameterId, pValue) NTypeSetParameterA(IrisCameraTypeOf(), hIrisCamera, 0, parameterId, pValue)
#define IrisCameraSetParameterW(hIrisCamera, parameterId, pValue) NTypeSetParameterW(IrisCameraTypeOf(), hIrisCamera, 0, parameterId, pValue)
#define IrisCameraSetParameter(hIrisCamera, parameterId, pValue) NTypeSetParameter(IrisCameraTypeOf(), hIrisCamera, 0, parameterId, pValue)

#ifdef N_MSVC
	#pragma deprecated("IrisCameraSetCallback", "IrisCameraCopyParameters", "IrisCameraGetParameterA", "IrisCameraGetParameterW", "IrisCameraGetParameter", "IrisCameraSetParameterA", "IrisCameraSetParameterW", "IrisCameraSetParameter")
#endif

#ifdef N_CPP
}
#endif

#endif // !IRIS_CAMERA_H_INCLUDED
