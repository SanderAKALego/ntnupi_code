#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <NImage.h>
#include <CameraParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(Camera);

#ifndef N_NO_ANSI_FUNC
NResult N_API CameraGetIdA(HCamera hCamera, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API CameraGetIdW(HCamera hCamera, NWChar * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API CameraGetId(HCamera hCamera, NChar * pValue);
#endif
#define CameraGetId N_FUNC_AW(CameraGetId)

typedef struct CameraVideoFormat_
{
	NInt FrameWidth;
	NInt FrameHeight;
	NFloat FrameRate;
} CameraVideoFormat;

NResult N_API CameraGetVideoFormats(HCamera hCamera, CameraVideoFormat * arVideoFormats);
NResult N_API CameraGetVideoFormat(HCamera hCamera, CameraVideoFormat * pVideoFormat);
NResult N_API CameraSetVideoFormat(HCamera hCamera, const CameraVideoFormat * pVideoFormat);

NResult N_API CameraIsCapturing(HCamera hCamera, NBool * pValue);
NResult N_API CameraStartCapturing(HCamera hCamera);
NResult N_API CameraStopCapturing(HCamera hCamera);
NResult N_API CameraGetCurrentFrame(HCamera hCamera, HNImage * pHImage);

#define CameraCopyParameters(hDstCamera, hSrcCamera) NObjectCopyParameters(hDstCamera, hSrcCamera)
#define CameraGetParameterA(hCamera, parameterId, pValue) NTypeGetParameterA(CameraTypeOf(), hCamera, 0, parameterId, pValue)
#define CameraGetParameterW(hCamera, parameterId, pValue) NTypeGetParameterW(CameraTypeOf(), hCamera, 0, parameterId, pValue)
#define CameraGetParameter(hCamera, parameterId, pValue) NTypeGetParameter(CameraTypeOf(), hCamera, 0, parameterId, pValue)
#define CameraSetParameterA(hCamera, parameterId, pValue) NTypeSetParameterA(CameraTypeOf(), hCamera, 0, parameterId, pValue)
#define CameraSetParameterW(hCamera, parameterId, pValue) NTypeSetParameterW(CameraTypeOf(), hCamera, 0, parameterId, pValue)
#define CameraSetParameter(hCamera, parameterId, pValue) NTypeSetParameter(CameraTypeOf(), hCamera, 0, parameterId, pValue)

#ifdef N_MSVC
	#pragma deprecated("CameraCopyParameters", "CameraGetParameterA", "CameraGetParameterW", "CameraGetParameter", "CameraSetParameterA", "CameraSetParameterW", "CameraSetParameter")
#endif

#ifdef N_CPP
}
#endif

#endif // !CAMERA_H_INCLUDED
