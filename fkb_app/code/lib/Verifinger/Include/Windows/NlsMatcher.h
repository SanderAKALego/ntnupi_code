#ifndef NLS_MATCHER_H_INCLUDED
#define NLS_MATCHER_H_INCLUDED

#include <NLMatcher.h>
#include <NlsmMatchDetails.h>
#include <NlsMatcherParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NlsMatcher)

NResult N_API NlsmCreate(HNlsMatcher * pHMatcher);

NResult N_API NlsmVerify(HNlsMatcher hMatcher, const void * template1, NSizeType template1Size,
	const void * template2, NSizeType template2Size, NlsmMatchDetails * * ppMatchDetails, NDouble * pScore);
NResult N_API NlsmIdentifyStart(HNlsMatcher hMatcher, const void * templ, NSizeType templSize,
	NlsmMatchDetails * * ppMatchDetails);
NResult N_API NlsmIdentifyNext(HNlsMatcher hMatcher, const void * templ, NSizeType templSize,
	NlsmMatchDetails * pMatchDetails, NDouble * pScore);
NResult N_API NlsmIdentifyEnd(HNlsMatcher hMatcher);

#define NlsmFree(hMatcher) NObjectFree(hMatcher)
#define NlsmCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NlsmGetParameterA(hMatcher, partId, parameterId, pValue) NTypeGetParameterA(NlsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NlsmGetParameterW(hMatcher, partId, parameterId, pValue) NTypeGetParameterW(NlsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NlsmGetParameter(hMatcher, partId, parameterId, pValue) NTypeGetParameter(NlsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NlsmSetParameterA(hMatcher, partId, parameterId, pValue) NTypeSetParameterA(NlsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NlsmSetParameterW(hMatcher, partId, parameterId, pValue) NTypeSetParameterW(NlsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NlsmSetParameter(hMatcher, partId, parameterId, pValue) NTypeSetParameter(NlsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NlsmReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NlsmFree", "NlsmCopyParameters", "NlsmGetParameterA", "NlsmGetParameterW", "NlsmGetParameter", "NlsmSetParameterA", "NlsmSetParameterW", "NlsmSetParameter", "NlsmReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NLS_MATCHER_H_INCLUDED
