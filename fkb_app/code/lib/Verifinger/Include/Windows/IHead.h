#ifndef IHEAD_H_INCLUDED
#define IHEAD_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API IHeadLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API IHeadLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API IHeadLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define IHeadLoadImageFromFile N_FUNC_AW(IHeadLoadImageFromFile)

NResult N_API IHeadLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API IHeadLoadImageFromStream(HNStream hStream, HNImage * pHImage);

#ifndef N_NO_ANSI_FUNC
NResult N_API IHeadSaveImageToFileA(HNImage hImage, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API IHeadSaveImageToFileW(HNImage hImage, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API IHeadSaveImageToFile(HNImage hImage, const NChar * szFileName);
#endif
#define IHeadSaveImageToFile N_FUNC_AW(IHeadSaveImageToFile)

NResult N_API IHeadSaveImageToMemory(HNImage hImage, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API IHeadSaveImageToStream(HNImage hImage, HNStream hStream);

#ifdef N_CPP
}
#endif

#endif // !IHEAD_H_INCLUDED
