#ifndef FP_SCANNER_HPP_INCLUDED
#define FP_SCANNER_HPP_INCLUDED

#include <NGrayscaleImage.hpp>
namespace Neurotec { namespace DeviceManager
{
using Neurotec::Images::HNGrayscaleImage;
#include <FPScanner.h>
}}

namespace Neurotec { namespace DeviceManager
{

class N_CLASS(FPScannerMan);
class N_CLASS(FPScanner);

class N_CLASS(FPScanner) : public N_CLASS(NObject)
{
private:
	NEvent1<N_CLASS(FPScanner) *> isCapturingChangedEvent;
	NEvent1<N_CLASS(FPScanner) *> fingerPlacedEvent;
	NEvent1<N_CLASS(FPScanner) *> fingerRemovedEvent;
	NEvent2<N_CLASS(FPScanner) *, Neurotec::Images::N_CLASS(NGrayscaleImage) *> imageScannedEvent;

	static void N_API OnIsCapturingChanged(HFPScanner, void * pParam)
	{
		N_CLASS(FPScanner) * pScanner = (N_CLASS(FPScanner) *)pParam;
		pScanner->isCapturingChangedEvent(pScanner);
	}

	static void N_API OnFingerPlaced(HFPScanner, void * pParam)
	{
		N_CLASS(FPScanner) * pScanner = (N_CLASS(FPScanner) *)pParam;
		pScanner->fingerPlacedEvent(pScanner);
	}

	static void N_API OnFingerRemoved(HFPScanner, void * pParam)
	{
		N_CLASS(FPScanner) * pScanner = (N_CLASS(FPScanner) *)pParam;
		pScanner->fingerRemovedEvent(pScanner);
	}

	static void N_API OnImageScanned(HFPScanner, HNGrayscaleImage hImage, void * pParam)
	{
		N_CLASS(FPScanner) * pScanner = (N_CLASS(FPScanner) *)pParam;
		if(pScanner->imageScannedEvent)
		{
			std::auto_ptr<Neurotec::Images::N_CLASS(NGrayscaleImage)> image((Neurotec::Images::N_CLASS(NGrayscaleImage) *)Neurotec::Images::N_CLASS(NImage)::FromHandle(hImage, false));
			pScanner->imageScannedEvent(pScanner, image.get());
		}
	}

	N_CLASS(FPScanner)(HFPScanner handle)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(FPScanner), false)
	{
		FPScannerSetIsCapturingChangedCallback(GetHandle(), OnIsCapturingChanged, this);
		FPScannerSetFingerPlacedCallback(GetHandle(), OnFingerPlaced, this);
		FPScannerSetFingerRemovedCallback(GetHandle(), OnFingerRemoved, this);
		FPScannerSetImageScannedCallback(GetHandle(), OnImageScanned, this);
	}

public:
	typedef void (* Callback)(N_CLASS(FPScanner) * pScanner, void * pParam);
	typedef void (* ImageScannedCallback)(N_CLASS(FPScanner) * pScanner, Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, void * pParam);

	virtual ~N_CLASS(FPScanner)()
	{
		FPScannerSetIsCapturingChangedCallback(GetHandle(), NULL, NULL);
		FPScannerSetFingerPlacedCallback(GetHandle(), NULL, NULL);
		FPScannerSetFingerRemovedCallback(GetHandle(), NULL, NULL);
		FPScannerSetImageScannedCallback(GetHandle(), NULL, NULL);
	}

	NInt GetId(NChar * pValue)
	{
		return NCheck(FPScannerGetId(GetHandle(), pValue));
	}

	NString GetId()
	{
		NString value;
		return NGetString(value, FPScannerGetId, this);
	}

	bool IsCapturing()
	{
		NBool value;
		NCheck(FPScannerIsCapturing(GetHandle(), &value));
		return value != 0;
	}

	void StartCapturing()
	{
		NCheck(FPScannerStartCapturing(GetHandle()));
	}

	void StartCapturingForOneImage()
	{
		NCheck(FPScannerStartCapturingForOneImage(GetHandle()));
	}

	void StopCapturing()
	{
		NCheck(FPScannerStopCapturing(GetHandle()));
	}

	void AddIsCapturingChangedCallback(Callback pCallback, void * pParam)
	{
		isCapturingChangedEvent.Add(pCallback, pParam);
	}

	void RemoveIsCapturingChangedCallback(Callback pCallback, void * pParam)
	{
		isCapturingChangedEvent.Remove(pCallback, pParam);
	}

	void AddFingerPlacedCallback(Callback pCallback, void * pParam)
	{
		fingerPlacedEvent.Add(pCallback, pParam);
	}

	void RemoveFingerPlacedCallback(Callback pCallback, void * pParam)
	{
		fingerPlacedEvent.Remove(pCallback, pParam);
	}

	void AddFingerRemovedCallback(Callback pCallback, void * pParam)
	{
		fingerRemovedEvent.Add(pCallback, pParam);
	}

	void RemoveFingerRemovedCallback(Callback pCallback, void * pParam)
	{
		fingerRemovedEvent.Remove(pCallback, pParam);
	}

	void AddImageScannedCallback(ImageScannedCallback pCallback, void * pParam)
	{
		imageScannedEvent.Add(pCallback, pParam);
	}

	void RemoveImageScannedCallback(ImageScannedCallback pCallback, void * pParam)
	{
		imageScannedEvent.Remove(pCallback, pParam);
	}

	N_DECLARE_OBJECT_TYPE(FPScanner)

	friend class N_CLASS(FPScannerMan);
};

}}

#endif // !FP_SCANNER_HPP_INCLUDED
