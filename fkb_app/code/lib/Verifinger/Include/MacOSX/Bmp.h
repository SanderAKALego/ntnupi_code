#ifndef BMP_H_INCLUDED
#define BMP_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API BmpLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API BmpLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API BmpLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define BmpLoadImageFromFile N_FUNC_AW(BmpLoadImageFromFile)

NResult N_API BmpLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API BmpLoadImageFromStream(HNStream hStream, HNImage * pHImage);

#ifdef N_DOCUMENTATION
NResult N_API BmpLoadImageFromHBitmap(NHandle handle, HNImage * pHImage);
#endif

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
NResult N_API BmpLoadImageFromHBitmap(NHandle handle, HNImage * pHImage);
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API BmpSaveImageToFileA(HNImage hImage, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API BmpSaveImageToFileW(HNImage hImage, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API BmpSaveImageToFile(HNImage hImage, const NChar * szFileName);
#endif
#define BmpSaveImageToFile N_FUNC_AW(BmpSaveImageToFile)

NResult N_API BmpSaveImageToMemory(HNImage hImage, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API BmpSaveImageToStream(HNImage hImage, HNStream hStream);

#ifdef N_DOCUMENTATION
NResult N_API BmpSaveImageToHBitmap(HNImage hImage, NHandle * pHandle);
#endif

#if defined(N_WINDOWS) || defined(N_DOCUMENTATION)
NResult N_API BmpSaveImageToHBitmap(HNImage hImage, NHandle * pHandle);
#endif

#ifdef N_CPP
}
#endif

#endif // !BMP_H_INCLUDED
