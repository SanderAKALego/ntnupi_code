#ifndef NL_EXTRACTOR_H_INCLUDED
#define NL_EXTRACTOR_H_INCLUDED

#include <NGrayscaleImage.h>
#include <NGeometry.h>
#include <NLTemplate.h>
#include <NExtractors.h>
#include <NLExtractorParams.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef struct NleRotation_
{
	NShort Yaw;
	NShort Pitch;
	NShort Roll;
} NleRotation;

typedef struct NleFace_
{
	NRect Rectangle;
	NleRotation Rotation;
	NDouble Confidence;
} NleFace;

typedef struct NleEyes_
{
	NPoint First;
	NDouble FirstConfidence;
	NPoint Second;
	NDouble SecondConfidence;
} NleEyes;

typedef struct NleDetectionDetails_
{
	NBool FaceAvailable;
	NleFace Face;
	NBool EyesAvailable;
	NleEyes Eyes;
} NleDetectionDetails;

typedef enum NleExtractionStatus_
{
	nleesNone = 0,
	nleesTemplateCreated = 1,
	nleesFaceNotDetected = 2,
	nleesEyesNotDetected = 3,
	nleesFaceTooCloseToImageBorder = 4,
	nleesQualityCheckGrayscaleDensityFailed = 100,
	nleesQualityCheckExposureFailed = 101,
	nleesQualityCheckSharpnessFailed = 102,
	nleesLivenessCheckFailed = 200,
	nleesGeneralizationFailed = 300,
} NleExtractionStatus;

DECLARE_N_OBJECT_HANDLE(NLExtractor);

NResult N_API NleCompressEx(HNLTemplate hSrcTemplate, NleTemplateSize dstTemplateSize,
	HNLTemplate *pHDstTemplate);

NResult N_API NleCreate(HNLExtractor *pHExtractor);

NResult N_API NleDetectFace(HNLExtractor hExtractor,
	HNGrayscaleImage hImage,
	NBool * pDetected, NleFace * pFace);
NResult N_API NleDetectFaces(HNLExtractor hExtractor,
	HNGrayscaleImage hImage,
	NInt * pFaceCount, NleFace * * pArFaces);

NResult N_API NleDetectFacialFeatures(HNLExtractor hExtractor,
	HNGrayscaleImage hImage, const NleFace * pFace, NleDetectionDetails * pDetectionDetails);

NResult N_API NleExtract(HNLExtractor hExtractor,
	HNGrayscaleImage hImage,
	NleDetectionDetails * pDetectionDetails,
	NleExtractionStatus * pStatus, HNLTemplate * pHTemplate);

NResult N_API NleExtractStart(HNLExtractor hExtractor,
	NInt durationInFrames);

N_DEPRECATED("function is deprecated, use NleExtractNextEx instead")
NResult N_API NleExtractNext(HNLExtractor hExtractor,
	HNGrayscaleImage hImage, NleDetectionDetails * pDetectionDetails,
	HNLTemplate * pHTemplate, NleExtractionStatus * pStatus);

NResult N_API NleExtractNextEx(HNLExtractor hExtractor, HNGrayscaleImage hImage, NleDetectionDetails * pDetectionDetails,
	NInt * pBaseFrameIndex, HNLTemplate * pHTemplate, NleExtractionStatus * pStatus);

NResult N_API NleExtractUsingDetails(HNLExtractor hExtractor,
	HNGrayscaleImage hImage, NleDetectionDetails * pDetectionDetails,
	NleExtractionStatus * pStatus, HNLTemplate * pHTemplate);

N_DEPRECATED("function is deprecated, use NleCompressEx instead")
NResult N_API NleCompress(HNLExtractor hExtractor,
	HNLTemplate hTemplate,
	HNLTemplate * pHCompressedTemplate);

N_DEPRECATED("function is deprecated, use NleGeneralizeEx instead")
NResult N_API NleGeneralize(HNLExtractor hExtractor,
	NInt templateCount, const HNLTemplate * arHTemplates,
	HNLTemplate * pHTemplate);

NResult N_API NleGeneralizeEx(HNLExtractor hExtractor, NInt templateCount, const HNLTemplate * arHTemplates,
	NleExtractionStatus * pStatus, NInt * pBaseTemplateIndex, HNLTemplate * pHTemplate);

#define NleFree(hExtractor) NObjectFree(hExtractor)
#define NleCopyParameters(hDstExtractor, hSrcExtractor) NObjectCopyParameters(hDstExtractor, hSrcExtractor)
#define NleGetParameterA(hExtractor, parameterId, pValue) NTypeGetParameterA(NLExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NleGetParameterW(hExtractor, parameterId, pValue) NTypeGetParameterW(NLExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NleGetParameter(hExtractor, parameterId, pValue) NTypeGetParameter(NLExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NleSetParameterA(hExtractor, parameterId, pValue) NTypeSetParameterA(NLExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NleSetParameterW(hExtractor, parameterId, pValue) NTypeSetParameterW(NLExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NleSetParameter(hExtractor, parameterId, pValue) NTypeSetParameter(NLExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NleReset(hExtractor) NObjectReset(hExtractor)

#ifdef N_MSVC
	#pragma deprecated("NleFree", "NleCopyParameters", "NleGetParameterA", "NleGetParameterW", "NleGetParameter", "NleSetParameterA", "NleSetParameterW", "NleSetParameter", "NleReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NL_EXTRACTOR_H_INCLUDED
