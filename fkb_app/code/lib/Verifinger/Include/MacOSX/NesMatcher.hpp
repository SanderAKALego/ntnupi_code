#ifndef NES_MATCHER_HPP_INCLUDED
#define NES_MATCHER_HPP_INCLUDED

#include <NEMatcher.hpp>
#include <NesmMatchDetails.hpp>
#include <NesMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NesMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NesMatcher) : public N_CLASS(NObject)
{
private:
	static HNesMatcher Create()
	{
		HNesMatcher handle;
		NCheck(NesmCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NesMatcher) * FromHandle(HNesMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NesMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NesMatcher)(HNesMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NesMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NesMatcher)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NesMatcher), true)
	{
	}

	NInt Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size,
		NesmMatchDetails * * ppMatchDetails = NULL)
	{
		NInt score;
		NCheck(NesmVerify((HNesMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			ppMatchDetails, &score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize,
		NesmMatchDetails * * ppMatchDetails = NULL)
	{
		NCheck(NesmIdentifyStart((HNesMatcher)GetHandle(), pTemplate, templateSize, ppMatchDetails));
	}

	NInt IdentifyNext(const void * pTemplate, NSizeType templateSize, NesmMatchDetails * pMatchDetails)
	{
		NInt score;
		NCheck(NesmIdentifyNext((HNesMatcher)GetHandle(), pTemplate, templateSize, pMatchDetails, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NesmIdentifyEnd((HNesMatcher)GetHandle()));
	}

	NInt GetMatchingThreshold()
	{
		return GetParameterAsInt32(NESM_PART_NONE, NESMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NInt value)
	{
		SetParameter(NESM_PART_NONE, NESMP_MATCHING_THRESHOLD, value);
	}

	NemSpeed GetMatchingSpeed()
	{
		return (NemSpeed)GetParameterAsInt32(NESM_PART_NEM, NEMP_MATCHING_SPEED);
	}

	void SetMatchingSpeed(NemSpeed value)
	{
		SetParameter(NESM_PART_NEM, NEMP_MATCHING_SPEED, value);
	}



	N_DECLARE_OBJECT_TYPE(NesMatcher)
};

}}

#endif // !NES_MATCHER_HPP_INCLUDED
