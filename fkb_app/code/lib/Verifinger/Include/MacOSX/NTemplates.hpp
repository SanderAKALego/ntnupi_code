#ifndef N_TEMPLATES_HPP_INCLUDED
#define N_TEMPLATES_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NTemplates.h>
}}

namespace Neurotec { namespace Biometrics
{

class NTemplates
{
private:
	NTemplates()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NTemplatesGetInfo(pValue));
	}
};

}}

#endif // !N_TEMPLATES_HPP_INCLUDED
