#ifndef NF_RECORD_HPP_INCLUDED
#define NF_RECORD_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NFRecord.h>
#include <NFRecordV1.h>
}}

namespace Neurotec { namespace Biometrics
{
#undef NFR_RESOLUTION
#undef NFR_MAX_FINGER_DIMENSION
#undef NFR_MAX_FINGER_MINUTIA_COUNT
#undef NFR_MAX_FINGER_CORE_COUNT
#undef NFR_MAX_FINGER_DELTA_COUNT
#undef NFR_MAX_FINGER_DOUBLE_CORE_COUNT
#undef NFR_MAX_PALM_DIMENSION
#undef NFR_MAX_PALM_MINUTIA_COUNT
#undef NFR_MAX_PALM_CORE_COUNT
#undef NFR_MAX_PALM_DELTA_COUNT
#undef NFR_MAX_PALM_DOUBLE_CORE_COUNT
#undef NFR_MAX_DIMENSION
#undef NFR_MAX_MINUTIA_COUNT
#undef NFR_MAX_CORE_COUNT
#undef NFR_MAX_DELTA_COUNT
#undef NFR_MAX_DOUBLE_CORE_COUNT

#undef NFR_SKIP_RIDGE_COUNTS
#undef NFR_SKIP_SINGULAR_POINTS
#undef NFR_SKIP_BLOCKED_ORIENTS
#undef NFR_SAVE_BLOCKED_ORIENTS
#undef NFR_ALLOW_OUT_OF_BOUNDS_FEATURES
#undef NFR_SKIP_QUALITIES
#undef NFR_SKIP_CURVATURES
#undef NFR_SKIP_GS
#undef NFR_SAVE_V2
#undef NFR_SAVE_V3

const NUShort NFR_RESOLUTION = 500;
const NUShort NFR_MAX_FINGER_DIMENSION = 2047;
const NInt NFR_MAX_FINGER_MINUTIA_COUNT = 255;
const NInt NFR_MAX_FINGER_CORE_COUNT = 15;
const NInt NFR_MAX_FINGER_DELTA_COUNT = 15;
const NInt NFR_MAX_FINGER_DOUBLE_CORE_COUNT = 15;
const NUShort NFR_MAX_PALM_DIMENSION = 16383;
const NInt NFR_MAX_PALM_MINUTIA_COUNT = 65535;
const NInt NFR_MAX_PALM_CORE_COUNT = 255;
const NInt NFR_MAX_PALM_DELTA_COUNT = 255;
const NInt NFR_MAX_PALM_DOUBLE_CORE_COUNT = 255;
const NUInt NFR_SKIP_RIDGE_COUNTS = 0x00010000;
const NUInt NFR_SKIP_SINGULAR_POINTS = 0x00020000;
const NUInt NFR_SKIP_BLOCKED_ORIENTS = 0x00040000;
const NUInt NFR_SAVE_BLOCKED_ORIENTS = 0x00040000;
const NUInt NFR_ALLOW_OUT_OF_BOUNDS_FEATURES = 0x00080000;
const NUInt NFR_SKIP_QUALITIES = 0x00100000;
const NUInt NFR_SKIP_CURVATURES = 0x00200000;
const NUInt NFR_SKIP_GS = 0x00400000;
const NUInt NFR_SAVE_V2 = 0x20000000;
const NUInt NFR_SAVE_V3 = 0x30000000;

class N_CLASS(NFRecord) : public N_CLASS(NObject)
{
public:
	class MinutiaCollection : public ::Neurotec::Collections::N_CLASS(NStructCollection)<NFMinutia>
	{
		MinutiaCollection(N_CLASS(NFRecord) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NStructCollection)<NFMinutia>(pOwner, NFRecordGetMinutiaCount, NFRecordGetMinutiaCapacity, NFRecordSetMinutiaCapacity,
				NFRecordGetMinutia, NFRecordGetMinutiae, NFRecordSetMinutia, NFRecordAddMinutia, NFRecordInsertMinutia, NFRecordRemoveMinutia, NULL, NFRecordClearMinutiae)
		{
		}

		friend class N_CLASS(NFRecord);
	};

	class MinutiaNeighborsCollection : public ::Neurotec::Collections::N_CLASS(NStructArrayCollection)<NFMinutiaNeighbor, NFMinutia>
	{
		MinutiaNeighborsCollection(N_CLASS(NFRecord) * pOwner, MinutiaCollection * pMinutiaCollection)
			: ::Neurotec::Collections::N_CLASS(NStructArrayCollection)<NFMinutiaNeighbor, NFMinutia>(pOwner, pMinutiaCollection, NFRecordGetMinutiaNeighborCount, NFRecordGetMinutiaNeighbor,
			NFRecordGetMinutiaNeighbors, NFRecordSetMinutiaNeighbor, NULL, NULL, NULL, NULL, NULL)
		{
		}

		friend class N_CLASS(NFRecord);
	};

	class CoreCollection : public ::Neurotec::Collections::N_CLASS(NStructCollection)<NFCore>
	{
		CoreCollection(N_CLASS(NFRecord) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NStructCollection)<NFCore>(pOwner, NFRecordGetCoreCount, NFRecordGetCoreCapacity, NFRecordSetCoreCapacity,
				NFRecordGetCore, NFRecordGetCores, NFRecordSetCore, NFRecordAddCore, NFRecordInsertCore, NFRecordRemoveCore, NULL, NFRecordClearCores)
		{
		}

		friend class N_CLASS(NFRecord);
	};

	class DeltaCollection : public ::Neurotec::Collections::N_CLASS(NStructCollection)<NFDelta>
	{
		DeltaCollection(N_CLASS(NFRecord) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NStructCollection)<NFDelta>(pOwner, NFRecordGetDeltaCount, NFRecordGetDeltaCapacity, NFRecordSetDeltaCapacity,
				NFRecordGetDelta, NFRecordGetDeltas, NFRecordSetDelta, NFRecordAddDelta, NFRecordInsertDelta, NFRecordRemoveDelta, NULL, NFRecordClearDeltas)
		{
		}

		friend class N_CLASS(NFRecord);
	};

	class DoubleCoreCollection : public ::Neurotec::Collections::N_CLASS(NStructCollection)<NFDoubleCore>
	{
		DoubleCoreCollection(N_CLASS(NFRecord) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NStructCollection)<NFDoubleCore>(pOwner, NFRecordGetDoubleCoreCount, NFRecordGetDoubleCoreCapacity, NFRecordSetDoubleCoreCapacity,
				NFRecordGetDoubleCore, NFRecordGetDoubleCores, NFRecordSetDoubleCore, NFRecordAddDoubleCore, NFRecordInsertDoubleCore, NFRecordRemoveDoubleCore, NULL, NFRecordClearDoubleCores)
		{
		}

		friend class N_CLASS(NFRecord);
	};

private:
	static HNFRecord Create(bool isPalm, NUShort width, NUShort height,
		NUShort horzResolution, NUShort vertResolution, NUInt flags)
	{
		HNFRecord handle;
		NCheck(NFRecordCreateEx(isPalm, width, height, horzResolution, vertResolution, flags, &handle));
		return handle;
	}

	static HNFRecord Create(const void * pBuffer, NSizeType bufferSize, NUInt flags, NFRecordInfo * pInfo)
	{
		HNFRecord handle;
		NCheck(NFRecordCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

	std::auto_ptr<MinutiaCollection> minutiae;
	std::auto_ptr<MinutiaNeighborsCollection> minutiaeNeighbors;
	std::auto_ptr<CoreCollection> cores;
	std::auto_ptr<DeltaCollection> deltas;
	std::auto_ptr<DoubleCoreCollection> doubleCores;

	void Init()
	{
		minutiae = std::auto_ptr<MinutiaCollection>(new MinutiaCollection(this));
		minutiaeNeighbors = std::auto_ptr<MinutiaNeighborsCollection>(new MinutiaNeighborsCollection(this, minutiae.get()));
		cores = std::auto_ptr<CoreCollection>(new CoreCollection(this));
		deltas = std::auto_ptr<DeltaCollection>(new DeltaCollection(this));
		doubleCores = std::auto_ptr<DoubleCoreCollection>(new DoubleCoreCollection(this));
	}

public:
	static N_CLASS(NFRecord)* FromHandle(HNFRecord handle, bool ownsHandle = true)
	{
		return new N_CLASS(NFRecord)(handle, ownsHandle);
	}

	static NUShort GetWidth(const void * pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NFRecordGetWidthMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetHeight(const void * pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NFRecordGetHeightMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetHorzResolution(const void * pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NFRecordGetHorzResolutionMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetVertResolution(const void * pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NFRecordGetVertResolutionMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NFPosition GetPosition(const void * pBuffer, NSizeType bufferSize)
	{
		NFPosition value;
		NCheck(NFRecordGetPositionMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NFImpressionType GetImpressionType(const void * pBuffer, NSizeType bufferSize)
	{
		NFImpressionType value;
		NCheck(NFRecordGetImpressionTypeMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NFPatternClass GetPatternClass(const void * pBuffer, NSizeType bufferSize)
	{
		NFPatternClass value;
		NCheck(NFRecordGetPatternClassMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NByte GetQuality(const void * pBuffer, NSizeType bufferSize)
	{
		NByte value;
		NCheck(NFRecordGetQualityMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NByte GetG(const void * pBuffer, NSizeType bufferSize)
	{
		NByte value;
		NCheck(NFRecordGetGMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetCbeffProductType(const void * pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NFRecordGetCbeffProductTypeMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NSizeType GetMaxSize(NInt version, NBool isPalm, NFMinutiaFormat minutiaFormat, NInt minutiaCount, NFRidgeCountsType ridgeCountsType,
		NInt coreCount, NInt deltaCount, NInt doubleCoreCount, NInt boWidth, NInt boHeight)
	{
		NSizeType value;
		NCheck(NFRecordGetMaxSize(version, isPalm, minutiaFormat, minutiaCount, ridgeCountsType,
			coreCount, deltaCount, doubleCoreCount, boWidth, boHeight, &value));
		return value;
	}

	static NSizeType GetMaxSizeV1(NFMinutiaFormat minutiaFormat, NInt minutiaCount,
		NInt coreCount, NInt deltaCount, NInt doubleCoreCount, NInt boWidth, NInt boHeight)
	{
		NSizeType value;
		NCheck(NFRecordGetMaxSizeV1(minutiaFormat, minutiaCount, coreCount, deltaCount, doubleCoreCount,
			boWidth, boHeight, &value));
		return value;
	}

	static void Check(const void * pBuffer, NSizeType bufferSize)
	{
		NCheck(NFRecordCheck(pBuffer, bufferSize));
	}

	explicit N_CLASS(NFRecord)(HNFRecord handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NFRecord), false)
	{
		Init();
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NFRecord)(bool isPalm, NUShort width, NUShort height,
		NUShort horzResolution, NUShort vertResolution, NUInt flags = 0)
		: N_CLASS(NObject)(Create(isPalm, width, height, horzResolution, vertResolution, flags), N_NATIVE_TYPE_OF(NFRecord), true)
	{
		Init();
	}

	N_CLASS(NFRecord)(NUShort width, NUShort height,
		NUShort horzResolution, NUShort vertResolution, NUInt flags = 0)
		: N_CLASS(NObject)(Create(false, width, height, horzResolution, vertResolution, flags), N_NATIVE_TYPE_OF(NFRecord), true)
	{
		Init();
	}

	N_CLASS(NFRecord)(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0, NFRecordInfo * pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NFRecord), true)
	{
		Init();
	}

	N_CLASS(NObject) * Clone() const
	{
		HNFRecord hClonedRecord;
		NCheck(NFRecordClone(GetHandle(), &hClonedRecord));
		try
		{
			return FromHandle(hClonedRecord);
		}
		catch(...)
		{
			NObjectFree(hClonedRecord);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NFRecordGetSize(GetHandle(), flags, &value));
		return value;
	}

	NSizeType Save(void * pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NFRecordSaveToMemory((HNFRecord)GetHandle(), pBuffer, bufferSize, flags, &value));
		return value;
	}

	void SortMinutiae(NFMinutiaOrder order)
	{
		NCheck(NFRecordSortMinutiae((HNFRecord)GetHandle(), order));
	}

	void TruncateMinutiae(NInt maxCount)
	{
		NCheck(NFRecordTruncateMinutiae((HNFRecord)GetHandle(), maxCount));
	}

	void TruncateMinutiaeByQuality(NByte threshold, NInt maxCount)
	{
		NCheck(NFRecordTruncateMinutiaeByQuality((HNFRecord)GetHandle(), threshold, maxCount));
	}

	NUShort GetWidth() const
	{
		NUShort value;
		NCheck(NFRecordGetWidth((HNFRecord)GetHandle(), &value));
		return value;
	}

	NUShort GetHeight() const
	{
		NUShort value;
		NCheck(NFRecordGetHeight((HNFRecord)GetHandle(), &value));
		return value;
	}

	NUShort GetHorzResolution() const
	{
		NUShort value;
		NCheck(NFRecordGetHorzResolution((HNFRecord)GetHandle(), &value));
		return value;
	}

	NUShort GetVertResolution() const
	{
		NUShort value;
		NCheck(NFRecordGetVertResolution((HNFRecord)GetHandle(), &value));
		return value;
	}

	NFPosition GetPosition() const
	{
		NFPosition value;
		NCheck(NFRecordGetPosition((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetPosition(NFPosition value)
	{
		NCheck(NFRecordSetPosition((HNFRecord)GetHandle(), value));
	}

	NFImpressionType GetImpressionType() const
	{
		NFImpressionType value;
		NCheck(NFRecordGetImpressionType((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetImpressionType(NFImpressionType value)
	{
		NCheck(NFRecordSetImpressionType((HNFRecord)GetHandle(), value));
	}

	NFPatternClass GetPatternClass() const
	{
		NFPatternClass value;
		NCheck(NFRecordGetPatternClass((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetPatternClass(NFPatternClass value)
	{
		NCheck(NFRecordSetPatternClass((HNFRecord)GetHandle(), value));
	}

	NByte GetQuality() const
	{
		NByte value;
		NCheck(NFRecordGetQuality((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetQuality(NByte value)
	{
		NCheck(NFRecordSetQuality((HNFRecord)GetHandle(), value));
	}

	NByte GetG() const
	{
		NByte value;
		NCheck(NFRecordGetG((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetG(NByte value)
	{
		NCheck(NFRecordSetG((HNFRecord)GetHandle(), value));
	}

	NUShort GetCbeffProductType() const
	{
		NUShort value;
		NCheck(NFRecordGetCbeffProductType((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetCbeffProductType(NUShort value)
	{
		NCheck(NFRecordSetCbeffProductType((HNFRecord)GetHandle(), value));
	}

	NFRidgeCountsType GetRidgeCountsType() const
	{
		NFRidgeCountsType value;
		NCheck(NFRecordGetRidgeCountsType((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetRidgeCountsType(NFRidgeCountsType value)
	{
		NCheck(NFRecordSetRidgeCountsType((HNFRecord)GetHandle(), value));
	}

	NFMinutiaFormat GetMinutiaFormat() const
	{
		NFMinutiaFormat value;
		NCheck(NFRecordGetMinutiaFormat((HNFRecord)GetHandle(), &value));
		return value;
	}

	void SetMinutiaFormat(NFMinutiaFormat value)
	{
		NCheck(NFRecordSetMinutiaFormat((HNFRecord)GetHandle(), value));
	}

	NSizeType GetSizeV1(NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NFRecordGetSizeV1((HNFRecord)GetHandle(), flags, &value));
		return value;
	}

	NSizeType SaveV1(void * pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NFRecordSaveToMemoryV1((HNFRecord)GetHandle(), pBuffer, bufferSize, flags, &value));
		return value;
	}

	MinutiaCollection * GetMinutiae() const
	{
		return minutiae.get();
	}

	MinutiaNeighborsCollection * GetMinutiaeNeighbors() const
	{
		return minutiaeNeighbors.get();
	}

	CoreCollection * GetCores() const
	{
		return cores.get();
	}

	DeltaCollection * GetDeltas() const
	{
		return deltas.get();
	}

	DoubleCoreCollection * GetDoubleCores() const
	{
		return doubleCores.get();
	}

	N_DECLARE_OBJECT_TYPE(NFRecord)
};

}}

#endif // !NF_RECORD_HPP_INCLUDED
