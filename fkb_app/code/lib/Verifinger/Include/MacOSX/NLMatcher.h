#ifndef NL_MATCHER_H_INCLUDED
#define NL_MATCHER_H_INCLUDED

#include <NLMatcherParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NLMatcher)

NResult N_API NlmCreate(HNLMatcher *pHMatcher);

NResult N_API NlmVerify(HNLMatcher hMatcher,
	const void *pTemplate1, NSizeType template1Size, const void *pTemplate2, NSizeType template2Size,
	NDouble *pScore);
NResult N_API NlmIdentifyStart(HNLMatcher hMatcher,
	const void *pTemplate, NSizeType templateSize);
NResult N_API NlmIdentifyNext(HNLMatcher hMatcher,
	const void *pTemplate, NSizeType templateSize,
	NDouble *pScore);
NResult N_API NlmIdentifyEnd(HNLMatcher hMatcher);

#define NlmFree(hMatcher) NObjectFree(hMatcher)
#define NlmCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NlmGetParameterA(hMatcher, parameterId, pValue) NTypeGetParameterA(NLMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NlmGetParameterW(hMatcher, parameterId, pValue) NTypeGetParameterW(NLMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NlmGetParameter(hMatcher, parameterId, pValue) NTypeGetParameter(NLMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NlmSetParameterA(hMatcher, parameterId, pValue) NTypeSetParameterA(NLMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NlmSetParameterW(hMatcher, parameterId, pValue) NTypeSetParameterW(NLMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NlmSetParameter(hMatcher, parameterId, pValue) NTypeSetParameter(NLMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NlmReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NlmFree", "NlmCopyParameters", "NlmGetParameterA", "NlmGetParameterW", "NlmGetParameter", "NlmSetParameterA", "NlmSetParameterW", "NlmSetParameter", "NlmReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NL_MATCHER_H_INCLUDED
