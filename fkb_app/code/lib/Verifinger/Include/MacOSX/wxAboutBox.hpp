#ifndef ABOUT_BOX_H_INCLUDED
#define ABOUT_BOX_H_INCLUDED

#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/artprov.h>
#include <wx/listctrl.h>
#include <wx/tokenzr.h>

#include <vector>

#include <NCore.hpp>
#include <NLibraryInfo.h>
#include <NeurotechnologyLogo.xpm>

#undef wxAboutBox_STYLE
#ifdef __WXMAC__
#define wxAboutBox_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMAXIMIZE_BOX
#else
#define wxAboutBox_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxCLOSE_BOX
#endif

namespace Neurotec { namespace Gui
{

typedef void (*GetLibraryInfoCallback)(NLibraryInfo * pValue);

class wxAboutBox : public wxDialog
{
public:
	wxAboutBox(wxWindow *parent, wxWindowID id,
		const wxString& productNameString, const wxString& versionString, const wxString& copyrightString,
		const wxString &windowTitle = wxT("About"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
		long style = wxAboutBox_STYLE)
		: wxDialog(parent, id, windowTitle, pos, size, style)
	{
		sizer = new wxBoxSizer(wxVERTICAL);
		SetSizer(sizer);
		SetAutoLayout(true);

		// header
		headerSizer = new wxFlexGridSizer(3);

		logo = new wxStaticBitmap(this, wxID_ANY, wxBitmap(NeurotechnologyLogo_XPM));
		logo->Enable(false);
		headerSizer->Add(logo, 0, wxALIGN_LEFT | wxALL, 5);

		infoSizer = new wxBoxSizer(wxVERTICAL);
		title = new wxStaticText(this, wxID_ANY, productNameString);
		infoSizer->Add(title, 0, wxALIGN_LEFT | wxALL, 5);
		version = new wxStaticText(this, wxID_ANY,
			wxString::Format(wxT("Version %s"), versionString.c_str()));
		infoSizer->Add(version, 0, wxALIGN_LEFT | wxALL, 5);
		copyright = new wxStaticText(this, wxID_ANY, copyrightString);
		infoSizer->Add(copyright, 0, wxALIGN_LEFT | wxALL, 5);
		headerSizer->Add(infoSizer, 0, wxALIGN_LEFT | wxALL | wxEXPAND, 5);
		headerSizer->AddGrowableCol(1);

		buttonRefresh = new wxButton(this, ID_BUTTON_REFRESH, wxT("&Refresh"));
		headerSizer->Add(buttonRefresh, 0, wxALIGN_RIGHT | wxALIGN_BOTTOM, 5);

		sizer->Add(headerSizer, 0, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

		components = new wxListCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT/* | wxLC_HRULES*/);
		components->InsertColumn(0, wxT("Component"));
		components->InsertColumn(1, wxT("Version"), wxLIST_FORMAT_CENTER);
		components->InsertColumn(2, wxT("Is Activated"), wxLIST_FORMAT_CENTER);
		components->InsertColumn(3, wxT("Copyright"));
		RefreshComponents();
		components->SetMinSize(wxSize(640, 240));
		sizer->Add(components, 1, wxALIGN_CENTER | wxALL | wxEXPAND, 5);

		buttonOK = new wxButton(this, wxID_OK, wxT("&OK"));
		sizer->Add(buttonOK, 0, wxALIGN_RIGHT | wxALL, 5);

		SetTitle(wxString::Format(wxT("%s %s"), wxT("About"), productNameString.c_str()));
		SetIcon(wxNullIcon);
		SetBackgroundColour(wxColour(255, 255, 255));
		GetSizer()->Layout();
		GetSizer()->Fit(this);
		GetSizer()->SetSizeHints(this);
		Center();

		this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(wxAboutBox::OnClose), NULL, this);
		buttonRefresh->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxAboutBox::OnRefreshClick), NULL, this);
	}

	virtual ~wxAboutBox()
	{
		buttonRefresh->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(wxAboutBox::OnRefreshClick), NULL, this);
		this->Disconnect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(wxAboutBox::OnClose), NULL, this);
	}

	void AddComponent(GetLibraryInfoCallback callback)
	{
		getInfoCallbacks.push_back(callback);
	}

	void RefreshComponents()
	{
		components->DeleteAllItems();

		for (std::vector<GetLibraryInfoCallback>::iterator it = getInfoCallbacks.begin();
			it != getInfoCallbacks.end(); it++)
		{
			NLibraryInfo li;
			(*it)(&li);
			ShowComponent(&li);
		}

		components->SetColumnWidth(0, wxLIST_AUTOSIZE);
		components->SetColumnWidth(1, wxLIST_AUTOSIZE_USEHEADER);
		components->SetColumnWidth(2, wxLIST_AUTOSIZE_USEHEADER);
		components->SetColumnWidth(3, wxLIST_AUTOSIZE);
	}

private:
	void OnClose(wxCloseEvent&)
	{
		Destroy();
	}

	void ShowComponent(NLibraryInfo *li)
	{
		long index = components->InsertItem(components->GetItemCount(), wxString(li->Title));
		components->SetItem(index, 1, wxString::Format(wxT("%d.%d.%d.%d"), li->VersionMajor, li->VersionMinor, li->VersionBuild, li->VersionRevision));
		components->SetItem(index, 3, wxString(li->Copyright));
		wxArrayString strings = wxStringTokenize(li->Activated, wxT(":,"), wxTOKEN_STRTOK);
		for (size_t i = 0; i < strings.Count() / 2; ++i)
		{
			index = components->InsertItem(components->GetItemCount(), wxT("    ") + strings[i * 2].Trim(false));
			components->SetItem(index, 2, strings[i * 2 + 1].Trim(false));
		}
	}

	void OnRefreshClick(wxCommandEvent&)
	{
		RefreshComponents();
	}

private:
	std::vector<GetLibraryInfoCallback> getInfoCallbacks;
	wxBoxSizer *sizer;
	wxFlexGridSizer *headerSizer;
	wxBoxSizer *infoSizer;
	wxStaticBitmap *logo;
	wxStaticText *title;
	wxStaticText *version;
	wxStaticText *copyright;
	wxButton *buttonRefresh;
	wxListCtrl *components;
	wxButton *buttonOK;

	enum
	{
		ID_BUTTON_REFRESH = 1000,
	};
};

}}

#endif
