#ifndef N_IMAGES_LIBRARY_CPP_INCLUDED
#define N_IMAGES_LIBRARY_CPP_INCLUDED

#include <NCoreLibrary.cpp>

#include <NImage.hpp>
#include <NGrayscaleImage.hpp>
#include <NMonochromeImage.hpp>
#include <NRgbImage.hpp>
#include <NImageFormat.hpp>
#include <NImageFile.hpp>
#include <NImages.hpp>
#include <Bmp.hpp>
#include <Tiff.hpp>
#include <Jpeg.hpp>
#include <Png.hpp>
#include <Wsq.hpp>
#include <IHead.hpp>
#include <Jpeg2K.hpp>

namespace Neurotec { namespace Images
{

N_CLASS(NImageFormat)::Initializer N_CLASS(NImageFormat)::initializer;

N_CLASS(NImage)* N_CLASS(NImage)::FromHandle(HNImage handle, bool ownsHandle)
{
	if (N_NATIVE_TYPE_OF(NMonochromeImage).IsInstanceOfType(handle)) return new N_CLASS(NMonochromeImage)(handle, ownsHandle);
	else if (N_NATIVE_TYPE_OF(NGrayscaleImage).IsInstanceOfType(handle)) return new N_CLASS(NGrayscaleImage)(handle, ownsHandle);
	else if (N_NATIVE_TYPE_OF(NRgbImage).IsInstanceOfType(handle)) return new N_CLASS(NRgbImage)(handle, ownsHandle);
	else return new N_CLASS(NImage)(handle, ownsHandle);
}

N_IMPLEMENT_OBJECT_TYPE(NImageFormat, NObject)
N_IMPLEMENT_OBJECT_TYPE(NImage, NObject)
N_IMPLEMENT_OBJECT_TYPE(NImageFile, NObject)
N_IMPLEMENT_OBJECT_TYPE(NMonochromeImage, NImage)
N_IMPLEMENT_OBJECT_TYPE(NGrayscaleImage, NImage)
N_IMPLEMENT_OBJECT_TYPE(NRgbImage, NImage)

}}

#endif // !N_IMAGES_LIBRARY_CPP_INCLUDED
