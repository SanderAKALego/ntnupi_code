#ifndef NE_TEMPLATE_H_INCLUDED
#define NE_TEMPLATE_H_INCLUDED

#include <NERecord.h>

#ifdef N_CPP
extern "C"
{
#endif

#define NET_MAX_RECORD_COUNT 255

DECLARE_N_OBJECT_HANDLE(NETemplate)

NResult N_API NETemplateCalculateSize(NInt recordCount, NSizeType * arRecordSizes, NSizeType * pSize);
NResult N_API NETemplatePack(NInt recordCount, const void * * arPRecords, NSizeType * arRecordSizes, void * pBuffer, NSizeType bufferSize, NSizeType * pSize);
NResult N_API NETemplateUnpack(const void * pBuffer, NSizeType bufferSize,
	NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
	NInt * pRecordCount, const void * * arPRecords, NSizeType * arRecordSizes);
NResult N_API NETemplateCheck(const void * pBuffer, NSizeType bufferSize);

NResult N_API NETemplateGetRecordCountMem(const void * pBuffer, NSizeType bufferSize, NInt * pValue);

typedef struct NETemplateInfo_
{
	NByte MajorVersion;
	NByte MinorVersion;
	NUInt Size;
	NByte HeaderSize;
	NInt RecordCount;
	NERecordInfo * RecordInfos;
} NETemplateInfo;

void N_API NETemplateInfoDispose(NETemplateInfo * pInfo);

#define NET_PROCESS_FIRST_RECORD_ONLY 0x00000100

NResult N_API NETemplateCreate(HNETemplate * pHTemplate);
NResult N_API NETemplateCreateEx(NUInt flags, HNETemplate * pHTemplate);
NResult N_API NETemplateCreateFromMemory(const void * pBuffer, NSizeType bufferSize,
	NUInt flags, NETemplateInfo * pInfo, HNETemplate * pHTemplate);

NResult N_API NETemplateGetRecordCount(HNETemplate hTemplate, NInt * pValue);
NResult N_API NETemplateGetRecord(HNETemplate hTemplate, NInt index, HNERecord * pValue);
NResult N_API NETemplateGetRecordCapacity(HNETemplate hTemplate, NInt * pValue);
NResult N_API NETemplateSetRecordCapacity(HNETemplate hTemplate, NInt value);
NResult N_API NETemplateAddRecord(HNETemplate hTemplate, NUShort width, NUShort height, NUInt flags, HNERecord * pHRecord);
NResult N_API NETemplateAddRecordFromMemory(HNETemplate hTemplate, const void * pBuffer, NSizeType bufferSize,
	NUInt flags, HNERecord * pHRecord);
NResult N_API NETemplateAddRecordCopy(HNETemplate hTemplate, HNERecord hSrcRecord, HNERecord * pHRecord);
NResult N_API NETemplateRemoveRecord(HNETemplate hTemplate, NInt index);
NResult N_API NETemplateClearRecords(HNETemplate hTemplate);

NResult N_API NETemplateClone(HNETemplate hTemplate, HNETemplate * pHClonedTemplate);
NResult N_API NETemplateGetSize(HNETemplate hTemplate, NUInt flags, NSizeType * pSize);
NResult N_API NETemplateSaveToMemory(HNETemplate hTemplate, void * pBuffer, NSizeType bufferSize, NUInt flags, NSizeType * pSize);

#define NETemplateFree(hTemplate) NObjectFree(hTemplate)

#ifdef N_MSVC
	#pragma deprecated("NETemplateFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !NE_TEMPLATE_H_INCLUDED
