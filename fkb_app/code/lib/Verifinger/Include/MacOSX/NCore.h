#ifndef N_CORE_H_INCLUDED
#define N_CORE_H_INCLUDED

#include <NTypes.h>
#include <NErrors.h>
#include <NMemory.h>
#include <NObject.h>
#include <NStream.h>
#include <NLibraryInfo.h>
#include <NProcessorInfo.h>

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API NCoreGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NCoreGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NCoreGetInfo(NLibraryInfo * pValue);
#endif
#define NCoreGetInfo N_FUNC_AW(NCoreGetInfo)

void N_API NCoreOnStart(void);
void N_API NCoreOnThreadStart(void);
void N_API NCoreOnThreadExit(void);
void N_API NCoreOnExit(void);

#ifdef N_CPP
}
#endif

#endif // !N_CORE_H_INCLUDED
