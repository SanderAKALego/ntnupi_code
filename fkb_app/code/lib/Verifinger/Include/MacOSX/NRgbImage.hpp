#ifndef N_RGB_IMAGE_HPP_INCLUDED
#define N_RGB_IMAGE_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <NRgbImage.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(NRgbImage) : public N_CLASS(NImage)
{
public:
	explicit N_CLASS(NRgbImage)(HNImage handle, bool ownsHandle = true)
		: N_CLASS(NImage)(handle, N_NATIVE_TYPE_OF(NRgbImage), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	NRgb GetPixel(NUInt x, NUInt y)
	{
		NRgb value;
		NCheck(NRgbImageGetPixel(GetHandle(), x, y, &value));
		return value;
	}

	void SetPixel(NUInt x, NUInt y, const NRgb &value)
	{
		NCheck(NRgbImageSetPixel(GetHandle(), x, y, &value));
	}

	N_DECLARE_OBJECT_TYPE(NRgbImage)
};

}}

#endif // !N_RGB_IMAGE_HPP_INCLUDED
