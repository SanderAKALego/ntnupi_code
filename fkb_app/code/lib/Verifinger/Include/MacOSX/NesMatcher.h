#ifndef NES_MATCHER_H_INCLUDED
#define NES_MATCHER_H_INCLUDED

#include <NEMatcher.h>
#include <NesmMatchDetails.h>
#include <NesMatcherParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NesMatcher)

NResult N_API NesmCreate(HNesMatcher * pHMatcher);

NResult N_API NesmVerify(HNesMatcher hMatcher, const void * template1, NSizeType template1Size,
	const void * template2, NSizeType template2Size, NesmMatchDetails * * ppMatchDetails, NInt * pScore);
NResult N_API NesmIdentifyStart(HNesMatcher hMatcher, const void * templ, NSizeType templSize,
	NesmMatchDetails * * ppMatchDetails);
NResult N_API NesmIdentifyNext(HNesMatcher hMatcher, const void * templ, NSizeType templSize,
	NesmMatchDetails * pMatchDetails, NInt * pScore);
NResult N_API NesmIdentifyEnd(HNesMatcher hMatcher);

#define NesmFree(hMatcher) NObjectFree(hMatcher)
#define NesmCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NesmGetParameterA(hMatcher, partId, parameterId, pValue) NTypeGetParameterA(NesMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NesmGetParameterW(hMatcher, partId, parameterId, pValue) NTypeGetParameterW(NesMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NesmGetParameter(hMatcher, partId, parameterId, pValue) NTypeGetParameter(NesMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NesmSetParameterA(hMatcher, partId, parameterId, pValue) NTypeSetParameterA(NesMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NesmSetParameterW(hMatcher, partId, parameterId, pValue) NTypeSetParameterW(NesMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NesmSetParameter(hMatcher, partId, parameterId, pValue) NTypeSetParameter(NesMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NesmReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NesmFree", "NesmCopyParameters", "NesmGetParameterA", "NesmGetParameterW", "NesmGetParameter", "NesmSetParameterA", "NesmSetParameterW", "NesmSetParameter", "NesmReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NES_MATCHER_H_INCLUDED
