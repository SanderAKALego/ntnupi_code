#ifndef NL_RECORD_HPP_INCLUDED
#define NL_RECORD_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NLRecord.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NLRecord) : public N_CLASS(NObject)
{
private:
	static HNLRecord Create(NUInt flags)
	{
		HNLRecord handle;
		NCheck(NLRecordCreate(flags, &handle));
		return handle;
	}

	static HNLRecord Create(const void *pBuffer, NSizeType bufferSize, NUInt flags, NLRecordInfo *pInfo)
	{
		HNLRecord handle;
		NCheck(NLRecordCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

public:
	static N_CLASS(NLRecord)* FromHandle(HNLRecord handle, bool ownsHandle = true)
	{
		return new N_CLASS(NLRecord)(handle, ownsHandle);
	}

	static NByte GetQuality(const void *pBuffer, NSizeType bufferSize)
	{
		NByte value;
		NCheck(NLRecordGetQualityMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetCbeffProductType(const void *pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NLRecordGetCbeffProductTypeMem(pBuffer, bufferSize, &value));
		return value;
	}

	static void Check(const void *pBuffer, NSizeType bufferSize)
	{
		NCheck(NLRecordCheck(pBuffer, bufferSize));
	}

	explicit N_CLASS(NLRecord)(HNLRecord handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NLRecord), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NLRecord)(NUInt flags = 0)
		: N_CLASS(NObject)(Create(flags), N_NATIVE_TYPE_OF(NLRecord), true)
	{
	}

	N_CLASS(NLRecord)(const void *pBuffer, NSizeType bufferSize,
		NUInt flags = 0, NLRecordInfo *pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NLRecord), true)
	{
	}

	N_CLASS(NObject) * Clone() const
	{
		HNLRecord hRecord;
		NCheck(NLRecordClone((HNLRecord)GetHandle(), &hRecord));
		try
		{
			return FromHandle(hRecord);
		}
		catch(...)
		{
			NObjectFree(hRecord);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType size;
		NCheck(NLRecordGetSize((HNLRecord)GetHandle(), flags, &size));
		return size;
	}

	NSizeType Save(void *pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType size;
		NCheck(NLRecordSaveToMemory((HNLRecord)GetHandle(), pBuffer, bufferSize, flags, &size));
		return size;
	}

	NByte GetQuality() const
	{
		NByte value;
		NCheck(NLRecordGetQuality((HNLRecord)GetHandle(), &value));
		return value;
	}

	void SetQuality(NByte value)
	{
		NCheck(NLRecordSetQuality((HNLRecord)GetHandle(), value));
	}

	NUShort GetCbeffProductType() const
	{
		NUShort value;
		NCheck(NLRecordGetCbeffProductType((HNLRecord)GetHandle(), &value));
		return value;
	}

	void SetCbeffProductType(NUShort value)
	{
		NCheck(NLRecordSetCbeffProductType((HNLRecord)GetHandle(), value));
	}

	N_DECLARE_OBJECT_TYPE(NLRecord)
};

}}

#endif // !NL_RECORD_HPP_INCLUDED
