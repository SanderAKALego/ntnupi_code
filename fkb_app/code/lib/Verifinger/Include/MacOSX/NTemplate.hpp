#ifndef N_TEMPLATE_HPP_INCLUDED
#define N_TEMPLATE_HPP_INCLUDED

#include <NFTemplate.hpp>
#include <NLTemplate.hpp>
#include <NETemplate.hpp>
#include <NTemplates.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NTemplate.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NTemplate) : public N_CLASS(NObject)
{
private:
	static HNTemplate Create(NUInt flags)
	{
		HNTemplate handle;
		NCheck(NTemplateCreateEx(flags, &handle));
		return handle;
	}

	static HNTemplate Create(const void * pBuffer, NSizeType bufferSize,
		NUInt flags, NTemplateInfo * pInfo)
	{
		HNTemplate handle;
		NCheck(NTemplateCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

	std::auto_ptr<N_CLASS(NFTemplate)> fingers;
	std::auto_ptr<N_CLASS(NLTemplate)> faces;
	std::auto_ptr<N_CLASS(NETemplate)> irises;
	std::auto_ptr<N_CLASS(NFTemplate)> palms;

	void Init()
	{
		HNFTemplate hFingers;
		NCheck(NTemplateGetFingers(GetHandle(), &hFingers));
		SetFingers(hFingers);

		HNLTemplate hFaces;
		NCheck(NTemplateGetFaces(GetHandle(), &hFaces));
		SetFaces(hFaces);

		HNETemplate hIrises;
		NCheck(NTemplateGetIrises(GetHandle(), &hIrises));
		SetIrises(hIrises);

		HNFTemplate hPalms;
		NCheck(NTemplateGetPalms(GetHandle(), &hPalms));
		SetPalms(hPalms);
	}

	N_CLASS(NFTemplate) * SetFingers(HNFTemplate hFingers)
	{
		N_CLASS(NFTemplate) * pFingers = hFingers ? N_CLASS(NFTemplate)::FromHandle(hFingers, false) : NULL;
		fingers = std::auto_ptr<N_CLASS(NFTemplate)>(pFingers);
		if(pFingers)
		{
			EmbedObject(pFingers);
		}
		return pFingers;
	}

	void RemoveFingersInternal()
	{
		ReleaseObject(fingers.get());
		fingers.release();
	}

	N_CLASS(NLTemplate) * SetFaces(HNLTemplate hFaces)
	{
		N_CLASS(NLTemplate) * pFaces = hFaces ? N_CLASS(NLTemplate)::FromHandle(hFaces, false) : NULL;
		faces = std::auto_ptr<N_CLASS(NLTemplate)>(pFaces);
		if(pFaces)
		{
			EmbedObject(pFaces);
		}
		return pFaces;
	}

	void RemoveFacesInternal()
	{
		ReleaseObject(faces.get());
		faces.release();
	}

	N_CLASS(NETemplate) * SetIrises(HNETemplate hIrises)
	{
		N_CLASS(NETemplate) * pIrises = hIrises ? N_CLASS(NETemplate)::FromHandle(hIrises, false) : NULL;
		irises = std::auto_ptr<N_CLASS(NETemplate)>(pIrises);
		if(irises.get())
		{
			EmbedObject(pIrises);
		}
		return pIrises;
	}

	void RemoveIrisesInternal()
	{
		ReleaseObject(irises.get());
		irises.release();
	}

	N_CLASS(NFTemplate) * SetPalms(HNFTemplate hPalms)
	{
		N_CLASS(NFTemplate) * pPalms = hPalms ? N_CLASS(NFTemplate)::FromHandle(hPalms, false) : NULL;
		palms = std::auto_ptr<N_CLASS(NFTemplate)>(pPalms);
		if(pPalms)
		{
			EmbedObject(pPalms);
		}
		return pPalms;
	}

	void RemovePalmsInternal()
	{
		ReleaseObject(palms.get());
		palms.release();
	}

	void ClearInternal()
	{
		RemoveFingersInternal();
		RemoveFacesInternal();
		RemoveIrisesInternal();
		RemovePalmsInternal();
	}

public:
	static N_CLASS(NTemplate)* FromHandle(HNTemplate handle, bool ownsHandle = true)
	{
		return new N_CLASS(NTemplate)(handle, ownsHandle);
	}

	static NSizeType CalculateSize(NSizeType fingersTemplateSize, NSizeType facesTemplateSize, NSizeType irisesTemplateSize, NSizeType palmsTemplateSize)
	{
		NSizeType value;
		NCheck(NTemplateCalculateSize(fingersTemplateSize, facesTemplateSize, irisesTemplateSize, palmsTemplateSize, &value));
		return value;
	}

	static NSizeType Pack(const void * pFingersTemplate, NSizeType fingersTemplateSize,
		const void * pFacesTemplate, NSizeType facesTemplateSize,
		const void * pIrisesTemplate, NSizeType irisesTemplateSize,
		const void * pPalmsTemplate, NSizeType palmsTemplateSize,
		void * pBuffer, NSizeType bufferSize)
	{
		NSizeType value;
		NCheck(NTemplatePack(pFingersTemplate, fingersTemplateSize,
			pFacesTemplate, facesTemplateSize,
			pIrisesTemplate, irisesTemplateSize,
			pPalmsTemplate, palmsTemplateSize,
			pBuffer, bufferSize, &value));
		return value;
	}

	static void Unpack(const void * pBuffer, NSizeType bufferSize,
		NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
		const void * * ppFingersTemplate, NSizeType * pFingersTemplateSize,
		const void * * ppFacesTemplate, NSizeType * pFacesTemplateSize,
		const void * * ppIrisesTemplate, NSizeType * pIrisesTemplateSize,
		const void * * ppPalmsTemplate, NSizeType * pPalmsTemplateSize)
	{
		NCheck(NTemplateUnpack(pBuffer, bufferSize,
			pMajorVersion, pMinorVersion, pSize, pHeaderSize,
			ppFingersTemplate, pFingersTemplateSize,
			ppFacesTemplate, pFacesTemplateSize,
			ppIrisesTemplate, pIrisesTemplateSize,
			ppPalmsTemplate, pPalmsTemplateSize));
	}

	static void Check(const void * pBuffer, NSizeType bufferSize)
	{
		NCheck(NTemplateCheck(pBuffer, bufferSize));
	}

	explicit N_CLASS(NTemplate)(HNTemplate handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NTemplate), false)
	{
		Init();
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NTemplate)(NUInt flags = 0)
		: N_CLASS(NObject)(Create(flags), N_NATIVE_TYPE_OF(NTemplate), true)
	{
		Init();
	}

	N_CLASS(NTemplate)(const void * pBuffer, NSizeType bufferSize,
		NUInt flags = 0, NTemplateInfo * pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NTemplate), true)
	{
		Init();
	}

	virtual ~N_CLASS(NTemplate)()
	{
		ClearInternal();
	}

	N_CLASS(NFTemplate) * GetFingers()
	{
		return fingers.get();
	}

	N_CLASS(NFTemplate) * AddFingers(NUInt flags = 0)
	{
		HNFTemplate hFingers;
		NCheck(NTemplateAddFingersEx(GetHandle(), flags, &hFingers));
		try
		{
			return SetFingers(hFingers);
		}
		catch(...)
		{
			NTemplateRemoveFingers(GetHandle());
			RemoveFingersInternal();
			throw;
		}
	}

	N_CLASS(NFTemplate) * AddFingers(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
	{
		HNFTemplate hFingers;
		NCheck(NTemplateAddFingersFromMemory(GetHandle(), pBuffer, bufferSize, flags, &hFingers));
		try
		{
			return SetFingers(hFingers);
		}
		catch(...)
		{
			NTemplateRemoveFingers(GetHandle());
			RemoveFingersInternal();
			throw;
		}
	}

	N_CLASS(NFTemplate) * AddFingersCopy(N_CLASS(NFTemplate) * pSrcRecord)
	{
		HNFTemplate hFingers;
		if (pSrcRecord == NULL)
		{
			NThrowArgumentNullException(N_T("pSrcRecord"));
		}
		NCheck(NTemplateAddFingersCopy(GetHandle(), pSrcRecord->GetHandle(), &hFingers));
		try
		{
			return SetFingers(hFingers);
		}
		catch(...)
		{
			NTemplateRemoveFingers(GetHandle());
			RemoveFingersInternal();
			throw;
		}
	}

	void RemoveFingers()
	{
		NCheck(NTemplateRemoveFingers(GetHandle()));
		RemoveFingersInternal();
	}

	N_CLASS(NLTemplate) * GetFaces()
	{
		return faces.get();
	}

	N_CLASS(NLTemplate) * AddFaces(NUInt flags = 0)
	{
		HNLTemplate hFaces;
		NCheck(NTemplateAddFacesEx(GetHandle(), flags, &hFaces));
		try
		{
			return SetFaces(hFaces);
		}
		catch(...)
		{
			NTemplateRemoveFaces(GetHandle());
			RemoveFacesInternal();
			throw;
		}
	}

	N_CLASS(NLTemplate) * AddFaces(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
	{
		HNLTemplate hFaces;
		NCheck(NTemplateAddFingersFromMemory(GetHandle(), pBuffer, bufferSize, flags, &hFaces));
		try
		{
			return SetFaces(hFaces);
		}
		catch(...)
		{
			NTemplateRemoveFaces(GetHandle());
			RemoveFacesInternal();
			throw;
		}
	}

	N_CLASS(NLTemplate) * AddFacesCopy(N_CLASS(NFTemplate) * pSrcRecord)
	{
		HNLTemplate hFaces;
		if (pSrcRecord == NULL)
		{
			NThrowArgumentNullException(N_T("pSrcRecord"));
		}
		NCheck(NTemplateAddFacesCopy(GetHandle(), pSrcRecord->GetHandle(), &hFaces));
		try
		{
			return SetFaces(hFaces);
		}
		catch(...)
		{
			NTemplateRemoveFaces(GetHandle());
			RemoveFacesInternal();
			throw;
		}
	}

	void RemoveFaces()
	{
		NCheck(NTemplateRemoveFaces(GetHandle()));
		RemoveFacesInternal();
	}

	N_CLASS(NETemplate) * GetIrises()
	{
		return irises.get();
	}

	N_CLASS(NETemplate) * AddIrises(NUInt flags = 0)
	{
		HNETemplate hIrises;
		NCheck(NTemplateAddIrisesEx(GetHandle(), flags, &hIrises));
		try
		{
			return SetIrises(hIrises);
		}
		catch(...)
		{
			NTemplateRemoveIrises(GetHandle());
			RemoveIrisesInternal();
			throw;
		}
	}

	N_CLASS(NETemplate) * AddIrises(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
	{
		HNETemplate hIrises;
		NCheck(NTemplateAddIrisesFromMemory(GetHandle(), pBuffer, bufferSize, flags, &hIrises));
		try
		{
			return SetIrises(hIrises);
		}
		catch(...)
		{
			NTemplateRemoveIrises(GetHandle());
			RemoveIrisesInternal();
			throw;
		}
	}

	N_CLASS(NETemplate) * AddIrisesCopy(N_CLASS(NETemplate) * pSrcRecord)
	{
		HNETemplate hIrises;
		if (pSrcRecord == NULL)
		{
			NThrowArgumentNullException(N_T("pSrcRecord"));
		}
		NCheck(NTemplateAddIrisesCopy(GetHandle(), pSrcRecord->GetHandle(), &hIrises));
		try
		{
			return SetIrises(hIrises);
		}
		catch(...)
		{
			NTemplateRemoveIrises(GetHandle());
			RemoveIrisesInternal();
			throw;
		}
	}

	void RemoveIrises()
	{
		NCheck(NTemplateRemoveIrises(GetHandle()));
		RemoveIrisesInternal();
	}

	N_CLASS(NFTemplate) * GetPalms()
	{
		return palms.get();
	}

	N_CLASS(NFTemplate) * AddPalms(NUInt flags = 0)
	{
		HNFTemplate hPalms;
		NCheck(NTemplateAddPalmsEx(GetHandle(), flags, &hPalms));
		try
		{
			return SetPalms(hPalms);
		}
		catch(...)
		{
			NTemplateRemovePalms(GetHandle());
			RemovePalmsInternal();
			throw;
		}
	}

	N_CLASS(NFTemplate) * AddPalms(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
	{
		HNFTemplate hPalms;
		NCheck(NTemplateAddPalmsFromMemory(GetHandle(), pBuffer, bufferSize, flags, &hPalms));
		try
		{
			return SetPalms(hPalms);
		}
		catch(...)
		{
			NTemplateRemovePalms(GetHandle());
			RemovePalmsInternal();
			throw;
		}
	}

	N_CLASS(NFTemplate) * AddPalmsCopy(N_CLASS(NFTemplate) * pSrcRecord)
	{
		HNFTemplate hPalms;
		if (pSrcRecord == NULL)
		{
			NThrowArgumentNullException(N_T("pSrcRecord"));
		}
		NCheck(NTemplateAddPalmsCopy(GetHandle(), pSrcRecord->GetHandle(), &hPalms));
		try
		{
			return SetPalms(hPalms);
		}
		catch(...)
		{
			NTemplateRemovePalms(GetHandle());
			RemovePalmsInternal();
			throw;
		}
	}

	void RemovePalms()
	{
		NCheck(NTemplateRemovePalms(GetHandle()));
		RemovePalmsInternal();
	}

	void Clear()
	{
		NCheck(NTemplateClear(GetHandle()));
		ClearInternal();
	}

	N_CLASS(NObject) * Clone() const
	{
		HNTemplate handle;
		NCheck(NTemplateClone(GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NTemplateGetSize(GetHandle(), flags, &value));
		return value;
	}

	NSizeType Save(void * pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NTemplateSaveToMemory(GetHandle(), pBuffer, bufferSize, flags, &value));
		return value;
	}

	N_DECLARE_OBJECT_TYPE(NTemplate)
};

}}

#endif // !N_TEMPLATE_HPP_INCLUDED
