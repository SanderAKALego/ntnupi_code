#ifndef __LIBUSBW_H__
#define __LIBUSBW_H__

#ifdef __cplusplus
extern "C"
{
#endif

struct usb_dev_handle;
#if defined(__linux__)
struct usb_device;
#endif

void *open_device(int vendor, int product, int device, int configuration);
#if defined(__linux__)
int get_fd(struct usb_dev_handle *dev);
void *get_device_descriptor(struct usb_dev_handle *dev);
void *get_config_descriptor(struct usb_dev_handle *dev);
struct usb_device *get_usb_device(struct usb_dev_handle *dev);
#elif defined(__APPLE__)
void * get_interface(struct usb_dev_handle *);
int get_endpoint_pipe(struct usb_dev_handle *, int);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __LIBUSBW_H__ */

