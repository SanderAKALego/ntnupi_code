#ifndef NF_EXTRACTOR_H_INCLUDED
#define NF_EXTRACTOR_H_INCLUDED

#include <NGrayscaleImage.h>
#include <NFRecord.h>
#include <NExtractors.h>
#include <NFExtractorParams.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef enum NfeExtractionStatus_
{
	nfeesTemplateCreated = 1,
	nfeesTooFewMinutiae = 90,
	nfeesQualityCheckFailed = 100,
	nfeesMatchingFailed = 200
} NfeExtractionStatus;

DECLARE_N_OBJECT_HANDLE(NFExtractor);

NResult N_API NfeMakeLightTemplate(HNFRecord hRecord, NBool makeSmallTemplate, NBool preserveRidgeCounts, HNFRecord * pHLightRecord);
NResult N_API NfeMakeSmallTemplate(HNFRecord hRecord, HNFRecord * pHSmallRecord);
NResult N_API NfeIsTemplateSmall(HNFRecord hRecord, NBool * pValue);

NResult N_API NfeCreate(HNFExtractor * pHExtractor);
NResult N_API NfeCreateEx(NBool isPalm, HNFExtractor * pHExtractor);

NResult N_API NfeExtract(HNFExtractor hExtractor, HNGrayscaleImage hImage,
	NFPosition position, NFImpressionType impressionType, NfeExtractionStatus * pExtractionStatus, HNFRecord * pHRecord);
NResult N_API NfeUpdateTemplate(HNFExtractor hExtractor, HNGrayscaleImage hImage, HNFRecord hRecord);
N_DEPRECATED("function is deprecated, use NfeGeneralizeEx instead")
NResult N_API NfeGeneralize(HNFExtractor hExtractor, NInt recordCount, const HNFRecord * arHRecords,
	HNFRecord * pHRecord, NInt * pBaseTemplateIndex);
NResult N_API NfeGeneralizeEx(HNFExtractor hExtractor, NInt recordCount, const HNFRecord * arHRecords,
	NfeExtractionStatus * pStatus, NInt * pBaseTemplateIndex, HNFRecord * pHRecord);

NResult N_API NfeIsPalm(HNFExtractor hExtractor, NBool * pValue);

#define NfeFree(hExtractor) NObjectFree(hExtractor)
#define NfeCopyParameters(hDstExtractor, hSrcExtractor) NObjectCopyParameters(hDstExtractor, hSrcExtractor)
#define NfeGetParameterA(hExtractor, parameterId, pValue) NTypeGetParameterA(NFExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NfeGetParameterW(hExtractor, parameterId, pValue) NTypeGetParameterW(NFExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NfeGetParameter(hExtractor, parameterId, pValue) NTypeGetParameter(NFExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NfeSetParameterA(hExtractor, parameterId, pValue) NTypeSetParameterA(NFExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NfeSetParameterW(hExtractor, parameterId, pValue) NTypeSetParameterW(NFExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NfeSetParameter(hExtractor, parameterId, pValue) NTypeSetParameter(NFExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NfeReset(hExtractor) NObjectReset(hExtractor)

#ifdef N_MSVC
	#pragma deprecated("NfeFree", "NfeCopyParameters", "NfeGetParameterA", "NfeGetParameterW", "NfeGetParameter", "NfeSetParameterA", "NfeSetParameterW", "NfeSetParameter", "NfeReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NF_EXTRACTOR_H_INCLUDED
