#ifndef N_TYPES_H_INCLUDED
#define N_TYPES_H_INCLUDED

#ifdef N_DOCUMENTATION
	#define N_64
	#define N_ANSI_C
	#define N_BIG_ENDIAN
	#define N_CPP
	#define N_DEBUG
	#define N_FAST_FLOAT
	#define N_GCC
	#define N_LIB
	#define N_LINUX
	#define N_MAC
	#define N_MSVC
	#define N_NO_ANSI_FUNC
	#define N_NO_INT_64
	#define N_UNICODE
	#define N_WINDOWS
#endif

#ifdef __cplusplus
	#define N_CPP
#endif

#ifdef N_CPP
extern "C"
{
#endif

#ifdef _DEBUG
	#define N_DEBUG
#endif

#ifdef _LIB
	#define N_LIB
#endif

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
	#define N_WINDOWS
#endif

#ifdef WINCE
	#define N_WINDOWS_CE
#endif

#ifdef __linux__
	#define N_LINUX
#endif

#ifdef __APPLE__
	#define N_MAC
#endif

#ifdef _AIX
	#define N_AIX
#endif

#if defined(_UNICODE) || defined(UNICODE)
	#define N_UNICODE
#endif


#ifdef _MSC_VER
	#if (_MSC_VER >= 1300)
		#define N_MSVC
	#else
		#define N_NO_INT_64
	#endif
#endif

#ifdef __GNUC__
	#define N_GCC
#endif

#if defined(__STDC__) && !defined(N_GCC)
	#define N_ANSI_C
#endif

#if defined(N_MSVC)
	#define N_DEPRECATED(message) __declspec(deprecated(message))
	#define N_NO_RETURN __declspec(noreturn)
	#define N_PACKED
#elif defined(N_GCC)
	#define N_DEPRECATED(message) __attribute__ ((deprecated))
	#define N_NO_RETURN __attribute__ ((noreturn))
	#define N_PACKED __attribute__((__packed__))
#else
	#define N_DEPRECATED(message)
	#define N_NO_RETURN
	#define N_PACKED
#endif

#ifdef N_CPP
	#if defined(N_MSVC)
		#define N_NO_THROW __declspec(nothrow)
	#elif defined(N_GCC)
		#define N_NO_THROW __attribute__ ((nothrow))
	#else
		#define N_NO_THROW
	#endif
#else
	#define N_NO_THROW
#endif

#if defined(_M_IX86) || defined(__i386__)
	#define N_X86
#endif

#if defined(_M_X64) || defined(__x86_64__)
	#define N_X64
#endif

#if defined(N_X86) || defined(N_X64)
	#define N_X86_FAMILY
#endif

#if defined(__POWERPC__) || defined(_POWER) || defined(_ARCH_PPC)
	#define N_POWER_PC
#endif

#if defined(N_POWER_PC)
	#define N_POWER_PC_FAMILY
#endif

#if defined(N_POWER_PC_FAMILY)
	#define N_BIG_ENDIAN
#endif

#if defined(N_X64)
	#define N_64
#endif

#if defined(N_64) && defined(N_NO_INT_64)
	#error N_64 and N_NO_INT_64 defined simultaneously
#endif

#if defined(N_X86_FAMILY) || defined(N_POWER_PC_FAMILY)
	#define N_FAST_FLOAT
#endif

#ifdef N_MSVC
	typedef unsigned __int8  NUInt8;
	typedef signed   __int8  NInt8;
	typedef unsigned __int16 NUInt16;
	typedef signed   __int16 NInt16;
	typedef unsigned __int32 NUInt32;
	typedef signed   __int32 NInt32;

	#define N_UINT8_MIN 0x00ui8
	#define N_UINT8_MAX 0xFFui8
	#define N_INT8_MIN 0x80i8
	#define N_INT8_MAX 0x7Fi8
	#define N_UINT16_MIN 0x0000ui16
	#define N_UINT16_MAX 0xFFFFui16
	#define N_INT16_MIN 0x8000i16
	#define N_INT16_MAX 0x7FFFi16
	#define N_UINT32_MIN 0x00000000ui32
	#define N_UINT32_MAX 0xFFFFFFFFui32
	#define N_INT32_MIN 0x80000000i32
	#define N_INT32_MAX 0x7FFFFFFFi32
#else
	typedef unsigned char  NUInt8;
	typedef signed   char  NInt8;
	typedef unsigned short NUInt16;
	typedef signed   short NInt16;
	typedef unsigned int   NUInt32;
	typedef signed   int   NInt32;

	#define N_UINT8_MIN ((NUInt8)0x00u)
	#define N_UINT8_MAX ((NUInt8)0xFFu)
	#define N_INT8_MIN ((NInt8)0x80)
	#define N_INT8_MAX ((NInt8)0x7F)
	#define N_UINT16_MIN ((NUInt16)0x0000u)
	#define N_UINT16_MAX ((NUInt16)0xFFFFu)
	#define N_INT16_MIN ((NInt16)0x8000)
	#define N_INT16_MAX ((NInt16)0x7FFF)
	#define N_UINT32_MIN 0x00000000u
	#define N_UINT32_MAX 0xFFFFFFFFu
	#define N_INT32_MIN 0x80000000
	#define N_INT32_MAX 0x7FFFFFFF
#endif

#ifndef N_NO_INT_64
	#ifdef N_MSVC
		typedef unsigned __int64 NUInt64;
		typedef signed   __int64 NInt64;

		#define N_UINT64_MIN 0x0000000000000000ui64
		#define N_UINT64_MAX 0xFFFFFFFFFFFFFFFFui64
		#define N_INT64_MIN 0x8000000000000000i64
		#define N_INT64_MAX 0x7FFFFFFFFFFFFFFFi64
	#else
		typedef unsigned long long NUInt64;
		typedef signed   long long NInt64;

		#define N_UINT64_MIN 0x0000000000000000ull
		#define N_UINT64_MAX 0xFFFFFFFFFFFFFFFFull
		#define N_INT64_MIN 0x8000000000000000ll
		#define N_INT64_MAX 0x7FFFFFFFFFFFFFFFll
	#endif
#endif


typedef NUInt8 NByte;
typedef NInt8 NSByte;
typedef NUInt16 NUShort;
typedef NInt16 NShort;
typedef NUInt32 NUInt;
typedef NInt32 NInt;

#ifndef N_NO_INT_64
	typedef NUInt64 NULong;
	typedef NInt64 NLong;
#endif

#define N_BYTE_MIN N_UINT8_MIN
#define N_BYTE_MAX N_UINT8_MAX
#define N_SBYTE_MIN N_INT8_MIN
#define N_SBYTE_MAX N_INT8_MAX
#define N_USHORT_MIN N_UINT16_MIN
#define N_USHORT_MAX N_UINT16_MAX
#define N_SHORT_MIN N_INT16_MIN
#define N_SHORT_MAX N_INT16_MAX
#define N_UINT_MIN N_UINT32_MIN
#define N_UINT_MAX N_UINT32_MAX
#define N_INT_MIN N_INT32_MIN
#define N_INT_MAX N_INT32_MAX

#ifndef N_NO_INT_64
	#define N_ULONG_MIN N_UINT64_MIN
	#define N_ULONG_MAX N_UINT64_MAX
	#define N_LONG_MIN N_INT64_MIN
	#define N_LONG_MAX N_INT64_MAX
#endif

#ifndef N_NO_FLOAT
	typedef float NSingle;
	typedef double NDouble;

	#define N_SINGLE_MIN 1.175494351e-38F
	#define N_SINGLE_MAX 3.402823466e+38F
	#define N_SINGLE_EPSILON 1.192092896e-07F
	#define N_DOUBLE_MIN 2.2250738585072014e-308
	#define N_DOUBLE_MAX 1.7976931348623158e+308
	#define N_DOUBLE_EPSILON 2.2204460492503131e-016

	typedef NSingle NFloat;

	#define N_FLOAT_MIN N_SINGLE_MIN
	#define N_FLOAT_MAX N_SINGLE_MAX
	#define N_FLOAT_EPSILON N_SINGLE_EPSILON
#endif

typedef int NBoolean;

#define NTrue 1
#define NFalse 0

typedef NBoolean NBool;


#if defined(N_NO_UNICODE) && defined(N_UNICODE)
	#error "N_NO_UNICODE and N_UNICODE defined simultaneously"
#endif

typedef char NAChar;

#ifndef N_NO_UNICODE
	#if defined(N_WINDOWS) || (defined(__SIZEOF_WCHAR_T__) && __SIZEOF_WCHAR_T__ == 2)
		#define N_WCHAR_SIZE 2

		#ifndef _WCHAR_T_DEFINED
			typedef NUShort NWChar;
		#endif
	#else // !defined(N_WINDOWS) && (!defined(__SIZEOF_WCHAR_T__) || __SIZEOF_WCHAR_T__ != 2)
		#define N_WCHAR_SIZE 4

		#ifndef _WCHAR_T_DEFINED
			#ifdef N_CPP
				typedef wchar_t NWChar;
			#else
				#ifdef __WCHAR_TYPE__
					typedef __WCHAR_TYPE__ NWChar;
				#else
					typedef int NWChar;
				#endif
			#endif
		#endif
	#endif // !defined(N_WINDOWS) && (!defined(__SIZEOF_WCHAR_T__) || __SIZEOF_WCHAR_T__ != 2)

	#ifdef _WCHAR_T_DEFINED
		typedef wchar_t NWChar;
	#endif
#endif // !N_NO_UNICODE

#ifdef N_UNICODE
	typedef NWChar NChar;
#else
	typedef NAChar NChar;
#endif

#ifdef N_UNICODE
	#define N_T_(text) L##text
#else
	#define N_T_(text) text
#endif

#define N_T(text) N_T_(text)

#define N_EMPTY_STRING_A ""
#ifndef N_NO_UNICODE
#define N_EMPTY_STRING_W L""
#endif
#define N_EMPTY_STRING N_T("")

#ifdef N_UNICODE
	#define N_MACRO_AW(name) name##W
	#define N_FUNC_AW(name) name##W
	#define N_CALLBACK_AW(name) name##W
	#define N_STRUCT_AW(name) name##W
#else
	#define N_MACRO_AW(name) name##A
	#define N_FUNC_AW(name) name##A
	#define N_CALLBACK_AW(name) name##A
	#define N_STRUCT_AW(name) name##A
#endif

#ifdef N_WINDOWS_CE
	#define N_NO_ANSI_FUNC
	#ifndef N_UNICODE
		#error "N_UNICODE must be defined under Windows CE"
	#endif
#endif

#if defined(N_NO_ANSI_FUNC) && !defined(N_UNICODE)
	#error "N_NO_ANSI_FUNC defined when N_UNICODE is not defined"
#endif

#ifdef N_64
	typedef NUInt64 NSizeType;

	#define N_SIZE_TYPE_MIN N_UINT64_MIN
	#define N_SIZE_TYPE_MAX N_UINT64_MAX
#else
	#ifdef N_MSVC
		typedef __w64 NUInt32 NSizeType;
	#else
		typedef NUInt32 NSizeType;
	#endif

	#define N_SIZE_TYPE_MIN N_UINT32_MIN
	#define N_SIZE_TYPE_MAX N_UINT32_MAX
#endif

#ifndef N_NO_INT_64
	typedef NInt64 NPosType;

	#define N_POS_TYPE_MIN N_INT64_MIN
	#define N_POS_TYPE_MAX N_INT64_MAX
#else
	#ifdef N_MSVC
		typedef __w64 NInt32 NPosType;
	#else
		typedef NInt32 NPosType;
	#endif

	#define N_POS_TYPE_MIN N_INT32_MIN
	#define N_POS_TYPE_MAX N_INT32_MAX
#endif

#ifndef NULL
	#define NULL 0
#endif

typedef NInt NResult;

#ifndef N_CALL_CONV
	#ifdef N_WINDOWS
		#define N_CALL_CONV __stdcall
	#else
		#define N_CALL_CONV
	#endif
#endif

#define N_API N_NO_THROW N_CALL_CONV
#ifdef N_MSVC
	#define N_API_PTR_RET N_CALL_CONV
#else
	#define N_API_PTR_RET N_NO_THROW N_CALL_CONV
#endif
#define N_CALLBACK N_CALL_CONV *

#ifdef N_MSVC
#define N_UNREFERENCED_PARAMETER(parameter) (parameter)
#else
#define N_UNREFERENCED_PARAMETER(parameter)
#endif

typedef void * NHandle;

#define N_DECLARE_HANDLE(name) typedef struct name##_ { int unused; } * name;

typedef struct NURational_
{
	NUInt Numerator;
	NUInt Denominator;
} NURational;

typedef struct NRational_
{
	NInt Numerator;
	NInt Denominator;
} NRational;

typedef struct NIndexPair_
{
	NInt Index1;
	NInt Index2;
} NIndexPair;

typedef struct NGuid_
{
	NUInt Data1;
	NUShort Data2;
	NUShort Data3;
	NByte Data4[8];
} NGuid;

typedef struct NRange_
{
	NInt From;
	NInt To;
} NRange;

#ifndef N_NO_UNICODE
	NSizeType NWCharGetSize(void);
#endif

#ifdef N_CPP
}
#endif

#endif // !N_TYPES_H_INCLUDED
