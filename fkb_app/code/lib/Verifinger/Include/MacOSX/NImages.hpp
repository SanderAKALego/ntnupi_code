#ifndef N_IMAGES_HPP_INCLUDED
#define N_IMAGES_HPP_INCLUDED

#include <NCore.hpp>
#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <NImages.h>
}}

namespace Neurotec { namespace Images
{

class NImages
{
private:
	NImages()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NImagesGetInfo(pValue));
	}

	static NString GetOpenFileFilter()
	{
		NString fileFilter;
		for (NInt i = 0; i != N_CLASS(NImageFormat)::GetFormats()->GetCount(); i++)
		{
			N_CLASS(NImageFormat) * imageFormat = N_CLASS(NImageFormat)::GetFormats()->Get(i);
			if (imageFormat->CanRead())
			{
				if (NGetStringLength(fileFilter) != 0)
					fileFilter = fileFilter + N_T(';');
				fileFilter = fileFilter + imageFormat->GetFileFilter();
			}
		}
		return fileFilter;
	}

	static NString GetSaveFileFilter()
	{
		NString fileFilter;
		for (NInt i = 0; i != N_CLASS(NImageFormat)::GetFormats()->GetCount(); i++)
		{
			N_CLASS(NImageFormat) * imageFormat = N_CLASS(NImageFormat)::GetFormats()->Get(i);
			if (imageFormat->CanWrite())
			{
				if (NGetStringLength(fileFilter) != 0)
					fileFilter = fileFilter + N_T(';');
				fileFilter = fileFilter + imageFormat->GetFileFilter();
			}
		}
		return fileFilter;
	}

	static NString GetOpenFileFilterString(bool addAllImage, bool addAllFiles)
	{
		NString fileFilter;
		NString fileFormat = GetOpenFileFilter();
		if (addAllImage)
		{
			fileFilter = N_T("All Image Files (") + fileFormat + N_T(")|") + fileFormat;
		}
		for (NInt i = 0; i != N_CLASS(NImageFormat)::GetFormats()->GetCount(); i++)
		{
			N_CLASS(NImageFormat) * imageFormat = N_CLASS(NImageFormat)::GetFormats()->Get(i);
			if (imageFormat->CanRead())
			{
				NString imageFormatFilter = imageFormat->GetFileFilter();
				if (NGetStringLength(fileFilter) != 0)
					fileFilter = fileFilter + N_T('|');
				fileFilter = fileFilter + imageFormat->GetName() + N_T(" Files (") + imageFormatFilter + N_T(")|") + imageFormatFilter;
			}
		}
		if (addAllFiles)
		{
			fileFilter = fileFilter + N_T("|All Files (*.*)|*.*");
		}
		return fileFilter;
	}

	static NString GetSaveFileFilterString()
	{
		NString fileFilter;
		for (NInt i = 0; i != N_CLASS(NImageFormat)::GetFormats()->GetCount(); i++)
		{
			N_CLASS(NImageFormat) * imageFormat = N_CLASS(NImageFormat)::GetFormats()->Get(i);
			if (imageFormat->CanWrite())
			{
				NString imageFormatFilter = imageFormat->GetFileFilter();
				if (NGetStringLength(fileFilter) != 0)
					fileFilter = fileFilter + N_T('|');
				fileFilter = fileFilter + imageFormat->GetName() + N_T(" Files (") + imageFormatFilter + N_T(")|") + imageFormatFilter;
			}
		}
		return fileFilter;
	}

	static N_CLASS(NImage) * GetGrayscaleColorWrapper(N_CLASS(NImage) * pImage, const NRgb & minColor, const NRgb & maxColor)
	{
		if(pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		HNImage hImage;
		NCheck(NImagesGetGrayscaleColorWrapperEx(pImage->GetHandle(), &minColor, &maxColor, &hImage));
		try
		{
			return N_CLASS(NImage)::FromHandle(hImage);
		}
		catch(...)
		{
			NObjectFree(hImage);
			throw;
		}
	}
};

}}

#endif // !N_IMAGES_HPP_INCLUDED
