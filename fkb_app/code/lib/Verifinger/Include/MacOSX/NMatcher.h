#ifndef N_MATCHER_H_INCLUDED
#define N_MATCHER_H_INCLUDED

#include <NfsMatcher.h>
#include <NlsMatcher.h>
#include <NesMatcher.h>
#include <NMMatchDetails.h>
#include <NMatcherParams.h>
#include <NMatchers.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NMatcher)

NResult N_API NMCreate(HNMatcher * pHMatcher);

NResult N_API NMVerify(HNMatcher hMatcher, const void * template1, NSizeType template1Size,
	const void * template2, NSizeType template2Size, NMMatchDetails * * ppMatchDetails, NInt * pScore);
NResult N_API NMIdentifyStart(HNMatcher hMatcher, const void * templ, NSizeType templSize,
	NMMatchDetails * * ppMatchDetails);
NResult N_API NMIdentifyNext(HNMatcher hMatcher, const void * templ, NSizeType templSize,
	NMMatchDetails * pMatchDetails, NInt * pScore);
NResult N_API NMIdentifyEnd(HNMatcher hMatcher);

#define NMFree(hMatcher) NObjectFree(hMatcher)
#define NMCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NMGetParameterA(hMatcher, partId, parameterId, pValue) NTypeGetParameterA(NMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NMGetParameterW(hMatcher, partId, parameterId, pValue) NTypeGetParameterW(NMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NMGetParameter(hMatcher, partId, parameterId, pValue) NTypeGetParameter(NMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NMSetParameterA(hMatcher, partId, parameterId, pValue) NTypeSetParameterA(NMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NMSetParameterW(hMatcher, partId, parameterId, pValue) NTypeSetParameterW(NMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NMSetParameter(hMatcher, partId, parameterId, pValue) NTypeSetParameter(NMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NMReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NMFree", "NMCopyParameters", "NMGetParameterA", "NMGetParameterW", "NMGetParameter", "NMSetParameterA", "NMSetParameterW", "NMSetParameter", "NMReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !N_MATCHER_H_INCLUDED
