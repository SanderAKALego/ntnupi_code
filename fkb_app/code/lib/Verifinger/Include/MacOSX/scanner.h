#ifndef __SCANNER_H__
#define __SCANNER_H__

struct scanner_info
{
	int		dpi;
	const char		*name;
	int		version;		/* driver version */
	void		(*close) (void);
	int		(*init) (void);
	unsigned char *	(*read) (int *width, int *height);
	int		timeout;		/* wait for getting immage */
	int             max_bad_area;           /*  default max bad area   */
	int 		lfd;                    /* live finger detection. 1 - enable, 0 - disable (default: disable) */
};


extern struct scanner_info	scanner_aes4000;
extern struct scanner_info	scanner_af_s2;
extern struct scanner_info	scanner_fx2k;
extern struct scanner_info	scanner_futronic_biolink;
extern struct scanner_info	scanner_shimizu;
extern struct scanner_info      scanner_mbf200;
extern struct scanner_info      scanner_fm200;
extern struct scanner_info      scanner_hamster3;
extern struct scanner_info	scanner_fam;
extern struct scanner_info	scanner_zf1;
extern struct scanner_info	scanner_enbsp;
extern struct scanner_info	scanner_upek;
extern struct scanner_info	scanner_uareu;
extern struct scanner_info	scanner_biomini;
extern struct scanner_info	scanner_lumidigm;

#endif
