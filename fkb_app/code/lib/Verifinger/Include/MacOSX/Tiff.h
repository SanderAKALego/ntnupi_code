#ifndef TIFF_H_INCLUDED
#define TIFF_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API TiffLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API TiffLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API TiffLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define TiffLoadImageFromFile N_FUNC_AW(TiffLoadImageFromFile)

NResult N_API TiffLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API TiffLoadImageFromStream(HNStream hStream, HNImage * pHImage);

#ifdef N_CPP
}
#endif

#endif // !TIFF_H_INCLUDED
