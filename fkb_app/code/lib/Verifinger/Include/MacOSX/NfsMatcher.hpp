#ifndef NFS_MATCHER_HPP_INCLUDED
#define NFS_MATCHER_HPP_INCLUDED

#include <NFMatcher.hpp>
#include <NfsmMatchDetails.hpp>
#include <NfsMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NfsMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NfsMatcher) : public N_CLASS(NObject)
{
private:
	static HNfsMatcher Create(bool isPalm)
	{
		HNfsMatcher handle;
		NCheck(NfsmCreateEx(isPalm, &handle));
		return handle;
	}

public:
	static N_CLASS(NfsMatcher) * FromHandle(HNfsMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NfsMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NfsMatcher)(HNfsMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NfsMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NfsMatcher)(bool isPalm = false)
		: N_CLASS(NObject)(Create(isPalm), N_NATIVE_TYPE_OF(NfsMatcher), true)
	{
	}

	NInt Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size,
		NfsmMatchDetails * * ppMatchDetails = NULL)
	{
		NInt score;
		NCheck(NfsmVerify((HNfsMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			ppMatchDetails, &score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize,
		NfsmMatchDetails * * ppMatchDetails = NULL)
	{
		NCheck(NfsmIdentifyStart((HNfsMatcher)GetHandle(), pTemplate, templateSize, ppMatchDetails));
	}

	NInt IdentifyNext(const void * pTemplate, NSizeType templateSize, NfsmMatchDetails * pMatchDetails)
	{
		NInt score;
		NCheck(NfsmIdentifyNext((HNfsMatcher)GetHandle(), pTemplate, templateSize, pMatchDetails, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NfsmIdentifyEnd((HNfsMatcher)GetHandle()));
	}

	NBool IsPalm()
	{
		NBool value;
		NCheck(NfsmIsPalm((HNfsMatcher)GetHandle(), &value));
		return value;
	}

	NInt GetMatchingThreshold()
	{
		return GetParameterAsInt32(NFSM_PART_NONE, NFSMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NInt value)
	{
		SetParameter(NFSM_PART_NONE, NFSMP_MATCHING_THRESHOLD, value);
	}

	NInt GetMinMatchedFingerCount()
	{
		return GetParameterAsInt32(NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT);
	}

	void SetMinMatchedFingerCount(NInt value)
	{
		SetParameter(NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT, value);
	}

	NInt GetMinMatchedFingerCountThreshold()
	{
		return GetParameterAsInt32(NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT_THRESHOLD);
	}

	void SetMinMatchedFingerCountThreshold(NInt value)
	{
		SetParameter(NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT_THRESHOLD, value);
	}

	NByte GetNfmMaximalRotation()
	{
		return GetParameterAsByte(NFSM_PART_NFM, NFMP_MAXIMAL_ROTATION);
	}

	void SetNfmMaximalRotation(NByte value)
	{
		SetParameter(NFSM_PART_NFM, NFMP_MAXIMAL_ROTATION, value);
	}

	NfmSpeed GetNfmMatchingSpeed()
	{
		return (NfmSpeed)GetParameterAsInt32(NFSM_PART_NFM, NFMP_MATCHING_SPEED);
	}

	void SetNfmMatchingSpeed(NfmSpeed value)
	{
		SetParameter(NFSM_PART_NFM, NFMP_MATCHING_SPEED, value);
	}

	NUInt GetMode()
	{
		return GetParameterAsUInt32(NFSM_PART_NFM, NFMP_MODE);
	}

	void SetMode(NUInt value)
	{
		SetParameter(NFSM_PART_NFM, NFMP_MODE, value);
	}

	N_DECLARE_OBJECT_TYPE(NfsMatcher)
};

}}

#endif // !NFS_MATCHER_HPP_INCLUDED
