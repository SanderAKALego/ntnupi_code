#ifndef IHEAD_HPP_INCLUDED
#define IHEAD_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <IHead.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(IHead)
{
private:
	N_CLASS(IHead)() {}
	N_CLASS(IHead)(const N_CLASS(IHead)&) {}

public:
	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(IHeadLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(IHeadLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(IHeadSaveImageToFile(pImage->GetHandle(), szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NString & fileName)
	{
		return SaveImage(pImage, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(IHeadSaveImageToMemory(pImage->GetHandle(), pBuffer, pBufferLength));
	}
};

}}

#endif // !IHEAD_HPP_INCLUDED
