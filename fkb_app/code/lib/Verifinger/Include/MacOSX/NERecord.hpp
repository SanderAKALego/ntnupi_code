#ifndef NE_RECORD_HPP_INCLUDED
#define NE_RECORD_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NERecord.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NERecord) : public N_CLASS(NObject)
{
private:
	static HNERecord Create(NUShort width, NUShort height, NUInt flags)
	{
		HNERecord handle;
		NCheck(NERecordCreate(width, height, flags, &handle));
		return handle;
	}

	static HNERecord Create(const void *pBuffer, NSizeType bufferSize, NUInt flags, NERecordInfo *pInfo)
	{
		HNERecord handle;
		NCheck(NERecordCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

public:
	static N_CLASS(NERecord)* FromHandle(HNERecord handle, bool ownsHandle = true)
	{
		return new N_CLASS(NERecord)(handle, ownsHandle);
	}

	static NUShort GetWidth(const void *pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NERecordGetWidthMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetHeight(const void *pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NERecordGetHeightMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NEPosition GetPosition(const void *pBuffer, NSizeType bufferSize)
	{
		NEPosition value;
		NCheck(NERecordGetPositionMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NByte GetQuality(const void *pBuffer, NSizeType bufferSize)
	{
		NByte value;
		NCheck(NERecordGetQualityMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NUShort GetCbeffProductType(const void *pBuffer, NSizeType bufferSize)
	{
		NUShort value;
		NCheck(NERecordGetCbeffProductTypeMem(pBuffer, bufferSize, &value));
		return value;
	}

	static void Check(const void *pBuffer, NSizeType bufferSize)
	{
		NCheck(NERecordCheck(pBuffer, bufferSize));
	}

	explicit N_CLASS(NERecord)(HNERecord handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NERecord), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NERecord)(NUShort width, NUShort height, NUInt flags = 0)
		: N_CLASS(NObject)(Create(width, height, flags), N_NATIVE_TYPE_OF(NERecord), true)
	{
	}

	N_CLASS(NERecord)(const void *pBuffer, NSizeType bufferSize, NUInt flags = 0, NERecordInfo *pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NERecord), true)
	{
	}

	N_CLASS(NObject) * Clone() const
	{
		HNERecord hRecord;
		NCheck(NERecordClone((HNERecord)GetHandle(), &hRecord));
		try
		{
			return FromHandle(hRecord);
		}
		catch(...)
		{
			NObjectFree(hRecord);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType size;
		NCheck(NERecordGetSize((HNERecord)GetHandle(), flags, &size));
		return size;
	}

	NSizeType Save(void *pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType size;
		NCheck(NERecordSaveToMemory((HNERecord)GetHandle(), pBuffer, bufferSize, flags, &size));
		return size;
	}

	NUShort GetWidth() const
	{
		NUShort value;
		NCheck(NERecordGetWidth((HNERecord)GetHandle(), &value));
		return value;
	}

	NUShort GetHeight() const
	{
		NUShort value;
		NCheck(NERecordGetHeight((HNERecord)GetHandle(), &value));
		return value;
	}

	NEPosition GetPosition() const
	{
		NEPosition value;
		NCheck(NERecordGetPosition((HNERecord)GetHandle(), &value));
		return value;
	}

	void SetPosition(NEPosition value)
	{
		NCheck(NERecordSetPosition((HNERecord)GetHandle(), value));
	}

	NByte GetQuality() const
	{
		NByte value;
		NCheck(NERecordGetQuality((HNERecord)GetHandle(), &value));
		return value;
	}

	void SetQuality(NByte value)
	{
		NCheck(NERecordSetQuality((HNERecord)GetHandle(), value));
	}

	NUShort GetCbeffProductType() const
	{
		NUShort value;
		NCheck(NERecordGetCbeffProductType((HNERecord)GetHandle(), &value));
		return value;
	}

	void SetCbeffProductType(NUShort value)
	{
		NCheck(NERecordSetCbeffProductType((HNERecord)GetHandle(), value));
	}

	N_DECLARE_OBJECT_TYPE(NERecord)
};

}}

#endif // !NE_RECORD_HPP_INCLUDED
