#ifndef NE_RECORD_H_INCLUDED
#define NE_RECORD_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef enum NEPosition_
{
	nepUnknown = 0,
	nepRight = 1,
	nepLeft = 2,
} NEPosition;

DECLARE_N_OBJECT_HANDLE(NERecord)

NResult N_API NERecordCheck(const void *pBuffer, NSizeType bufferSize);

typedef struct NERecordInfo_
{
	NByte MajorVersion;
	NByte MinorVersion;
	NUInt Size;
	NByte HeaderSize;
} NERecordInfo;

void N_API NERecordInfoDispose(NERecordInfo *pInfo);

NResult N_API NERecordCreate(NUShort width, NUShort height,
	NUInt flags, HNERecord *pHRecord);

NResult N_API NERecordCreateFromMemory(const void *pBuffer, NSizeType bufferSize,
	NUInt flags, NERecordInfo *pInfo, HNERecord *pHRecord);

NResult N_API NERecordClone(HNERecord hRecord, HNERecord *pHClonedRecord);
NResult N_API NERecordGetSize(HNERecord hRecord, NUInt flags, NSizeType *pSize);
NResult N_API NERecordSaveToMemory(HNERecord hRecord,
	void *pBuffer, NSizeType bufferSize, NUInt flags, NSizeType *pSize);

NResult N_API NERecordGetWidthMem(const void *pBuffer, NSizeType bufferSize, NUShort *pValue);
NResult N_API NERecordGetHeightMem(const void *pBuffer, NSizeType bufferSize, NUShort *pValue);
NResult N_API NERecordGetPositionMem(const void *pBuffer, NSizeType bufferSize, NEPosition *pValue);
NResult N_API NERecordGetQualityMem(const void *pBuffer, NSizeType bufferSize, NByte *pValue);
NResult N_API NERecordGetCbeffProductTypeMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);

NResult N_API NERecordGetWidth(HNERecord hRecord, NUShort *pValue);
NResult N_API NERecordGetHeight(HNERecord hRecord, NUShort *pValue);
NResult N_API NERecordGetPosition(HNERecord hRecord, NEPosition *pValue);
NResult N_API NERecordSetPosition(HNERecord hRecord, NEPosition value);
NResult N_API NERecordGetQuality(HNERecord hRecord, NByte *pValue);
NResult N_API NERecordSetQuality(HNERecord hRecord, NByte value);
NResult N_API NERecordGetCbeffProductType(HNERecord hRecord, NUShort *pValue);
NResult N_API NERecordSetCbeffProductType(HNERecord hRecord, NUShort value);

#define NERecordFree(hRecord) NObjectFree(hRecord)

#ifdef N_MSVC
	#pragma deprecated("NERecordFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !NE_RECORD_H_INCLUDED
