#ifndef N_CORE_HPP_INCLUDED
#define N_CORE_HPP_INCLUDED

#include <NTypes.hpp>
#include <NMemory.hpp>
#include <NObject.hpp>
#include <NErrors.hpp>
#include <NStream.hpp>
#include <NEvent.hpp>
#include <NLibraryInfo.hpp>
namespace Neurotec
{
#include <NCore.h>
}

namespace Neurotec
{

class NCore
{
private:
	NCore()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NCoreGetInfo(pValue));
	}
};

inline NChar * NGetStringBufferWithCheck(NString & value, NResult result)
{
	return NGetStringBuffer(value, NCheck(result));
}

inline NString & NReleaseStringBufferWithCheck(NString & value, NResult result)
{
	return NReleaseStringBuffer(value, NCheck(result));
}

typedef NResult (N_CALLBACK NGetStringProc)(NChar * pValue);
typedef NResult (N_CALLBACK NGetStringWithIndexProc)(NInt index, NChar * pValue);
typedef NResult (N_CALLBACK NGetStringWithHandleProc)(HNObject handle, NChar * pValue);
typedef NResult (N_CALLBACK NGetStringWithHandleAndIndexProc)(HNObject handle, NInt index, NChar * pValue);

inline NString & NGetString(NString & value, NGetStringProc pGetStringProc)
{
	return NReleaseStringBufferWithCheck(value, pGetStringProc(NGetStringBufferWithCheck(value, pGetStringProc(NULL))));
}

inline NString & NGetString(NString & value, NGetStringWithIndexProc pGetStringProc, NInt index)
{
	return NReleaseStringBufferWithCheck(value, pGetStringProc(index, NGetStringBufferWithCheck(value, pGetStringProc(index, NULL))));
}

inline NString & NGetString(NString & value, NGetStringWithHandleProc pGetStringProc, N_CLASS(NObject) * pObj)
{
	HNObject hObj = pObj->GetHandle();
	return NReleaseStringBufferWithCheck(value, pGetStringProc(hObj, NGetStringBufferWithCheck(value, pGetStringProc(hObj, NULL))));
}

inline NString & NGetString(NString & value, NGetStringWithHandleAndIndexProc pGetStringProc, N_CLASS(NObject) * pObj, NInt index)
{
	HNObject hObj = pObj->GetHandle();
	return NReleaseStringBufferWithCheck(value, pGetStringProc(hObj, index, NGetStringBufferWithCheck(value, pGetStringProc(hObj, index, NULL))));
}

}

#include <NCollections.hpp>
#include <NProcessorInfo.hpp>

#endif // !N_CORE_HPP_INCLUDED
