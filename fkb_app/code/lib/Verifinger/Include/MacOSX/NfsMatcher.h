#ifndef NFS_MATCHER_H_INCLUDED
#define NFS_MATCHER_H_INCLUDED

#include <NFMatcher.h>
#include <NfsmMatchDetails.h>
#include <NfsMatcherParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NfsMatcher)

NResult N_API NfsmCreate(HNfsMatcher * pHMatcher);
NResult N_API NfsmCreateEx(NBool isPalm, HNfsMatcher * pHMatcher);

NResult N_API NfsmVerify(HNfsMatcher hMatcher, const void * template1, NSizeType template1Size,
	const void * template2, NSizeType template2Size, NfsmMatchDetails * * ppMatchDetails, NInt * pScore);
NResult N_API NfsmIdentifyStart(HNfsMatcher hMatcher, const void * templ, NSizeType templSize,
	NfsmMatchDetails * * ppMatchDetails);
NResult N_API NfsmIdentifyNext(HNfsMatcher hMatcher, const void * templ, NSizeType templSize,
	NfsmMatchDetails * pMatchDetails, NInt * pScore);
NResult N_API NfsmIdentifyEnd(HNfsMatcher hMatcher);

NResult N_API NfsmIsPalm(HNfsMatcher hMatcher, NBool * pValue);

#define NfsmFree(hMatcher) NObjectFree(hMatcher)
#define NfsmCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NfsmGetParameterA(hMatcher, partId, parameterId, pValue) NTypeGetParameterA(NfsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NfsmGetParameterW(hMatcher, partId, parameterId, pValue) NTypeGetParameterW(NfsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NfsmGetParameter(hMatcher, partId, parameterId, pValue) NTypeGetParameter(NfsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NfsmSetParameterA(hMatcher, partId, parameterId, pValue) NTypeSetParameterA(NfsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NfsmSetParameterW(hMatcher, partId, parameterId, pValue) NTypeSetParameterW(NfsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NfsmSetParameter(hMatcher, partId, parameterId, pValue) NTypeSetParameter(NfsMatcherTypeOf(), hMatcher, partId, parameterId, pValue)
#define NfsmReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NfsmFree", "NfsmCopyParameters", "NfsmGetParameterA", "NfsmGetParameterW", "NfsmGetParameter", "NfsmSetParameterA", "NfsmSetParameterW", "NfsmSetParameter", "NfsmReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NFS_MATCHER_H_INCLUDED
