#ifndef N_CORE_LIBRARY_CPP_INCLUDED
#define N_CORE_LIBRARY_CPP_INCLUDED

#include <NCore.hpp>
#include <NGeometry.hpp>
#include <NParameters.hpp>

namespace Neurotec
{

const N_CLASS(NNativeType) N_CLASS(NNativeType)::Null(NULL);

#if defined(N_FRAMEWORK_MFC)
	N_IMPLEMENT_ABSTRACT_TYPE(NException, Exception)
	N_IMPLEMENT_ABSTRACT_OBJECT_TYPE(NObject, Object)
#elif defined(N_FRAMEWORK_WX)
	N_IMPLEMENT_ABSTRACT_TYPE(NException, Object)
	N_IMPLEMENT_ABSTRACT_OBJECT_TYPE(NObject, Object)
#else
	const NType NObjectBase::typeNObjectBase = { N_T("NObjectBase"), NULL, true, sizeof(NObjectBase) };
	N_IMPLEMENT_ABSTRACT_TYPE(NException, NObjectBase)
	N_IMPLEMENT_ABSTRACT_OBJECT_TYPE(NObject, NObjectBase)
#endif

N_IMPLEMENT_TYPE(NDummyObject, NObject)

N_IMPLEMENT_TYPE(NCoreException, NException)
N_IMPLEMENT_TYPE(NArgumentException, NCoreException)
N_IMPLEMENT_TYPE(NArgumentNullException, NArgumentException)
N_IMPLEMENT_TYPE(NArgumentOutOfRangeException, NArgumentException)
N_IMPLEMENT_TYPE(NInvalidEnumArgumentException, NArgumentException)
N_IMPLEMENT_TYPE(NArithmeticException, NCoreException)
N_IMPLEMENT_TYPE(NOverflowException, NArithmeticException)
N_IMPLEMENT_TYPE(NFormatException, NCoreException)
N_IMPLEMENT_TYPE(NIndexOutOfRangeException, NCoreException)
N_IMPLEMENT_TYPE(NInvalidCastException, NCoreException)
N_IMPLEMENT_TYPE(NInvalidOperationException, NCoreException)
namespace IO
{
N_IMPLEMENT_TYPE(NIOException, NCoreException)
N_IMPLEMENT_TYPE(NDirectoryNotFoundException, NIOException)
N_IMPLEMENT_TYPE(NDriveNotFoundException, NIOException)
N_IMPLEMENT_TYPE(NEndOfStreamException, NIOException)
N_IMPLEMENT_TYPE(NFileNotFoundException, NIOException)
N_IMPLEMENT_TYPE(NFileLoadException, NIOException)
N_IMPLEMENT_TYPE(NPathTooLongException, NIOException)
}
N_IMPLEMENT_TYPE(NNotImplementedException, NCoreException)
N_IMPLEMENT_TYPE(NNotSupportedException, NCoreException)
N_IMPLEMENT_TYPE(NNullReferenceException, NCoreException)
N_IMPLEMENT_TYPE(NOutOfMemoryException, NCoreException)
namespace Security
{
N_IMPLEMENT_TYPE(NSecurityException, NCoreException)
}
N_IMPLEMENT_TYPE(NExternalException, NCoreException)
N_IMPLEMENT_TYPE(NClrException, NExternalException)
N_IMPLEMENT_TYPE(NComException, NExternalException)
N_IMPLEMENT_TYPE(NSysException, NExternalException)
N_IMPLEMENT_TYPE(NWin32Exception, NExternalException)
N_IMPLEMENT_TYPE(NParameterException, NCoreException)
N_IMPLEMENT_TYPE(NParameterReadOnlyException, NParameterException)
N_IMPLEMENT_TYPE(NNotActivatedException, NCoreException)
N_IMPLEMENT_TYPE(NeurotecException, NCoreException)

}

#endif // !N_CORE_LIBRARY_CPP_INCLUDED
