#ifndef NE_EXTRACTOR_HPP_INCLUDED
#define NE_EXTRACTOR_HPP_INCLUDED

#include <NGrayscaleImage.hpp>
#include <NGeometry.hpp>
#include <NERecord.hpp>
#include <NEExtractorParams.hpp>
namespace Neurotec { namespace Biometrics
{
using Neurotec::Images::HNGrayscaleImage;
using Neurotec::Drawing::NPoint;
#include <NEExtractor.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NEExtractor) : public N_CLASS(NObject)
{
private:
	static HNEExtractor Create()
	{
		HNEExtractor handle;
		NCheck(NeeCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NEExtractor) * FromHandle(HNEExtractor handle, bool ownsHandle = true)
	{
		return new N_CLASS(NEExtractor)(handle, ownsHandle);
	}

	explicit N_CLASS(NEExtractor)(HNEExtractor handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NEExtractor), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NEExtractor)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NEExtractor), true)
	{
	}

	N_CLASS(NERecord) * Extract(const Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NEPosition position, NeeSegmentationDetails * pDetails, NeeExtractionStatus * pExtractionStatus)
	{
		HNERecord hTemplate;
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(NeeExtract(GetHandle(), pImage->GetHandle(), position, pDetails, pExtractionStatus, &hTemplate));
		try
		{
			return ((hTemplate == NULL) ? NULL : N_CLASS(NERecord)::FromHandle(hTemplate));
		}
		catch(...)
		{
			NObjectFree(hTemplate);
			throw;
		}
	}

	N_CLASS(NERecord) * Extract(const Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NEPosition position, NeeExtractionStatus * pExtractionStatus)
	{
		return Extract(pImage, position, NULL, pExtractionStatus);
	}

	bool GetDeinterlace()
	{
		return GetParameterAsBoolean(NEEP_DEINTERLACE);
	}

	void SetDeinterlace(bool value)
	{
		SetParameter(NEEP_DEINTERLACE, value);
	}

	NInt GetInnerBoundaryFrom()
	{
		return GetParameterAsInt32(NEEP_INNER_BOUNDARY_FROM);
	}

	void SetInnerBoundaryFrom(NInt value)
	{
		SetParameter(NEEP_INNER_BOUNDARY_FROM, value);
	}

	NInt GetInnerBoundaryTo()
	{
		return GetParameterAsInt32(NEEP_INNER_BOUNDARY_TO);
	}

	void SetInnerBoundaryTo(NInt value)
	{
		SetParameter(NEEP_INNER_BOUNDARY_TO, value);
	}

	NInt GetOuterBoundaryFrom()
	{
		return GetParameterAsInt32(NEEP_OUTER_BOUNDARY_FROM);
	}

	void SetOuterBoundaryFrom(NInt value)
	{
		SetParameter(NEEP_OUTER_BOUNDARY_FROM, value);
	}

	NInt GetOuterBoundaryTo()
	{
		return GetParameterAsInt32(NEEP_OUTER_BOUNDARY_TO);
	}

	void SetOuterBoundaryTo(NInt value)
	{
		SetParameter(NEEP_OUTER_BOUNDARY_TO, value);
	}

	N_DECLARE_OBJECT_TYPE(NEExtractor)
};

}}

#endif // !NE_EXTRACTOR_HPP_INCLUDED
