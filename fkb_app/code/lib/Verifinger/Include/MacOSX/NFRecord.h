#ifndef NF_RECORD_H_INCLUDED
#define NF_RECORD_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef enum NFPosition_
{
	nfpUnknown = 0,
	nfpRightThumb = 1,
	nfpRightIndex = 2,
	nfpRightMiddle = 3,
	nfpRightRing = 4,
	nfpRightLittle = 5,
	nfpLeftThumb = 6,
	nfpLeftIndex = 7,
	nfpLeftMiddle = 8,
	nfpLeftRing = 9,
	nfpLeftLittle = 10,
	nfpPlainRightThumb = 11,
	nfpPlainLeftThumb = 12,
	nfpPlainRightFourFingers = 13,
	nfpPlainLeftFourFingers = 14,
	nfpPlainThumbs = 15,
	nfpUnknownPalm = 20,
	nfpRightFullPalm = 21,
	nfpRightWritersPalm = 22,
	nfpLeftFullPalm = 23,
	nfpLeftWritersPalm = 24,
	nfpRightLowerPalm = 25,
	nfpRightUpperPalm = 26,
	nfpLeftLowerPalm = 27,
	nfpLeftUpperPalm = 28,
	nfpRightOther = 29,
	nfpLeftOther = 30,
	nfpRightInterdigital = 31,
	nfpRightThenar = 32,
	nfpRightHypothenar = 33,
	nfpLeftInterdigital = 34,
	nfpLeftThenar = 35,
	nfpLeftHypothenar = 36
} NFPosition;

typedef enum NFImpressionType_
{
	nfitLiveScanPlain = 0,
	nfitLiveScanRolled = 1,
	nfitNonliveScanPlain = 2,
	nfitNonliveScanRolled = 3,
	nfitLatentImpression = 4,
	nfitLatentTracing = 5,
	nfitLatentPhoto = 6,
	nfitLatentLift = 7,
	nfitSwipe = 8,
	nfitLiveScanContactless = 9,
	nfitLiveScanPalm = 10,
	nfitNonliveScanPalm = 11,
	nfitLatentPalmImpression = 12,
	nfitLatentPalmTracing = 13,
	nfitLatentPalmPhoto = 14,
	nfitLatentPalmLift = 15
} NFImpressionType;

typedef enum NFPatternClass_
{
	nfpcUnknown = 0,
	nfpcPlainArch = 1,
	nfpcTentedArch = 2,
	nfpcRadialLoop = 3,
	nfpcUlnarLoop = 4,
	nfpcPlainWhorl = 5,
	nfpcCentralPocketLoop = 6,
	nfpcDoubleLoop = 7,
	nfpcAccidentalWhorl = 8,
	nfpcWhorl = 9,
	nfpcRightSlantLoop = 10,
	nfpcLeftSlantLoop = 11,
	nfpcScar = 12,
	nfpcAmputation = 15
} NFPatternClass;

#define NFR_RESOLUTION 500

#define NFR_MAX_FINGER_DIMENSION 2047

#define NFR_MAX_FINGER_MINUTIA_COUNT     255
#define NFR_MAX_FINGER_CORE_COUNT         15
#define NFR_MAX_FINGER_DELTA_COUNT        15
#define NFR_MAX_FINGER_DOUBLE_CORE_COUNT  15

#define NFR_MAX_PALM_DIMENSION 16383

#define NFR_MAX_PALM_MINUTIA_COUNT     65535
#define NFR_MAX_PALM_CORE_COUNT          255
#define NFR_MAX_PALM_DELTA_COUNT         255
#define NFR_MAX_PALM_DOUBLE_CORE_COUNT   255

#define NFR_MAX_DIMENSION NFR_MAX_FINGER_DIMENSION

#define NFR_MAX_MINUTIA_COUNT     NFR_MAX_FINGER_MINUTIA_COUNT
#define NFR_MAX_CORE_COUNT        NFR_MAX_FINGER_CORE_COUNT
#define NFR_MAX_DELTA_COUNT       NFR_MAX_FINGER_DELTA_COUNT
#define NFR_MAX_DOUBLE_CORE_COUNT NFR_MAX_FINGER_DOUBLE_CORE_COUNT

#ifdef N_MSVC
#pragma deprecated("NFR_MAX_DIMENSION", "NFR_MAX_MINUTIA_COUNT", "NFR_MAX_CORE_COUNT", "NFR_MAX_DELTA_COUNT", "NFR_MAX_DOUBLE_CORE_COUNT")
#endif

typedef enum NFMinutiaFormat_
{
	nfmfNone = 0,
	nfmfHasQuality = 1,
	nfmfHasCurvature = 2,
	nfmfHasG = 4
} NFMinutiaFormat;

typedef enum NFMinutiaType_
{
	nfmtUnknown = 0,
	nfmtEnd = 1,
	nfmtBifurcation = 2,
	nfmtOther = 3
} NFMinutiaType;

typedef struct NFMinutia_
{
	NUShort X;
	NUShort Y;
	NFMinutiaType Type;
	NByte Angle;
	NByte Quality;
	NByte Curvature;
	NByte G;
} NFMinutia;

typedef enum NFRidgeCountsType_
{
	nfrctNone = 0,
	nfrctFourNeighbors = 1,
	nfrctEightNeighbors = 2,
	nfrctFourNeighborsWithIndexes = 5,
	nfrctEightNeighborsWithIndexes = 6,
	nfrctUnspecified = 128 + 4
} NFRidgeCountsType;

typedef struct NFMinutiaNeighbor_
{
	NInt Index;
	NByte RidgeCount;
} NFMinutiaNeighbor;

typedef struct NFCore_
{
	NUShort X;
	NUShort Y;
	NInt Angle;
} NFCore;

typedef struct NFDelta_
{
	NUShort X;
	NUShort Y;
	NInt Angle1;
	NInt Angle2;
	NInt Angle3;
} NFDelta;

typedef struct NFDoubleCore_
{
	NUShort X;
	NUShort Y;
} NFDoubleCore;

DECLARE_N_OBJECT_HANDLE(NFRecord)

NResult N_API NFRecordGetMaxSize(NInt version, NBool isPalm, NFMinutiaFormat minutiaFormat, NInt minutiaCount, NFRidgeCountsType ridgeCountsType,
	NInt coreCount, NInt deltaCount, NInt doubleCoreCount, NInt boWidth, NInt boHeight, NSizeType * pSize);
NResult N_API NFRecordCheck(const void * pBuffer, NSizeType bufferSize);
NResult N_API NFRecordGetWidthMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);
NResult N_API NFRecordGetHeightMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);
NResult N_API NFRecordGetHorzResolutionMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);
NResult N_API NFRecordGetVertResolutionMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);
NResult N_API NFRecordGetPositionMem(const void * pBuffer, NSizeType bufferSize, NFPosition * pValue);
NResult N_API NFRecordGetImpressionTypeMem(const void * pBuffer, NSizeType bufferSize, NFImpressionType * pValue);
NResult N_API NFRecordGetPatternClassMem(const void * pBuffer, NSizeType bufferSize, NFPatternClass * pValue);
NResult N_API NFRecordGetQualityMem(const void * pBuffer, NSizeType bufferSize, NByte * pValue);
NResult N_API NFRecordGetGMem(const void * pBuffer, NSizeType bufferSize, NByte * pValue);
NResult N_API NFRecordGetCbeffProductTypeMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);

typedef struct NFRecordInfo_
{
	NByte MajorVersion;
	NByte MinorVersion;
	NUInt Size;
	NUShort HeaderSize;
} NFRecordInfo;

void N_API NFRecordInfoDispose(NFRecordInfo * pInfo);

#ifndef NF_RECORD_EX_H_INCLUDED
typedef enum NFMinutiaOrder_
{
	nfmoAscending = 0x01,
	nfmoDescending = 0x02,
	nfmoCartesianXY = 0x04,
	nfmoCartesianYX = 0x08,
	nfmoAngle = 0x0C,
	nfmoPolar = 0x10
} NFMinutiaOrder;
#endif

#define NFR_SKIP_RIDGE_COUNTS            0x00010000
#define NFR_SKIP_SINGULAR_POINTS         0x00020000
#define NFR_SKIP_BLOCKED_ORIENTS         0x00040000
#define NFR_SAVE_BLOCKED_ORIENTS         0x00040000
#define NFR_ALLOW_OUT_OF_BOUNDS_FEATURES 0x00080000
#define NFR_SKIP_QUALITIES               0x00100000
#define NFR_SKIP_CURVATURES              0x00200000
#define NFR_SKIP_GS                      0x00400000
#define NFR_SAVE_V2                      0x20000000
#define NFR_SAVE_V3                      0x30000000

NResult N_API NFRecordCreate(NUShort width, NUShort height,
	NUShort horzResolution, NUShort vertResolution, NUInt flags, HNFRecord * pHRecord);
NResult N_API NFRecordCreateEx(NBool isPalm, NUShort width, NUShort height,
	NUShort horzResolution, NUShort vertResolution, NUInt flags, HNFRecord * pHRecord);
NResult N_API NFRecordCreateFromMemory(const void * pBuffer, NSizeType bufferSize,
	NUInt flags, NFRecordInfo * pInfo, HNFRecord * pHRecord);

NResult N_API NFRecordGetMinutiaCount(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordGetMinutia(HNFRecord hRecord, NInt index, NFMinutia * pValue);
NResult N_API NFRecordSetMinutia(HNFRecord hRecord, NInt index, const NFMinutia * pValue);
NResult N_API NFRecordGetMinutiae(HNFRecord hRecord, NFMinutia * arMinutiae);
NResult N_API NFRecordGetMinutiaCapacity(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordSetMinutiaCapacity(HNFRecord hRecord, NInt value);
NResult N_API NFRecordAddMinutia(HNFRecord hRecord, const NFMinutia * pValue);
NResult N_API NFRecordInsertMinutia(HNFRecord hRecord, NInt index, const NFMinutia * pValue);
NResult N_API NFRecordRemoveMinutia(HNFRecord hRecord, NInt index);
NResult N_API NFRecordClearMinutiae(HNFRecord hRecord);

NResult N_API NFRecordGetMinutiaNeighborCount(HNFRecord hRecord, NInt minutiaIndex, NInt * pValue);
NResult N_API NFRecordGetMinutiaNeighbor(HNFRecord hRecord, NInt minutiaIndex, NInt index, NFMinutiaNeighbor * pValue);
NResult N_API NFRecordSetMinutiaNeighbor(HNFRecord hRecord, NInt minutiaIndex, NInt index, const NFMinutiaNeighbor * pValue);
NResult N_API NFRecordGetMinutiaNeighbors(HNFRecord hRecord, NInt minutiaIndex, NFMinutiaNeighbor * arMinutiaNeighbors);

NResult N_API NFRecordGetCoreCount(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordGetCore(HNFRecord hRecord, NInt index, NFCore * pValue);
NResult N_API NFRecordSetCore(HNFRecord hRecord, NInt index, const NFCore * pValue);
NResult N_API NFRecordGetCores(HNFRecord hRecord, NFCore * arCores);
NResult N_API NFRecordGetCoreCapacity(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordSetCoreCapacity(HNFRecord hRecord, NInt value);
NResult N_API NFRecordAddCore(HNFRecord hRecord, const NFCore * pValue);
NResult N_API NFRecordInsertCore(HNFRecord hRecord, NInt index, const NFCore * pValue);
NResult N_API NFRecordRemoveCore(HNFRecord hRecord, NInt index);
NResult N_API NFRecordClearCores(HNFRecord hRecord);

NResult N_API NFRecordGetDeltaCount(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordGetDelta(HNFRecord hRecord, NInt index, NFDelta * pValue);
NResult N_API NFRecordSetDelta(HNFRecord hRecord, NInt index, const NFDelta * pValue);
NResult N_API NFRecordGetDeltas(HNFRecord hRecord, NFDelta * arDeltas);
NResult N_API NFRecordGetDeltaCapacity(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordSetDeltaCapacity(HNFRecord hRecord, NInt value);
NResult N_API NFRecordAddDelta(HNFRecord hRecord, const NFDelta * pValue);
NResult N_API NFRecordInsertDelta(HNFRecord hRecord, NInt index, const NFDelta * pValue);
NResult N_API NFRecordRemoveDelta(HNFRecord hRecord, NInt index);
NResult N_API NFRecordClearDeltas(HNFRecord hRecord);

NResult N_API NFRecordGetDoubleCoreCount(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordGetDoubleCore(HNFRecord hRecord, NInt index, NFDoubleCore * pValue);
NResult N_API NFRecordSetDoubleCore(HNFRecord hRecord, NInt index, const NFDoubleCore * pValue);
NResult N_API NFRecordGetDoubleCores(HNFRecord hRecord, NFDoubleCore * arDoubleCores);
NResult N_API NFRecordGetDoubleCoreCapacity(HNFRecord hRecord, NInt * pValue);
NResult N_API NFRecordSetDoubleCoreCapacity(HNFRecord hRecord, NInt value);
NResult N_API NFRecordAddDoubleCore(HNFRecord hRecord, const NFDoubleCore * pValue);
NResult N_API NFRecordInsertDoubleCore(HNFRecord hRecord, NInt index, const NFDoubleCore * pValue);
NResult N_API NFRecordRemoveDoubleCore(HNFRecord hRecord, NInt index);
NResult N_API NFRecordClearDoubleCores(HNFRecord hRecord);

NResult N_API NFRecordClone(HNFRecord hRecord, HNFRecord * pHClonedRecord);
NResult N_API NFRecordGetSize(HNFRecord hRecord, NUInt flags, NSizeType * pSize);
NResult N_API NFRecordSaveToMemory(HNFRecord hRecord, void * pBuffer, NSizeType bufferSize, NUInt flags, NSizeType * pSize);
NResult N_API NFRecordSortMinutiae(HNFRecord hRecord, NFMinutiaOrder order);
NResult N_API NFRecordTruncateMinutiaeByQuality(HNFRecord hRecord, NByte threshold, NInt maxCount);
NResult N_API NFRecordTruncateMinutiae(HNFRecord hRecord, NInt maxCount);

NResult N_API NFRecordGetWidth(HNFRecord hRecord, NUShort * pValue);
NResult N_API NFRecordGetHeight(HNFRecord hRecord, NUShort * pValue);
NResult N_API NFRecordGetHorzResolution(HNFRecord hRecord, NUShort * pValue);
NResult N_API NFRecordGetVertResolution(HNFRecord hRecord, NUShort * pValue);
NResult N_API NFRecordGetPosition(HNFRecord hRecord, NFPosition * pValue);
NResult N_API NFRecordGetImpressionType(HNFRecord hRecord, NFImpressionType * pValue);
NResult N_API NFRecordGetPatternClass(HNFRecord hRecord, NFPatternClass * pValue);
NResult N_API NFRecordGetQuality(HNFRecord hRecord, NByte * pValue);
NResult N_API NFRecordGetG(HNFRecord hRecord, NByte * pValue);
NResult N_API NFRecordGetCbeffProductType(HNFRecord hRecord, NUShort * pValue);
NResult N_API NFRecordSetImpressionType(HNFRecord hRecord, NFImpressionType value);
NResult N_API NFRecordSetPosition(HNFRecord hRecord, NFPosition value);
NResult N_API NFRecordSetPatternClass(HNFRecord hRecord, NFPatternClass value);
NResult N_API NFRecordSetQuality(HNFRecord hRecord, NByte value);
NResult N_API NFRecordSetG(HNFRecord hRecord, NByte value);
NResult N_API NFRecordSetCbeffProductType(HNFRecord hRecord, NUShort value);
NResult N_API NFRecordGetRidgeCountsType(HNFRecord hRecord, NFRidgeCountsType * pValue);
NResult N_API NFRecordSetRidgeCountsType(HNFRecord hRecord, NFRidgeCountsType value);
NResult N_API NFRecordGetMinutiaFormat(HNFRecord hRecord, NFMinutiaFormat * pValue);
NResult N_API NFRecordSetMinutiaFormat(HNFRecord hRecord, NFMinutiaFormat value);

#define NFRecordFree(hRecord) NObjectFree(hRecord)

#ifdef N_MSVC
	#pragma deprecated("NFRecordFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !NF_RECORD_H_INCLUDED
