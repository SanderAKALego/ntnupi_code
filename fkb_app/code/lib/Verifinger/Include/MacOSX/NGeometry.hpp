#ifndef N_GEOMETRY_HPP_INCLUDED
#define N_GEOMETRY_HPP_INCLUDED

#include <NTypes.hpp>
namespace Neurotec { namespace Drawing
{
#include <NGeometry.h>
}}

#endif // !N_GEOMETRY_HPP_INCLUDED
