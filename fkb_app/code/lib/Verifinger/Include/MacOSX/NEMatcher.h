#ifndef NE_MATCHER_H_INCLUDED
#define NE_MATCHER_H_INCLUDED

#include <NemMatchDetails.h>
#include <NEMatcherParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NEMatcher)

// Work
NResult N_API NemCreate(HNEMatcher * pHMatcher);

NResult N_API NemVerify(HNEMatcher hMatcher,
	const void * pTemplate1, NSizeType template1Size, const void * pTemplate2, NSizeType template2Size,
	NemMatchDetails * * ppMatchDetails, NInt * pScore);
NResult N_API NemIdentifyStart(HNEMatcher hMatcher,
	const void * pTemplate, NSizeType templateSize, NemMatchDetails * * ppMatchDetails);
NResult N_API NemIdentifyNext(HNEMatcher hMatcher,
	const void * pTemplate, NSizeType templateSize,
	NemMatchDetails * pMatchDetails, NInt * pScore);
NResult N_API NemIdentifyEnd(HNEMatcher hMatcher);

#define NemFree(hMatcher) NObjectFree(hMatcher)
#define NemCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NemGetParameterA(hMatcher, parameterId, pValue) NTypeGetParameterA(NEMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NemGetParameterW(hMatcher, parameterId, pValue) NTypeGetParameterW(NEMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NemGetParameter(hMatcher, parameterId, pValue) NTypeGetParameter(NEMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NemSetParameterA(hMatcher, parameterId, pValue) NTypeSetParameterA(NEMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NemSetParameterW(hMatcher, parameterId, pValue) NTypeSetParameterW(NEMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NemSetParameter(hMatcher, parameterId, pValue) NTypeSetParameter(NEMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NemReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NemFree", "NemCopyParameters", "NemGetParameterA", "NemGetParameterW", "NemGetParameter", "NemSetParameterA", "NemSetParameterW", "NemSetParameter", "NemReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NE_MATCHER_H_INCLUDED
