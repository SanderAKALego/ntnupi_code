#ifndef N_PIXEL_FORMAT_H_INCLUDED
#define N_PIXEL_FORMAT_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

#define NCalcRowSizeEx(bitCount, length, alignment) ((((bitCount) * (length) + ((alignment) << 3) - 1) / ((alignment) << 3)) * (alignment))
#define NCalcRowSize(bitCount, length) NCalcRowSizeEx(bitCount, length, 1)
#define NPixelFormatGetRowSizeEx(pixelFormat, length, alignment) NCalcRowSizeEx(NPixelFormatGetBitsPerPixel(pixelFormat), length, alignment)
#define NPixelFormatGetRowSize(pixelFormat, length) NPixelFormatGetRowSizeEx(pixelFormat, length, 1)

enum NPixelFormat_
{
	npfMonochrome = 0x00001001,
	npfGrayscale  = 0x00301001,
	npfRgb        = 0x00303003
};

#ifndef N_PIXEL_FORMAT_EX_H_INCLUDED

typedef enum NPixelFormat_ NPixelFormat;

NUInt N_API NPixelFormatGetBitsPerPixelFunc(NPixelFormat pixelFormat);
#define NPixelFormatGetBitsPerPixel(pixelFormat) NPixelFormatGetBitsPerPixelFunc(pixelFormat)

NBool N_API NPixelFormatIsValid(NPixelFormat value);

#endif

typedef struct NRgb_
{
	NByte Red;
	NByte Green;
	NByte Blue;
} N_PACKED NRgb;

#define NRgbConst(red, green, blue) { red, green, blue }

#ifdef N_CPP
}
#endif

#endif // !N_PIXEL_FORMAT_H_INCLUDED
