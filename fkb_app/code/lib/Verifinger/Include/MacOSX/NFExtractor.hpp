#ifndef NF_EXTRACTOR_HPP_INCLUDED
#define NF_EXTRACTOR_HPP_INCLUDED

#include <NGrayscaleImage.hpp>
#include <NFRecord.hpp>
#include <NFExtractorParams.hpp>
namespace Neurotec { namespace Biometrics
{
using Neurotec::Images::HNGrayscaleImage;
#include <NFExtractor.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NFExtractor) : public N_CLASS(NObject)
{
private:
	static HNFExtractor Create(bool isPalm)
	{
		HNFExtractor handle;
		NCheck(NfeCreateEx(isPalm, &handle));
		return handle;
	}

public:
	static N_CLASS(NFExtractor) * FromHandle(HNFExtractor handle, bool ownsHandle = true)
	{
		return new N_CLASS(NFExtractor)(handle, ownsHandle);
	}

	explicit N_CLASS(NFExtractor)(HNFExtractor handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NFExtractor), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NFExtractor)(bool isPalm = false)
		: N_CLASS(NObject)(Create(isPalm), N_NATIVE_TYPE_OF(NFExtractor), true)
	{
	}

	N_CLASS(NFRecord) * Extract(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NFPosition position, NFImpressionType impressionType, NfeExtractionStatus * pExtractionStatus)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		HNFRecord hRecord;
		NCheck(NfeExtract(GetHandle(), pImage->GetHandle(), position, impressionType, pExtractionStatus, &hRecord));
		try
		{
			return hRecord == NULL ? NULL : N_CLASS(NFRecord)::FromHandle(hRecord);
		}
		catch(...)
		{
			NObjectFree(hRecord);
			throw;
		}
	}

	void UpdateTemplate(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, N_CLASS(NFRecord) * pRecord)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		if (pRecord == NULL)
			NThrowArgumentNullException(N_T("pRecord"));
		NCheck(NfeUpdateTemplate(GetHandle(), pImage->GetHandle(), pRecord->GetHandle()));
	}

	N_CLASS(NFRecord) * Generalize(NInt recordCount, N_CLASS(NFRecord) ** arRecords, NfeExtractionStatus * pStatus, NInt * pBaseTemplateIndex)
	{
		if (arRecords == NULL)
			NThrowArgumentNullException(N_T("arRecords"));
		if (recordCount < 0)
			NThrowArgumentLessThanZeroException(N_T("templateCount"));

		std::vector<HNFRecord> arHRecords(recordCount);
		HNFRecord hRecord;

		for (NInt i = 0; i != recordCount; i++)
		{
			if (arRecords[i] == NULL)
				NThrowArgumentNullException(N_T("arTemplates"), N_T("One of templates elements is null"));
			arHRecords[i] = arRecords[i]->GetHandle();
		}

		NCheck(NfeGeneralizeEx(GetHandle(), recordCount, recordCount ? &*arHRecords.begin() : NULL, pStatus, pBaseTemplateIndex, &hRecord));
		try
		{
			return hRecord == NULL ? NULL : N_CLASS(NFRecord)::FromHandle(hRecord);
		}
		catch(...)
		{
			NObjectFree(hRecord);
			throw;
		}
	}

	NfeTemplateSize GetTemplateSize()
	{
		return (NfeTemplateSize)GetParameterAsInt32(NFEP_TEMPLATE_SIZE);
	}

	void SetTemplateSize(NfeTemplateSize value)
	{
		SetParameter(NFEP_TEMPLATE_SIZE, value);
	}

	NInt GetMinMinutiaCount()
	{
		return GetParameterAsInt32(NFEP_MIN_MINUTIA_COUNT);
	}

	void SetMinMinutiaCount(NInt value)
	{
		SetParameter(NFEP_MIN_MINUTIA_COUNT, value);
	}

	NFRidgeCountsType GetExtractedRidgeCounts()
	{
		return (NFRidgeCountsType)GetParameterAsInt32(NFEP_EXTRACTED_RIDGE_COUNTS);
	}

	void SetExtractedRidgeCounts(NFRidgeCountsType value)
	{
		SetParameter(NFEP_EXTRACTED_RIDGE_COUNTS, (NInt)value);
	}

	NfeReturnedImage GetReturnedImage()
	{
		return (NfeReturnedImage)GetParameterAsInt32(NFEP_RETURNED_IMAGE);
	}

	void SetReturnedImage(NfeReturnedImage value)
	{
		SetParameter(NFEP_RETURNED_IMAGE, (NInt)value);
	}

	NUInt GetMode()
	{
		return GetParameterAsUInt32(NFEP_MODE);
	}

	void SetMode(NUInt value)
	{
		SetParameter(NFEP_MODE, value);
	}

	bool GetUseQuality()
	{
		return GetParameterAsBoolean(NFEP_USE_QUALITY);
	}

	void SetUseQuality(bool value)
	{
		SetParameter(NFEP_USE_QUALITY, value);
	}

	NByte GetQualityThreshold()
	{
		return GetParameterAsByte(NFEP_QUALITY_THRESHOLD);
	}

	void SetQualityThreshold(NByte value)
	{
		SetParameter(NFEP_QUALITY_THRESHOLD, value);
	}

	NInt GetGeneralizationThreshold()
	{
		return GetParameterAsInt32(NFEP_GENERALIZATION_THRESHOLD);
	}

	void SetGeneralizationThreshold(NInt value)
	{
		SetParameter(NFEP_GENERALIZATION_THRESHOLD, value);
	}

	NByte GetGeneralizationMaximalRotation()
	{
		return GetParameterAsByte(NFEP_GENERALIZATION_MAXIMAL_ROTATION);
	}

	void SetGeneralizationMaximalRotation(NByte value)
	{
		SetParameter(NFEP_GENERALIZATION_MAXIMAL_ROTATION, value);
	}

	NInt GetGeneralizationMinMinutiaCount()
	{
		return GetParameterAsInt32(NFEP_GENERALIZATION_MIN_MINUTIA_COUNT);
	}

	void SetGeneralizationMinMinutiaCount(NInt value)
	{
		SetParameter(NFEP_GENERALIZATION_MIN_MINUTIA_COUNT, value);
	}

	N_DECLARE_OBJECT_TYPE(NFExtractor)
};

}}

#endif // !NF_EXTRACTOR_HPP_INCLUDED
