#ifndef NF_MATCHER_H_INCLUDED
#define NF_MATCHER_H_INCLUDED

#include <NfmMatchDetails.h>
#include <NFMatcherParams.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NFMatcher)

NResult N_API NfmCreate(HNFMatcher * pHMatcher);
NResult N_API NfmCreateEx(NBool isPalm, HNFMatcher * pHMatcher);

NResult N_API NfmVerify(HNFMatcher hMatcher, const void * template1, NSizeType template1Size,
	const void * template2, NSizeType template2Size, NfmMatchDetails * * ppMatchDetails, NInt * pScore);
NResult N_API NfmIdentifyStart(HNFMatcher hMatcher, const void * templ, NSizeType templSize,
	NfmMatchDetails * * ppMatchDetails);
NResult N_API NfmIdentifyNext(HNFMatcher hMatcher, const void * templ, NSizeType templSize,
	NfmMatchDetails * pMatchDetails, NInt * pScore);
NResult N_API NfmIdentifyEnd(HNFMatcher hMatcher);

NResult N_API NfmIsPalm(HNFMatcher hMatcher, NBool * pValue);

#define NfmFree(hMatcher) NObjectFree(hMatcher)
#define NfmCopyParameters(hDstMatcher, hSrcMatcher) NObjectCopyParameters(hDstMatcher, hSrcMatcher)
#define NfmGetParameterA(hMatcher, parameterId, pValue) NTypeGetParameterA(NFMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NfmGetParameterW(hMatcher, parameterId, pValue) NTypeGetParameterW(NFMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NfmGetParameter(hMatcher, parameterId, pValue) NTypeGetParameter(NFMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NfmSetParameterA(hMatcher, parameterId, pValue) NTypeSetParameterA(NFMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NfmSetParameterW(hMatcher, parameterId, pValue) NTypeSetParameterW(NFMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NfmSetParameter(hMatcher, parameterId, pValue) NTypeSetParameter(NFMatcherTypeOf(), hMatcher, 0, parameterId, pValue)
#define NfmReset(hMatcher) NObjectReset(hMatcher)

#ifdef N_MSVC
	#pragma deprecated("NfmFree", "NfmCopyParameters", "NfmGetParameterA", "NfmGetParameterW", "NfmGetParameter", "NfmSetParameterA", "NfmSetParameterW", "NfmSetParameter", "NfmReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NF_MATCHER_H_INCLUDED
