#ifndef NL_MATCHER_HPP_INCLUDED
#define NL_MATCHER_HPP_INCLUDED

#include <NLMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NLMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NLMatcher) : public N_CLASS(NObject)
{
private:
	static HNLMatcher Create()
	{
		HNLMatcher handle;
		NCheck(NlmCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NLMatcher) * FromHandle(HNLMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NLMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NLMatcher)(HNLMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NLMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NLMatcher)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NLMatcher), true)
	{
	}

	NDouble Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size)
	{
		NDouble score;
		NCheck(NlmVerify((HNLMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			&score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize)
	{
		NCheck(NlmIdentifyStart((HNLMatcher)GetHandle(), pTemplate, templateSize));
	}

	NDouble IdentifyNext(const void * pTemplate, NSizeType templateSize)
	{
		NDouble score;
		NCheck(NlmIdentifyNext((HNLMatcher)GetHandle(), pTemplate, templateSize, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NlmIdentifyEnd((HNLMatcher)GetHandle()));
	}

	NDouble GetMatchingThreshold()
	{
		return GetParameterAsDouble(NLMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NDouble value)
	{
		SetParameter(NLMP_MATCHING_THRESHOLD, value);
	}

	NlmSpeed GetMatchingSpeed()
	{
		return (NlmSpeed)GetParameterAsInt32(NLMP_MATCHING_SPEED);
	}

	void SetMatchingSpeed(NlmSpeed value)
	{
		SetParameter(NLMP_MATCHING_SPEED, (NInt)value);
	}

	N_DECLARE_OBJECT_TYPE(NLMatcher)
};

}}

#endif // !NL_MATCHER_HPP_INCLUDED
