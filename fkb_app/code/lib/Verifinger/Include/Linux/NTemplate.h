#ifndef N_TEMPLATE_H_INCLUDED
#define N_TEMPLATE_H_INCLUDED

#include <NFTemplate.h>
#include <NLTemplate.h>
#include <NETemplate.h>
#include <NTemplates.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NTemplate)

NResult N_API NTemplateCalculateSize(NSizeType fingersTemplateSize, NSizeType facesTemplateSize, NSizeType irisesTemplateSize, NSizeType palmsTemplateSize, NSizeType * pSize);
NResult N_API NTemplatePack(
	const void * pFingersTemplate, NSizeType fingersTemplateSize,
	const void * pFacesTemplate, NSizeType facesTemplateSize,
	const void * pIrisesTemplate, NSizeType irisesTemplateSize,
	const void * pPalmsTemplate, NSizeType palmsTemplateSize,
	void * pBuffer, NSizeType bufferSize, NSizeType * pSize);
NResult N_API NTemplateUnpack(const void * pBuffer, NSizeType bufferSize,
	NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
	const void * * ppFingersTemplate, NSizeType * pFingersTemplateSize,
	const void * * ppFacesTemplate, NSizeType * pFacesTemplateSize,
	const void * * ppIrisesTemplate, NSizeType * pIrisesTemplateSize,
	const void * * ppPalmsTemplate, NSizeType * pPalmsTemplateSize);
NResult N_API NTemplateCheck(const void * pBuffer, NSizeType bufferSize);

typedef struct NTemplateInfo_
{
	NByte MajorVersion;
	NByte MinorVersion;
	NUInt Size;
	NByte HeaderSize;
	NFTemplateInfo * FingersInfo;
	NLTemplateInfo * FacesInfo;
	NETemplateInfo * IrisesInfo;
	NFTemplateInfo * PalmsInfo;
} NTemplateInfo;

void N_API NTemplateInfoDispose(NTemplateInfo * pInfo);

NResult N_API NTemplateCreate(HNTemplate * pHTemplate);
NResult N_API NTemplateCreateEx(NUInt flags, HNTemplate * pHTemplate);
NResult N_API NTemplateCreateFromMemory(const void * pBuffer, NSizeType bufferSize,
	NUInt flags, NTemplateInfo * pInfo, HNTemplate * pHTemplate);

NResult N_API NTemplateGetFingers(HNTemplate hTemplate, HNFTemplate * pValue);
NResult N_API NTemplateAddFingers(HNTemplate hTemplate, HNFTemplate * pHFingers);
NResult N_API NTemplateAddFingersEx(HNTemplate hTemplate, NUInt flags, HNFTemplate * pHFingers);
NResult N_API NTemplateAddFingersFromMemory(HNTemplate hTemplate, const void * pBuffer, NSizeType bufferSize,
	NUInt flags, HNFTemplate * pHFingers);
NResult N_API NTemplateAddFingersCopy(HNTemplate hTemplate, HNFTemplate hSrcFingers, HNFTemplate * pHFingers);
NResult N_API NTemplateRemoveFingers(HNTemplate hTemplate);

NResult N_API NTemplateGetFaces(HNTemplate hTemplate, HNLTemplate * pValue);
NResult N_API NTemplateAddFaces(HNTemplate hTemplate, HNLTemplate * pHFaces);
NResult N_API NTemplateAddFacesEx(HNTemplate hTemplate, NUInt flags, HNLTemplate * pHFaces);
NResult N_API NTemplateAddFacesFromMemory(HNTemplate hTemplate, const void * pBuffer, NSizeType bufferSize,
	NUInt flags, HNLTemplate * pHFaces);
NResult N_API NTemplateAddFacesCopy(HNTemplate hTemplate, HNLTemplate hSrcFaces, HNLTemplate * pHFaces);
NResult N_API NTemplateRemoveFaces(HNTemplate hTemplate);

NResult N_API NTemplateGetIrises(HNTemplate hTemplate, HNETemplate * pValue);
NResult N_API NTemplateAddIrises(HNTemplate hTemplate, HNETemplate * pHIrises);
NResult N_API NTemplateAddIrisesEx(HNTemplate hTemplate, NUInt flags, HNETemplate * pHIrises);
NResult N_API NTemplateAddIrisesFromMemory(HNTemplate hTemplate, const void * pBuffer, NSizeType bufferSize,
	NUInt flags, HNETemplate * pHIrises);
NResult N_API NTemplateAddIrisesCopy(HNTemplate hTemplate, HNETemplate hSrcIrises, HNETemplate * pHIrises);
NResult N_API NTemplateRemoveIrises(HNTemplate hTemplate);

NResult N_API NTemplateGetPalms(HNTemplate hTemplate, HNFTemplate * pValue);
NResult N_API NTemplateAddPalms(HNTemplate hTemplate, HNFTemplate * pHPalms);
NResult N_API NTemplateAddPalmsEx(HNTemplate hTemplate, NUInt flags, HNFTemplate * pHPalms);
NResult N_API NTemplateAddPalmsFromMemory(HNTemplate hTemplate, const void * pBuffer, NSizeType bufferSize,
	NUInt flags, HNFTemplate * pHPalms);
NResult N_API NTemplateAddPalmsCopy(HNTemplate hTemplate, HNFTemplate hSrcPalms, HNFTemplate * pHPalms);
NResult N_API NTemplateRemovePalms(HNTemplate hTemplate);

NResult N_API NTemplateClear(HNTemplate hTemplate);
NResult N_API NTemplateClone(HNTemplate hTemplate, HNTemplate * pHClonedTemplate);
NResult N_API NTemplateGetSize(HNTemplate hTemplate, NUInt flags, NSizeType * pSize);
NResult N_API NTemplateSaveToMemory(HNTemplate hTemplate, void * pBuffer, NSizeType bufferSize, NUInt flags, NSizeType * pSize);

#define NTemplateFree(hTemplate) NObjectFree(hTemplate)

#ifdef N_MSVC
	#pragma deprecated("NTemplateFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !N_TEMPLATE_H_INCLUDED
