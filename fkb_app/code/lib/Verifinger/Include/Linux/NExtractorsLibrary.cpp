#ifndef N_EXTRACTORS_LIBRARY_CPP_INCLUDED
#define N_EXTRACTORS_LIBRARY_CPP_INCLUDED

#include <NTemplatesLibrary.cpp>
#include <NImagesLibrary.cpp>

#include <NEExtractor.hpp>
#include <NFExtractor.hpp>
#include <NLExtractor.hpp>

namespace Neurotec { namespace Biometrics
{

N_IMPLEMENT_OBJECT_TYPE(NEExtractor, NObject)
N_IMPLEMENT_OBJECT_TYPE(NFExtractor, NObject)
N_IMPLEMENT_OBJECT_TYPE(NLExtractor, NObject)

}}

#endif // !N_EXTRACTORS_LIBRARY_CPP_INCLUDED
