#ifndef JPEG_2K_H_INCLUDED
#define JPEG_2K_H_INCLUDED

#include "NImage.h"

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API Jpeg2KLoadImageFromFileA(const NAChar * szFileName, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API Jpeg2KLoadImageFromFileW(const NWChar * szFileName, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API Jpeg2KLoadImageFromFile(const NChar * szFileName, HNImage * pHImage);
#endif
#define Jpeg2KLoadImageFromFile N_FUNC_AW(Jpeg2KLoadImageFromFile)

NResult N_API Jpeg2KLoadImageFromMemory(const void * buffer, NSizeType bufferLength, HNImage * pHImage);
NResult N_API Jpeg2KLoadImageFromStream(HNStream hStream, HNImage * pHImage);

#define JPEG_2K_DEFAULT_RATIO 10.0f

#ifndef N_NO_ANSI_FUNC
NResult N_API Jpeg2KSaveImageToFileA(HNImage hImage, NFloat ratio, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API Jpeg2KSaveImageToFileW(HNImage hImage, NFloat ratio, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API Jpeg2KSaveImageToFile(HNImage hImage, NInt ratio, const NChar * szFileName);
#endif
#define Jpeg2KSaveImageToFile N_FUNC_AW(Jpeg2KSaveImageToFile)

NResult N_API Jpeg2KSaveImageToMemory(HNImage hImage, NFloat ratio, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API Jpeg2KSaveImageToStream(HNImage hImage, NFloat ratio, HNStream hStream);

typedef enum Jpeg2KProfile_
{
	jpeg2kpNone = 0,
	jpeg2kpFingerprint1000ppi = 1000,
	jpeg2kpFaceLossy = 2000,
	jpeg2kpFaceLossless = 2001,
} Jpeg2KProfile;

#ifndef N_NO_ANSI_FUNC
NResult N_API Jpeg2KSaveImageToFileWithProfileA(HNImage hImage, Jpeg2KProfile profile, const NAChar * szFileName);
#endif
#ifndef N_NO_UNICODE
NResult N_API Jpeg2KSaveImageToFileWithProfileW(HNImage hImage, Jpeg2KProfile profile, const NWChar * szFileName);
#endif
#ifdef N_DOCUMENTATION
NResult N_API Jpeg2KSaveImageToFileWithProfile(HNImage hImage, Jpeg2KProfile profile, const NChar * szFileName);
#endif
#define Jpeg2KSaveImageToFileWithProfile N_FUNC_AW(Jpeg2KSaveImageToFileWithProfile)

NResult N_API Jpeg2KSaveImageToMemoryWithProfile(HNImage hImage, Jpeg2KProfile profile, void * * pBuffer, NSizeType * pBufferLength);
NResult N_API Jpeg2KSaveImageToStreamWithProfile(HNImage hImage, Jpeg2KProfile profile, HNStream hStream);

#ifdef N_CPP
}
#endif

#endif // !JPEG_2K_H_INCLUDED
