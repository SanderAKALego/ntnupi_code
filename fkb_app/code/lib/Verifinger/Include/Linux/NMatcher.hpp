#ifndef N_MATCHER_HPP_INCLUDED
#define N_MATCHER_HPP_INCLUDED

#include <NfsMatcher.hpp>
#include <NlsMatcher.hpp>
#include <NesMatcher.hpp>
#include <NMMatchDetails.hpp>
#include <NMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NMatcher) : public N_CLASS(NObject)
{
private:
	static HNMatcher Create()
	{
		HNMatcher handle;
		NCheck(NMCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NMatcher) * FromHandle(HNMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NMatcher)(HNMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NMatcher)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NMatcher), true)
	{
	}

	NInt Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size,
		NMMatchDetails * * ppMatchDetails = NULL)
	{
		NInt score;
		NCheck(NMVerify((HNMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			ppMatchDetails, &score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize,
		NMMatchDetails * * ppMatchDetails = NULL)
	{
		NCheck(NMIdentifyStart((HNMatcher)GetHandle(), pTemplate, templateSize, ppMatchDetails));
	}

	NInt IdentifyNext(const void * pTemplate, NSizeType templateSize, NMMatchDetails * pMatchDetails)
	{
		NInt score;
		NCheck(NMIdentifyNext((HNMatcher)GetHandle(), pTemplate, templateSize, pMatchDetails, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NMIdentifyEnd((HNMatcher)GetHandle()));
	}

	NInt GetMatchingThreshold()
	{
		return GetParameterAsInt32(NM_PART_NONE, NMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NInt value)
	{
		SetParameter(NM_PART_NONE, NMP_MATCHING_THRESHOLD, value);
	}

	NMFusionType GetFusionType()
	{
		return (NMFusionType)GetParameterAsInt32(NM_PART_NONE, NMP_FUSION_TYPE);
	}

	void SetFusionType(NMFusionType value)
	{
		SetParameter(NM_PART_NONE, NMP_FUSION_TYPE, (NInt)value);
	}

	NDouble GetFacesMatchingThreshold()
	{
		return (NDouble)GetParameterAsDouble(NM_PART_NONE, NMP_FACES_MATCHING_THRESHOLD);
	}

	void SetFacesMatchingThreshold(NDouble value)
	{
		SetParameter(NM_PART_NONE, NMP_FACES_MATCHING_THRESHOLD, value);
	}

	NDouble GetIrisesMatchingThreshold()
	{
		return (NDouble)GetParameterAsDouble(NM_PART_NONE, NMP_IRISES_MATCHING_THRESHOLD);
	}

	void SetIrisesMatchingThreshold(NDouble value)
	{
		SetParameter(NM_PART_NONE, NMP_IRISES_MATCHING_THRESHOLD, value);
	}

	NInt GetFingersMinMatchedFingerCount()
	{
		return GetParameterAsInt32(NM_PART_FINGERS + NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT);
	}

	void SetFingersMinMatchedFingerCount(NInt value)
	{
		SetParameter(NM_PART_FINGERS + NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT, value);
	}

	NInt GetFingersMinMatchedFingerCountThreshold()
	{
		return GetParameterAsInt32(NM_PART_FINGERS + NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT_THRESHOLD);
	}

	void SetFingersMinMatchedFingerCountThreshold(NInt value)
	{
		SetParameter(NM_PART_FINGERS + NFSM_PART_NONE, NFSMP_MIN_MATCHED_FINGER_COUNT_THRESHOLD, value);
	}

	NByte GetFingersNfmMaximalRotation()
	{
		return GetParameterAsByte(NM_PART_FINGERS + NFSM_PART_NFM, NFMP_MAXIMAL_ROTATION);
	}

	void SetFingersNfmMaximalRotation(NByte value)
	{
		SetParameter(NM_PART_FINGERS + NFSM_PART_NFM, NFMP_MAXIMAL_ROTATION, value);
	}

	NfmSpeed GetFingersNfmMatchingSpeed()
	{
		return (NfmSpeed)GetParameterAsInt32(NM_PART_FINGERS + NFSM_PART_NFM, NFMP_MATCHING_SPEED);
	}

	void SetFingersNfmMatchingSpeed(NfmSpeed value)
	{
		SetParameter(NM_PART_FINGERS + NFSM_PART_NFM, NFMP_MATCHING_SPEED, (NInt)value);
	}

	NUInt GetFingersNfmMode()
	{
		return GetParameterAsUInt32(NM_PART_FINGERS + NFSM_PART_NFM, NFMP_MODE);
	}

	void SetFingersNfmMode(NUInt value)
	{
		SetParameter(NM_PART_FINGERS + NFSM_PART_NFM, NFMP_MODE, value);
	}

	NlmSpeed GetFacesNlmMatchingSpeed()
	{
		return (NlmSpeed)GetParameterAsInt32(NM_PART_FACES + NLSM_PART_NLM, NLMP_MATCHING_SPEED);
	}

	void SetFacesNlmMatchingSpeed(NlmSpeed value)
	{
		SetParameter(NM_PART_FACES + NLSM_PART_NLM, NLMP_MATCHING_SPEED, (NInt)value);
	}

	NemSpeed GetIrisesNemMatchingSpeed()
	{
		return (NemSpeed)GetParameterAsInt32(NM_PART_IRISES + NESM_PART_NEM, NEMP_MATCHING_SPEED);
	}

	void SetIrisesNemMatchingSpeed(NemSpeed value)
	{
		SetParameter(NM_PART_IRISES + NESM_PART_NEM, NEMP_MATCHING_SPEED, value);
	}

	NByte GetIrisesNemMaximalRotation()
	{
		return GetParameterAsByte(NM_PART_IRISES + NESM_PART_NEM, NEMP_MAXIMAL_ROTATION);
	}

	void SetIrisesNemMaximalRotation(NByte value)
	{
		SetParameter(NM_PART_IRISES + NESM_PART_NEM, NEMP_MAXIMAL_ROTATION, value);
	}

	NByte GetPalmsNfmMaximalRotation()
	{
		return GetParameterAsByte(NM_PART_PALMS + NFSM_PART_NFM, NFMP_MAXIMAL_ROTATION);
	}

	void SetPalmsNfmMaximalRotation(NByte value)
	{
		SetParameter(NM_PART_PALMS + NFSM_PART_NFM, NFMP_MAXIMAL_ROTATION, value);
	}

	NfmSpeed GetPalmsNfmMatchingSpeed()
	{
		return (NfmSpeed)GetParameterAsInt32(NM_PART_PALMS + NFSM_PART_NFM, NFMP_MATCHING_SPEED);
	}

	void SetPalmsNfmMatchingSpeed(NfmSpeed value)
	{
		SetParameter(NM_PART_PALMS + NFSM_PART_NFM, NFMP_MATCHING_SPEED, value);
	}

	N_DECLARE_OBJECT_TYPE(NMatcher)
};

}}

#endif // !N_MATCHER_HPP_INCLUDED
