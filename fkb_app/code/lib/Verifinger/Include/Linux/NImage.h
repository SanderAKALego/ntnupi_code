#ifndef N_IMAGE_H_INCLUDED
#define N_IMAGE_H_INCLUDED

#include <NPixelFormat.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NImage)

#ifdef N_CPP
}
#endif

#include <NImageFormat.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef enum NImageRotateFlipType_
{
	nirftRotateNone = 0,
	nirftRotate90 = 1,
	nirftRotate180 = 2,
	nirftRotate270 = 3,
	nirftFlipNone = 0,
	nirftFlipX = 4,
	nirftFlipY = 8,
	nirftFlipXY = nirftFlipX | nirftFlipY,
	nirftNone = nirftRotateNone | nirftFlipNone
} NImageRotateFlipType;

NResult N_API NImageCopy(HNImage hSrcImage, NUInt left, NUInt top, NUInt width, NUInt height, HNImage hDstImage, NUInt dstLeft, NUInt dstTop);

NResult N_API NImageCreateWrapper(NPixelFormat pixelFormat,
	NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
	void * pixels, NBool ownsPixels, HNImage * pHImage);

NResult N_API NImageCreateWrapperForPart(NPixelFormat pixelFormat,
	NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
	void * pixels, NUInt left, NUInt top, HNImage * pHImage);

NResult N_API NImageCreateWrapperForImagePart(HNImage hSrcImage, NUInt left, NUInt top, NUInt width, NUInt height,
	HNImage * pHImage);

NResult N_API NImageCreateWrapperForImagePartEx(HNImage hSrcImage, NUInt left, NUInt top, NUInt width, NUInt height,
	NFloat horzResolution, NFloat vertResolution,
	HNImage * pHImage);

NResult N_API NImageCreate(NPixelFormat pixelFormat,
	NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
	HNImage * pHImage);

NResult N_API NImageCreateFromData(NPixelFormat pixelFormat,
	NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
	NSizeType srcStride, const void * srcPixels, HNImage * pHImage);

NResult N_API NImageCreateFromDataPart(NPixelFormat pixelFormat,
	NUInt width, NUInt height, NSizeType stride, NFloat horzResolution, NFloat vertResolution,
	NSizeType srcStride, const void * srcPixels, NUInt left, NUInt top, HNImage * pHImage);

NResult N_API NImageCreateFromImage(NPixelFormat pixelFormat,
	NSizeType stride, HNImage hSrcImage, HNImage * pHImage);

NResult N_API NImageCreateFromImageEx(NPixelFormat pixelFormat,
	NSizeType stride, NFloat horzResolution, NFloat vertResolution, HNImage hSrcImage, HNImage * pHImage);

NResult N_API NImageCreateFromImagePart(NPixelFormat pixelFormat,
	NSizeType stride, HNImage hSrcImage,
	NUInt left, NUInt top, NUInt width, NUInt height, HNImage * pHImage);

NResult N_API NImageCreateFromImagePartEx(NPixelFormat pixelFormat,
	NSizeType stride, NFloat horzResolution, NFloat vertResolution, HNImage hSrcImage,
	NUInt left, NUInt top, NUInt width, NUInt height, HNImage * pHImage);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageCreateFromFileA(const NAChar * szFileName, HNImageFormat hImageFormat, HNImage * pHImage);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageCreateFromFileW(const NWChar * szFileName, HNImageFormat hImageFormat, HNImage * pHImage);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageCreateFromFile(const NChar * szFileName, HNImageFormat hImageFormat, HNImage * pHImage);
#endif
#define NImageCreateFromFile N_FUNC_AW(NImageCreateFromFile)

NResult N_API NImageClone(HNImage hImage, HNImage * pHClonedImage);

#ifndef N_NO_ANSI_FUNC
NResult N_API NImageSaveToFileA(HNImage hImage, const NAChar * szFileName, HNImageFormat hImageFormat);
#endif
#ifndef N_NO_UNICODE
NResult N_API NImageSaveToFileW(HNImage hImage, const NWChar * szFileName, HNImageFormat hImageFormat);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NImageSaveToFile(HNImage hImage, const NChar * szFileName, HNImageFormat hImageFormat);
#endif
#define NImageSaveToFile N_FUNC_AW(NImageSaveToFile)

NResult N_API NImageFlipHorizontally(HNImage hImage);
NResult N_API NImageFlipVertically(HNImage hImage);
NResult N_API NImageFlipDiagonally(HNImage hImage);
NResult N_API NImageRotateFlip(HNImage hImage, NImageRotateFlipType rotateFlipType, HNImage * pHResultImage);
NResult N_API NImageCrop(HNImage hImage, NUInt left, NUInt top, NUInt width, NUInt height, HNImage * pHResultImage);

NResult N_API NImageGetPixelFormat(HNImage hImage, NPixelFormat * pValue);
NResult N_API NImageGetWidth(HNImage hImage, NUInt * pValue);
NResult N_API NImageGetHeight(HNImage hImage, NUInt * pValue);
NResult N_API NImageGetStride(HNImage hImage, NSizeType * pValue);
NResult N_API NImageGetSize(HNImage hImage, NSizeType * pValue);
NResult N_API NImageGetHorzResolution(HNImage hImage, NFloat * pValue);
NResult N_API NImageGetVertResolution(HNImage hImage, NFloat * pValue);
NResult N_API NImageGetPixels(HNImage hImage, void * * pValue);

#define NImageFree(hImage) NObjectFree(hImage)

#ifdef N_MSVC
	#pragma deprecated("NImageFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !N_IMAGE_H_INCLUDED
