#ifndef NE_MATCHER_HPP_INCLUDED
#define NE_MATCHER_HPP_INCLUDED

#include <NemMatchDetails.hpp>
#include <NEMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NEMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NEMatcher) : public N_CLASS(NObject)
{
private:
	static HNEMatcher Create()
	{
		HNEMatcher handle;
		NCheck(NemCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NEMatcher) * FromHandle(HNEMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NEMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NEMatcher)(HNEMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NEMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NEMatcher)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NEMatcher), true)
	{
	}

	NInt Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size,
		NemMatchDetails * * ppMatchDetails = NULL)
	{
		NInt score;
		NCheck(NemVerify((HNEMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			ppMatchDetails, &score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize,
		NemMatchDetails * * ppMatchDetails = NULL)
	{
		NCheck(NemIdentifyStart((HNEMatcher)GetHandle(), pTemplate, templateSize, ppMatchDetails));
	}

	NInt IdentifyNext(const void * pTemplate, NSizeType templateSize, NemMatchDetails * pMatchDetails)
	{
		NInt score;
		NCheck(NemIdentifyNext((HNEMatcher)GetHandle(), pTemplate, templateSize, pMatchDetails, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NemIdentifyEnd((HNEMatcher)GetHandle()));
	}

	NInt GetMatchingThreshold()
	{
		return GetParameterAsInt32(NEMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NInt value)
	{
		SetParameter(NEMP_MATCHING_THRESHOLD, value);
	}

	NemSpeed GetMatchingSpeed()
	{
		return (NemSpeed)GetParameterAsInt32(NEMP_MATCHING_SPEED);
	}

	void SetMatchingSpeed(NemSpeed value)
	{
		SetParameter(NEMP_MATCHING_SPEED, (NInt)value);
	}

	NByte GetMaximalRotation()
	{
		return GetParameterAsByte(NEMP_MAXIMAL_ROTATION);
	}

	void SetMaximalRotation(NByte value)
	{
		SetParameter(NEMP_MAXIMAL_ROTATION, value);
	}

	N_DECLARE_OBJECT_TYPE(NEMatcher)
};

}}

#endif // !NE_MATCHER_HPP_INCLUDED
