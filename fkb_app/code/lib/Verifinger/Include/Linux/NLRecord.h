#ifndef NL_RECORD_H_INCLUDED
#define NL_RECORD_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NLRecord)

NResult N_API NLRecordCheck(const void *pBuffer, NSizeType bufferSize);

typedef struct NLRecordInfo_
{
	NByte MajorVersion;
	NByte MinorVersion;
	NUInt Size;
	NByte HeaderSize;
} NLRecordInfo;

void N_API NLRecordInfoDispose(NLRecordInfo *pInfo);

NResult N_API NLRecordCreate(NUInt flags, HNLRecord *pHRecord);

NResult N_API NLRecordCreateFromMemory(const void *pBuffer, NSizeType bufferSize,
	NUInt flags, NLRecordInfo *pInfo, HNLRecord *pHRecord);

NResult N_API NLRecordClone(HNLRecord hRecord, HNLRecord *pHClonedRecord);
NResult N_API NLRecordGetSize(HNLRecord hRecord, NUInt flags, NSizeType *pSize);
NResult N_API NLRecordSaveToMemory(HNLRecord hRecord,
	void *pBuffer, NSizeType bufferSize, NUInt flags, NSizeType *pSize);

NResult N_API NLRecordGetQualityMem(const void *pBuffer, NSizeType bufferSize, NByte *pValue);
NResult N_API NLRecordGetCbeffProductTypeMem(const void * pBuffer, NSizeType bufferSize, NUShort * pValue);

NResult N_API NLRecordGetQuality(HNLRecord hRecord, NByte *pValue);
NResult N_API NLRecordSetQuality(HNLRecord hRecord, NByte value);
NResult N_API NLRecordGetCbeffProductType(HNLRecord hRecord, NUShort *pValue);
NResult N_API NLRecordSetCbeffProductType(HNLRecord hRecord, NUShort value);

#define NLRecordFree(hRecord) NObjectFree(hRecord)

#ifdef N_MSVC
	#pragma deprecated("NLRecordFree")
#endif

#ifdef N_CPP
}
#endif

#endif // !NL_RECORD_H_INCLUDED
