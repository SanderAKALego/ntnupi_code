#ifndef N_OBJECT_H_INCLUDED
#define N_OBJECT_H_INCLUDED

#include <NTypes.h>
#include <NErrors.h>
#include <NParameters.h>

#ifdef N_CPP
extern "C"
{
#endif

N_DECLARE_HANDLE(HNType)
N_DECLARE_HANDLE(HNObject)

NResult N_API NTypeIsSubclassOf(HNType hType, HNType hOtherType, NBool * pValue);
NResult N_API NTypeIsAssignableFrom(HNType hType, HNType hOtherType, NBool * pValue);
NResult N_API NTypeIsInstanceOfType(HNType hType, HNObject hObject, NBool * pValue);
NResult N_API NTypeReset(HNType hType, HNObject hObject);

#ifndef N_NO_ANSI_FUNC
NResult N_API NTypeGetParameterA(HNType hType, HNObject hObject, NUShort partId, NUInt parameterId, void * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NTypeGetParameterW(HNType hType, HNObject hObject, NUShort partId, NUInt parameterId, void * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NTypeGetParameter(HNType hType, HNObject hObject, NUShort partId, NUInt parameterId, void * pValue);
#endif
#define NTypeGetParameter N_FUNC_AW(NTypeGetParameter)

#ifndef N_NO_ANSI_FUNC
NResult N_API NTypeSetParameterA(HNType hType, HNObject hObject, NUShort partId, NUInt parameterId, const void * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NTypeSetParameterW(HNType hType, HNObject hObject, NUShort partId, NUInt parameterId, const void * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NTypeSetParameter(HNType hType, HNObject hObject, NUShort partId, NUInt parameterId, const void * pValue);
#endif
#define NTypeSetParameter N_FUNC_AW(NTypeSetParameter)

#ifndef N_NO_ANSI_FUNC
NResult N_API NTypeGetNameA(HNType hType, NAChar * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NTypeGetNameW(HNType hType, NWChar * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NTypeGetName(HNType hType, NChar * pValue);
#endif
#define NTypeGetName N_FUNC_AW(NTypeGetName)

NResult N_API NTypeGetBaseType(HNType hType, HNType * pValue);
NResult N_API NTypeIsAbstract(HNType hType, NBool * pValue);
NResult N_API NTypeSupportsParameters(HNType hType, NBool * pValue);

#define _N_TYPE_OF(name) (name##TypeOf())

HNType N_API NObjectTypeOf(void);

NResult N_API NObjectCopyParameters(HNObject hDstObject, HNObject hSrcObject);

void N_API NObjectFree(HNObject hObject);

NResult N_API NObjectGetType(HNObject hObject, HNType * pValue);
NResult N_API NObjectGetOwner(HNObject hObject, HNObject * pValue);
NResult N_API NObjectReset(HNObject hObject);

#ifndef N_NO_ANSI_FUNC
NResult N_API NObjectGetParameterWithPartA(HNObject hObject, NUShort partId, NUInt parameterId, void * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NObjectGetParameterWithPartW(HNObject hObject, NUShort partId, NUInt parameterId, void * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NObjectGetParameterWithPart(HNObject hObject, NUShort partId, NUInt parameterId, void * pValue);
#endif
#define NObjectGetParameterWithPart N_FUNC_AW(NObjectGetParameterWithPart)

#ifndef N_NO_ANSI_FUNC
NResult N_API NObjectSetParameterWithPartA(HNObject hObject, NUShort partId, NUInt parameterId, const void * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NObjectSetParameterWithPartW(HNObject hObject, NUShort partId, NUInt parameterId, const void * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NObjectSetParameterWithPart(HNObject hObject, NUShort partId, NUInt parameterId, const void * pValue);
#endif
#define NObjectSetParameterWithPart N_FUNC_AW(NObjectSetParameterWithPart)

#ifndef N_NO_ANSI_FUNC
NResult N_API NObjectGetParameterA(HNObject hObject, NUInt parameterId, void * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NObjectGetParameterW(HNObject hObject, NUInt parameterId, void * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NObjectGetParameter(HNObject hObject, NUInt parameterId, void * pValue);
#endif
#define NObjectGetParameter N_FUNC_AW(NObjectGetParameter)

#ifndef N_NO_ANSI_FUNC
NResult N_API NObjectSetParameterA(HNObject hObject, NUInt parameterId, const void * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NObjectSetParameterW(HNObject hObject, NUInt parameterId, const void * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NObjectSetParameter(HNObject hObject, NUInt parameterId, const void * pValue);
#endif
#define NObjectSetParameter N_FUNC_AW(NObjectSetParameter)

#define DECLARE_N_OBJECT_HANDLE(name) \
	HNType N_API name##TypeOf(void);\
	typedef HNObject H##name;

#ifdef N_CPP
}
#endif

#endif // !N_OBJECT_H_INCLUDED
