#ifndef N_IMAGE_FORMAT_HPP_INCLUDED
#define N_IMAGE_FORMAT_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Images
{
using ::Neurotec::IO::HNStream;
using ::Neurotec::IO::NFileAccess;
#include <NImageFormat.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(NImage);
class N_CLASS(NImageFile);

class N_CLASS(NImageFormat) : public N_CLASS(NObject)
{
public:
	class ImageFormatCollection : public ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(NImageFormat)>
	{
		ImageFormatCollection()
			: ::Neurotec::Collections::N_CLASS(NObjectStaticReadOnlyCollection)<N_CLASS(NImageFormat)>(NImageFormatGetFormatCount, NImageFormatGetFormat, N_CLASS(NImageFormat)::FromHandle, false)
		{
		}

		friend class N_CLASS(NImageFormat);
	};

private:
	static struct Initializer
	{
		HNImageFormat hBmpFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> bmp;
		HNImageFormat hTiffFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> tiff;
		HNImageFormat hJpegFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> jpeg;
		HNImageFormat hPngFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> png;
		HNImageFormat hWsqFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> wsq;
		HNImageFormat hIHeadFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> iHead;
		HNImageFormat hJpeg2KFormat;
		std::auto_ptr<N_CLASS(NImageFormat)> jpeg2K;
		std::auto_ptr<ImageFormatCollection> formats;

		Initializer()
		{
			NCheck(NImageFormatGetBmp(&hBmpFormat));
			bmp = std::auto_ptr<N_CLASS(NImageFormat)>(hBmpFormat ? new N_CLASS(NImageFormat)(hBmpFormat) : NULL);

			NCheck(NImageFormatGetTiff(&hTiffFormat));
			tiff = std::auto_ptr<N_CLASS(NImageFormat)>(hTiffFormat ? new N_CLASS(NImageFormat)(hTiffFormat) : NULL);

			NCheck(NImageFormatGetJpeg(&hJpegFormat));
			jpeg = std::auto_ptr<N_CLASS(NImageFormat)>(hJpegFormat ? new N_CLASS(NImageFormat)(hJpegFormat) : NULL);

			NCheck(NImageFormatGetPng(&hPngFormat));
			png = std::auto_ptr<N_CLASS(NImageFormat)>(hPngFormat ? new N_CLASS(NImageFormat)(hPngFormat) : NULL);

			NCheck(NImageFormatGetWsq(&hWsqFormat));
			wsq = std::auto_ptr<N_CLASS(NImageFormat)>(hWsqFormat ? new N_CLASS(NImageFormat)(hWsqFormat) : NULL);

			NCheck(NImageFormatGetIHead(&hIHeadFormat));
			iHead = std::auto_ptr<N_CLASS(NImageFormat)>(hIHeadFormat ? new N_CLASS(NImageFormat)(hIHeadFormat) : NULL);

			NCheck(NImageFormatGetJpeg2K(&hJpeg2KFormat));
			jpeg2K = std::auto_ptr<N_CLASS(NImageFormat)>(hJpeg2KFormat ? new N_CLASS(NImageFormat)(hJpeg2KFormat) : NULL);

			formats = std::auto_ptr<ImageFormatCollection>(new ImageFormatCollection());
		}
	} initializer;

	N_CLASS(NImageFormat)(HNImageFormat handle)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NImageFormat), false)
	{
	}

public:
	static N_CLASS(NImageFormat) * GetBmp()
	{
		return initializer.bmp.get();
	}

	static N_CLASS(NImageFormat) * GetTiff()
	{
		return initializer.tiff.get();
	}

	static N_CLASS(NImageFormat) * GetJpeg()
	{
		return initializer.jpeg.get();
	}

	static N_CLASS(NImageFormat) * GetPng()
	{
		return initializer.png.get();
	}

	static N_CLASS(NImageFormat) * GetWsq()
	{
		return initializer.wsq.get();
	}

	static N_CLASS(NImageFormat) * GetIHead()
	{
		return initializer.iHead.get();
	}

	static N_CLASS(NImageFormat) * GetJpeg2K()
	{
		return initializer.jpeg2K.get();
	}

	static ImageFormatCollection * GetFormats()
	{
		return initializer.formats.get();
	}

	static N_CLASS(NImageFormat) * FromHandle(HNImageFormat handle)
	{
		if (!handle) NThrowArgumentNullException(N_T("handle"));
		if (handle == initializer.hBmpFormat) return initializer.bmp.get();
		else if (handle == initializer.hTiffFormat) return initializer.tiff.get();
		else if (handle == initializer.hJpegFormat) return initializer.jpeg.get();
		else if (handle == initializer.hPngFormat) return initializer.png.get();
		else if (handle == initializer.hWsqFormat) return initializer.wsq.get();
		else if (handle == initializer.hIHeadFormat) return initializer.iHead.get();
		else if (handle == initializer.hJpeg2KFormat) return initializer.jpeg2K.get();
		else NThrowArgumentException(N_T("Unknown NImageFormat handle"));
	}

	static N_CLASS(NImageFormat) * Select(const NChar * szFileName, NFileAccess fileAccess)
	{
		HNImageFormat handle;
		NCheck(NImageFormatSelect(szFileName, fileAccess, &handle));
		return (handle != NULL) ? N_CLASS(NImageFormat)::FromHandle(handle) : NULL;
	}

	static N_CLASS(NImageFormat) * Select(const NString & fileName, NFileAccess fileAccess)
	{
		return N_CLASS(NImageFormat)::Select(NGetConstStringBuffer(fileName), fileAccess);
	}

	N_CLASS(NImageFile)* OpenFile(const void * pBuffer, NSizeType bufferLength);
	N_CLASS(NImageFile)* OpenFile(const NChar * szFileName);
	N_CLASS(NImageFile)* OpenFile(const NString & fileName);
	N_CLASS(NImage)* LoadImage(const NChar * szFileName);
	N_CLASS(NImage)* LoadImage(const NString & fileName);
	N_CLASS(NImage)* LoadImage(const void *pBuffer, NSizeType bufferLength);
	void SaveImage(N_CLASS(NImage) * pImage, const NChar * szFileName);
	void SaveImage(N_CLASS(NImage) * pImage, const NString & fileName);
	void SaveImages(NInt imageCount, N_CLASS(NImage) ** arImages, const NChar * szFileName);
	void SaveImages(NInt imageCount, N_CLASS(NImage) ** arImages, const NString & fileName);

	NInt GetName(NChar * pValue)
	{
		return NCheck(NImageFormatGetName(GetHandle(), pValue));
	}

	NString GetName()
	{
		NString value;
		return NGetString(value, NImageFormatGetName, this);
	}

	NInt GetDefaultFileExtension(NChar * pValue)
	{
		return NCheck(NImageFormatGetDefaultFileExtension(GetHandle(), pValue));
	}

	NString GetDefaultFileExtension()
	{
		NString value;
		return NGetString(value, NImageFormatGetDefaultFileExtension, this);
	}

	NInt GetFileFilter(NChar * pValue)
	{
		return NCheck(NImageFormatGetFileFilter(GetHandle(), pValue));
	}

	NString GetFileFilter()
	{
		NString value;
		return NGetString(value, NImageFormatGetFileFilter, this);
	}

	bool CanRead()
	{
		NBool value;
		NCheck(NImageFormatCanRead(GetHandle(), &value));
		return value != 0;
	}

	bool CanWrite()
	{
		NBool value;
		NCheck(NImageFormatCanWrite(GetHandle(), &value));
		return value != 0;
	}

	bool CanWriteMultiple()
	{
		NBool value;
		NCheck(NImageFormatCanWriteMultiple(GetHandle(), &value));
		return value != 0;
	}

	N_DECLARE_OBJECT_TYPE(NImageFormat)
};

}
}

#include <NImage.hpp>
#include <NImageFile.hpp>

namespace Neurotec { namespace Images
{

inline N_CLASS(NImageFile)* N_CLASS(NImageFormat)::OpenFile(const void * pBuffer, NSizeType bufferLength)
{
	HNImageFile handle;
	NCheck(NImageFormatOpenFileFromMemory(GetHandle(), pBuffer, bufferLength, &handle));
	try
	{
		return N_CLASS(NImageFile)::FromHandle(handle);
	}
	catch(...)
	{
		NObjectFree(handle);
		throw;
	}
}

inline N_CLASS(NImageFile)* N_CLASS(NImageFormat)::OpenFile(const NChar * szFileName)
{
	HNImageFile handle;
	NCheck(NImageFormatOpenFile(GetHandle(), szFileName, &handle));
	try
	{
		return N_CLASS(NImageFile)::FromHandle(handle);
	}
	catch(...)
	{
		NObjectFree(handle);
		throw;
	}
}

inline N_CLASS(NImageFile)* N_CLASS(NImageFormat)::OpenFile(const NString & fileName)
{
	return N_CLASS(NImageFormat)::OpenFile(NGetConstStringBuffer(fileName));
}

inline N_CLASS(NImage)* N_CLASS(NImageFormat)::LoadImage(const NChar * szFileName)
{
	HNImage handle;
	NCheck(NImageFormatLoadImageFromFile(GetHandle(), szFileName, &handle));
	try
	{
		return N_CLASS(NImage)::FromHandle(handle);
	}
	catch(...)
	{
		NObjectFree(handle);
		throw;
	}
}

inline N_CLASS(NImage)* N_CLASS(NImageFormat)::LoadImage(const NString & fileName)
{
	return N_CLASS(NImageFormat)::LoadImage(NGetConstStringBuffer(fileName));
}

inline N_CLASS(NImage)* N_CLASS(NImageFormat)::LoadImage(const void *pBuffer, NSizeType bufferLength)
{
	HNImage handle;
	NCheck(NImageFormatLoadImageFromMemory(GetHandle(), pBuffer, bufferLength, &handle));
	try
	{
		return N_CLASS(NImage)::FromHandle(handle);
	}
	catch(...)
	{
		NObjectFree(handle);
		throw;
	}
}

inline void N_CLASS(NImageFormat)::SaveImage(N_CLASS(NImage) * pImage, const NChar * szFileName)
{
	if (!pImage) NThrowArgumentNullException(N_T("pImage"));
	NCheck(NImageFormatSaveImageToFile(GetHandle(), pImage->GetHandle(), szFileName));
}

inline void N_CLASS(NImageFormat)::SaveImage(N_CLASS(NImage) * pImage, const NString & fileName)
{
	N_CLASS(NImageFormat)::SaveImage(pImage, NGetConstStringBuffer(fileName));
}

inline void N_CLASS(NImageFormat)::SaveImages(NInt imageCount, N_CLASS(NImage) ** arImages, const NChar * szFileName)
{
	if (!arImages) NThrowArgumentNullException(N_T("arImages"));
	if (imageCount < 0) NThrowArgumentLessThanZeroException(N_T("imageCount"));

	std::vector<HNImage> arHImages(imageCount);
	for (NInt i = 0; i < imageCount; i++)
	{
		if (!arImages[0]) NThrowArgumentNullException(N_T("arImages"), N_T("One of arImages elements is NULL"));
		arHImages[i] = arImages[i]->GetHandle();
	}

	NCheck(NImageFormatSaveImagesToFile(GetHandle(), imageCount, imageCount ? &*arHImages.begin() : NULL, szFileName));
}

inline void N_CLASS(NImageFormat)::SaveImages(NInt imageCount, N_CLASS(NImage) ** arImages, const NString & fileName)
{
	N_CLASS(NImageFormat)::SaveImages(imageCount, arImages, NGetConstStringBuffer(fileName));
}

}}

#endif // !N_IMAGE_FORMAT_HPP_INCLUDED
