#ifndef JPEG2K_HPP_INCLUDED
#define JPEG2K_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <Jpeg2K.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(Jpeg2K)
{
private:
	N_CLASS(Jpeg2K)() {}
	N_CLASS(Jpeg2K)(const N_CLASS(Jpeg2K)&) {}
public:
	static const float DefaultRatio;

	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(Jpeg2KLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(Jpeg2KLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NChar * szFileName)
	{
		SaveImage(pImage, DefaultRatio, szFileName);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NString & fileName)
	{
		SaveImage(pImage, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NFloat ratio, const NString & fileName)
	{
		SaveImage(pImage, ratio, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NFloat ratio, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(Jpeg2KSaveImageToFile(pImage->GetHandle(), ratio, szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, void * * pBuffer, NSizeType * pBufferLength)
	{
		SaveImage(pImage, DefaultRatio, pBuffer, pBufferLength);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NFloat ratio, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(Jpeg2KSaveImageToMemory(pImage->GetHandle(), ratio, pBuffer, pBufferLength));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, Jpeg2KProfile profile, const NString & fileName)
	{
		SaveImage(pImage, profile, fileName);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, Jpeg2KProfile profile, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(Jpeg2KSaveImageToFileWithProfile(pImage->GetHandle(), profile, szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, Jpeg2KProfile profile, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(Jpeg2KSaveImageToMemoryWithProfile(pImage->GetHandle(), profile, pBuffer, pBufferLength));
	}
};

const float N_CLASS(Jpeg2K)::DefaultRatio = 10.0f;

}}

#endif // !JPEG2K_HPP_INCLUDED
