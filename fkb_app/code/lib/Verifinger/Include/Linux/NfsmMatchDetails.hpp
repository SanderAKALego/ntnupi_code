#ifndef NFSM_MATCH_DETAILS_HPP_INCLUDED
#define NFSM_MATCH_DETAILS_HPP_INCLUDED

#include <NfmMatchDetails.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NfsmMatchDetails.h>
}}

namespace Neurotec { namespace Biometrics
{

inline void MatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NfsmMatchDetails ** ppMatchDetails)
{
	NCheck(NfsmMatchDetailsDeserialize(buffer, bufferLength, ppMatchDetails));
}

inline NSizeType MatchDetailsSerialize(NfsmMatchDetails * pMatchDetails, void * * pBuffer)
{
	NSizeType bufferLength;
	NCheck(NfsmMatchDetailsSerialize(pMatchDetails, pBuffer, &bufferLength));
	return bufferLength;
}

}}

#endif // !NFSM_MATCH_DETAILS_HPP_INCLUDED
