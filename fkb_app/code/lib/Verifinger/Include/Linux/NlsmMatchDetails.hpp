#ifndef NLSM_MATCH_DETAILS_HPP_INCLUDED
#define NLSM_MATCH_DETAILS_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
using Neurotec::IO::HNStream;
#include <NlsmMatchDetails.h>
}}

namespace Neurotec { namespace Biometrics
{

inline void MatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NlsmMatchDetails ** ppMatchDetails)
{
	NCheck(NlsmMatchDetailsDeserialize(buffer, bufferLength, ppMatchDetails));
}

inline NSizeType MatchDetailsSerialize(NlsmMatchDetails * pMatchDetails, void * * pBuffer)
{
	NSizeType bufferLength;
	NCheck(NlsmMatchDetailsSerialize(pMatchDetails, pBuffer, &bufferLength));
	return bufferLength;
}

}}

#endif // !NLSM_MATCH_DETAILS_HPP_INCLUDED
