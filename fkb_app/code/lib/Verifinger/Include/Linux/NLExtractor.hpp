#ifndef NL_EXTRACTOR_HPP_INCLUDED
#define NL_EXTRACTOR_HPP_INCLUDED

#include <NGrayscaleImage.hpp>
#include <NGeometry.hpp>
#include <NLTemplate.hpp>
#include <NLExtractorParams.hpp>
namespace Neurotec { namespace Biometrics
{
using Neurotec::Images::HNGrayscaleImage;
#include <NLExtractor.h>
}}
#include <vector>

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NLExtractor) : public N_CLASS(NObject)
{
private:
	static HNLExtractor Create()
	{
		HNLExtractor handle;
		NCheck(NleCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NLExtractor) * FromHandle(HNLExtractor handle, bool ownsHandle = true)
	{
		return new N_CLASS(NLExtractor)(handle, ownsHandle);
	}

	static N_CLASS(NLTemplate) * Compress(N_CLASS(NLTemplate) * pTemplate, NleTemplateSize dstTemplateSize)
	{
		HNLTemplate hTemplate;
		if (pTemplate == NULL)
			NThrowArgumentNullException(N_T("pTemplate"));
		NCheck(NleCompressEx(pTemplate->GetHandle(), dstTemplateSize, &hTemplate));
		try
		{
			return ((hTemplate == NULL) ? NULL : N_CLASS(NLTemplate)::FromHandle(hTemplate));
		}
		catch(...)
		{
			NObjectFree(hTemplate);
			throw;
		}
	}

	explicit N_CLASS(NLExtractor)(HNLExtractor handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NLExtractor), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NLExtractor)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NLExtractor), true)
	{
	}

	// Returns faceCount
	NInt DetectFaces(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NleFace ** pArFaces)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NInt faceCount;
		NCheck(NleDetectFaces(GetHandle(), pImage->GetHandle(), &faceCount, pArFaces));
		return faceCount;
	}

	bool DetectFace(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NleFace * pFace)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NBool detected;
		NCheck(NleDetectFace(GetHandle(), pImage->GetHandle(), &detected, pFace));
		return detected == NTrue;
	}

	NleDetectionDetails DetectFacialFeatures(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NleFace * pFace)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NleDetectionDetails details;
		NCheck(NleDetectFacialFeatures(GetHandle(), pImage->GetHandle(), pFace, &details));
		return details;
	}

	N_CLASS(NLTemplate) * Extract(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NleDetectionDetails * pDetectionDetails, NleExtractionStatus * pExtractionStatus)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		HNLTemplate hTemplate;
		NCheck(NleExtract(GetHandle(), pImage->GetHandle(), pDetectionDetails, pExtractionStatus, &hTemplate));
		try
		{
			return hTemplate == NULL ? NULL : N_CLASS(NLTemplate)::FromHandle(hTemplate);
		}
		catch(...)
		{
			NObjectFree(hTemplate);
			throw;
		}
	}

	void ExtractStart(NInt durationInFrames)
	{
		NCheck(NleExtractStart(GetHandle(), durationInFrames));
	}

	NleExtractionStatus ExtractNext(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NleDetectionDetails * pDetectionDetails, NInt * pBaseFrameIndex, N_CLASS(NLTemplate) * * ppTemplate)
	{
		NleExtractionStatus status;
		HNLTemplate hTemplate;
		if (!pImage) NThrowArgumentNullException(N_T("pImage"));
		if (!ppTemplate) NThrowArgumentNullException(N_T("ppTemplate"));
		NCheck(NleExtractNextEx(GetHandle(), pImage->GetHandle(), pDetectionDetails, pBaseFrameIndex, &hTemplate, &status));
		try
		{
			*ppTemplate = !hTemplate ? NULL : N_CLASS(NLTemplate)::FromHandle(hTemplate);
			return status;
		}
		catch(...)
		{
			NObjectFree(hTemplate);
			throw;
		}
	}

	N_CLASS(NLTemplate) * ExtractUsingDetails(Neurotec::Images::N_CLASS(NGrayscaleImage) * pImage, NleDetectionDetails * pDetectionDetails, NleExtractionStatus * pExtractionStatus)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		HNLTemplate hTemplate;
		NCheck(NleExtractUsingDetails(GetHandle(), pImage->GetHandle(), pDetectionDetails, pExtractionStatus, &hTemplate));
		try
		{
			return hTemplate == NULL ? NULL : N_CLASS(NLTemplate)::FromHandle(hTemplate);
		}
		catch(...)
		{
			NObjectFree(hTemplate);
			throw;
		}
	}

	N_CLASS(NLTemplate) * Generalize(NInt templateCount, N_CLASS(NLTemplate) ** arTemplates, NleExtractionStatus * pStatus, NInt * pBaseTemplateIndex)
	{
		if (arTemplates == NULL)
			NThrowArgumentNullException(N_T("arTemplates"));
		if (templateCount < 0)
			NThrowArgumentLessThanZeroException(N_T("templateCount"));

		std::vector<HNLTemplate> arHTemplates(templateCount);
		HNLTemplate hTemplate;

		for (NInt i = 0; i != templateCount; i++)
		{
			if (arTemplates[i] == NULL)
				NThrowArgumentNullException(N_T("arTemplates"), N_T("One of templates elements is null"));
			arHTemplates[i] = arTemplates[i]->GetHandle();
		}

		NCheck(NleGeneralizeEx(GetHandle(), templateCount, templateCount ? &*arHTemplates.begin() : NULL, pStatus, pBaseTemplateIndex, &hTemplate));

		try
		{
			return !hTemplate ? NULL : N_CLASS(NLTemplate)::FromHandle(hTemplate);
		}
		catch(...)
		{
			NObjectFree(hTemplate);
			throw;
		}
	}

	NInt GetMinIod()
	{
		return GetParameterAsInt32(NLEP_MIN_IOD);
	}

	void SetMinIod(NInt value)
	{
		SetParameter(NLEP_MIN_IOD, value);
	}

	NInt GetMaxIod()
	{
		return GetParameterAsInt32(NLEP_MAX_IOD);
	}

	void SetMaxIod(NInt value)
	{
		SetParameter(NLEP_MAX_IOD, value);
	}

	NDouble GetFaceConfidenceThreshold()
	{
		return GetParameterAsDouble(NLEP_FACE_CONFIDENCE_THRESHOLD);
	}

	void SetFaceConfidenceThreshold(NDouble value)
	{
		SetParameter(NLEP_FACE_CONFIDENCE_THRESHOLD, value);
	}

	bool GetFavorLargestFace()
	{
		return GetParameterAsBoolean(NLEP_FAVOR_LARGEST_FACE);
	}

	void SetFavorLargestFace(bool value)
	{
		SetParameter(NLEP_FAVOR_LARGEST_FACE, value);
	}

	NShort GetMaxRollAngleDeviation()
	{
		return GetParameterAsInt16(NLEP_MAX_ROLL_ANGLE_DEVIATION);
	}

	void SetMaxRollAngleDeviation(NShort value)
	{
		SetParameter(NLEP_MAX_ROLL_ANGLE_DEVIATION, value);
	}

	NByte GetFaceQualityThreshold()
	{
		return GetParameterAsByte(NLEP_FACE_QUALITY_THRESHOLD);
	}

	void SetFaceQualityThreshold(NByte value)
	{
		SetParameter(NLEP_FACE_QUALITY_THRESHOLD, value);
	}

	NleTemplateSize GetTemplateSize()
	{
		return (NleTemplateSize)GetParameterAsInt32(NLEP_TEMPLATE_SIZE);
	}

	void SetTemplateSize(NleTemplateSize value)
	{
		SetParameter(NLEP_TEMPLATE_SIZE, (NInt)value);
	}

	bool GetUseLivenessCheck()
	{
		return GetParameterAsBoolean(NLEP_USE_LIVENESS_CHECK);
	}

	void SetUseLivenessCheck(bool value)
	{
		SetParameter(NLEP_USE_LIVENESS_CHECK, value);
	}

	NDouble GetLivenessThreshold()
	{
		return GetParameterAsDouble(NLEP_LIVENESS_THRESHOLD);
	}

	void SetLivenessThreshold(NDouble value)
	{
		SetParameter(NLEP_LIVENESS_THRESHOLD, value);
	}

	NInt GetMaxRecordsPerTemplate()
	{
		return GetParameterAsInt32(NLEP_MAX_RECORDS_PER_TEMPLATE);
	}

	void SetMaxRecordsPerTemplate(NInt value)
	{
		SetParameter(NLEP_MAX_RECORDS_PER_TEMPLATE, value);
	}

	N_DECLARE_OBJECT_TYPE(NLExtractor)
};

}}

#endif // !NL_EXTRACTOR_HPP_INCLUDED
