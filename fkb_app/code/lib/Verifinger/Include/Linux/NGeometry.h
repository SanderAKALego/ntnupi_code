#ifndef N_GEOMETRY_H_INCLUDED
#define N_GEOMETRY_H_INCLUDED

#include <NTypes.h>

#ifdef N_CPP
extern "C"
{
#endif

typedef struct NPoint_
{
	NInt X;
	NInt Y;
} NPoint;

#ifndef N_NO_FLOAT

typedef struct NPointF_
{
	NFloat X;
	NFloat Y;
} NPointF;

typedef struct NPointD_
{
	NDouble X;
	NDouble Y;
} NPointD;

#endif

typedef struct NSize_
{
	NInt Width;
	NInt Height;
} NSize;

#ifndef N_NO_FLOAT

typedef struct NSizeF_
{
	NFloat Width;
	NFloat Height;
} NSizeF;

typedef struct NSizeD_
{
	NDouble Width;
	NDouble Height;
} NSizeD;

#endif

typedef struct NRect_
{
	NInt X;
	NInt Y;
	NInt Width;
	NInt Height;
} NRect;

#ifndef N_NO_FLOAT

typedef struct NRectF_
{
	NFloat X;
	NFloat Y;
	NFloat Width;
	NFloat Height;
} NRectF;

typedef struct NRectD_
{
	NDouble X;
	NDouble Y;
	NDouble Width;
	NDouble Height;
} NRectD;

#endif

#ifdef N_CPP
}
#endif

#endif // !N_GEOMETRY_H_INCLUDED
