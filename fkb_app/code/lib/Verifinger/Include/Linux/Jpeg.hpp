#ifndef JPEG_HPP_INCLUDED
#define JPEG_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <Jpeg.h>
}}

namespace Neurotec { namespace Images
{

class Jpeg
{
private:
	Jpeg() {}
	Jpeg(const Jpeg&) {}

public:
	static const NInt DefaultQuality = 75;

	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(JpegLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(JpegLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NChar * szFileName)
	{
		SaveImage(pImage, DefaultQuality, szFileName);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, const NString & fileName)
	{
		SaveImage(pImage, DefaultQuality, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NInt quality, const NChar * szFileName)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(JpegSaveImageToFile(pImage->GetHandle(), quality, szFileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NInt quality, const NString & fileName)
	{
		SaveImage(pImage, quality, NGetConstStringBuffer(fileName));
	}

	static void SaveImage(N_CLASS(NImage)* pImage, void * * pBuffer, NSizeType * pBufferLength)
	{
		SaveImage(pImage, DefaultQuality, pBuffer, pBufferLength);
	}

	static void SaveImage(N_CLASS(NImage)* pImage, NInt quality, void * * pBuffer, NSizeType * pBufferLength)
	{
		if (pImage == NULL)
			NThrowArgumentNullException(N_T("pImage"));
		NCheck(JpegSaveImageToMemory(pImage->GetHandle(), quality, pBuffer, pBufferLength));
	}
};

}}

#endif // !JPEG_HPP_INCLUDED
