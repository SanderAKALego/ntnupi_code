#ifndef N_STREAM_HPP_INCLUDED
#define N_STREAM_HPP_INCLUDED

#include <NObject.hpp>
namespace Neurotec { namespace IO
{
#include <NStream.h>
}}

namespace Neurotec { namespace IO
{
#undef NIsReverseByteOrder

inline bool NIsReverseByteOrder(NByteOrder byteOrder)
{
	return byteOrder != nboSystem;
}
}}

#endif // !N_STREAM_HPP_INCLUDED
