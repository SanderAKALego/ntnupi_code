#ifndef NF_TEMPLATE_HPP_INCLUDED
#define NF_TEMPLATE_HPP_INCLUDED

#include <NFRecord.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NFTemplate.h>
}}

namespace Neurotec { namespace Biometrics
{
#undef NFT_MAX_RECORD_COUNT
#undef NFT_PROCESS_FIRST_RECORD_ONLY

const NInt NFT_MAX_RECORD_COUNT = 255;
const NUInt NFT_PROCESS_FIRST_RECORD_ONLY = 0x00000100;

class N_CLASS(NFTemplate) : public N_CLASS(NObject)
{
public:
	class RecordCollection : public ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NFRecord)>
	{
		RecordCollection(N_CLASS(NFTemplate) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NFRecord)>(pOwner, NFTemplateGetRecordCount, NFTemplateGetRecord, FromHandle,
			NFTemplateGetRecordCapacity, NFTemplateSetRecordCapacity, NFTemplateRemoveRecord, NULL, NFTemplateClearRecords)
		{
		}

		static N_CLASS(NFRecord) * FromHandle(HNFRecord handle)
		{
			return N_CLASS(NFRecord)::FromHandle(handle, false);
		}

		friend class N_CLASS(NFTemplate);

	public:
		N_CLASS(NFRecord)* Add(NUShort width, NUShort height, NUShort horzResolution, NUShort vertResolution, NUInt flags = 0)
		{
			HNFRecord hRecord;
			NCheck(NFTemplateAddRecord(GetOwner()->GetHandle(), width, height,
				horzResolution, vertResolution, flags, &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NFRecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NFTemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}

		N_CLASS(NFRecord)* Add(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
		{
			HNFRecord hRecord;
			NCheck(NFTemplateAddRecordFromMemory(GetOwner()->GetHandle(), pBuffer, bufferSize, flags, &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NFRecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NFTemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}

		N_CLASS(NFRecord)* AddCopy(N_CLASS(NFRecord)* pSrcRecord)
		{
			HNFRecord hRecord;
			if (pSrcRecord == NULL)
			{
				NThrowArgumentNullException(N_T("pSrcRecord"));
			}

			NCheck(NFTemplateAddRecordCopy(GetOwner()->GetHandle(), pSrcRecord->GetHandle(), &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NFRecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NFTemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}
	};

private:
	static HNFTemplate Create(bool isPalm, NUInt flags)
	{
		HNFTemplate handle;
		NCheck(NFTemplateCreateEx(isPalm, flags, &handle));
		return handle;
	}

	static HNFTemplate Create(const void * pBuffer, NSizeType bufferSize,
		NUInt flags, NFTemplateInfo * pInfo)
	{
		HNFTemplate handle;
		NCheck(NFTemplateCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

	std::auto_ptr<RecordCollection> records;

	void Init()
	{
		records = std::auto_ptr<RecordCollection>(new RecordCollection(this));
	}

public:
	static N_CLASS(NFTemplate)* FromHandle(HNFTemplate handle, bool ownsHandle = true)
	{
		return new N_CLASS(NFTemplate)(handle, ownsHandle);
	}

	static NSizeType CalculateSize(NBool isPalm, NInt recordCount, NSizeType * arRecordSizes)
	{
		NSizeType value;
		NCheck(NFTemplateCalculateSize(isPalm, recordCount, arRecordSizes, &value));
		return value;
	}

	static NSizeType Pack(NBool isPalm, NInt recordCount, const void * * arPRecords, NSizeType * arRecordSizes, void * pBuffer, NSizeType bufferSize)
	{
		NSizeType value;
		NCheck(NFTemplatePack(isPalm, recordCount, arPRecords, arRecordSizes, pBuffer, bufferSize, &value));
		return value;
	}

	static void Unpack(const void * pBuffer, NSizeType bufferSize, NBool *isPalm,
		NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
		NInt * pRecordCount, const void * * arPRecords, NSizeType * arRecordSizes)
	{
		NCheck(NFTemplateUnpack(pBuffer, bufferSize, isPalm,
			pMajorVersion, pMinorVersion, pSize, pHeaderSize,
			pRecordCount, arPRecords, arRecordSizes));
	}

	static void Check(const void * pBuffer, NSizeType bufferSize)
	{
		NCheck(NFTemplateCheck(pBuffer, bufferSize));
	}

	static NBool IsPalm(const void * pBuffer, NSizeType bufferSize)
	{
		NBool value;
		NCheck(NFTemplateIsPalmMem(pBuffer, bufferSize, &value));
		return value;
	}

	static NInt GetRecordCount(const void * pBuffer, NSizeType bufferSize)
	{
		NInt value;
		NCheck(NFTemplateGetRecordCountMem(pBuffer, bufferSize, &value));
		return value;
	}

	explicit N_CLASS(NFTemplate)(HNFTemplate handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NFTemplate), false)
	{
		Init();
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NFTemplate)(bool isPalm = false, NUInt flags = 0)
		: N_CLASS(NObject)(Create(isPalm, flags), N_NATIVE_TYPE_OF(NFTemplate), true)
	{
		Init();
	}

	N_CLASS(NFTemplate)(const void * pBuffer, NSizeType bufferSize,
		NUInt flags = 0, NFTemplateInfo * pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NFTemplate), true)
	{
		Init();
	}

	RecordCollection * GetRecords() const
	{
		return records.get();
	}

	N_CLASS(NObject) * Clone() const
	{
		HNFTemplate handle;
		NCheck(NFTemplateClone((HNFTemplate)GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NFTemplateGetSize((HNFTemplate)GetHandle(), flags, &value));
		return value;
	}

	NSizeType Save(void * pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NFTemplateSaveToMemory((HNFTemplate)GetHandle(), pBuffer, bufferSize, flags, &value));
		return value;
	}

	NBool IsPalm() const
	{
		NBool value;
		NCheck(NFTemplateIsPalm((HNFTemplate)GetHandle(), &value));
		return value;
	}

	N_DECLARE_OBJECT_TYPE(NFTemplate)
};

}}

#endif // !NF_TEMPLATE_HPP_INCLUDED
