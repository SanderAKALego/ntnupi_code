#ifndef NEM_MATCH_DETAILS_HPP_INCLUDED
#define NEM_MATCH_DETAILS_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
using Neurotec::IO::HNStream;
#include <NemMatchDetails.h>
}}

namespace Neurotec { namespace Biometrics
{

inline void MatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NemMatchDetails ** ppMatchDetails)
{
	NCheck(NemMatchDetailsDeserialize(buffer, bufferLength, ppMatchDetails));
}

inline NSizeType MatchDetailsSerialize(NemMatchDetails * pMatchDetails, void * * pBuffer)
{
	NSizeType bufferLength;
	NCheck(NemMatchDetailsSerialize(pMatchDetails, pBuffer, &bufferLength));
	return bufferLength;
}

}}

#endif // !NEM_MATCH_DETAILS_HPP_INCLUDED
