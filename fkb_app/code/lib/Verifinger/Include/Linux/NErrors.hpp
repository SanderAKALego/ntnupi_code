#ifndef N_ERRORS_HPP_INCLUDED
#define N_ERRORS_HPP_INCLUDED

#include <NTypes.hpp>
#include <NMemory.hpp>
#include <NObject.hpp>
namespace Neurotec
{
#include <NErrors.h>
}
#include <stdio.h>
#ifdef N_WINDOWS
	#include <NWindows.hpp>
#else
	#include <errno.h>
#endif

namespace Neurotec
{
#undef N_OK
#undef N_E_FAILED
#undef N_E_CORE
#undef N_E_ARGUMENT
#undef N_E_ARGUMENT_NULL
#undef N_E_ARGUMENT_OUT_OF_RANGE
#undef N_E_INVALID_ENUM_ARGUMENT
#undef N_E_ARITHMETIC
#undef N_E_OVERFLOW
#undef N_E_FORMAT
#undef N_E_INDEX_OUT_OF_RANGE
#undef N_E_INVALID_CAST
#undef N_E_INVALID_OPERATION
#undef N_E_IO
#undef N_E_DIRECTORY_NOT_FOUND
#undef N_E_DRIVE_NOT_FOUND
#undef N_E_END_OF_STREAM
#undef N_E_FILE_NOT_FOUND
#undef N_E_FILE_LOAD
#undef N_E_PATH_TOO_LONG
#undef N_E_NOT_IMPLEMENTED
#undef N_E_NOT_SUPPORTED
#undef N_E_NULL_REFERENCE
#undef N_E_OUT_OF_MEMORY
#undef N_E_SECURITY
#undef N_E_EXTERNAL
#undef N_E_CLR
#undef N_E_COM
#undef N_E_SYS
#undef N_E_WIN32
#undef N_E_PARAMETER
#undef N_E_PARAMETER_READ_ONLY
#undef N_E_NOT_ACTIVATED

#undef NFailed
#undef NSucceeded

const NResult N_OK                            =   0;
const NResult N_E_FAILED                      =  -1;
  const NResult N_E_CORE                      =  -2;
    const NResult N_E_ARGUMENT                = -10;
      const NResult N_E_ARGUMENT_NULL         = -11;
      const NResult N_E_ARGUMENT_OUT_OF_RANGE = -12;
      const NResult N_E_INVALID_ENUM_ARGUMENT = -16;
    const NResult N_E_ARITHMETIC              = -17;
      const NResult N_E_OVERFLOW              =  -8;
    const NResult N_E_FORMAT                  = -13;
    const NResult N_E_INDEX_OUT_OF_RANGE      =  -9;
    const NResult N_E_INVALID_CAST            = -18;
    const NResult N_E_INVALID_OPERATION       =  -7;
    const NResult N_E_IO                      = -14;
      const NResult N_E_DIRECTORY_NOT_FOUND   = -19;
      const NResult N_E_DRIVE_NOT_FOUND       = -20;
      const NResult N_E_END_OF_STREAM         = -15;
      const NResult N_E_FILE_NOT_FOUND        = -21;
      const NResult N_E_FILE_LOAD             = -22;
      const NResult N_E_PATH_TOO_LONG         = -23;
    const NResult N_E_NOT_IMPLEMENTED         =  -5;
    const NResult N_E_NOT_SUPPORTED           =  -6;
    const NResult N_E_NULL_REFERENCE          =  -3;
    const NResult N_E_OUT_OF_MEMORY           =  -4;
    const NResult N_E_SECURITY                = -24;

    const NResult N_E_EXTERNAL                = -90;
      const NResult N_E_CLR                   = -93;
      const NResult N_E_COM                   = -92;
      const NResult N_E_SYS                   = -94;
      const NResult N_E_WIN32                 = -91;

    const NResult N_E_PARAMETER              = -100;
      const NResult N_E_PARAMETER_READ_ONLY  = -101;

    const NResult N_E_NOT_ACTIVATED          = -200;

inline bool NFailed(NResult result)
{
	return result < 0;
}

inline bool NSucceeded(NResult result)
{
	return result >= 0;
}

class N_CLASS(NException)
#if defined(N_FRAMEWORK_MFC)
	: public CException
#else
	: public N_CLASS(NObjectBase)
#endif
{
private:
	static NString GetDefaultMessage(NResult code)
	{
		NInt mLen = NErrorGetDefaultMessage(code, NULL);
		if(mLen == -1) return NString();
		NString message;
		if (NErrorGetDefaultMessage(code, NGetStringBuffer(message, mLen)) == -1) return NString();
		NReleaseStringBuffer(message, mLen);
		return message;
	}

	static NInt GetCode(HNError hError)
	{
		return NErrorGetCode(hError);
	}

	static NString GetMessage(HNError hError)
	{
		NInt mLen = NErrorGetMessage(hError, NULL);
		if (mLen == -1) return NString();
		NString message;
		if (NErrorGetMessage(hError, NGetStringBuffer(message, mLen)) == -1) return NString();
		NReleaseStringBuffer(message, mLen);
		return message;
	}

	static NString GetParam(HNError hError)
	{
		NInt pLen = NErrorGetParam(hError, NULL);
		if (pLen == -1) return NString();
		NString param;
		if (NErrorGetParam(hError, NGetStringBuffer(param, pLen)) == -1) return NString();
		NReleaseStringBuffer(param, pLen);
		return param;
	}

	static NString GetCallStack(HNError hError)
	{
		NString callStack;
		NInt ecsLen = NErrorGetExternalCallStack(hError, NULL);
		if (ecsLen == -1) return NString();
		if (NErrorGetExternalCallStack(hError, NGetStringBuffer(callStack, ecsLen)) == -1) return NString();
		NReleaseStringBuffer(callStack, ecsLen);
		if (ecsLen != 0) callStack += N_T("\n");

		NInt csLen = NErrorGetCallStackLength(hError);
		if (csLen == -1) return NString();
		for (NInt i = 0; i != csLen; i++)
		{
			NInt fnLen = NErrorGetCallStackFunction(hError, i, NULL);
			if (fnLen == -1) return NString();
			NString function;
			if (NErrorGetCallStackFunction(hError, i, NGetStringBuffer(function, fnLen)) == -1) return NString();
			NReleaseStringBuffer(function, fnLen);
			NInt flLen = NErrorGetCallStackFile(hError, i, NULL);
			if (flLen == -1) return NString();
			if (flLen != 0)
			{
				NString file;
				if (NErrorGetCallStackFile(hError, i, NGetStringBuffer(file, flLen)) == -1) return NString();
				NReleaseStringBuffer(file, flLen);
				NInt line = NErrorGetCallStackLine(hError, i);
				if (line == -1) return NString();
				callStack += N_T("   at ");
				callStack += function;
				callStack += N_T(" in ");
				callStack += file;
				callStack += N_T(":line ");
				callStack += IntToString(line);
				callStack += N_T("\n");
			}
			else
			{
				callStack += N_T("   at ");
				callStack += function;
				callStack += N_T("\n");
			}
		}

		return callStack;
	}

	static HNError GetInnerError(HNError hError)
	{
		return NErrorGetInnerError(hError);
	}

	static NHandle GetClrError(HNError hError)
	{
		return NErrorGetClrError(hError);
	}

	static NInt GetExternalError(HNError hError)
	{
		return NErrorGetExternalError(hError);
	}

	static N_CLASS(NException) * GetException(NResult code, const NString & message, const NString & param, NHandle hClrError, NInt externalError);

	static N_CLASS(NException) * GetException(HNError hError)
	{
		HNError hInnerError = GetInnerError(hError);
		N_CLASS(NException) * pException = GetException(GetCode(hError), GetMessage(hError), GetParam(hError), GetClrError(hError), GetExternalError(hError));
		pException->callStack = GetCallStack(hError);
		pException->pInnerException = !hInnerError ? NULL : GetException(hInnerError);
		return pException;
	}

	static N_CLASS(NException) * GetLast(NResult code)
	{
		HNError hError = NErrorGetLast();
		if (!hError || GetCode(hError) != code)
		{
			return GetException(code, N_CLASS(NException)::GetDefaultMessage(code), NString(), NULL, 0);
		}
		else
		{
			return GetException(hError);
		}
	}

	static void CheckCode(NResult code);

	NResult code;
	NString message;
	NString callStack;
	N_CLASS(NException) * pInnerException;

	N_CLASS(NException)(const N_CLASS(NException) &)
	{
	}

protected:
	static NString IntToString(NInt value)
	{
		NChar szValue[11];
		#ifdef N_UNICODE
			#ifdef N_MSVC
				#ifdef N_WINDOWS_CE
					swprintf(szValue, L"%d", value);
				#else
					swprintf_s(szValue, sizeof(szValue) / sizeof(szValue[0]), L"%d", value);
				#endif
			#else
				swprintf(szValue, sizeof(szValue) / sizeof(szValue[0]), L"%d", value);
			#endif
		#else
			#ifdef N_MSVC
				sprintf_s(szValue, sizeof(szValue) / sizeof(szValue[0]), "%d", value);
			#else
				sprintf(szValue, "%d", value);
			#endif
		#endif
		return szValue;
	}

	N_CLASS(NException)(NResult code, N_CLASS(NException) * pInnerException)
		: code(code), message(GetDefaultMessage(code)), callStack(), pInnerException(pInnerException)
		#ifdef N_FRAMEWORK_MFC
			, CException(TRUE)
		#endif
	{
		CheckCode(code);
	}

	N_CLASS(NException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: code(code), message(message), callStack(), pInnerException(pInnerException)
		#ifdef N_FRAMEWORK_MFC
			, CException(TRUE)
		#endif
	{
		CheckCode(code);
	}

	virtual NString GetParams() const
	{
		return NString();
	}

public:
	N_CLASS(NException)()
		: code(N_E_FAILED), message(GetDefaultMessage(code)), callStack(), pInnerException(NULL)
		#ifdef N_FRAMEWORK_MFC
			, CException(TRUE)
		#endif
	{
		CheckCode(code);
	}

	explicit N_CLASS(NException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: code(N_E_FAILED), message(message), callStack(), pInnerException(pInnerException)
		#ifdef N_FRAMEWORK_MFC
			, CException(TRUE)
		#endif
	{
		CheckCode(code);
	}

	virtual ~N_CLASS(NException)()
	{
		if(pInnerException)
		{
			delete pInnerException;
		}
	}

	NResult GetCode() const
	{
		return code;
	}

	const NString & GetMessage() const
	{
		return message;
	}

	const NString & GetCallStack() const
	{
		return callStack;
	}

	N_CLASS(NException) * GetInnerException() const
	{
		return pInnerException;
	}

	N_CLASS(NException) * GetBaseException() const
	{
		return pInnerException ? pInnerException->GetBaseException() : (N_CLASS(NException) *)this;
	}

	NString ToString() const
	{
		NString value = N_GET_OBJECT_TYPE_NAME(this);
		value += N_T(": ");
		value += message;
		NString params = GetParams();
		if(NGetStringLength(params) != 0)
		{
			value += N_T("\n");
			value += params;
		}
		if (pInnerException)
		{
			value += N_T(" ---> ");
			value += pInnerException->ToString();
			value += N_T("\n   --- End of inner exception stack trace ---");
		}
		if(NGetStringLength(callStack) != 0)
		{
			value += N_T("\n");
			value += callStack;
		}
		return value;
	}

	N_DECLARE_TYPE(NException)

	friend void N_NO_RETURN NRaiseError(NResult error);
};

class N_CLASS(NCoreException) : public N_CLASS(NException)
{
protected:
	N_CLASS(NCoreException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NException)(code, pInnerException)
	{
	}

	N_CLASS(NCoreException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NCoreException)()
		: N_CLASS(NException)(N_E_CORE, NULL)
	{
	}

	explicit N_CLASS(NCoreException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NException)(N_E_CORE, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NCoreException)
};

class N_CLASS(NArgumentException) : public N_CLASS(NCoreException)
{
private:
	NString paramName;

protected:
	N_CLASS(NArgumentException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException), paramName()
	{
	}

	N_CLASS(NArgumentException)(NResult code, const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException), paramName(paramName)
	{
	}

	virtual NString GetParams() const
	{
		return NGetStringLength(paramName) == 0 ? N_CLASS(NException)::GetParams() : N_T("ParamName: ") + paramName;
	}

public:
	N_CLASS(NArgumentException)()
		: N_CLASS(NCoreException)(N_E_ARGUMENT, NULL), paramName()
	{
	}

	explicit N_CLASS(NArgumentException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_ARGUMENT, message, pInnerException), paramName()
	{
	}

	N_CLASS(NArgumentException)(const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_ARGUMENT, message, pInnerException), paramName(paramName)
	{
	}

	const NString & GetParamName() const
	{
		return paramName;
	}

	N_DECLARE_TYPE(NArgumentException)
};

class N_CLASS(NArgumentNullException) : public N_CLASS(NArgumentException)
{
private:
	static NString GetMessage(const NString & paramName)
	{
		return paramName + N_T(" is NULL");
	}

protected:
	N_CLASS(NArgumentNullException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(code, pInnerException)
	{
	}

	N_CLASS(NArgumentNullException)(NResult code, const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(code, message, paramName, pInnerException)
	{
	}

public:
	N_CLASS(NArgumentNullException)()
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_NULL, NULL)
	{
	}

	explicit N_CLASS(NArgumentNullException)(const NString & paramName)
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_NULL, GetMessage(paramName), paramName, NULL)
	{
	}

	N_CLASS(NArgumentNullException)(const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_NULL, message, NString(), pInnerException)
	{
	}

	N_CLASS(NArgumentNullException)(const NString & paramName, const NString & message)
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_NULL, message, paramName, NULL)
	{
	}

	N_DECLARE_TYPE(NArgumentNullException)
};

class N_CLASS(NArgumentOutOfRangeException) : public N_CLASS(NArgumentException)
{
private:
	static NString GetMessage(const NString & paramName)
	{
		return paramName + N_T(" is out of range");
	}

protected:
	N_CLASS(NArgumentOutOfRangeException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(code, pInnerException)
	{
	}

	N_CLASS(NArgumentOutOfRangeException)(NResult code, const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(code, message, paramName, pInnerException)
	{
	}

public:
	N_CLASS(NArgumentOutOfRangeException)()
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_NULL, NULL)
	{
	}

	explicit N_CLASS(NArgumentOutOfRangeException)(const NString & paramName)
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_OUT_OF_RANGE, GetMessage(paramName), paramName, NULL)
	{
	}

	N_CLASS(NArgumentOutOfRangeException)(const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_OUT_OF_RANGE, message, NString(), pInnerException)
	{
	}

	N_CLASS(NArgumentOutOfRangeException)(const NString & paramName, const NString & message)
		: N_CLASS(NArgumentException)(N_E_ARGUMENT_OUT_OF_RANGE, message, paramName, NULL)
	{
	}

	N_DECLARE_TYPE(NArgumentOutOfRangeException)
};

class N_CLASS(NInvalidEnumArgumentException) : public N_CLASS(NArgumentException)
{
private:
	static NString GetMessage(const NString & paramName)
	{
		return paramName + N_T(" is an invalid enum value");
	}

protected:
	N_CLASS(NInvalidEnumArgumentException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(code, pInnerException)
	{
	}

	N_CLASS(NInvalidEnumArgumentException)(NResult code, const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(code, message, paramName, pInnerException)
	{
	}

public:
	N_CLASS(NInvalidEnumArgumentException)()
		: N_CLASS(NArgumentException)(N_E_INVALID_ENUM_ARGUMENT, NULL)
	{
	}

	explicit N_CLASS(NInvalidEnumArgumentException)(const NString & paramName)
		: N_CLASS(NArgumentException)(N_E_INVALID_ENUM_ARGUMENT, GetMessage(paramName), paramName, NULL)
	{
	}

	N_CLASS(NInvalidEnumArgumentException)(const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArgumentException)(N_E_INVALID_ENUM_ARGUMENT, message, NString(), pInnerException)
	{
	}

	N_CLASS(NInvalidEnumArgumentException)(const NString & paramName, const NString & message)
		: N_CLASS(NArgumentException)(N_E_INVALID_ENUM_ARGUMENT, message, paramName, NULL)
	{
	}

	N_DECLARE_TYPE(NInvalidEnumArgumentException)
};

class N_CLASS(NArithmeticException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NArithmeticException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NArithmeticException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NArithmeticException)()
		: N_CLASS(NCoreException)(N_E_ARITHMETIC, NULL)
	{
	}

	explicit N_CLASS(NArithmeticException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_ARITHMETIC, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NArithmeticException)
};

class N_CLASS(NOverflowException) : public N_CLASS(NArithmeticException)
{
protected:
	N_CLASS(NOverflowException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArithmeticException)(code, pInnerException)
	{
	}

	N_CLASS(NOverflowException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NArithmeticException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NOverflowException)()
		: N_CLASS(NArithmeticException)(N_E_OVERFLOW, NULL)
	{
	}

	explicit N_CLASS(NOverflowException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NArithmeticException)(N_E_OVERFLOW, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NOverflowException)
};

class N_CLASS(NFormatException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NFormatException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NFormatException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NFormatException)()
		: N_CLASS(NCoreException)(N_E_FORMAT, NULL)
	{
	}

	explicit N_CLASS(NFormatException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_FORMAT, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NFormatException)
};

class N_CLASS(NIndexOutOfRangeException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NIndexOutOfRangeException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NIndexOutOfRangeException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NIndexOutOfRangeException)()
		: N_CLASS(NCoreException)(N_E_INDEX_OUT_OF_RANGE, NULL)
	{
	}

	explicit N_CLASS(NIndexOutOfRangeException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_INDEX_OUT_OF_RANGE, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NIndexOutOfRangeException)
};

class N_CLASS(NInvalidCastException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NInvalidCastException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NInvalidCastException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NInvalidCastException)()
		: N_CLASS(NCoreException)(N_E_INVALID_CAST, NULL)
	{
	}

	explicit N_CLASS(NInvalidCastException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_INVALID_CAST, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NInvalidCastException)
};

class N_CLASS(NInvalidOperationException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NInvalidOperationException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NInvalidOperationException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NInvalidOperationException)()
		: N_CLASS(NCoreException)(N_E_INVALID_OPERATION, NULL)
	{
	}

	explicit N_CLASS(NInvalidOperationException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_INVALID_OPERATION, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NInvalidOperationException)
};

namespace IO
{

class N_CLASS(NIOException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NIOException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NIOException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NIOException)()
		: N_CLASS(NCoreException)(N_E_IO, NULL)
	{
	}

	explicit N_CLASS(NIOException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_IO, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NIOException)
};

class N_CLASS(NDirectoryNotFoundException) : public N_CLASS(NIOException)
{
protected:
	N_CLASS(NDirectoryNotFoundException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, pInnerException)
	{
	}

	N_CLASS(NDirectoryNotFoundException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NDirectoryNotFoundException)()
		: N_CLASS(NIOException)(N_E_DIRECTORY_NOT_FOUND, NULL)
	{
	}

	explicit N_CLASS(NDirectoryNotFoundException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_DIRECTORY_NOT_FOUND, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NDirectoryNotFoundException)
};

class N_CLASS(NDriveNotFoundException) : public N_CLASS(NIOException)
{
protected:
	N_CLASS(NDriveNotFoundException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, pInnerException)
	{
	}

	N_CLASS(NDriveNotFoundException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NDriveNotFoundException)()
		: N_CLASS(NIOException)(N_E_DRIVE_NOT_FOUND, NULL)
	{
	}

	explicit N_CLASS(NDriveNotFoundException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_DRIVE_NOT_FOUND, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NDriveNotFoundException)
};

class N_CLASS(NEndOfStreamException) : public N_CLASS(NIOException)
{
protected:
	N_CLASS(NEndOfStreamException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, pInnerException)
	{
	}

	N_CLASS(NEndOfStreamException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NEndOfStreamException)()
		: N_CLASS(NIOException)(N_E_END_OF_STREAM, NULL)
	{
	}

	explicit N_CLASS(NEndOfStreamException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_END_OF_STREAM, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NEndOfStreamException)
};

class N_CLASS(NFileNotFoundException) : public N_CLASS(NIOException)
{
private:
	NString fileName;

protected:
	N_CLASS(NFileNotFoundException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, pInnerException), fileName()
	{
	}

	N_CLASS(NFileNotFoundException)(NResult code, const NString & message, const NString & fileName, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, message, pInnerException), fileName(fileName)
	{
	}

	virtual NString GetParams() const
	{
		return NGetStringLength(fileName) == 0 ? N_CLASS(NException)::GetParams() : N_T("FileName: ") + fileName;
	}

public:
	N_CLASS(NFileNotFoundException)()
		: N_CLASS(NIOException)(N_E_FILE_NOT_FOUND, NULL), fileName()
	{
	}

	explicit N_CLASS(NFileNotFoundException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_FILE_NOT_FOUND, message, pInnerException), fileName()
	{
	}

	N_CLASS(NFileNotFoundException)(const NString & message, const NString & fileName, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_FILE_NOT_FOUND, message, pInnerException), fileName(fileName)
	{
	}

	const NString & GetFileName() const
	{
		return fileName;
	}

	N_DECLARE_TYPE(NFileNotFoundException)
};

class N_CLASS(NFileLoadException) : public N_CLASS(NIOException)
{
private:
	NString fileName;

protected:
	N_CLASS(NFileLoadException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, pInnerException), fileName()
	{
	}

	N_CLASS(NFileLoadException)(NResult code, const NString & message, const NString & fileName, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, message, pInnerException), fileName(fileName)
	{
	}

	virtual NString GetParams() const
	{
		return NGetStringLength(fileName) == 0 ? N_CLASS(NException)::GetParams() : N_T("FileName: ") + fileName;
	}

public:
	N_CLASS(NFileLoadException)()
		: N_CLASS(NIOException)(N_E_FILE_LOAD, NULL), fileName()
	{
	}

	explicit N_CLASS(NFileLoadException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_FILE_LOAD, message, pInnerException), fileName()
	{
	}

	N_CLASS(NFileLoadException)(const NString & message, const NString & fileName, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_FILE_NOT_FOUND, message, pInnerException), fileName(fileName)
	{
	}

	const NString & GetFileName() const
	{
		return fileName;
	}

	N_DECLARE_TYPE(NFileLoadException)
};

class N_CLASS(NPathTooLongException) : public N_CLASS(NIOException)
{
protected:
	N_CLASS(NPathTooLongException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, pInnerException)
	{
	}

	N_CLASS(NPathTooLongException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NIOException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NPathTooLongException)()
		: N_CLASS(NIOException)(N_E_PATH_TOO_LONG, NULL)
	{
	}

	explicit N_CLASS(NPathTooLongException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NIOException)(N_E_PATH_TOO_LONG, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NPathTooLongException)
};

} // namespace IO

class N_CLASS(NNotImplementedException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NNotImplementedException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NNotImplementedException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NNotImplementedException)()
		: N_CLASS(NCoreException)(N_E_NOT_IMPLEMENTED, NULL)
	{
	}

	explicit N_CLASS(NNotImplementedException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_NOT_IMPLEMENTED, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NNotImplementedException)
};

class N_CLASS(NNotSupportedException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NNotSupportedException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NNotSupportedException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NNotSupportedException)()
		: N_CLASS(NCoreException)(N_E_NOT_SUPPORTED, NULL)
	{
	}

	explicit N_CLASS(NNotSupportedException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_NOT_SUPPORTED, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NNotSupportedException)
};

class N_CLASS(NNullReferenceException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NNullReferenceException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NNullReferenceException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NNullReferenceException)()
		: N_CLASS(NCoreException)(N_E_NULL_REFERENCE, NULL)
	{
	}

	explicit N_CLASS(NNullReferenceException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_NULL_REFERENCE, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NNullReferenceException)
};

class N_CLASS(NOutOfMemoryException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NOutOfMemoryException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NOutOfMemoryException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NOutOfMemoryException)()
		: N_CLASS(NCoreException)(N_E_OUT_OF_MEMORY, NULL)
	{
	}

	explicit N_CLASS(NOutOfMemoryException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_OUT_OF_MEMORY, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NOutOfMemoryException)
};

namespace Security
{

class N_CLASS(NSecurityException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NSecurityException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NSecurityException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NSecurityException)()
		: N_CLASS(NCoreException)(N_E_SECURITY, NULL)
	{
	}

	explicit N_CLASS(NSecurityException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_SECURITY, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NSecurityException)
};

} // namespace Security

class N_CLASS(NExternalException) : public N_CLASS(NCoreException)
{
private:
	NInt errorCode;

protected:
	N_CLASS(NExternalException)(NResult code, NInt errorCode)
		: N_CLASS(NCoreException)(code, NULL), errorCode(errorCode)
	{
	}

	N_CLASS(NExternalException)(NResult code, const NString & message, NInt errorCode)
		: N_CLASS(NCoreException)(code, message, NULL), errorCode(errorCode)
	{
	}

	virtual NString GetParams() const
	{
		return errorCode == 0 ? N_CLASS(NException)::GetParams() : N_T("ErrorCode: ") + IntToString(errorCode);
	}

public:
	N_CLASS(NExternalException)()
		: N_CLASS(NCoreException)(N_E_EXTERNAL, NULL), errorCode(0)
	{
	}

	N_CLASS(NExternalException)(const NString & message, NInt errorCode)
		: N_CLASS(NCoreException)(N_E_EXTERNAL, message, NULL), errorCode(errorCode)
	{
	}

	explicit N_CLASS(NExternalException)(const NString & message)
		: N_CLASS(NCoreException)(N_E_EXTERNAL, message, NULL), errorCode(0)
	{
	}

	NInt GetErrorCode() const
	{
		return errorCode;
	}

	N_DECLARE_TYPE(NExternalException)
};

class N_CLASS(NClrException) : public N_CLASS(NExternalException)
{
private:
	NHandle handle;

protected:
	N_CLASS(NClrException)(NResult code)
		: N_CLASS(NExternalException)(code, 0), handle(NULL)
	{
	}

	N_CLASS(NClrException)(NResult code, const NString & message, NHandle handle)
		: N_CLASS(NExternalException)(code, message, 0), handle(handle)
	{
	}

public:
	N_CLASS(NClrException)()
		: N_CLASS(NExternalException)(N_E_CLR, 0), handle(NULL)
	{
	}

	N_CLASS(NClrException)(const NString & message, NHandle handle)
		: N_CLASS(NExternalException)(N_E_CLR, message, 0), handle(handle)
	{
	}

	explicit N_CLASS(NClrException)(const NString & message)
		: N_CLASS(NExternalException)(N_E_CLR, message, 0), handle(NULL)
	{
	}

	NHandle GetHandle() const
	{
		return handle;
	}

	N_DECLARE_TYPE(NClrException)
};

class N_CLASS(NComException) : public N_CLASS(NExternalException)
{
protected:
	N_CLASS(NComException)(NResult code, NInt errorCode)
		: N_CLASS(NExternalException)(code, errorCode)
	{
	}

	N_CLASS(NComException)(NResult code, const NString & message, NInt errorCode)
		: N_CLASS(NExternalException)(code, message, errorCode)
	{
	}

public:
	N_CLASS(NComException)()
		: N_CLASS(NExternalException)(N_E_COM, 0)
	{
	}

	N_CLASS(NComException)(const NString & message, NInt errorCode)
		: N_CLASS(NExternalException)(N_E_COM, message, errorCode)
	{
	}

	explicit N_CLASS(NComException)(const NString & message)
		: N_CLASS(NExternalException)(N_E_COM, message, 0)
	{
	}

	N_DECLARE_TYPE(NComException)
};

class N_CLASS(NSysException) : public N_CLASS(NExternalException)
{
	static NString GetErrorMessage(NInt errorCode)
	{
		NInt mLen = NErrorGetSysErrorMessage(errorCode, NULL);
		if(mLen == -1) return NString();
		NString message;
		if (NErrorGetSysErrorMessage(errorCode, NGetStringBuffer(message, mLen)) == -1) return NString();
		NReleaseStringBuffer(message, mLen);
		return message;
	}

protected:
	N_CLASS(NSysException)(NResult code, NInt errorCode)
		: N_CLASS(NExternalException)(code, errorCode)
	{
	}

	N_CLASS(NSysException)(NResult code, NInt errorCode, const NString & message)
		: N_CLASS(NExternalException)(code, message, errorCode)
	{
	}

public:
#ifdef N_WINDOWS_CE
	N_CLASS(NSysException)()
		: N_CLASS(NExternalException)(N_E_SYS, GetErrorMessage(0), 0)
	{
	}
#else
	N_CLASS(NSysException)()
		: N_CLASS(NExternalException)(N_E_SYS, GetErrorMessage(errno), errno)
	{
	}
#endif

	explicit N_CLASS(NSysException)(NInt errorCode)
		: N_CLASS(NExternalException)(N_E_SYS, GetErrorMessage(errorCode), errorCode)
	{
	}

	N_CLASS(NSysException)(NInt errorCode, const NString & message)
		: N_CLASS(NExternalException)(N_E_SYS, message, errorCode)
	{
	}

#ifdef N_WINDOWS_CE
	explicit N_CLASS(NSysException)(const NString & message)
		: N_CLASS(NExternalException)(N_E_SYS, message, 0)
	{
	}
#else
	explicit N_CLASS(NSysException)(const NString & message)
		: N_CLASS(NExternalException)(N_E_SYS, message, errno)
	{
	}
#endif

	N_DECLARE_TYPE(NSysException)
};

class N_CLASS(NWin32Exception) : public N_CLASS(NExternalException)
{
private:
	#ifndef N_WINDOWS

	static NUInt GetLastError()
	{
		return 0;
	}

	#endif

	static NString GetErrorMessage(NUInt errorCode)
	{
	#ifdef N_WINDOWS
		NChar * szMessage;
		if(!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorCode, 0, (NChar *)&szMessage, 0, NULL)) return NString();
		AutoLocalFree message(szMessage);
		return NString(szMessage);
	#else
		return NString();
	#endif
	}

protected:
	N_CLASS(NWin32Exception)(NResult code, NUInt errorCode)
		: N_CLASS(NExternalException)(code, (NInt)errorCode)
	{
	}

	N_CLASS(NWin32Exception)(NResult code, NUInt errorCode, const NString & message)
		: N_CLASS(NExternalException)(code, message, (NInt)errorCode)
	{
	}

public:
	N_CLASS(NWin32Exception)()
	#ifdef N_WINDOWS
		: N_CLASS(NExternalException)(N_E_WIN32, GetErrorMessage(GetLastError()), (NInt)GetLastError())
	#else
		: N_CLASS(NExternalException)(N_E_WIN32, NString(), 0)
	#endif
	{
	}

	explicit N_CLASS(NWin32Exception)(NUInt errorCode)
		: N_CLASS(NExternalException)(N_E_WIN32, GetErrorMessage(errorCode), (NInt)errorCode)
	{
	}

	N_CLASS(NWin32Exception)(NUInt errorCode, const NString & message)
		: N_CLASS(NExternalException)(N_E_WIN32, message, (NInt)errorCode)
	{
	}

	explicit N_CLASS(NWin32Exception)(const NString & message)
	#ifdef N_WINDOWS
		: N_CLASS(NExternalException)(N_E_WIN32, message, (NInt)GetLastError())
	#else
		: N_CLASS(NExternalException)(N_E_WIN32, message, 0)
	#endif
	{
	}

	N_DECLARE_TYPE(NWin32Exception)
};

class N_CLASS(NParameterException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NParameterException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NParameterException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NParameterException)()
		: N_CLASS(NCoreException)(N_E_PARAMETER, NULL)
	{
	}

	explicit N_CLASS(NParameterException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_PARAMETER, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NParameterException)
};

class N_CLASS(NParameterReadOnlyException) : public N_CLASS(NParameterException)
{
protected:
	N_CLASS(NParameterReadOnlyException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NParameterException)(code, pInnerException)
	{
	}

	N_CLASS(NParameterReadOnlyException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NParameterException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NParameterReadOnlyException)()
		: N_CLASS(NParameterException)(N_E_PARAMETER_READ_ONLY, NULL)
	{
	}

	explicit N_CLASS(NParameterReadOnlyException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NParameterException)(N_E_PARAMETER_READ_ONLY, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NParameterReadOnlyException)
};

class N_CLASS(NNotActivatedException) : public N_CLASS(NCoreException)
{
protected:
	N_CLASS(NNotActivatedException)(NResult code, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, pInnerException)
	{
	}

	N_CLASS(NNotActivatedException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

public:
	N_CLASS(NNotActivatedException)()
		: N_CLASS(NCoreException)(N_E_NOT_ACTIVATED, NULL)
	{
	}

	explicit N_CLASS(NNotActivatedException)(const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(N_E_NOT_ACTIVATED, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NNotActivatedException)
};

class N_CLASS(NeurotecException) : public N_CLASS(NCoreException)
{
protected:
	virtual NString GetParams() const
	{
		return N_T("Code: ") + IntToString(GetCode());
	}

public:
	N_CLASS(NeurotecException)(NResult code)
		: N_CLASS(NCoreException)(code, NULL)
	{
	}

	N_CLASS(NeurotecException)(NResult code, const NString & message, N_CLASS(NException) * pInnerException = NULL)
		: N_CLASS(NCoreException)(code, message, pInnerException)
	{
	}

	N_DECLARE_TYPE(NeurotecException)
};

inline N_CLASS(NException) * N_CLASS(NException)::GetException(NResult code, const NString & message, const NString & param, NHandle hClrError, NInt externalError)
{
	switch(code)
	{
	case N_E_FAILED:
		return new N_CLASS(NException)(message);
	case N_E_CORE:
		return new N_CLASS(NCoreException)(message);
	case N_E_ARGUMENT:
		return new N_CLASS(NArgumentException)(message, param);
	case N_E_ARGUMENT_NULL:
		return new N_CLASS(NArgumentNullException)(param, message);
	case N_E_ARGUMENT_OUT_OF_RANGE:
		return new N_CLASS(NArgumentOutOfRangeException)(param, message);
	case N_E_INVALID_ENUM_ARGUMENT:
		return new N_CLASS(NInvalidEnumArgumentException)(param, message);
	case N_E_ARITHMETIC:
		return new N_CLASS(NArithmeticException)(message);
	case N_E_OVERFLOW:
		return new N_CLASS(NOverflowException)(message);
	case N_E_FORMAT:
		return new N_CLASS(NFormatException)(message);
	case N_E_INDEX_OUT_OF_RANGE:
		return new N_CLASS(NIndexOutOfRangeException)(message);
	case N_E_INVALID_CAST:
		return new N_CLASS(NInvalidCastException)(message);
	case N_E_INVALID_OPERATION:
		return new N_CLASS(NInvalidOperationException)(message);
	case N_E_IO:
		return new IO::N_CLASS(NIOException)(message);
	case N_E_DIRECTORY_NOT_FOUND:
		return new IO::N_CLASS(NDirectoryNotFoundException)(message);
	case N_E_DRIVE_NOT_FOUND:
		return new IO::N_CLASS(NDriveNotFoundException)(message);
	case N_E_END_OF_STREAM:
		return new IO::N_CLASS(NEndOfStreamException)(message);
	case N_E_FILE_NOT_FOUND:
		return new IO::N_CLASS(NFileNotFoundException)(message, param);
	case N_E_FILE_LOAD:
		return new IO::N_CLASS(NFileLoadException)(message, param);
	case N_E_PATH_TOO_LONG:
		return new IO::N_CLASS(NPathTooLongException)(message);
	case N_E_NOT_IMPLEMENTED:
		return new N_CLASS(NNotImplementedException)(message);
	case N_E_NOT_SUPPORTED:
		return new N_CLASS(NNotSupportedException)(message);
	case N_E_NULL_REFERENCE:
		return new N_CLASS(NNullReferenceException)(message);
	case N_E_OUT_OF_MEMORY:
		return new N_CLASS(NOutOfMemoryException)(message);
	case N_E_SECURITY:
		return new Security::N_CLASS(NSecurityException)(message);
	case N_E_EXTERNAL:
		return new N_CLASS(NExternalException)(message, externalError);
	case N_E_CLR:
		return new N_CLASS(NClrException)(message, hClrError);
	case N_E_COM:
		return new N_CLASS(NComException)(message, externalError);
	case N_E_SYS:
		return new N_CLASS(NSysException)(externalError, message);
	case N_E_WIN32:
		return new N_CLASS(NWin32Exception)(externalError, message);
	case N_E_PARAMETER:
		return new N_CLASS(NParameterException)(message);
	case N_E_PARAMETER_READ_ONLY:
		return new N_CLASS(NParameterReadOnlyException)(message);
	case N_E_NOT_ACTIVATED:
		return new N_CLASS(NNotActivatedException)(message);
	default:
		return new N_CLASS(NeurotecException)(code, message);
	}
}

inline void N_NO_RETURN NRaiseError(NResult error)
{
	N_CLASS(NException)::CheckCode(error);
	throw N_CLASS(NException)::GetLast(error);
}

inline NInt NCheck(NResult result)
{
	if(NFailed(result))
	{
		NRaiseError(result);
	}
	return result;
}

inline void N_NO_RETURN NThrowException() { throw new N_CLASS(NException)(); }
inline void N_NO_RETURN NThrowException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NException)(message, pInnerException); }
inline void N_NO_RETURN NThrowCoreException() { throw new N_CLASS(NCoreException)(); }
inline void N_NO_RETURN NThrowCoreException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NCoreException)(message, pInnerException); }
inline void N_NO_RETURN NThrowArgumentException() { throw new N_CLASS(NArgumentException)(); }
inline void N_NO_RETURN NThrowArgumentException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NArgumentException)(message, pInnerException); }
inline void N_NO_RETURN NThrowArgumentException(const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException) { throw new N_CLASS(NArgumentException)(message, paramName, pInnerException); }
inline void N_NO_RETURN NThrowArgumentTypeException(const NString & paramName) { throw new N_CLASS(NArgumentException)(paramName + N_T(" type is not the one that is expected"), paramName); }
inline void N_NO_RETURN NThrowArgumentNullException() { throw new N_CLASS(NArgumentNullException)(); }
inline void N_NO_RETURN NThrowArgumentNullException(const NString & paramName) { throw new N_CLASS(NArgumentNullException)(paramName); }
inline void N_NO_RETURN NThrowArgumentNullException(const NString & message, N_CLASS(NException) * pInnerException) { throw new N_CLASS(NArgumentNullException)(message, pInnerException); }
inline void N_NO_RETURN NThrowArgumentNullException(const NString & paramName, const NString & message) { throw new N_CLASS(NArgumentNullException)(paramName, message); }
inline void N_NO_RETURN NThrowArgumentOutOfRangeException() { throw new N_CLASS(NArgumentOutOfRangeException)(); }
inline void N_NO_RETURN NThrowArgumentOutOfRangeException(const NString & paramName) { throw new N_CLASS(NArgumentOutOfRangeException)(paramName); }
inline void N_NO_RETURN NThrowArgumentOutOfRangeException(const NString & message, N_CLASS(NException) * pInnerException) { throw new N_CLASS(NArgumentOutOfRangeException)(message, pInnerException); }
inline void N_NO_RETURN NThrowArgumentOutOfRangeException(const NString & paramName, const NString & message) { throw new N_CLASS(NArgumentOutOfRangeException)(paramName, message); }
inline void N_NO_RETURN NThrowArgumentLessThanZeroException(const NString & paramName) { throw new N_CLASS(NArgumentOutOfRangeException)(paramName, paramName + N_T(" is less than zero")); }
inline void N_NO_RETURN NThrowArgumentLessThanOneException(const NString & paramName) { throw new N_CLASS(NArgumentOutOfRangeException)(paramName, paramName + N_T(" is less than one")); }
inline void N_NO_RETURN NThrowInvalidEnumArgumentException() { throw new N_CLASS(NInvalidEnumArgumentException)(); }
inline void N_NO_RETURN NThrowInvalidEnumArgumentException(const NString & paramName) { throw new N_CLASS(NInvalidEnumArgumentException)(paramName); }
inline void N_NO_RETURN NThrowInvalidEnumArgumentException(const NString & message, N_CLASS(NException) * pInnerException) { throw new N_CLASS(NInvalidEnumArgumentException)(message, pInnerException); }
inline void N_NO_RETURN NThrowInvalidEnumArgumentException(const NString & paramName, const NString & message) { throw new N_CLASS(NInvalidEnumArgumentException)(paramName, message); }
inline void N_NO_RETURN NThrowArithmeticException() { throw new N_CLASS(NArithmeticException)(); }
inline void N_NO_RETURN NThrowArithmeticException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NArithmeticException)(message, pInnerException); }
inline void N_NO_RETURN NThrowOverflowException() { throw new N_CLASS(NOverflowException)(); }
inline void N_NO_RETURN NThrowOverflowException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NOverflowException)(message, pInnerException); }
inline void N_NO_RETURN NThrowFormatException() { throw new N_CLASS(NFormatException)(); }
inline void N_NO_RETURN NThrowFormatException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NFormatException)(message, pInnerException); }
inline void N_NO_RETURN NThrowArgumentFormatException(const NString & paramName) { throw new N_CLASS(NFormatException)(paramName +  N_T(" is in invalid format")); }
inline void N_NO_RETURN NThrowIndexOutOfRangeException() { throw new N_CLASS(NIndexOutOfRangeException)(); }
inline void N_NO_RETURN NThrowIndexOutOfRangeException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NIndexOutOfRangeException)(message, pInnerException); }
inline void N_NO_RETURN NThrowInvalidCastException() { throw new N_CLASS(NInvalidCastException)(); }
inline void N_NO_RETURN NThrowInvalidCastException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NInvalidCastException)(message, pInnerException); }
inline void N_NO_RETURN NThrowInvalidOperationException() { throw new N_CLASS(NInvalidOperationException)(); }
inline void N_NO_RETURN NThrowInvalidOperationException(const NString & message, N_CLASS(NException) * pInnerException) { throw new N_CLASS(NInvalidOperationException)(message, pInnerException); }
inline void N_NO_RETURN NThrowIOException() { throw new IO::N_CLASS(NIOException)(); }
inline void N_NO_RETURN NThrowIOException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NIOException)(message, pInnerException); }
inline void N_NO_RETURN NThrowDirectoryNotFoundException() { throw new IO::N_CLASS(NDirectoryNotFoundException)(); }
inline void N_NO_RETURN NThrowDirectoryNotFoundException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NDirectoryNotFoundException)(message, pInnerException); }
inline void N_NO_RETURN NThrowDriveNotFoundException() { throw new IO::N_CLASS(NDriveNotFoundException)(); }
inline void N_NO_RETURN NThrowDriveNotFoundException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NDriveNotFoundException)(message, pInnerException); }
inline void N_NO_RETURN NThrowEndOfStreamException() { throw new IO::N_CLASS(NEndOfStreamException)(); }
inline void N_NO_RETURN NThrowEndOfStreamException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NEndOfStreamException)(message, pInnerException); }
inline void N_NO_RETURN NThrowFileNotFoundException() { throw new IO::N_CLASS(NFileNotFoundException)(); }
inline void N_NO_RETURN NThrowFileNotFoundException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NFileNotFoundException)(message, pInnerException); }
inline void N_NO_RETURN NThrowFileNotFoundException(const NString & message, const NString & fileName, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NFileNotFoundException)(message, fileName, pInnerException); }
inline void N_NO_RETURN NThrowFileLoadException() { throw new IO::N_CLASS(NFileLoadException)(); }
inline void N_NO_RETURN NThrowFileLoadException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NFileLoadException)(message, pInnerException); }
inline void N_NO_RETURN NThrowFileLoadException(const NString & message, const NString & fileName, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NFileLoadException)(message, fileName, pInnerException); }
inline void N_NO_RETURN NThrowPathTooLongException() { throw new IO::N_CLASS(NPathTooLongException)(); }
inline void N_NO_RETURN NThrowPathTooLongException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new IO::N_CLASS(NPathTooLongException)(message, pInnerException); }
inline void N_NO_RETURN NThrowNotImplementedException() { throw new N_CLASS(NNotImplementedException)(); }
inline void N_NO_RETURN NThrowNotImplementedException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NNotImplementedException)(message, pInnerException); }
inline void N_NO_RETURN NThrowNotSupportedException() { throw new N_CLASS(NNotSupportedException)(); }
inline void N_NO_RETURN NThrowNotSupportedException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NNotSupportedException)(message, pInnerException); }
inline void N_NO_RETURN NThrowNullReferenceException() { throw new N_CLASS(NNullReferenceException)(); }
inline void N_NO_RETURN NThrowNullReferenceException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NNullReferenceException)(message, pInnerException); }
inline void N_NO_RETURN NThrowOutOfMemoryException() { throw new N_CLASS(NOutOfMemoryException)(); }
inline void N_NO_RETURN NThrowOutOfMemoryException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NOutOfMemoryException)(message, pInnerException); }
inline void N_NO_RETURN NThrowSecurityException() { throw new Security::N_CLASS(NSecurityException)(); }
inline void N_NO_RETURN NThrowSecurityException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new Security::N_CLASS(NSecurityException)(message, pInnerException); }
inline void N_NO_RETURN NThrowComException() { throw new N_CLASS(NComException)(); }
inline void N_NO_RETURN NThrowComException(const NString & message, NInt errorCode) { throw new N_CLASS(NComException)(message, errorCode); }
inline void N_NO_RETURN NThrowComException(const NString & message) { throw new N_CLASS(NComException)(message); }
inline void N_NO_RETURN NThrowSysException() { throw new N_CLASS(NSysException)(); }
inline void N_NO_RETURN NThrowSysException(NInt errorCode) { throw new N_CLASS(NSysException)(errorCode); }
inline void N_NO_RETURN NThrowSysException(NInt errorCode, const NString & message) { throw new N_CLASS(NSysException)(errorCode, message); }
inline void N_NO_RETURN NThrowSysException(const NString & message) { throw new N_CLASS(NSysException)(message); }
inline void N_NO_RETURN NThrowWin32Exception() { throw new N_CLASS(NWin32Exception)(); }
inline void N_NO_RETURN NThrowWin32Exception(NUInt errorCode) { throw new N_CLASS(NWin32Exception)(errorCode); }
inline void N_NO_RETURN NThrowWin32Exception(NUInt errorCode, const NString & message) { throw new N_CLASS(NWin32Exception)(errorCode, message); }
inline void N_NO_RETURN NThrowWin32Exception(const NString & message) { throw new N_CLASS(NWin32Exception)(message); }
inline void N_NO_RETURN NThrowParameterException() { throw new N_CLASS(NParameterException)(); }
inline void N_NO_RETURN NThrowParameterException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NParameterException)(message, pInnerException); }
inline void N_NO_RETURN NThrowParameterReadOnlyException() { throw new N_CLASS(NParameterReadOnlyException)(); }
inline void N_NO_RETURN NThrowParameterReadOnlyException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NParameterReadOnlyException)(message, pInnerException); }
inline void N_NO_RETURN NThrowNotActivatedException() { throw new N_CLASS(NNotActivatedException)(); }
inline void N_NO_RETURN NThrowNotActivatedException(const NString & message, N_CLASS(NException) * pInnerException = NULL) { throw new N_CLASS(NNotActivatedException)(message, pInnerException); }

inline void N_CLASS(NException)::CheckCode(NResult code)
{
	if(NSucceeded(code))
	{
		NThrowArgumentOutOfRangeException(N_T("code"), N_T("error is greater than or equal to zero"));
	}
}

}

#endif // !N_ERRORS_HPP_INCLUDED
