#ifndef N_OBJECT_HPP_INCLUDED
#define N_OBJECT_HPP_INCLUDED

#include <NTypes.hpp>
#include <NParameters.hpp>
namespace Neurotec
{
#include <NObject.h>
}

namespace Neurotec
{
class N_CLASS(NObject);
class N_CLASS(NException);
NInt NCheck(NResult result);
NChar * NGetStringBufferWithCheck(NString & value, NResult result);
NString & NReleaseStringBufferWithCheck(NString & value, NResult result);
void N_NO_RETURN NThrowArgumentException(const NString & message, const NString & paramName, N_CLASS(NException) * pInnerException = NULL);
void N_NO_RETURN NThrowArgumentNullException(const NString & paramName);
void N_NO_RETURN NThrowInvalidOperationException(const NString & message, N_CLASS(NException) * pInnerException = NULL);

class N_CLASS(NNativeType)
{
private:
	HNType handle;

	void CheckParameterType(NUShort partId, NUShort parameterId, NInt typeId) const;
	NInt GetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, void * pValue, NInt typeId) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const void * pValue, NInt typeId) const;

public:
	static const N_CLASS(NNativeType) Null;

	explicit N_CLASS(NNativeType)(HNType handle)
		: handle(handle)
	{
	}

	N_CLASS(NNativeType)(const N_CLASS(NNativeType) & value)
		: handle(value.handle)
	{
	}

	bool IsSubclassOf(const N_CLASS(NNativeType) & type) const
	{
		NBool value;
		NCheck(NTypeIsSubclassOf(handle, type.handle, &value));
		return value != 0;
	}

	bool IsAssignableFrom(const N_CLASS(NNativeType) & type) const
	{
		NBool value;
		NCheck(NTypeIsAssignableFrom(handle, type.handle, &value));
		return value != 0;
	}

	bool IsInstanceOfType(HNObject hObject) const
	{
		NBool value;
		NCheck(NTypeIsInstanceOfType(handle, hObject, &value));
		return value != 0;
	}

	bool IsInstanceOfType(N_CLASS(NObject) * pObject) const;
	void Reset(N_CLASS(NObject) * pObject) const;
	NInt GetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUInt parameterId, void * pValue) const;
	NByte GetParameterAsByte(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NSByte GetParameterAsSByte(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NUShort GetParameterAsUInt16(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NShort GetParameterAsInt16(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NUInt GetParameterAsUInt32(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NInt GetParameterAsInt32(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NULong GetParameterAsUInt64(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NLong GetParameterAsInt64(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	bool GetParameterAsBoolean(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NChar GetParameterAsChar(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NFloat GetParameterAsSingle(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NDouble GetParameterAsDouble(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	NInt GetParameterAsString(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NChar * pValue) const;
	NString GetParameterAsString(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const void * pValue) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NByte value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NSByte value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NUShort value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NShort value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NUInt value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NInt value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NULong value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NLong value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, bool value) const;
	void SetParameterAsChar(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NChar value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NFloat value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NDouble value) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const NChar * szValue) const;
	void SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const NString & value) const;

	NString GetName() const
	{
		NString value;
		return NReleaseStringBufferWithCheck(value, NTypeGetName(handle, NGetStringBufferWithCheck(value, NTypeGetName(handle, NULL))));
	}

	N_CLASS(NNativeType) GetBaseType() const
	{
		HNType hValue;
		NCheck(NTypeGetBaseType(handle, &hValue));
		N_CLASS(NNativeType) value(hValue);
		return value;
	}

	bool IsAbstract() const
	{
		NBool value;
		NCheck(NTypeIsAbstract(handle, &value));
		return value != 0;
	}

	bool SupportsParameters() const
	{
		NBool value;
		NCheck(NTypeSupportsParameters(handle, &value));
		return value != 0;
	}

	bool operator ==(const N_CLASS(NNativeType) & value) const
	{
		return handle == value.handle;
	}

	bool operator !=(const N_CLASS(NNativeType) & value) const
	{
		return handle != value.handle;
	}
};

#if defined(N_FRAMEWORK_MFC)
	typedef CObject CNObjectBase;
	#define N_TYPE_OF(name) RUNTIME_CLASS(C##name)
	#define N_GET_OBJECT_TYPE(pObj) (pObj->GetRuntimeClass())
	#define N_GET_OBJECT_TYPE_NAME(pObj) CString(N_GET_OBJECT_TYPE(pObj)->m_lpszClassName)
	#define N_DECLARE_TYPE(name) DECLARE_DYNAMIC(C##name)
	#define N_IMPLEMENT_TYPE(name, baseName) IMPLEMENT_DYNAMIC(C##name, C##baseName)
	#define N_IMPLEMENT_ABSTRACT_TYPE(name, baseName) IMPLEMENT_DYNAMIC(C##name, C##baseName)
	#define N_NATIVE_TYPE_OF(name) (C##name::nativeType##C##name)
	#define N_DECLARE_OBJECT_TYPE(name) \
		public:\
			static const N_CLASS(NNativeType) nativeType##C##name;\
		N_DECLARE_TYPE(name)
	#define N_IMPLEMENT_OBJECT_TYPE(name, baseName) \
		const N_CLASS(NNativeType) C##name::nativeType##C##name(name##TypeOf());\
		N_IMPLEMENT_TYPE(name, baseName)
	#define N_IMPLEMENT_ABSTRACT_OBJECT_TYPE(name, baseName) \
		const N_CLASS(NNativeType) C##name::nativeType##C##name(name##TypeOf());\
		N_IMPLEMENT_ABSTRACT_TYPE(name, baseName)
#elif defined(N_FRAMEWORK_WX)
	typedef wxObject wxNObjectBase;
	#define N_TYPE_OF(name) CLASSINFO(wx##name)
	#define N_GET_OBJECT_TYPE(pObj) (pObj->GetClassInfo())
	#define N_GET_OBJECT_TYPE_NAME(pObj) wxString(N_GET_OBJECT_TYPE(pObj)->GetClassName())
	#define N_DECLARE_TYPE(name) DECLARE_CLASS(wx##name)
	#define N_IMPLEMENT_TYPE(name, baseName) IMPLEMENT_CLASS(wx##name, wx##baseName)
	#define N_IMPLEMENT_ABSTRACT_TYPE(name, baseName) IMPLEMENT_CLASS(wx##name, wx##baseName)
	#define N_NATIVE_TYPE_OF(name) (wx##name::nativeType##wx##name)
	#define N_DECLARE_OBJECT_TYPE(name) \
		public:\
			static const N_CLASS(NNativeType) nativeType##wx##name;\
		N_DECLARE_TYPE(name)
	#define N_IMPLEMENT_OBJECT_TYPE(name, baseName) \
		const N_CLASS(NNativeType) wx##name::nativeType##wx##name(name##TypeOf());\
		N_IMPLEMENT_TYPE(name, baseName)
	#define N_IMPLEMENT_ABSTRACT_OBJECT_TYPE(name, baseName) \
		const N_CLASS(NNativeType) wx##name::nativeType##wx##name(name##TypeOf());\
		N_IMPLEMENT_ABSTRACT_TYPE(name, baseName)
#else
	class NObjectBase;

	struct NType
	{
		const NChar * Name;
		const NType * BaseType;
		bool IsAbstract;
		NSizeType InstanceSize;

		bool IsSubclassOf(const NType * pType) const
		{
			if (!pType) NThrowArgumentNullException(N_T("pType"));
			const NType * pThisType = BaseType;
			while (pThisType)
			{
				if (pThisType == pType) return true;
				pThisType = pThisType->BaseType;
			}
			return false;
		}

		bool IsAssignableFrom(const NType * pType) const
		{
			if (!pType) return false;
			while (pType)
			{
				if (pType == this) return true;
				pType = pType->BaseType;
			}
			return false;
		}

		bool IsInstanceOfType(const NObjectBase * pObject) const;
	};

	class NObjectBase
	{
	private:
		NObjectBase(const NObjectBase &)
		{
		}

	protected:
		NObjectBase()
		{
		}

	public:
		static const NType typeNObjectBase;

		virtual ~NObjectBase()
		{
		}

		virtual const NType * GetType() const
		{
			return &typeNObjectBase;
		}
	};

	inline bool NType::IsInstanceOfType(const NObjectBase * pObject) const
	{
		if (!pObject) return false;
		return IsAssignableFrom(pObject->GetType());
	}

	#define N_TYPE_OF(name) (&name::type##name)
	#define N_GET_OBJECT_TYPE(pObj) (pObj->GetType())
	#define N_GET_OBJECT_TYPE_NAME(pObj) NString(N_GET_OBJECT_TYPE(pObj)->Name)
	#define N_DECLARE_TYPE(name) \
		public:\
			static const NType type##name;\
			virtual const NType * GetType() const;
	#define N_IMPLEMENT_TYPE(name, baseName) \
		const NType name::type##name = { N_T(#name), &baseName::type##baseName, false, sizeof(name) };\
		const NType * name::GetType() const { return &name::type##name; }
	#define N_IMPLEMENT_ABSTRACT_TYPE(name, baseName) \
		const NType name::type##name = { N_T(#name), &baseName::type##baseName, true, sizeof(name) };\
		const NType * name::GetType() const { return &name::type##name; }
	#define N_NATIVE_TYPE_OF(name) (name::nativeType##name)
	#define N_DECLARE_OBJECT_TYPE(name) \
		public:\
			static const N_CLASS(NNativeType) nativeType##name;\
		N_DECLARE_TYPE(name)
	#define N_IMPLEMENT_OBJECT_TYPE(name, baseName) \
		const N_CLASS(NNativeType) name::nativeType##name(name##TypeOf());\
		N_IMPLEMENT_TYPE(name, baseName)
	#define N_IMPLEMENT_ABSTRACT_OBJECT_TYPE(name, baseName) \
		const N_CLASS(NNativeType) name::nativeType##name(name##TypeOf());\
		N_IMPLEMENT_ABSTRACT_TYPE(name, baseName)
#endif

class N_CLASS(NDummyObject);

namespace Collections
{
template<typename T> class N_CLASS(NObjectReadOnlyCollection);
template<typename T> class N_CLASS(NObjectCollection);
}

class N_CLASS(NObject) : public N_CLASS(NObjectBase)
{
private:
	HNObject handle;
	bool ownsHandle;
	N_CLASS(NObject) * pOwner;

	N_CLASS(NObject)(const N_CLASS(NObject) &)
	{
	}

protected:
	N_CLASS(NObject)(HNObject handle, const N_CLASS(NNativeType) & requiredType, bool claimHandle);

	virtual void ClaimHandle()
	{
		if (ownsHandle) NThrowInvalidOperationException(N_T("Handle is already claimed"));
		ownsHandle = true;
	}

	virtual void SetOwner(N_CLASS(NObject) * pValue);

	virtual void EmbedObject(N_CLASS(NObject) * pObject)
	{
		if (!pObject) NThrowArgumentNullException(N_T("pObject"));
		pObject->SetOwner(this);
	}

	virtual void ReleaseObjectNoDelete(N_CLASS(NObject) * pObject)
	{
		if (pObject)
		{
			if (pObject->pOwner != this) NThrowArgumentException(N_T("The specified pObject is not owned by this NObject"), N_T("pObject"));
			pObject->pOwner = NULL;
		}
	}

	void ReleaseObject(N_CLASS(NObject) * pObject)
	{
		ReleaseObjectNoDelete(pObject);
		delete pObject;
	}

public:
	static void CopyParameters(N_CLASS(NObject) * pDstObject, N_CLASS(NObject) * pSrcObject)
	{
		if (!pDstObject) NThrowArgumentNullException(N_T("pDstObject"));
		if (!pSrcObject) NThrowArgumentNullException(N_T("pSrcObject"));
		NCheck(NObjectCopyParameters(pDstObject->GetHandle(), pSrcObject->GetHandle()));
	}

	static N_CLASS(NNativeType) GetNativeType(HNObject hObject)
	{
		HNType hType;
		NCheck(NObjectGetType(hObject, &hType));
		return N_CLASS(NNativeType)(hType);
	}

	virtual ~N_CLASS(NObject)();

	virtual HNObject GetHandle() const
	{
		return handle;
	}

	HNObject ReleaseHandle()
	{
		if (pOwner) NThrowInvalidOperationException(N_T("The NObject is owned by another NObject"));
		if (!ownsHandle) NThrowInvalidOperationException(N_T("The NObject does not own its handle"));
		HNObject handle = this->handle;
		ownsHandle = false;
		return handle;
	}

	N_CLASS(NNativeType) GetNativeType() const
	{
		return GetNativeType(GetHandle());
	}

	N_CLASS(NObject) * GetOwner() const
	{
		return pOwner;
	}

	void Reset()
	{
		NCheck(NObjectReset(GetHandle()));
	}

	NInt GetParameter(NUShort partId, NUShort parameterId, void * pValue) { return GetNativeType().GetParameter(this, partId, parameterId, pValue); }
	NByte GetParameterAsByte(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsByte(this, partId, parameterId); }
	NSByte GetParameterAsSByte(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsSByte(this, partId, parameterId); }
	NUShort GetParameterAsUInt16(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsUInt16(this, partId, parameterId); }
	NShort GetParameterAsInt16(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsInt16(this, partId, parameterId); }
	NUInt GetParameterAsUInt32(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsUInt32(this, partId, parameterId); }
	NInt GetParameterAsInt32(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsInt32(this, partId, parameterId); }
	NULong GetParameterAsUInt64(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsUInt64(this, partId, parameterId); }
	NLong GetParameterAsInt64(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsInt64(this, partId, parameterId); }
	bool GetParameterAsBoolean(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsBoolean(this, partId, parameterId); }
	NChar GetParameterAsChar(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsChar(this, partId, parameterId); }
	NFloat GetParameterAsSingle(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsSingle(this, partId, parameterId); }
	NDouble GetParameterAsDouble(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsDouble(this, partId, parameterId); }
	NInt GetParameterAsString(NUShort partId, NUShort parameterId, NChar * pValue) { return GetNativeType().GetParameterAsString(this, partId, parameterId, pValue); }
	NString GetParameterAsString(NUShort partId, NUShort parameterId) { return GetNativeType().GetParameterAsString(this, partId, parameterId); }
	void SetParameter(NUShort partId, NUShort parameterId, const void * pValue) { return GetNativeType().SetParameter(this, partId, parameterId, pValue); }
	void SetParameter(NUShort partId, NUShort parameterId, NByte value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NSByte value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NUShort value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NShort value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NUInt value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NInt value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NULong value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NLong value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, bool value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameterAsChar(NUShort partId, NUShort parameterId, NChar value) { return GetNativeType().SetParameterAsChar(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NFloat value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, NDouble value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }
	void SetParameter(NUShort partId, NUShort parameterId, const NChar * szValue) { return GetNativeType().SetParameter(this, partId, parameterId, szValue); }
	void SetParameter(NUShort partId, NUShort parameterId, const NString & value) { return GetNativeType().SetParameter(this, partId, parameterId, value); }

	NInt GetParameter(NUShort parameterId, void * pValue) { return GetNativeType().GetParameter(this, 0, parameterId, pValue); }
	NByte GetParameterAsByte(NUShort parameterId) { return GetNativeType().GetParameterAsByte(this, 0, parameterId); }
	NSByte GetParameterAsSByte(NUShort parameterId) { return GetNativeType().GetParameterAsSByte(this, 0, parameterId); }
	NUShort GetParameterAsUInt16(NUShort parameterId) { return GetNativeType().GetParameterAsUInt16(this, 0, parameterId); }
	NShort GetParameterAsInt16(NUShort parameterId) { return GetNativeType().GetParameterAsInt16(this, 0, parameterId); }
	NUInt GetParameterAsUInt32(NUShort parameterId) { return GetNativeType().GetParameterAsUInt32(this, 0, parameterId); }
	NInt GetParameterAsInt32(NUShort parameterId) { return GetNativeType().GetParameterAsInt32(this, 0, parameterId); }
	NULong GetParameterAsUInt64(NUShort parameterId) { return GetNativeType().GetParameterAsUInt64(this, 0, parameterId); }
	NLong GetParameterAsInt64(NUShort parameterId) { return GetNativeType().GetParameterAsInt64(this, 0, parameterId); }
	bool GetParameterAsBoolean(NUShort parameterId) { return GetNativeType().GetParameterAsBoolean(this, 0, parameterId); }
	NChar GetParameterAsChar(NUShort parameterId) { return GetNativeType().GetParameterAsChar(this, 0, parameterId); }
	NFloat GetParameterAsSingle(NUShort parameterId) { return GetNativeType().GetParameterAsSingle(this, 0, parameterId); }
	NDouble GetParameterAsDouble(NUShort parameterId) { return GetNativeType().GetParameterAsDouble(this, 0, parameterId); }
	NInt GetParameterAsString(NUShort parameterId, NChar * pValue) { return GetNativeType().GetParameterAsString(this, 0, parameterId, pValue); }
	NString GetParameterAsString(NUShort parameterId) { return GetNativeType().GetParameterAsString(this, 0, parameterId); }
	void SetParameter(NUShort parameterId, const void * pValue) { return GetNativeType().SetParameter(this, 0, parameterId, pValue); }
	void SetParameter(NUShort parameterId, NByte value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NSByte value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NUShort value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NShort value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NUInt value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NInt value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NULong value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NLong value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, bool value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameterAsChar(NUShort parameterId, NChar value) { return GetNativeType().SetParameterAsChar(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NFloat value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, NDouble value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }
	void SetParameter(NUShort parameterId, const NChar * szValue) { return GetNativeType().SetParameter(this, 0, parameterId, szValue); }
	void SetParameter(NUShort parameterId, const NString & value) { return GetNativeType().SetParameter(this, 0, parameterId, value); }

	N_DECLARE_OBJECT_TYPE(NObject)

	template<typename T> friend class ::Neurotec::Collections::N_CLASS(NObjectReadOnlyCollection);
	template<typename T> friend class ::Neurotec::Collections::N_CLASS(NObjectCollection);
};

class N_CLASS(NDummyObject) : public N_CLASS(NObject)
{
private:
	N_CLASS(NDummyObject)(HNObject handle)
		: N_CLASS(NObject)(handle, N_CLASS(NObject)::GetNativeType(handle), false)
	{
	}

	N_DECLARE_TYPE(NDummyObject)

	friend class N_CLASS(NObject);
};

inline N_CLASS(NObject)::N_CLASS(NObject)(HNObject handle, const N_CLASS(NNativeType) & requiredType, bool claimHandle)
	: handle(handle), ownsHandle(claimHandle), pOwner(NULL)
{
	try
	{
		if (requiredType != N_CLASS(NNativeType)::Null)
		{
			if (!handle) NThrowArgumentNullException(N_T("handle"));
			if (!requiredType.IsInstanceOfType(handle)) NThrowArgumentException(N_T("handle is not an instance of requiredType"), N_T("handle"));
			HNObject hOwner;
			NCheck(NObjectGetOwner(handle, &hOwner));
			if (hOwner)
			{
				pOwner = hOwner == handle ? this : new N_CLASS(NDummyObject)(hOwner);
			}
		}
	}
	catch(...)
	{
		if (claimHandle)
		{
			NObjectFree(handle);
		}
		throw;
	}
}

inline N_CLASS(NObject)::~N_CLASS(NObject)()
{
	if (ownsHandle)
	{
		NObjectFree(handle);
	}
	if(pOwner && N_GET_OBJECT_TYPE(pOwner) == N_TYPE_OF(NDummyObject))
	{
		delete pOwner;
	}
}

inline void N_CLASS(NObject)::SetOwner(N_CLASS(NObject) * pValue)
{
	HNObject hOwner;
	NCheck(NObjectGetOwner(GetHandle(), &hOwner));
	if (pValue)
	{
		if (hOwner != pValue->GetHandle()) NThrowArgumentException(N_T("The native NObject wrapped by this is not owned by native NObject wrapped by pValue"), N_T("pValue"));
	}
	else
	{
		if (hOwner) NThrowArgumentException(N_T("The native NObject wrapped by this has an owner and pValue is NULL"), N_T("pValue"));
	}
	if(pOwner && N_GET_OBJECT_TYPE(pOwner) == N_TYPE_OF(NDummyObject))
	{
		delete pOwner;
	}
	pOwner = pValue;
}

inline bool N_CLASS(NNativeType)::IsInstanceOfType(N_CLASS(NObject) * pObject) const
{
	if (!pObject) NThrowArgumentNullException(N_T("pObject"));
	return IsInstanceOfType(pObject->GetHandle());
}

inline void N_CLASS(NNativeType)::Reset(N_CLASS(NObject) * pObject) const
{
	if (!pObject) NThrowArgumentNullException(N_T("pObject"));
	NCheck(NTypeReset(handle, pObject->GetHandle()));
}

inline NInt N_CLASS(NNativeType)::GetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUInt parameterId, void * pValue) const
{
	return NCheck(NTypeGetParameter(handle, pObject ? pObject->GetHandle() : NULL, partId, parameterId, pValue));
}

inline void N_CLASS(NNativeType)::CheckParameterType(NUShort partId, NUShort parameterId, NInt typeId) const
{
	NInt theTypeId;
	GetParameter(NULL, partId, NParameterMakeId(N_PC_TYPE_ID, 0, parameterId), &theTypeId);
	if (theTypeId != typeId) NThrowArgumentException(N_T("The type of parameter specified by parameterId do not match with the specified type"), N_EMPTY_STRING);
}

inline NInt N_CLASS(NNativeType)::GetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, void * pValue, NInt typeId) const
{
	CheckParameterType(partId, parameterId, typeId);
	return GetParameter(pObject, partId, parameterId, pValue);
}

inline NByte N_CLASS(NNativeType)::GetParameterAsByte(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NByte value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_BYTE);
	return value;
}

inline NSByte N_CLASS(NNativeType)::GetParameterAsSByte(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NSByte value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_SBYTE);
	return value;
}

inline NUShort N_CLASS(NNativeType)::GetParameterAsUInt16(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NUShort value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_USHORT);
	return value;
}

inline NShort N_CLASS(NNativeType)::GetParameterAsInt16(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NShort value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_SHORT);
	return value;
}

inline NUInt N_CLASS(NNativeType)::GetParameterAsUInt32(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NUInt value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_UINT);
	return value;
}

inline NInt N_CLASS(NNativeType)::GetParameterAsInt32(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NInt value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_INT);
	return value;
}

inline NULong N_CLASS(NNativeType)::GetParameterAsUInt64(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NULong value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_ULONG);
	return value;
}

inline NLong N_CLASS(NNativeType)::GetParameterAsInt64(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NLong value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_LONG);
	return value;
}

inline bool N_CLASS(NNativeType)::GetParameterAsBoolean(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NBool value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_BOOL);
	return value != 0;
}

inline NChar N_CLASS(NNativeType)::GetParameterAsChar(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NChar value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_CHAR);
	return value;
}

inline NFloat N_CLASS(NNativeType)::GetParameterAsSingle(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NFloat value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_FLOAT);
	return value;
}

inline NDouble N_CLASS(NNativeType)::GetParameterAsDouble(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NDouble value;
	GetParameter(pObject, partId, parameterId, &value, N_TYPE_DOUBLE);
	return value;
}

inline NInt N_CLASS(NNativeType)::GetParameterAsString(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NChar * pValue) const
{
	return GetParameter(pObject, partId, parameterId, pValue, N_TYPE_STRING);
}

inline NString N_CLASS(NNativeType)::GetParameterAsString(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId) const
{
	NString value;
	return NReleaseStringBuffer(value, GetParameterAsString(pObject, partId, parameterId, NGetStringBuffer(value, GetParameterAsString(pObject, partId, parameterId, NULL))));
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const void * pValue) const
{
	NCheck(NTypeSetParameter(handle, pObject ? pObject->GetHandle() : NULL, partId, parameterId, pValue));
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const void * pValue, NInt typeId) const
{
	CheckParameterType(partId, parameterId, typeId);
	SetParameter(pObject, partId, parameterId, pValue);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NByte value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_BYTE);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NSByte value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_SBYTE);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NUShort value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_USHORT);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NShort value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_SHORT);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NUInt value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_UINT);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NInt value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_INT);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NULong value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_ULONG);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NLong value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_LONG);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, bool value) const
{
	NBool theValue = value ? NTrue : NFalse;
	SetParameter(pObject, partId, parameterId, &theValue, N_TYPE_BOOL);
}

inline void N_CLASS(NNativeType)::SetParameterAsChar(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NChar value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_CHAR);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NFloat value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_FLOAT);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, NDouble value) const
{
	SetParameter(pObject, partId, parameterId, &value, N_TYPE_DOUBLE);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const NChar * szValue) const
{
	SetParameter(pObject, partId, parameterId, szValue, N_TYPE_STRING);
}

inline void N_CLASS(NNativeType)::SetParameter(N_CLASS(NObject) * pObject, NUShort partId, NUShort parameterId, const NString & value) const
{
	SetParameter(pObject, partId, parameterId, NGetConstStringBuffer(value));
}

}

#endif // !N_OBJECT_HPP_INCLUDED
