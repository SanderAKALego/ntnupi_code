#ifndef N_ERRORS_H_INCLUDED
#define N_ERRORS_H_INCLUDED

#include <NTypes.h>

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ERROR_CODES

#define N_OK                               0
#define N_E_FAILED                        -1
  #define N_E_CORE                        -2
    #define N_E_ARGUMENT                 -10
      #define N_E_ARGUMENT_NULL          -11
      #define N_E_ARGUMENT_OUT_OF_RANGE  -12
      #define N_E_INVALID_ENUM_ARGUMENT  -16
    #define N_E_ARITHMETIC               -17
      #define N_E_OVERFLOW                -8
    #define N_E_FORMAT                   -13
    #define N_E_INDEX_OUT_OF_RANGE        -9
    #define N_E_INVALID_CAST             -18
    #define N_E_INVALID_OPERATION         -7
    #define N_E_IO                       -14
      #define N_E_DIRECTORY_NOT_FOUND    -19
      #define N_E_DRIVE_NOT_FOUND        -20
      #define N_E_END_OF_STREAM          -15
      #define N_E_FILE_NOT_FOUND         -21
      #define N_E_FILE_LOAD              -22
      #define N_E_PATH_TOO_LONG          -23
    #define N_E_NOT_IMPLEMENTED           -5
    #define N_E_NOT_SUPPORTED             -6
    #define N_E_NULL_REFERENCE            -3
    #define N_E_OUT_OF_MEMORY             -4
    #define N_E_SECURITY                 -24

    #define N_E_EXTERNAL                 -90
      #define N_E_CLR                    -93
      #define N_E_COM                    -92
      #define N_E_SYS                    -94
      #define N_E_WIN32                  -91

    #define N_E_PARAMETER               -100
      #define N_E_PARAMETER_READ_ONLY   -101

    #define N_E_NOT_ACTIVATED           -200

#endif // !N_NO_ERROR_CODES

#define NFailed(result) ((result) < 0)
#define NSucceeded(result) ((result) >= 0)

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetDefaultMessageA(NResult code, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetDefaultMessageW(NResult code, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetDefaultMessage(NResult code, NChar * szValue);
#endif
#define NErrorGetDefaultMessage N_FUNC_AW(NErrorGetDefaultMessage)

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetSysErrorMessageA(NInt errnum, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetSysErrorMessageW(NInt errnum, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetSysErrorMessage(NInt errnum, NChar * szValue);
#endif
#define NErrorGetSysErrorMessage N_FUNC_AW(NErrorGetSysErrorMessage)

N_DECLARE_HANDLE(HNError)

HNError N_API NErrorGetLast();

NResult N_API NErrorGetCode(HNError hError);

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetMessageA(HNError hError, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetMessageW(HNError hError, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetMessage(HNError hError, NChar * szValue);
#endif
#define NErrorGetMessage N_FUNC_AW(NErrorGetMessage)

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetParamA(HNError hError, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetParamW(HNError hError, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetParam(HNError hError, NChar * szValue);
#endif
#define NErrorGetParam N_FUNC_AW(NErrorGetParam)

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetExternalCallStackA(HNError hError, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetExternalCallStackW(HNError hError, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetExternalCallStack(HNError hError, NChar * szValue);
#endif
#define NErrorGetExternalCallStack N_FUNC_AW(NErrorGetExternalCallStack)

NInt N_API NErrorGetCallStackLength(HNError hError);

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetCallStackFunctionA(HNError hError, NInt index, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetCallStackFunctionW(HNError hError, NInt index, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetCallStackFunction(HNError hError, NInt index, NChar * szValue);
#endif
#define NErrorGetCallStackFunction N_FUNC_AW(NErrorGetCallStackFunction)

#ifndef N_NO_ANSI_FUNC
NInt N_API NErrorGetCallStackFileA(HNError hError, NInt index, NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NInt N_API NErrorGetCallStackFileW(HNError hError, NInt index, NWChar * szValue);
#endif
#ifdef N_DOCUMENTATION
NInt N_API NErrorGetCallStackFile(HNError hError, NInt index, NChar * szValue);
#endif
#define NErrorGetCallStackFile N_FUNC_AW(NErrorGetCallStackFile)

NInt N_API NErrorGetCallStackLine(HNError hError, NInt index);
HNError N_API NErrorGetInnerError(HNError hError);

NHandle N_API NErrorGetClrError(HNError hError);
NInt N_API NErrorGetExternalError(HNError hError);

#define NErrorGetComError(hError) (NErrorGetCode(hError) == N_E_COM ? NErrorGetExternalError(HNError hError) : 0)
#define NErrorGetSysError(hError) (NErrorGetCode(hError) == N_E_SYS ? NErrorGetExternalError(HNError hError) : 0)
#define NErrorGetWin32Error(hError) (NErrorGetCode(hError) == N_E_WIN32 ? (NUInt)NErrorGetExternalError(HNError hError) : 0)

#ifdef N_MSVC
#pragma deprecated("NErrorGetComError", "NErrorGetSysError", "NErrorGetWin32Error")
#endif

#ifdef N_CPP
}
#endif

#endif // !N_ERRORS_H_INCLUDED
