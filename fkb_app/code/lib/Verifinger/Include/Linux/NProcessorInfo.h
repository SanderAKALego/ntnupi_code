#ifndef N_PROCESSOR_INFO_H_INCLUDED
#define N_PROCESSOR_INFO_H_INCLUDED

#include <NTypes.h>
#include <NErrors.h>

#ifdef N_CPP
extern "C"
{
#endif

#define N_PROCESSOR_INFO_MAX_VENDOR_NAME_LENGTH 13
#define N_PROCESSOR_INFO_MAX_MODEL_NAME_LENGTH  48

typedef enum NProcessorVendor_
{
	npvUnknown = 0,
	npvAmd = 1,
	npvCentaur = 2,
	npvCyrix = 3,
	npvIntel = 4,
	npvNationalSemiconductor = 5,
	npvNexGen = 6,
	npvRiseTechnology = 7,
	npvSiS = 8,
	npvTransmeta = 9,
	npvUmc = 10,
	npvVia = 11
} NProcessorVendor;

#ifndef N_NO_ANSI_FUNC
NResult N_API NProcessorInfoGetVendorNameA(NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NProcessorInfoGetVendorNameW(NWChar * szValue);
#endif
#define NProcessorInfoGetVendorName N_FUNC_AW(NProcessorInfoGetVendorName)

NResult N_API NProcessorInfoGetVendor(NProcessorVendor * pValue);
NResult N_API NProcessorInfoGetModelInfo(NInt * pFamily, NInt * pModel, NInt * pStepping);

#ifndef N_NO_ANSI_FUNC
NResult N_API NProcessorInfoGetModelNameA(NAChar * szValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NProcessorInfoGetModelNameW(NWChar * szValue);
#endif
#define NProcessorInfoGetModelName N_FUNC_AW(NProcessorInfoGetModelName)

NBool N_API NProcessorInfoIsMmxSupported();
NBool N_API NProcessorInfoIs3DNowSupported();
NBool N_API NProcessorInfoIsSseSupported();
NBool N_API NProcessorInfoIsSse2Supported();
NBool N_API NProcessorInfoIsSse3Supported();
NBool N_API NProcessorInfoIsSsse3Supported();
NBool N_API NProcessorInfoIsLZCntSupported();
NBool N_API NProcessorInfoIsPopCntSupported();
NBool N_API NProcessorInfoIsSse41Supported();
NBool N_API NProcessorInfoIsSse42Supported();
NBool N_API NProcessorInfoIsSse4aSupported();
NBool N_API NProcessorInfoIsSse5Supported();

#ifdef N_CPP
}
#endif

#endif // !N_PROCESSOR_INFO_H_INCLUDED
