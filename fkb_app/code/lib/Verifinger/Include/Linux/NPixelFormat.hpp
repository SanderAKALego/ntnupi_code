#ifndef N_PIXEL_FORMAT_HPP_INCLUDED
#define N_PIXEL_FORMAT_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Images
{
#include <NPixelFormat.h>
}}

namespace Neurotec { namespace Images
{
#undef NCalcRowSizeEx
#undef NCalcRowSize
#undef NPixelFormatGetRowSizeEx
#undef NPixelFormatGetRowSize
#ifndef N_PIXEL_FORMAT_EX_H_INCLUDED
#undef NPixelFormatGetBitsPerPixel
#endif

inline NSizeType NCalcRowSizeEx(NUInt bitCount, NUInt length, NUInt alignment)
{
	return ((bitCount * length + (alignment << 3) - 1) / (alignment << 3)) * alignment;
}

inline NSizeType NCalcRowSize(NUInt bitCount, NUInt length)
{
	return NCalcRowSizeEx(bitCount, length, 1);
}

#ifndef N_PIXEL_FORMAT_EX_H_INCLUDED
inline NUInt NPixelFormatGetBitsPerPixel(NPixelFormat pixelFormat)
{
	return NPixelFormatGetBitsPerPixelFunc(pixelFormat);
}
#endif

inline NSizeType NPixelFormatGetRowSizeEx(NPixelFormat pixelFormat, NUInt length, NUInt alignment)
{
	return NCalcRowSizeEx(NPixelFormatGetBitsPerPixel(pixelFormat), length, alignment);
}

inline NSizeType NPixelFormatGetRowSize(NPixelFormat pixelFormat, NUInt length)
{
	return NPixelFormatGetRowSizeEx(pixelFormat, length, 1);
}

}}

#endif // !N_PIXEL_FORMAT_HPP_INCLUDED
