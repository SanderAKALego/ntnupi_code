#ifndef NF_MATCHER_HPP_INCLUDED
#define NF_MATCHER_HPP_INCLUDED

#include <NfmMatchDetails.hpp>
#include <NFMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NFMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NFMatcher) : public N_CLASS(NObject)
{
private:
	static HNFMatcher Create(bool isPalm)
	{
		HNFMatcher handle;
		NCheck(NfmCreateEx(isPalm, &handle));
		return handle;
	}

public:
	static N_CLASS(NFMatcher) * FromHandle(HNFMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NFMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NFMatcher)(HNFMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NFMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NFMatcher)(bool isPalm = false)
		: N_CLASS(NObject)(Create(isPalm), N_NATIVE_TYPE_OF(NFMatcher), true)
	{
	}

	NInt Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size,
		NfmMatchDetails * * ppMatchDetails = NULL)
	{
		NInt score;
		NCheck(NfmVerify((HNFMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			ppMatchDetails, &score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize,
		NfmMatchDetails * * ppMatchDetails = NULL)
	{
		NCheck(NfmIdentifyStart((HNFMatcher)GetHandle(), pTemplate, templateSize, ppMatchDetails));
	}

	NInt IdentifyNext(const void * pTemplate, NSizeType templateSize, NfmMatchDetails * pMatchDetails)
	{
		NInt score;
		NCheck(NfmIdentifyNext((HNFMatcher)GetHandle(), pTemplate, templateSize, pMatchDetails, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NfmIdentifyEnd((HNFMatcher)GetHandle()));
	}

	NBool IsPalm()
	{
		NBool value;
		NCheck(NfmIsPalm((HNFMatcher)GetHandle(), &value));
		return value;
	}

	NInt GetMatchingThreshold()
	{
		return GetParameterAsInt32(NFMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NInt value)
	{
		SetParameter(NFMP_MATCHING_THRESHOLD, value);
	}

	NByte GetMaximalRotation()
	{
		return GetParameterAsByte(NFMP_MAXIMAL_ROTATION);
	}

	void SetMaximalRotation(NByte value)
	{
		SetParameter(NFMP_MAXIMAL_ROTATION, value);
	}

	NfmSpeed GetMatchingSpeed()
	{
		return (NfmSpeed)GetParameterAsInt32(NFMP_MATCHING_SPEED);
	}

	void SetMatchingSpeed(NfmSpeed value)
	{
		SetParameter(NFMP_MATCHING_SPEED, value);
	}

	NUInt GetMode()
	{
		return GetParameterAsUInt32(NFMP_MODE);
	}

	void SetMode(NUInt value)
	{
		SetParameter(NFMP_MODE, value);
	}

	N_DECLARE_OBJECT_TYPE(NFMatcher)
};

}}

#endif // !NF_MATCHER_HPP_INCLUDED
