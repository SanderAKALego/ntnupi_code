#ifndef N_COLLECTIONS_HPP_INCLUDED
#define N_COLLECTIONS_HPP_INCLUDED

#include <NCore.hpp>
#include <vector>
#include <memory.h>

namespace Neurotec { namespace Collections
{

template<typename T> class N_CLASS(NReadOnlyCollection) : public N_CLASS(NObjectBase)
{
private:
	N_CLASS(NObject) * pOwner;
	NInt version;

	N_CLASS(NReadOnlyCollection)(const N_CLASS(NReadOnlyCollection) &)
	{
	}

protected:
	N_CLASS(NReadOnlyCollection)(N_CLASS(NObject) * pOwner)
		: pOwner(pOwner), version(0)
	{
	}

	N_CLASS(NObject) * GetOwner() const
	{
		return pOwner;
	}

	virtual NInt GetVersion() const
	{
		return version;
	}

	virtual void Touch()
	{
		version++;
	}

public:
	typedef T ItemType;
	typedef T const ItemConstType;
	typedef T * ItemPtrType;
	typedef T const * ItemConstPtrType;
	typedef T & ItemRefType;
	typedef T const & ItemConstRefType;

	virtual ~N_CLASS(NReadOnlyCollection)()
	{
	}

	virtual NInt GetCount() const = 0;
	virtual ItemType Get(NInt index) const = 0;
	virtual void GetAll(ItemPtrType arValues) const = 0;
	virtual NInt IndexOf(ItemConstRefType value) const = 0;

	bool Contains(ItemConstRefType value) const
	{
		return IndexOf(value) != -1;
	}

	// Declare pure virtual const_iterator<T> retrieval here

	ItemType operator[](NInt index)
	{
		return Get(index);
	}
};

template<typename T> class N_CLASS(NCollection) : public N_CLASS(NReadOnlyCollection)<T>
{
protected:
	N_CLASS(NCollection)(N_CLASS(NObject) * pOwner)
		: N_CLASS(NReadOnlyCollection)<T>(pOwner)
	{
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) = 0;
	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) = 0;
	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) = 0;
	virtual void RemoveAtInternal(NInt index) = 0;
	virtual void RemoveRangeInternal(NInt index, NInt count) = 0;
	virtual void ClearInternal() = 0;

public:
	virtual void Set(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		SetInternal(index, value);
		this->Touch();
	}

	NInt Add(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		NInt index = AddInternal(value);
		this->Touch();
		return index;
	}

	void Insert(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		InsertInternal(index, value);
		this->Touch();
	}

	bool Remove(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		NInt index = IndexOf(value);
		if(index == -1) return false;
		RemoveAt(index);
		return true;
	}

	void RemoveAt(NInt index)
	{
		RemoveAtInternal(index);
		this->Touch();
	}

	void RemoveRange(NInt index, NInt count)
	{
		RemoveRangeInternal(index, count);
		this->Touch();
	}

	void Clear()
	{
		ClearInternal();
		this->Touch();
	}

	template<typename U, typename UBase> friend class N_CLASS(NArrayCollection);
};

template<typename T> class NObjectVectorFinalizer
{
private:
	NInt oldCount;
	std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> * pOldItems;
	bool ownsItems;

public:
	NObjectVectorFinalizer(NInt oldCount, std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> * pOldItems, bool ownsItems = true)
		: oldCount(oldCount), pOldItems(pOldItems), ownsItems(ownsItems)
	{
	}

	~NObjectVectorFinalizer()
	{
		if(ownsItems)
		{
			for (NInt j = 0; j != oldCount; j++)
			{
				typename N_CLASS(NReadOnlyCollection)<T *>::ItemType oldItem = (*pOldItems)[j];
				if (oldItem)
				{
					delete oldItem;
				}
			}
		}
	}
};

template<typename T> class N_CLASS(NObjectStaticReadOnlyCollection) : public N_CLASS(NReadOnlyCollection)<T *>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(NInt * pValue);
	typedef NResult (N_CALLBACK GetProc)(NInt index, HNObject * pValue);
	typedef typename N_CLASS(NReadOnlyCollection)<T *>::ItemType (* FromHandleProc)(HNObject handle);
	std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> items;

private:
	GetCountProc pGetCountProc;
	GetProc pGetProc;
	FromHandleProc pFromHandleProc;
	bool ownsItems;

	void DeleteItems(NInt index, NInt count)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator s = items.begin() + index, e = s + count, i;
		if(ownsItems)
		{
			for(i = s; i < e; i++)
			{
				delete *i;
			}
		}
		items.erase(s, e);
	}

	void DeleteItem(NInt index)
	{
		DeleteItems(index, 1);
	}

	void DeleteItems()
	{
		DeleteItems(0, (NInt)items.size());
	}

protected:
	N_CLASS(NObjectStaticReadOnlyCollection)(GetCountProc pGetCountProc, GetProc pGetProc, FromHandleProc pFromHandleProc, bool ownsItems)
		: N_CLASS(NReadOnlyCollection)<T *>(NULL), pGetCountProc(pGetCountProc), pGetProc(pGetProc), pFromHandleProc(pFromHandleProc), ownsItems(ownsItems)
	{
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pFromHandleProc) NThrowArgumentNullException(N_T("pFromHandleProc"));
		RefreshItems();
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType AddItem(HNObject hItem)
	{
		return InsertItem((NInt)items.size(), hItem);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType InsertItem(NInt index, HNObject hItem)
	{
		if (!hItem) NThrowArgumentNullException(N_T("hItem"));
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem);
		try
		{
			InsertItem(index, item);
		}
		catch(...)
		{
			if (ownsItems)
			{
				delete item;
			}
			throw;
		}
		return item;
	}

	void AddItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		InsertItem((NInt)items.size(), item);
	}

	void InsertItem(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		if (!item) NThrowArgumentNullException(N_T("item"));
		items.insert(items.begin() + index, item);
		this->Touch();
	}

	virtual void OnRefreshItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		N_UNREFERENCED_PARAMETER(item);
	}

	void RefreshItems()
	{
		NInt oldCount = (NInt)items.size();
		std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> oldItems(items);
		items.erase(items.begin(), items.end());
		NObjectVectorFinalizer<T> finalizer(oldCount, &oldItems, ownsItems);
		NInt count;
		NCheck(pGetCountProc(&count));
		items.reserve((NSizeType)count);
		try
		{
			for (NInt i = 0; i != count; i++)
			{
				bool isOldItem = false;
				HNObject hItem;
				NCheck(pGetProc(i, &hItem));
				for (NInt j = 0; j != oldCount; j++)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType oldItem = oldItems[j];
					if (oldItem && oldItem->GetHandle() == hItem)
					{
						items.push_back(oldItem);
						oldItems[j] = NULL;
						isOldItem = true;
						OnRefreshItem(oldItem);
						break;
					}
				}
				if (!isOldItem)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem);
					try
					{
						items.push_back(item);
					}
					catch(...)
					{
						if (ownsItems)
						{
							delete item;
						}
						throw;
					}
				}
			}
			this->Touch();
		}
		catch(...)
		{
			DeleteItems();
			throw;
		}
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType RemoveItemNoDelete(NInt index)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator i = items.begin() + index;
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = *i;
		items.erase(i);
		this->Touch();
		return item;
	}

	void RemoveItem(NInt index)
	{
		DeleteItem(index);
		this->Touch();
	}

	void RemoveItemRange(NInt index, NInt count)
	{
		DeleteItems(index, count);
		this->Touch();
	}

	void ClearItems()
	{
		DeleteItems();
		this->Touch();
	}

public:
	virtual ~N_CLASS(NObjectStaticReadOnlyCollection)()
	{
		DeleteItems();
	}

	virtual NInt GetCount() const
	{
		return (NInt)items.size();
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(NInt index) const
	{
		if(index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
		if(index >= this->GetCount()) NThrowArgumentOutOfRangeException(N_T("index"), N_T("is greater than or equal to GetCount()"));
		return items[(NSizeType)index];
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T *>::ItemPtrType arValues) const
	{
		if(!arValues) NThrowArgumentNullException(N_T("arValues"));
		if (!items.empty())
		{
			memcpy(arValues, &*items.begin(), items.size());
		}
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if(*i == value) return index;
		}
		return -1;
	}

	NInt IndexOf(HNObject handle) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if((*i)->GetHandle() == handle) return index;
		}
		return -1;
	}

	NBool Contains(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType item) const // repeat it here because the following member hides it
	{
		return IndexOf(item) != -1;
	}

	NBool Contains(HNObject handle) const
	{
		return IndexOf(handle) != -1;
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(HNObject handle) const
	{
		NInt index = IndexOf(handle);
		return index == -1 ? NULL : items.at(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](NInt index) const // repeat it here because the following member hides it
	{
		return Get(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](HNObject handle) const
	{
		return Get(handle);
	}
};

template<typename T> class N_CLASS(NObjectReadOnlyCollection) : public N_CLASS(NReadOnlyCollection)<T *>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(HNObject handle, NInt * pValue);
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, HNObject * pValue);
	typedef typename N_CLASS(NReadOnlyCollection)<T *>::ItemType (* FromHandleProc)(HNObject handle);
	std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> items;

private:
	GetCountProc pGetCountProc;
	GetProc pGetProc;
	FromHandleProc pFromHandleProc;

	void DeleteItems(NInt index, NInt count)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator s = items.begin() + index, e = s + count, i;
		for(i = s; i < e; i++)
		{
			delete *i;
		}
		items.erase(s, e);
	}

	void DeleteItem(NInt index)
	{
		DeleteItems(index, 1);
	}

	void DeleteItems()
	{
		DeleteItems(0, (NInt)items.size());
	}

protected:
	N_CLASS(NObjectReadOnlyCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, GetProc pGetProc, FromHandleProc pFromHandleProc)
		: N_CLASS(NReadOnlyCollection)<T *>(pOwner), pGetCountProc(pGetCountProc), pGetProc(pGetProc), pFromHandleProc(pFromHandleProc)
	{
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pFromHandleProc) NThrowArgumentNullException(N_T("pFromHandleProc"));
		RefreshItems();
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType AddItem(HNObject hItem)
	{
		return InsertItem((NInt)items.size(), hItem);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType InsertItem(NInt index, HNObject hItem)
	{
		if (!hItem) NThrowArgumentNullException(N_T("hItem"));
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem);
		try
		{
			item->SetOwner(this->GetOwner());
			InsertItem(index, item);
		}
		catch(...)
		{
			delete item;
			throw;
		}
		return item;
	}

	void AddItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		InsertItem((NInt)items.size(), item);
	}

	void InsertItem(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		if (!item) NThrowArgumentNullException(N_T("item"));
		items.insert(items.begin() + index, item);
		this->Touch();
	}

	virtual void OnRefreshItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		N_UNREFERENCED_PARAMETER(item);
	}

	void RefreshItems()
	{
		NInt oldCount = (NInt)items.size();
		std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> oldItems(items);
		items.erase(items.begin(), items.end());
		NObjectVectorFinalizer<T> finalizer(oldCount, &oldItems);
		NInt count;
		HNObject hOwner = this->GetOwner()->GetHandle();
		NCheck(pGetCountProc(hOwner, &count));
		items.reserve((NSizeType)count);
		try
		{
			for (NInt i = 0; i != count; i++)
			{
				bool isOldItem = false;
				HNObject hItem;
				NCheck(pGetProc(hOwner, i, &hItem));
				for (NInt j = 0; j != oldCount; j++)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType oldItem = oldItems[j];
					if (oldItem && oldItem->GetHandle() == hItem)
					{
						items.push_back(oldItem);
						oldItems[j] = NULL;
						isOldItem = true;
						OnRefreshItem(oldItem);
						break;
					}
				}
				if (!isOldItem)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem);
					try
					{
						item->SetOwner(this->GetOwner());
						items.push_back(item);
					}
					catch(...)
					{
						delete item;
						throw;
					}
				}
			}
			this->Touch();
		}
		catch(...)
		{
			DeleteItems();
			throw;
		}
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType RemoveItemNoDelete(NInt index)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator i = items.begin() + index;
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = *i;
		items.erase(i);
		this->Touch();
		return item;
	}

	void RemoveItem(NInt index)
	{
		DeleteItem(index);
		this->Touch();
	}

	void RemoveItemRange(NInt index, NInt count)
	{
		DeleteItems(index, count);
		this->Touch();
	}

	void ClearItems()
	{
		DeleteItems();
		this->Touch();
	}

public:
	virtual ~N_CLASS(NObjectReadOnlyCollection)()
	{
		DeleteItems();
	}

	virtual NInt GetCount() const
	{
		return (NInt)items.size();
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(NInt index) const
	{
		if(index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
		if(index >= this->GetCount()) NThrowArgumentOutOfRangeException(N_T("index"), N_T("is greater than or equal to GetCount()"));
		return items[(NSizeType)index];
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T *>::ItemPtrType arValues) const
	{
		if(!arValues) NThrowArgumentNullException(N_T("arValues"));
		if (!items.empty())
		{
			memcpy(arValues, &*items.begin(), items.size());
		}
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if(*i == value) return index;
		}
		return -1;
	}

	NInt IndexOf(HNObject handle) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if((*i)->GetHandle() == handle) return index;
		}
		return -1;
	}

	NBool Contains(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType item) const // repeat it here because the following member hides it
	{
		return IndexOf(item) != -1;
	}

	NBool Contains(HNObject handle) const
	{
		return IndexOf(handle) != -1;
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(HNObject handle) const
	{
		NInt index = IndexOf(handle);
		return index == -1 ? NULL : items.at(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](NInt index) const // repeat it here because the following member hides it
	{
		return Get(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](HNObject handle) const
	{
		return Get(handle);
	}
};

template<typename T> class N_CLASS(NObjectCollection) : public N_CLASS(NCollection)<T *>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(HNObject handle, NInt * pValue);
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, HNObject * pValue);
	typedef typename N_CLASS(NReadOnlyCollection)<T *>::ItemType (* FromHandleProc)(HNObject handle);
	typedef NResult (N_CALLBACK GetCapacityProc)(HNObject handle, NInt * pValue);
	typedef NResult (N_CALLBACK SetCapacityProc)(HNObject handle, NInt value);
	typedef NResult (N_CALLBACK RemoveAtProc)(HNObject handle, NInt index);
	typedef NResult (N_CALLBACK RemoveRangeProc)(HNObject handle, NInt index, NInt count);
	typedef NResult (N_CALLBACK ClearProc)(HNObject handle);

private:
	GetCountProc pGetCountProc;
	GetProc pGetProc;
	FromHandleProc pFromHandleProc;
	GetCapacityProc pGetCapacityProc;
	SetCapacityProc pSetCapacityProc;
	RemoveAtProc pRemoveAtProc;
	RemoveRangeProc pRemoveRangeProc;
	ClearProc pClearProc;
	std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> items;

	void DeleteItems(NInt index, NInt count)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator s = items.begin() + index, e = s + count, i;
		for(i = s; i < e; i++)
		{
			delete *i;
		}
		items.erase(s, e);
	}

	void DeleteItem(NInt index)
	{
		DeleteItems(index, 1);
	}

	void DeleteItems()
	{
		DeleteItems(0, (NInt)items.size());
	}

	void UpdateCapacity()
	{
		items.reserve((NSizeType)GetCapacity());
	}

protected:
	N_CLASS(NObjectCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, GetProc pGetProc, FromHandleProc pFromHandleProc,
		GetCapacityProc pGetCapacityProc, SetCapacityProc pSetCapacityProc, RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NCollection)<T *>(pOwner), pGetCountProc(pGetCountProc), pGetProc(pGetProc), pFromHandleProc(pFromHandleProc),
			pGetCapacityProc(pGetCapacityProc), pSetCapacityProc(pSetCapacityProc), pRemoveAtProc(pRemoveAtProc), pRemoveRangeProc(pRemoveRangeProc), pClearProc(pClearProc)
	{
		if (!pOwner) NThrowArgumentNullException(N_T("pOwner"));
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pFromHandleProc) NThrowArgumentNullException(N_T("pFromHandleProc"));
		if (!pGetCapacityProc)
		{
			if (pSetCapacityProc) NThrowArgumentException(N_T("pSetCapacityProc is not NULL when pGetCapacityProc is NULL"), N_EMPTY_STRING);
		}
		else
		{
			if (!pSetCapacityProc) NThrowArgumentNullException(N_T("pSetCapacityProc"));
		}
		if (!pRemoveAtProc) NThrowArgumentNullException(N_T("pRemoveAtProc"));
		RefreshItems();
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual void RemoveAtInternal(NInt index)
	{
		NCheck(pRemoveAtProc(this->GetOwner()->GetHandle(), index));
		OnRemoveItemRange(index, 1);
	}

	virtual void RemoveRangeInternal(NInt index, NInt count)
	{
		if(pRemoveRangeProc)
		{
			NCheck(pRemoveRangeProc(this->GetOwner()->GetHandle(), index, count));
			OnRemoveItemRange(index, count);
		}
		else if (index == 0 && count == (NInt)items.size())
		{
			ClearInternal();
		}
		else if (count == 1)
		{
			RemoveAtInternal(index);
		}
		else
		{
			HNObject hOwner;
			if (index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
			if (count < 0) NThrowArgumentLessThanZeroException(N_T("count"));
			if (index + count > (NInt)items.size()) NThrowArgumentException(N_T("index plus count is greater than GetCount()"), N_EMPTY_STRING);
			hOwner = this->GetOwner()->GetHandle();
			for (NInt i = count - 1; i >= 0; i--)
			{
				NCheck(pRemoveAtProc(hOwner, index + i));
			}
			OnRemoveItemRange(index, count);
		}
	}

	virtual void ClearInternal()
	{
		if (!pClearProc) NThrowNotSupportedException();
		NInt count = (NInt)items.size();
		NCheck(pClearProc(this->GetOwner()->GetHandle()));
		OnRemoveItemRange(0, count);
	}

	virtual void OnInsertItem(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		items.insert(items.begin() + index, item);
	}

	virtual void OnRemoveItemRange(NInt index, NInt count)
	{
		DeleteItems(index, count);
	}

	virtual void OnRefreshItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		N_UNREFERENCED_PARAMETER(item);
	}

	void RefreshItems()
	{
		NInt oldCount = (NInt)items.size();
		std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> oldItems(items);
		items.erase(items.begin(), items.end());
		NObjectVectorFinalizer<T> finalizer(oldCount, &oldItems);
		NInt count;
		HNObject hOwner = this->GetOwner()->GetHandle();
		NCheck(pGetCountProc(hOwner, &count));
		UpdateCapacity();
		try
		{
			for (NInt i = 0; i != count; i++)
			{
				bool isOldItem = false;
				HNObject hItem;
				NCheck(pGetProc(hOwner, i, &hItem));
				for (NInt j = 0; j != oldCount; j++)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType oldItem = oldItems[j];
					if (oldItem && oldItem->GetHandle() == hItem)
					{
						items.push_back(oldItem);
						oldItems[j] = NULL;
						isOldItem = true;
						OnRefreshItem(oldItem);
						break;
					}
				}
				if (!isOldItem)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem);
					try
					{
						item->SetOwner(this->GetOwner());
						items.push_back(item);
					}
					catch(...)
					{
						delete item;
						throw;
					}
				}
			}
			this->Touch();
		}
		catch(...)
		{
			DeleteItems();
			throw;
		}
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType AddItem(HNObject hItem)
	{
		return InsertItem((NInt)items.size(), hItem);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType InsertItem(NInt index, HNObject hItem)
	{
		if (!hItem) NThrowArgumentNullException(N_T("hItem"));
		UpdateCapacity();
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem);
		try
		{
			item->SetOwner(this->GetOwner());
			InsertItem(index, item);
		}
		catch(...)
		{
			delete item;
			throw;
		}
		return item;
	}

	void AddItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		InsertItem((NInt)items.size(), item);
	}

	void InsertItem(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		if (!item) NThrowArgumentNullException(N_T("item"));
		OnInsertItem(index, item);
		this->Touch();
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType RemoveItemNoDelete(NInt index)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator i = items.begin() + index;
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = *i;
		items.erase(i);
		this->Touch();
		return item;
	}

	void RemoveItem(NInt index)
	{
		DeleteItem(index);
		this->Touch();
	}

	void RemoveItemRange(NInt index, NInt count)
	{
		DeleteItems(index, count);
		this->Touch();
	}

	void ClearItems()
	{
		DeleteItems();
		this->Touch();
	}

public:
	virtual ~N_CLASS(NObjectCollection)()
	{
		DeleteItems();
	}

	virtual NInt GetCount() const
	{
		return (NInt)items.size();
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(NInt index) const
	{
		if(index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
		if(index >= this->GetCount()) NThrowArgumentOutOfRangeException(N_T("index"), N_T("is greater than or equal to GetCount()"));
		return items[(NSizeType)index];
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T *>::ItemPtrType arValues) const
	{
		if(!arValues) NThrowArgumentNullException(N_T("arValues"));
		if (!items.empty())
		{
			memcpy(arValues, &*items.begin(), items.size());
		}
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if(*i == value) return index;
		}
		return -1;
	}

	NInt GetCapacity() const
	{
		if (!pGetCapacityProc) NThrowNotSupportedException();
		NInt value;
		NCheck(pGetCapacityProc(this->GetOwner()->GetHandle(), &value));
		return value;
	}

	void SetCapacity(NInt value)
	{
		if (!pSetCapacityProc) NThrowNotSupportedException();
		NCheck(pSetCapacityProc(this->GetOwner()->GetHandle(), value));
		UpdateCapacity();
	}

	NInt IndexOf(HNObject handle) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if((*i)->GetHandle() == handle) return index;
		}
		return -1;
	}

	NBool Contains(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType item) const // repeat it here because the following member hides it
	{
		return IndexOf(item) != -1;
	}

	NBool Contains(HNObject handle) const
	{
		return IndexOf(handle) != -1;
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(HNObject handle) const
	{
		NInt index = IndexOf(handle);
		return index == -1 ? NULL : items.at(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](NInt index) const // repeat it here because the following member hides it
	{
		return Get(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](HNObject handle) const
	{
		return Get(handle);
	}
};

template<typename T> class N_CLASS(NObjectFlatCollection) : public N_CLASS(NCollection)<T *>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(HNObject handle, NInt * pValue);
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, HNObject * pValue);
	typedef typename N_CLASS(NReadOnlyCollection)<T *>::ItemType (* FromHandleProc)(HNObject handle, N_CLASS(NObject) * pOwner);
	typedef NResult (N_CALLBACK AddProc)(HNObject handle, HNObject value);
	typedef NResult (N_CALLBACK InsertProc)(HNObject handle, NInt index, HNObject value);
	typedef NResult (N_CALLBACK RemoveAtProc)(HNObject handle, NInt index);
	typedef NResult (N_CALLBACK RemoveRangeProc)(HNObject handle, NInt index, NInt count);
	typedef NResult (N_CALLBACK ClearProc)(HNObject handle);

private:
	GetCountProc pGetCountProc;
	GetProc pGetProc;
	FromHandleProc pFromHandleProc;
	AddProc pAddProc;
	InsertProc pInsertProc;
	RemoveAtProc pRemoveAtProc;
	RemoveRangeProc pRemoveRangeProc;
	ClearProc pClearProc;
	std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> items;

	void DeleteItems(NInt index, NInt count)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator s = items.begin() + index, e = s + count;
		items.erase(s, e);
	}

	void DeleteItem(NInt index)
	{
		DeleteItems(index, 1);
	}

	void DeleteItems()
	{
		DeleteItems(0, (NInt)items.size());
	}

protected:
	N_CLASS(NObjectFlatCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc,
		RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NCollection)<T *>(pOwner), pGetCountProc(pGetCountProc), pGetProc(NULL), pFromHandleProc(NULL),
			pAddProc(NULL), pInsertProc(NULL),
			pRemoveAtProc(pRemoveAtProc), pRemoveRangeProc(pRemoveRangeProc), pClearProc(pClearProc)
	{
		if (!pOwner) NThrowArgumentNullException(N_T("pOwner"));
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
		if (!pRemoveAtProc) NThrowArgumentNullException(N_T("pRemoveAtProc"));
		if (!pFromHandleProc)
		{
			NInt count;
			NCheck(pGetCountProc(pOwner->GetHandle(), &count));
			items.reserve((NSizeType)count);
		}
		else
		{
			RefreshItems();
		}
	}

	N_CLASS(NObjectFlatCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, GetProc pGetProc, FromHandleProc pFromHandleProc,
		AddProc pAddProc, InsertProc pInsertProc,
		RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NCollection)<T *>(pOwner), pGetCountProc(pGetCountProc), pGetProc(pGetProc), pFromHandleProc(pFromHandleProc),
			pAddProc(pAddProc), pInsertProc(pInsertProc),
			pRemoveAtProc(pRemoveAtProc), pRemoveRangeProc(pRemoveRangeProc), pClearProc(pClearProc)
	{
		if (!pOwner) NThrowArgumentNullException(N_T("pOwner"));
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pFromHandleProc) NThrowArgumentNullException(N_T("pFromHandleProc"));
		if (!pAddProc) NThrowArgumentNullException(N_T("pAddProc"));
		if (!pRemoveAtProc) NThrowArgumentNullException(N_T("pRemoveAtProc"));
		if (!pFromHandleProc)
		{
			NInt count;
			NCheck(pGetCountProc(pOwner->GetHandle(), &count));
			items.reserve((NSizeType)count);
		}
		else
		{
			RefreshItems();
		}
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		if (!pAddProc) NThrowNotSupportedException();
		if (!value) NThrowArgumentNullException(N_T("value"));
		NInt index = (NInt)items.size();
		HNObject hItem = value->GetHandle();
		NCheck(pAddProc(this->GetOwner()->GetHandle(), hItem));
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType newItem = pFromHandleProc(hItem, this->GetOwner());
		OnInsertItem(index, newItem);
		return index;
	}

	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		if (!pInsertProc) NThrowNotSupportedException();
		if (!value) NThrowArgumentNullException(N_T("value"));
		HNObject hItem = value->GetHandle();
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), index, hItem));
		typename N_CLASS(NReadOnlyCollection)<T *>::ItemType newItem = pFromHandleProc(hItem, this->GetOwner());
		OnInsertItem(index, newItem);
	}

	virtual void RemoveAtInternal(NInt index)
	{
		NCheck(pRemoveAtProc(this->GetOwner()->GetHandle(), index));
		OnRemoveItemRange(index, 1);
	}

	virtual void RemoveRangeInternal(NInt index, NInt count)
	{
		if(pRemoveRangeProc)
		{
			NCheck(pRemoveRangeProc(this->GetOwner()->GetHandle(), index, count));
			OnRemoveItemRange(index, count);
		}
		else if (index == 0 && count == (NInt)items.size())
		{
			ClearInternal();
		}
		else if (count == 1)
		{
			RemoveAtInternal(index);
		}
		else
		{
			HNObject hOwner;
			if (index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
			if (count < 0) NThrowArgumentLessThanZeroException(N_T("count"));
			if (index + count > (NInt)items.size()) NThrowArgumentException(N_T("index plus count is greater than GetCount()"), N_EMPTY_STRING);
			hOwner = this->GetOwner()->GetHandle();
			for (NInt i = count - 1; i >= 0; i--)
			{
				NCheck(pRemoveAtProc(hOwner, index + i));
			}
			OnRemoveItemRange(index, count);
		}
	}

	virtual void ClearInternal()
	{
		if (!pClearProc) NThrowNotSupportedException();
		NInt count = (NInt)items.size();
		NCheck(pClearProc(this->GetOwner()->GetHandle()));
		OnRemoveItemRange(0, count);
	}

	virtual void OnInsertItem(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		items.insert(items.begin() + index, item);
	}

	virtual void OnRemoveItemRange(NInt index, NInt count)
	{
		DeleteItems(index, count);
	}

	virtual void OnRefreshItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		N_UNREFERENCED_PARAMETER(item);
	}

	void RefreshItems()
	{
		NInt oldCount = (NInt)items.size();
		std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType> oldItems(items);
		items.erase(items.begin(), items.end());
		NInt count;
		N_CLASS(NObject) * pOwner = this->GetOwner();
		HNObject hOwner = pOwner->GetHandle();
		NCheck(pGetCountProc(hOwner, &count));
		items.reserve((NSizeType)count);
		try
		{
			for (NInt i = 0; i != count; i++)
			{
				bool isOldItem = false;
				HNObject hItem;
				NCheck(pGetProc(hOwner, i, &hItem));
				for (NInt j = 0; j != oldCount; j++)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType oldItem = oldItems[j];
					if (oldItem && oldItem->GetHandle() == hItem)
					{
						items.push_back(oldItem);
						oldItems[j] = NULL;
						isOldItem = true;
						OnRefreshItem(oldItem);
						break;
					}
				}
				if (!isOldItem)
				{
					typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item = pFromHandleProc(hItem, pOwner);
					items.push_back(item);
				}
			}
			this->Touch();
		}
		catch(...)
		{
			DeleteItems();
			throw;
		}
	}

	void AddItem(typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		InsertItem((NInt)items.size(), item);
	}

	void InsertItem(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemType item)
	{
		if (!item) NThrowArgumentNullException(N_T("item"));
		OnInsertItem(index, item);
		this->Touch();
	}

	void RemoveItem(NInt index)
	{
		typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::iterator i = items.begin() + index;
		DeleteItems(index);
		this->Touch();
	}

	void RemoveItemRange(NInt index, NInt count)
	{
		DeleteItems(index, count);
		this->Touch();
	}

	void ClearItems()
	{
		DeleteItems();
		this->Touch();
	}

public:
	virtual ~N_CLASS(NObjectFlatCollection)()
	{
		DeleteItems();
	}

	virtual NInt GetCount() const
	{
		return (NInt)items.size();
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(NInt index) const
	{
		if(index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
		if(index >= this->GetCount()) NThrowArgumentOutOfRangeException(N_T("index"), N_T("is greater than or equal to GetCount()"));
		return items[(NSizeType)index];
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T *>::ItemPtrType arValues) const
	{
		if(!arValues) NThrowArgumentNullException(N_T("arValues"));
		if (!items.empty())
		{
			memcpy(arValues, &*items.begin(), items.size());
		}
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if(*i == value) return index;
		}
		return -1;
	}

	NInt IndexOf(HNObject handle) const
	{
		NInt index = 0;
		for(typename std::vector<typename N_CLASS(NReadOnlyCollection)<T *>::ItemType>::const_iterator i = items.begin(); i < items.end(); i++, index++)
		{
			if((*i)->GetHandle() == handle) return index;
		}
		return -1;
	}

	NBool Contains(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType item) const // repeat it here because the following member hides it
	{
		return IndexOf(item) != -1;
	}

	NBool Contains(HNObject handle) const
	{
		return IndexOf(handle) != -1;
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(HNObject handle) const
	{
		NInt index = IndexOf(handle);
		return index == -1 ? NULL : items.at(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](NInt index) const // repeat it here because the following member hides it
	{
		return Get(index);
	}

	typename N_CLASS(NReadOnlyCollection)<T *>::ItemType operator[](HNObject handle) const
	{
		return Get(handle);
	}
};

template<typename T> class N_CLASS(NSimpleReadOnlyCollection) : public N_CLASS(NReadOnlyCollection)<T>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(HNObject handle, NInt * pValue);

private:
	GetCountProc pGetCountProc;

protected:
	N_CLASS(NSimpleReadOnlyCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc)
		: N_CLASS(NReadOnlyCollection)<T>(pOwner), pGetCountProc(pGetCountProc)
	{
		if (!pOwner) NThrowArgumentNullException(N_T("pOwner"));
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
	}

public:
	virtual NInt GetCount() const
	{
		NInt value;
		NCheck(pGetCountProc(this->GetOwner()->GetHandle(), &value));
		return value;
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(value);
	}
};

template<typename T> class N_CLASS(NValueReadOnlyCollection) : public N_CLASS(NSimpleReadOnlyCollection)<T>
{
protected:
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType pValue);
	typedef NResult (N_CALLBACK GetAllProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues);

private:
	GetProc pGetProc;
	GetAllProc pGetAllProc;

protected:
	N_CLASS(NValueReadOnlyCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleReadOnlyCollection)<T>::GetCountProc pGetCountProc, GetProc pGetProc, GetAllProc pGetAllProc)
		: N_CLASS(NSimpleReadOnlyCollection)<T>(pOwner, pGetCountProc), pGetProc(pGetProc), pGetAllProc(pGetAllProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
	}

public:
	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType Get(NInt index) const
	{
		typename N_CLASS(NReadOnlyCollection)<T>::ItemType value;
		NCheck(pGetProc(this->GetOwner()->GetHandle(), index, &value));
		return value;
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues) const
	{
		NCheck(pGetAllProc(this->GetOwner()->GetHandle(), arValues));
	}
};

class N_CLASS(NStringReadOnlyCollection) : public N_CLASS(NSimpleReadOnlyCollection)<NString>
{
private:
	NGetStringWithHandleAndIndexProc pGetProc;

protected:
	N_CLASS(NStringReadOnlyCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, NGetStringWithHandleAndIndexProc pGetProc)
		: N_CLASS(NSimpleReadOnlyCollection)<NString>(pOwner, pGetCountProc), pGetProc(pGetProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
	}

public:
	NInt Get(NInt index, NChar * pValue) const
	{
		return NCheck(pGetProc(this->GetOwner()->GetHandle(), index, pValue));
	}

	virtual N_CLASS(NReadOnlyCollection)<NString>::ItemType Get(NInt index) const
	{
		NString value;
		return NGetString(value, pGetProc, this->GetOwner(), index);
	}

	virtual void GetAll(N_CLASS(NReadOnlyCollection)<NString>::ItemPtrType arValues) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(arValues);
	}
};

template<typename T> class N_CLASS(NStructReadOnlyCollection) : public N_CLASS(NSimpleReadOnlyCollection)<T>
{
protected:
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType pValue);
	typedef NResult (N_CALLBACK GetAllProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues);

private:
	GetProc pGetProc;
	GetAllProc pGetAllProc;

protected:
	N_CLASS(NStructReadOnlyCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleReadOnlyCollection)<T>::GetCountProc pGetCountProc, GetProc pGetProc, GetAllProc pGetAllProc)
		: N_CLASS(NSimpleReadOnlyCollection)<T>(pOwner, pGetCountProc), pGetProc(pGetProc), pGetAllProc(pGetAllProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
	}

public:
	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType Get(NInt index) const
	{
		typename N_CLASS(NReadOnlyCollection)<T>::ItemType value;
		NCheck(pGetProc(this->GetOwner()->GetHandle(), index, &value));
		return value;
	}

	void Get(NInt index, T * pValue) const
	{
		NCheck(pGetProc(this->GetOwner()->GetHandle(), index, pValue));
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues) const
	{
		NCheck(pGetAllProc(this->GetOwner()->GetHandle(), arValues));
	}
};

template<typename T> class N_CLASS(NStructWithStringsReadOnlyCollection) : public N_CLASS(NSimpleReadOnlyCollection)<T>
{
protected:
	N_CLASS(NStructWithStringsReadOnlyCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleReadOnlyCollection)<T>::GetCountProc pGetCountProc)
		: N_CLASS(NSimpleReadOnlyCollection)<T>(pOwner, pGetCountProc)
	{
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType GetStruct(HNObject hOwner, NInt index) const = 0;

public:
	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType Get(NInt index) const
	{
		return GetStruct(this->GetOwner()->GetHandle(), index);
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(arValue);
	}
};

template<typename T> class N_CLASS(NSimpleCollection) : public N_CLASS(NCollection)<T>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(HNObject handle, NInt * pValue);
	typedef NResult (N_CALLBACK GetCapacityProc)(HNObject handle, NInt * pValue);
	typedef NResult (N_CALLBACK SetCapacityProc)(HNObject handle, NInt value);
	typedef NResult (N_CALLBACK RemoveAtProc)(HNObject handle, NInt index);
	typedef NResult (N_CALLBACK RemoveRangeProc)(HNObject handle, NInt index, NInt count);
	typedef NResult (N_CALLBACK ClearProc)(HNObject handle);

private:
	GetCountProc pGetCountProc;
	GetCapacityProc pGetCapacityProc;
	SetCapacityProc pSetCapacityProc;
	RemoveAtProc pRemoveAtProc;
	RemoveRangeProc pRemoveRangeProc;
	ClearProc pClearProc;

protected:
	N_CLASS(NSimpleCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, GetCapacityProc pGetCapacityProc, SetCapacityProc pSetCapacityProc,
		RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NCollection)<T>(pOwner), pGetCountProc(pGetCountProc), pGetCapacityProc(pGetCapacityProc), pSetCapacityProc(pSetCapacityProc),
			pRemoveAtProc(pRemoveAtProc), pRemoveRangeProc(pRemoveRangeProc), pClearProc(pClearProc)
	{
		if (!pOwner) NThrowArgumentNullException(N_T("pOwner"));
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
		if (!pGetCapacityProc)
		{
			if (pSetCapacityProc) NThrowArgumentException(N_T("pSetCapacityProc is not NULL when pGetCapacityProc is NULL"), N_EMPTY_STRING);
		}
		else
		{
			if (!pSetCapacityProc) NThrowArgumentNullException(N_T("pSetCapacityProc"));
		}
		if (!pRemoveAtProc) NThrowArgumentNullException(N_T("pRemoveAtProc"));
	}

	virtual void RemoveAtInternal(NInt index)
	{
		NCheck(pRemoveAtProc(this->GetOwner()->GetHandle(), index));
		OnRemoveItemRange(index, 1);
	}

	virtual void RemoveRangeInternal(NInt index, NInt count)
	{
		if(pRemoveRangeProc)
		{
			NCheck(pRemoveRangeProc(this->GetOwner()->GetHandle(), index, count));
			OnRemoveItemRange(index, count);
		}
		else if (index == 0 && count == GetCount())
		{
			ClearInternal();
		}
		else if (count == 1)
		{
			RemoveAtInternal(index);
		}
		else
		{
			HNObject hOwner;
			if (index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
			if (count < 0) NThrowArgumentLessThanZeroException(N_T("count"));
			if (index + count > GetCount()) NThrowArgumentException(N_T("index plus count is greater than GetCount()"), N_EMPTY_STRING);
			hOwner = this->GetOwner()->GetHandle();
			for (NInt i = count - 1; i >= 0; i--)
			{
				NCheck(pRemoveAtProc(hOwner, index + i));
			}
			OnRemoveItemRange(index, count);
		}
	}

	virtual void ClearInternal()
	{
		if (!pClearProc) NThrowNotSupportedException();
		NInt count = GetCount();
		NCheck(pClearProc(this->GetOwner()->GetHandle()));
		OnRemoveItemRange(0, count);
	}

	virtual void OnSetItem(NInt index)
	{
		N_UNREFERENCED_PARAMETER(index);
	}

	virtual void OnInsertItem(NInt index)
	{
		N_UNREFERENCED_PARAMETER(index);
	}

	virtual void OnRemoveItemRange(NInt index, NInt count)
	{
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(count);
	}

public:
	virtual NInt GetCount() const
	{
		NInt value;
		NCheck(pGetCountProc(this->GetOwner()->GetHandle(), &value));
		return value;
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(value);
	}

	NInt GetCapacity() const
	{
		if (!pGetCapacityProc) NThrowNotSupportedException();
		NInt value;
		NCheck(pGetCapacityProc(this->GetOwner()->GetHandle(), &value));
		return value;
	}

	void SetCapacity(NInt value)
	{
		if (!pSetCapacityProc) NThrowNotSupportedException();
		NCheck(pSetCapacityProc(this->GetOwner()->GetHandle(), value));
	}
};

template<typename T> class N_CLASS(NValueCollection) : public N_CLASS(NSimpleCollection)<T>
{
protected:
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType pValue);
	typedef NResult (N_CALLBACK GetAllProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues);
	typedef NResult (N_CALLBACK SetProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemType value);
	typedef NResult (N_CALLBACK AddProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemType value);
	typedef NResult (N_CALLBACK AddWithIndexProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemType value, NInt * pIndex);
	typedef NResult (N_CALLBACK InsertProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemType value);

private:
	GetProc pGetProc;
	GetAllProc pGetAllProc;
	SetProc pSetProc;
	AddProc pAddProc;
	AddWithIndexProc pAddWithIndexProc;
	InsertProc pInsertProc;

protected:
	N_CLASS(NValueCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleCollection)<T>::GetCountProc pGetCountProc,
		typename N_CLASS(NSimpleCollection)<T>::GetCapacityProc pGetCapacityProc, typename N_CLASS(NSimpleCollection)<T>::SetCapacityProc pSetCapacityProc,
		GetProc pGetProc, GetAllProc pGetAllProc, SetProc pSetProc, AddProc pAddProc, InsertProc pInsertProc,
		typename N_CLASS(NSimpleCollection)<T>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NSimpleCollection)<T>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NSimpleCollection)<T>::ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<T>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(pSetProc), pAddProc(pAddProc), pAddWithIndexProc(NULL), pInsertProc(pInsertProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
		if (!pSetProc) NThrowArgumentNullException(N_T("pSetProc"));
		if (!pAddProc) NThrowArgumentNullException(N_T("pAddProc"));
		if (!pInsertProc) NThrowArgumentNullException(N_T("pInsertProc"));
	}

	N_CLASS(NValueCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleCollection)<T>::GetCountProc pGetCountProc,
		typename N_CLASS(NSimpleCollection)<T>::GetCapacityProc pGetCapacityProc, typename N_CLASS(NSimpleCollection)<T>::SetCapacityProc pSetCapacityProc,
		GetProc pGetProc, GetAllProc pGetAllProc, AddWithIndexProc pAddWithIndexProc,
		typename N_CLASS(NSimpleCollection)<T>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NSimpleCollection)<T>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NSimpleCollection)<T>::ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<T>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(NULL), pAddProc(NULL), pAddWithIndexProc(pAddWithIndexProc), pInsertProc(NULL)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
		if (!pAddWithIndexProc) NThrowArgumentNullException(N_T("pAddWithIndexProc"));
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		if (!pSetProc) NThrowNotSupportedException();
		NCheck(pSetProc(this->GetOwner()->GetHandle(), index, value));
		this->OnSetItem(index);
	}

	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		NInt index;
		if (pAddProc)
		{
			index = this->GetCount();
			NCheck(pAddProc(this->GetOwner()->GetHandle(), value));
		}
		else
		{
			NCheck(pAddWithIndexProc(this->GetOwner()->GetHandle(), value, &index));
		}
		this->OnInsertItem(index);
		return index;
	}

	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		if (!pInsertProc) NThrowNotSupportedException();
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), index, value));
		this->OnInsertItem(index);
	}

public:
	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType Get(NInt index) const
	{
		typename N_CLASS(NReadOnlyCollection)<T>::ItemType value;
		NCheck(pGetProc(this->GetOwner()->GetHandle(), index, &value));
		return value;
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues) const
	{
		NCheck(pGetAllProc(this->GetOwner()->GetHandle(), arValues));
	}
};

class N_CLASS(NStringCollection) : public N_CLASS(NSimpleCollection)<NString>
{
protected:
	typedef NResult (N_CALLBACK SetProc)(HNObject handle, NInt index, const NChar * szValue);
	typedef NResult (N_CALLBACK AddProc)(HNObject handle, const NChar * szValue);
	typedef NResult (N_CALLBACK AddWithIndexProc)(HNObject handle, const NChar * szValue, NInt * pIndex);
	typedef NResult (N_CALLBACK InsertProc)(HNObject handle, NInt index, const NChar * szValue);

private:
	NGetStringWithHandleAndIndexProc pGetProc;
	SetProc pSetProc;
	AddProc pAddProc;
	AddWithIndexProc pAddWithIndexProc;
	InsertProc pInsertProc;

protected:
	N_CLASS(NStringCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, GetCapacityProc pGetCapacityProc, SetCapacityProc pSetCapacityProc,
		NGetStringWithHandleAndIndexProc pGetProc, SetProc pSetProc, AddProc pAddProc, InsertProc pInsertProc,
		RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<NString>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pSetProc(pSetProc), pAddProc(pAddProc), pAddWithIndexProc(NULL), pInsertProc(pInsertProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pSetProc) NThrowArgumentNullException(N_T("pSetProc"));
		if (!pAddProc) NThrowArgumentNullException(N_T("pAddProc"));
		if (!pInsertProc) NThrowArgumentNullException(N_T("pInsertProc"));
	}

	N_CLASS(NStringCollection)(N_CLASS(NObject) * pOwner, GetCountProc pGetCountProc, GetCapacityProc pGetCapacityProc, SetCapacityProc pSetCapacityProc,
		NGetStringWithHandleAndIndexProc pGetProc, AddWithIndexProc pAddWithIndexProc,
		RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<NString>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pSetProc(NULL), pAddProc(NULL), pAddWithIndexProc(pAddWithIndexProc), pInsertProc(NULL)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pAddWithIndexProc) NThrowArgumentNullException(N_T("pAddWithIndexProc"));
	}

	virtual void SetInternal(NInt index, N_CLASS(NReadOnlyCollection)<NString>::ItemConstRefType value)
	{
		if (!pSetProc) NThrowNotSupportedException();
		NCheck(pSetProc(this->GetOwner()->GetHandle(), index, NGetConstStringBuffer(value)));
		OnSetItem(index);
	}

	virtual NInt AddInternal(N_CLASS(NReadOnlyCollection)<NString>::ItemConstRefType value)
	{
		NInt index;
		if (pAddProc)
		{
			index = this->GetCount();
			NCheck(pAddProc(this->GetOwner()->GetHandle(), NGetConstStringBuffer(value)));
		}
		else
		{
			NCheck(pAddWithIndexProc(this->GetOwner()->GetHandle(), NGetConstStringBuffer(value), &index));
		}
		OnInsertItem(index);
		return index;
	}

	virtual void InsertInternal(NInt index, N_CLASS(NReadOnlyCollection)<NString>::ItemConstRefType value)
	{
		if (!pInsertProc) NThrowNotSupportedException();
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), index, NGetConstStringBuffer(value)));
		OnInsertItem(index);
	}

public:
	NInt Get(NInt index, NChar * pValue) const
	{
		return NCheck(pGetProc(this->GetOwner()->GetHandle(), index, pValue));
	}

	virtual N_CLASS(NReadOnlyCollection)<NString>::ItemType Get(NInt index) const
	{
		NString value;
		return NGetString(value, pGetProc, this->GetOwner(), index);
	}

	virtual void GetAll(N_CLASS(NReadOnlyCollection)<NString>::ItemPtrType arValues) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(arValues);
	}

	virtual void Set(NInt index, N_CLASS(NReadOnlyCollection)<NString>::ItemConstRefType value) // repeat this and the following two members here because the following three member after them hides them
	{
		SetInternal(index, value);
		this->Touch();
	}

	NInt Add(N_CLASS(NReadOnlyCollection)<NString>::ItemConstRefType value)
	{
		NInt index = AddInternal(value);
		this->Touch();
		return index;
	}

	void Insert(NInt index, N_CLASS(NReadOnlyCollection)<NString>::ItemConstRefType value)
	{
		InsertInternal(index, value);
		this->Touch();
	}

	void Set(NInt index, const NChar * szValue)
	{
		NCheck(pSetProc(this->GetOwner()->GetHandle(), index, szValue));
		OnSetItem(index);
		this->Touch();
	}

	NInt Add(const NChar * szValue)
	{
		NCheck(pAddProc(this->GetOwner()->GetHandle(), szValue));
		OnInsertItem(GetCount() - 1);
		this->Touch();
		return this->GetCount() - 1;
	}

	void Insert(NInt index, const NChar * szValue)
	{
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), index, szValue));
		OnInsertItem(index);
		this->Touch();
	}
};

template<typename T> class N_CLASS(NStructCollection) : public N_CLASS(NSimpleCollection)<T>
{
protected:
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType pValue);
	typedef NResult (N_CALLBACK GetAllProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues);
	typedef NResult (N_CALLBACK SetProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstPtrType pValue);
	typedef NResult (N_CALLBACK AddProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstPtrType pValue);
	typedef NResult (N_CALLBACK AddWithIndexProc)(HNObject handle, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstPtrType pValue, NInt * pIndex);
	typedef NResult (N_CALLBACK InsertProc)(HNObject handle, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstPtrType pValue);

private:
	GetProc pGetProc;
	GetAllProc pGetAllProc;
	SetProc pSetProc;
	AddProc pAddProc;
	AddWithIndexProc pAddWithIndexProc;
	InsertProc pInsertProc;

protected:
	N_CLASS(NStructCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleCollection)<T>::GetCountProc pGetCountProc,
		typename N_CLASS(NSimpleCollection)<T>::GetCapacityProc pGetCapacityProc, typename N_CLASS(NSimpleCollection)<T>::SetCapacityProc pSetCapacityProc,
		GetProc pGetProc, GetAllProc pGetAllProc, SetProc pSetProc, AddProc pAddProc, InsertProc pInsertProc,
		typename N_CLASS(NSimpleCollection)<T>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NSimpleCollection)<T>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NSimpleCollection)<T>::ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<T>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(pSetProc), pAddProc(pAddProc), pAddWithIndexProc(NULL), pInsertProc(pInsertProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
		if (!pSetProc) NThrowArgumentNullException(N_T("pSetProc"));
		if (!pAddProc) NThrowArgumentNullException(N_T("pAddProc"));
		if (!pInsertProc) NThrowArgumentNullException(N_T("pInsertProc"));
	}

	N_CLASS(NStructCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleCollection)<T>::GetCountProc pGetCountProc,
		typename N_CLASS(NSimpleCollection)<T>::GetCapacityProc pGetCapacityProc, typename N_CLASS(NSimpleCollection)<T>::SetCapacityProc pSetCapacityProc,
		GetProc pGetProc, GetAllProc pGetAllProc, AddWithIndexProc pAddWithIndexProc,
		typename N_CLASS(NSimpleCollection)<T>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NSimpleCollection)<T>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NSimpleCollection)<T>::ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<T>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(NULL), pAddProc(NULL), pAddWithIndexProc(pAddWithIndexProc), pInsertProc(NULL)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
		if (!pAddWithIndexProc) NThrowArgumentNullException(N_T("pAddWithIndexProc"));
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		if (!pSetProc) NThrowNotSupportedException();
		NCheck(pSetProc(this->GetOwner()->GetHandle(), index, &value));
		this->OnSetItem(index);
	}

	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		NInt index;
		if (pAddProc)
		{
			index = this->GetCount();
			NCheck(pAddProc(this->GetOwner()->GetHandle(), &value));
		}
		else
		{
			NCheck(pAddWithIndexProc(this->GetOwner()->GetHandle(), &value, &index));
		}
		this->OnInsertItem(index);
		return index;
	}

	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		if (!pInsertProc) NThrowNotSupportedException();
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), index, &value));
		this->OnInsertItem(index);
	}

public:
	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType Get(NInt index) const
	{
		typename N_CLASS(NReadOnlyCollection)<T>::ItemType value;
		NCheck(pGetProc(this->GetOwner()->GetHandle(), index, &value));
		return value;
	}

	void Get(NInt index, T * pValue) const
	{
		NCheck(pGetProc(this->GetOwner()->GetHandle(), index, pValue));
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues) const
	{
		NCheck(pGetAllProc(this->GetOwner()->GetHandle(), arValues));
	}
};

template<typename T> class N_CLASS(NStructWithStringsCollection) : public N_CLASS(NSimpleCollection)<T>
{
protected:
	N_CLASS(NStructWithStringsCollection)(N_CLASS(NObject) * pOwner, typename N_CLASS(NSimpleCollection)<T>::GetCountProc pGetCountProc,
		typename N_CLASS(NSimpleCollection)<T>::GetCapacityProc pGetCapacityProc, typename N_CLASS(NSimpleCollection)<T>::SetCapacityProc pSetCapacityProc,
		typename N_CLASS(NSimpleCollection)<T>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NSimpleCollection)<T>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NSimpleCollection)<T>::ClearProc pClearProc)
		: N_CLASS(NSimpleCollection)<T>(pOwner, pGetCountProc, pGetCapacityProc, pSetCapacityProc, pRemoveAtProc, pRemoveRangeProc, pClearProc)
	{
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		SetStruct(this->GetOwner()->GetHandle(), index, value);
		this->OnSetItem(index);
	}

	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		NInt index = AddStruct(this->GetOwner()->GetHandle(), value);
		this->OnInsertItem(index);
		return index;
	}

	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value)
	{
		InsertStruct(this->GetOwner()->GetHandle(), index, value);
		this->OnInsertItem(index);
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType GetStruct(HNObject hOwner, NInt index) const = 0;
	virtual void SetStruct(HNObject hOwner, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) const = 0;
	virtual NInt AddStruct(HNObject hOwner, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) const = 0;
	virtual void InsertStruct(HNObject hOwner, NInt index, typename N_CLASS(NReadOnlyCollection)<T>::ItemConstRefType value) const = 0;

public:
	virtual typename N_CLASS(NReadOnlyCollection)<T>::ItemType Get(NInt index) const
	{
		return GetStruct(this->GetOwner()->GetHandle(), index);
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T>::ItemPtrType arValues) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(arValue);
	}
};

template<typename T, typename TBase> class N_CLASS(NArrayCollection) : public N_CLASS(NCollection)<T *>
{
protected:
	typedef NResult (N_CALLBACK GetCountProc)(HNObject handle, NInt baseIndex, NInt * pValue);
	typedef NResult (N_CALLBACK RemoveAtProc)(HNObject handle, NInt baseIndex, NInt index);
	typedef NResult (N_CALLBACK RemoveRangeProc)(HNObject handle, NInt baseIndex, NInt index, NInt count);
	typedef NResult (N_CALLBACK ClearProc)(HNObject handle, NInt baseIndex);

private:
	N_CLASS(NCollection)<TBase> * pBaseCollection;
	GetCountProc pGetCountProc;
	RemoveAtProc pRemoveAtProc;
	RemoveRangeProc pRemoveRangeProc;
	ClearProc pClearProc;

protected:
	N_CLASS(NArrayCollection)(N_CLASS(NObject) * pOwner, N_CLASS(NCollection)<TBase> * pBaseCollection, GetCountProc pGetCountProc,
		RemoveAtProc pRemoveAtProc, RemoveRangeProc pRemoveRangeProc, ClearProc pClearProc)
		: N_CLASS(NCollection)<T *>(pOwner), pBaseCollection(pBaseCollection), pGetCountProc(pGetCountProc),
			pRemoveAtProc(pRemoveAtProc), pRemoveRangeProc(pRemoveRangeProc), pClearProc(pClearProc)
	{
		if (!pOwner) NThrowArgumentNullException(N_T("pOwner"));
		if (!pBaseCollection) NThrowArgumentNullException(N_T("pBaseCollection"));
		if (!pGetCountProc) NThrowArgumentNullException(N_T("pGetCountProc"));
	}

	virtual NInt GetVersion() const
	{
		return pBaseCollection->GetVersion();
	}

	virtual void Touch()
	{
		pBaseCollection->Touch();
	}

	virtual void SetInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual NInt AddInternal(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual void InsertInternal(NInt index, typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(value);
	}

	virtual void RemoveAtInternal(NInt index)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
	}

	virtual void RemoveRangeInternal(NInt index, NInt count)
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(count);
	}

	virtual void ClearInternal()
	{
		NThrowNotSupportedException();
	}

	virtual void OnSetItem(NInt baseIndex, NInt index)
	{
		N_UNREFERENCED_PARAMETER(baseIndex);
		N_UNREFERENCED_PARAMETER(index);
	}

	virtual void OnInsertItem(NInt baseIndex, NInt index)
	{
		N_UNREFERENCED_PARAMETER(baseIndex);
		N_UNREFERENCED_PARAMETER(index);
	}

	virtual void OnRemoveItemRange(NInt baseIndex, NInt index, NInt count)
	{
		N_UNREFERENCED_PARAMETER(baseIndex);
		N_UNREFERENCED_PARAMETER(index);
		N_UNREFERENCED_PARAMETER(count);
	}

public:
	virtual NInt GetCount() const
	{
		return pBaseCollection->GetCount();
	}

	virtual typename N_CLASS(NReadOnlyCollection)<T *>::ItemType Get(NInt index) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(index);
	}

	virtual void GetAll(typename N_CLASS(NReadOnlyCollection)<T *>::ItemPtrType arValues) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(arValues);
	}

	virtual NInt IndexOf(typename N_CLASS(NReadOnlyCollection)<T *>::ItemConstRefType value) const
	{
		NThrowNotSupportedException();
		N_UNREFERENCED_PARAMETER(value);
	}

	NInt GetCount(NInt baseIndex) const
	{
		NInt value;
		NCheck(pGetCountProc(this->GetOwner()->GetHandle(), baseIndex, &value));
		return value;
	}

	virtual T Get(NInt baseIndex, NInt index) const = 0;
	virtual void GetAll(NInt baseIndex, T * arValues) const = 0;
	virtual void Set(NInt baseIndex, NInt index, const T & value) = 0;
	virtual NInt Add(NInt baseIndex, const T & value) = 0;
	virtual void Insert(NInt baseIndex, NInt index, const T & value) = 0;

	void RemoveAt(NInt baseIndex, NInt index)
	{
		if (!pRemoveAtProc) NThrowNotSupportedException();
		NCheck(pRemoveAtProc(this->GetOwner()->GetHandle(), baseIndex, index));
		OnRemoveItemRange(baseIndex, index, 1);
		this->Touch();
	}

	void RemoveRange(NInt baseIndex, NInt index, NInt count)
	{
		if(pRemoveRangeProc)
		{
			NCheck(pRemoveRangeProc(this->GetOwner()->GetHandle(), baseIndex, index, count));
			OnRemoveItemRange(baseIndex, index, count);
			this->Touch();
		}
		else if (index == 0 && count == GetCount(baseIndex))
		{
			Clear(baseIndex);
		}
		else if (count == 1)
		{
			RemoveAt(baseIndex, index);
		}
		else
		{
			if (!pRemoveAtProc) NThrowNotSupportedException();
			HNObject hOwner;
			if (index < 0) NThrowArgumentLessThanZeroException(N_T("index"));
			if (count < 0) NThrowArgumentLessThanZeroException(N_T("count"));
			if (index + count > GetCount(baseIndex)) NThrowArgumentException(N_T("index plus count is greater than GetCount(baseIndex)"), N_EMPTY_STRING);
			hOwner = this->GetOwner()->GetHandle();
			for (NInt i = count - 1; i >= 0; i--)
			{
				NCheck(pRemoveAtProc(hOwner, baseIndex, index + i));
			}
			OnRemoveItemRange(baseIndex, index, count);
			this->Touch();
		}
	}

	void Clear(NInt baseIndex)
	{
		if (!pClearProc) NThrowNotSupportedException();
		NInt count = GetCount(baseIndex);
		NCheck(pClearProc(this->GetOwner()->GetHandle(), baseIndex));
		OnRemoveItemRange(baseIndex, 0, count);
		this->Touch();
	}
};

template<typename T, typename TBase> class N_CLASS(NValueArrayCollection) : public N_CLASS(NArrayCollection)<T, TBase>
{
protected:
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt baseIndex, NInt index, T * pValue);
	typedef NResult (N_CALLBACK GetAllProc)(HNObject handle, NInt baseIndex, T * arValues);
	typedef NResult (N_CALLBACK SetProc)(HNObject handle, NInt baseIndex, NInt index, T value);
	typedef NResult (N_CALLBACK AddProc)(HNObject handle, NInt baseIndex, T value);
	typedef NResult (N_CALLBACK AddWithIndexProc)(HNObject handle, NInt baseIndex, T value, NInt * pIndex);
	typedef NResult (N_CALLBACK InsertProc)(HNObject handle, NInt baseIndex, NInt index, T value);

private:
	GetProc pGetProc;
	GetAllProc pGetAllProc;
	SetProc pSetProc;
	AddProc pAddProc;
	AddWithIndexProc pAddWithIndexProc;
	InsertProc pInsertProc;

protected:
	N_CLASS(NValueArrayCollection)(N_CLASS(NObject) * pOwner, N_CLASS(NCollection)<TBase> * pBaseCollection, typename N_CLASS(NArrayCollection)<T, TBase>::GetCountProc pGetCountProc,
		GetProc pGetProc, GetAllProc pGetAllProc, SetProc pSetProc, AddProc pAddProc, InsertProc pInsertProc,
		typename N_CLASS(NArrayCollection)<T, TBase>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NArrayCollection)<T, TBase>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NArrayCollection)<T, TBase>::ClearProc pClearProc)
		: N_CLASS(NArrayCollection)<T, TBase>(pOwner, pBaseCollection, pGetCountProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(pSetProc), pAddProc(pAddProc), pAddWithIndexProc(NULL), pInsertProc(pInsertProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
		if (!pSetProc) NThrowArgumentNullException(N_T("pSetProc"));
	}

	N_CLASS(NValueArrayCollection)(N_CLASS(NObject) * pOwner, N_CLASS(NCollection)<TBase> * pBaseCollection, typename N_CLASS(NArrayCollection)<T, TBase>::GetCountProc pGetCountProc,
		GetProc pGetProc, GetAllProc pGetAllProc, AddWithIndexProc pAddWithIndexProc,
		typename N_CLASS(NArrayCollection)<T, TBase>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NArrayCollection)<T, TBase>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NArrayCollection)<T, TBase>::ClearProc pClearProc)
		: N_CLASS(NArrayCollection)<T, TBase>(pOwner, pBaseCollection, pGetCountProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(NULL), pAddProc(NULL), pAddWithIndexProc(pAddWithIndexProc), pInsertProc(NULL)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
	}

public:
	virtual T Get(NInt baseIndex, NInt index) const
	{
		T value;
		NCheck(pGetProc(this->GetOwner()->GetHandle(), baseIndex, index, &value));
		return value;
	}

	virtual void GetAll(NInt baseIndex, T * arValues) const
	{
		NCheck(pGetAllProc(this->GetOwner()->GetHandle(), baseIndex, arValues));
	}

	virtual void Set(NInt baseIndex, NInt index, const T & value)
	{
		if (!pSetProc) NThrowNotSupportedException();
		NCheck(pSetProc(this->GetOwner()->GetHandle(), baseIndex, index, value));
		this->OnSetItem(baseIndex, index);
		this->Touch();
	}

	virtual NInt Add(NInt baseIndex, const T & value)
	{
		NInt index;
		if (pAddProc)
		{
			index = this->GetCount(baseIndex);
			NCheck(pAddProc(this->GetOwner()->GetHandle(), baseIndex, value));
		}
		else if (pAddWithIndexProc)
		{
			NCheck(pAddWithIndexProc(this->GetOwner()->GetHandle(), baseIndex, value, &index));
		}
		else NThrowNotSupportedException();
		this->OnInsertItem(baseIndex, index);
		this->Touch();
		return index;
	}

	virtual void Insert(NInt baseIndex, NInt index, const T & value)
	{
		if (!pInsertProc) NThrowNotSupportedException();
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), baseIndex, index, value));
		this->OnInsertItem(baseIndex, index);
		this->Touch();
	}
};

template<typename T, typename TBase> class N_CLASS(NStructArrayCollection) : public N_CLASS(NArrayCollection)<T, TBase>
{
protected:
	typedef NResult (N_CALLBACK GetProc)(HNObject handle, NInt baseIndex, NInt index, T * pValue);
	typedef NResult (N_CALLBACK GetAllProc)(HNObject handle, NInt baseIndex, T * arValues);
	typedef NResult (N_CALLBACK SetProc)(HNObject handle, NInt baseIndex, NInt index, const T * pValue);
	typedef NResult (N_CALLBACK AddProc)(HNObject handle, NInt baseIndex, const T * pValue);
	typedef NResult (N_CALLBACK AddWithIndexProc)(HNObject handle, NInt baseIndex, const T * pValue, NInt * pIndex);
	typedef NResult (N_CALLBACK InsertProc)(HNObject handle, NInt baseIndex, NInt index, const T * pValue);

private:
	GetProc pGetProc;
	GetAllProc pGetAllProc;
	SetProc pSetProc;
	AddProc pAddProc;
	AddWithIndexProc pAddWithIndexProc;
	InsertProc pInsertProc;

protected:
	N_CLASS(NStructArrayCollection)(N_CLASS(NObject) * pOwner, N_CLASS(NCollection)<TBase> * pBaseCollection, typename N_CLASS(NArrayCollection)<T, TBase>::GetCountProc pGetCountProc,
		GetProc pGetProc, GetAllProc pGetAllProc, SetProc pSetProc, AddProc pAddProc, InsertProc pInsertProc,
		typename N_CLASS(NArrayCollection)<T, TBase>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NArrayCollection)<T, TBase>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NArrayCollection)<T, TBase>::ClearProc pClearProc)
		: N_CLASS(NArrayCollection)<T, TBase>(pOwner, pBaseCollection, pGetCountProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(pSetProc), pAddProc(pAddProc), pAddWithIndexProc(NULL), pInsertProc(pInsertProc)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
		if (!pSetProc) NThrowArgumentNullException(N_T("pSetProc"));
	}

	N_CLASS(NStructArrayCollection)(N_CLASS(NObject) * pOwner, N_CLASS(NCollection)<TBase> * pBaseCollection, typename N_CLASS(NArrayCollection)<T, TBase>::GetCountProc pGetCountProc,
		GetProc pGetProc, GetAllProc pGetAllProc, AddWithIndexProc pAddWithIndexProc,
		typename N_CLASS(NArrayCollection)<T, TBase>::RemoveAtProc pRemoveAtProc, typename N_CLASS(NArrayCollection)<T, TBase>::RemoveRangeProc pRemoveRangeProc, typename N_CLASS(NArrayCollection)<T, TBase>::ClearProc pClearProc)
		: N_CLASS(NArrayCollection)<T, TBase>(pOwner, pBaseCollection, pGetCountProc, pRemoveAtProc, pRemoveRangeProc, pClearProc),
			pGetProc(pGetProc), pGetAllProc(pGetAllProc), pSetProc(NULL), pAddProc(NULL), pAddWithIndexProc(pAddWithIndexProc), pInsertProc(NULL)
	{
		if (!pGetProc) NThrowArgumentNullException(N_T("pGetProc"));
		if (!pGetAllProc) NThrowArgumentNullException(N_T("pGetAllProc"));
	}

public:
	virtual T Get(NInt baseIndex, NInt index) const
	{
		T value;
		NCheck(pGetProc(this->GetOwner()->GetHandle(), baseIndex, index, &value));
		return value;
	}

	void Get(NInt baseIndex, NInt index, T * pValue) const
	{
		NCheck(pGetProc(this->GetOwner()->GetHandle(), baseIndex, index, pValue));
	}

	virtual void GetAll(NInt baseIndex, T * arValues) const
	{
		NCheck(pGetAllProc(this->GetOwner()->GetHandle(), baseIndex, arValues));
	}

	virtual void Set(NInt baseIndex, NInt index, const T & value)
	{
		if (!pSetProc) NThrowNotSupportedException();
		NCheck(pSetProc(this->GetOwner()->GetHandle(), baseIndex, index, &value));
		this->OnSetItem(baseIndex, index);
		this->Touch();
	}

	virtual NInt Add(NInt baseIndex, const T & value)
	{
		NInt index;
		if (pAddProc)
		{
			index = this->GetCount(baseIndex);
			NCheck(pAddProc(this->GetOwner()->GetHandle(), baseIndex, &value));
		}
		else if (pAddWithIndexProc)
		{
			NCheck(pAddWithIndexProc(this->GetOwner()->GetHandle(), baseIndex, &value, &index));
		}
		else NThrowNotSupportedException();
		this->OnInsertItem(baseIndex, index);
		this->Touch();
		return index;
	}

	virtual void Insert(NInt baseIndex, NInt index, const T & value)
	{
		if (!pInsertProc) NThrowNotSupportedException();
		NCheck(pInsertProc(this->GetOwner()->GetHandle(), baseIndex, index, &value));
		this->OnInsertItem(baseIndex, index);
		this->Touch();
	}
};

}}

#endif // !N_COLLECTIONS_HPP_INCLUDED
