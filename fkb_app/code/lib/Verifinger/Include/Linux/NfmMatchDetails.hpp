#ifndef NFM_MATCH_DETAILS_HPP_INCLUDED
#define NFM_MATCH_DETAILS_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
using Neurotec::IO::HNStream;
#include <NfmMatchDetails.h>
}}

namespace Neurotec { namespace Biometrics
{

inline void MatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NfmMatchDetails ** ppMatchDetails)
{
	NCheck(NfmMatchDetailsDeserialize(buffer, bufferLength, ppMatchDetails));
}

inline NSizeType MatchDetailsSerialize(NfmMatchDetails * pMatchDetails, void * * pBuffer)
{
	NSizeType bufferLength;
	NCheck(NfmMatchDetailsSerialize(pMatchDetails, pBuffer, &bufferLength));
	return bufferLength;
}

}}

#endif // !NFM_MATCH_DETAILS_HPP_INCLUDED
