#ifndef TIFF_HPP_INCLUDED
#define TIFF_HPP_INCLUDED

#include <NImage.hpp>
namespace Neurotec { namespace Images
{
#include <Tiff.h>
}}

namespace Neurotec { namespace Images
{

class N_CLASS(Tiff)
{
private:
	N_CLASS(Tiff)() {}
	N_CLASS(Tiff)(const N_CLASS(Tiff)&) {}

public:
	static N_CLASS(NImage)* LoadImage(const NChar * szFileName)
	{
		HNImage handle;
		NCheck(TiffLoadImageFromFile(szFileName, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	static N_CLASS(NImage)* LoadImage(const NString & fileName)
	{
		return LoadImage(NGetConstStringBuffer(fileName));
	}

	static N_CLASS(NImage)* LoadImage(const void * buffer, NSizeType bufferLength)
	{
		HNImage handle;
		NCheck(TiffLoadImageFromMemory(buffer, bufferLength, &handle));
		try
		{
			return N_CLASS(NImage)::FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}
};

}}

#endif // !TIFF_HPP_INCLUDED
