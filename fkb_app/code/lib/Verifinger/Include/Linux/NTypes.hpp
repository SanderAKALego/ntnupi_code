#ifndef N_TYPES_HPP_INCLUDED
#define N_TYPES_HPP_INCLUDED

namespace Neurotec
{
#include <NTypes.h>
}

#ifndef N_CPP
	#error Only C++ is supported.
#endif

#ifndef N_FRAMEWORK_NO_AUTO_DETECT
	// Try auto-detect the framework
	#if defined(_MFC_VER)
		#define N_FRAMEWORK_MFC
	#elif defined(wxMAJOR_VERSION)
		#define N_FRAMEWORK_WX
	#else
	#endif
#endif

namespace Neurotec
{
#undef N_UINT8_MIN
#undef N_UINT8_MAX
#undef N_INT8_MIN
#undef N_INT8_MAX
#undef N_UINT16_MIN
#undef N_UINT16_MAX
#undef N_INT16_MIN
#undef N_INT16_MAX
#undef N_UINT32_MIN
#undef N_UINT32_MAX
#undef N_INT32_MIN
#undef N_INT32_MAX
#ifndef N_NO_INT_64
	#undef N_UINT64_MIN
	#undef N_UINT64_MAX
	#undef N_INT64_MIN
	#undef N_INT64_MAX
#endif
#undef N_BYTE_MIN
#undef N_BYTE_MAX
#undef N_SBYTE_MIN
#undef N_SBYTE_MAX
#undef N_USHORT_MIN
#undef N_USHORT_MAX
#undef N_SHORT_MIN
#undef N_SHORT_MAX
#undef N_UINT_MIN
#undef N_UINT_MAX
#undef N_INT_MIN
#undef N_INT_MAX
#ifndef N_NO_INT_64
	#undef N_ULONG_MIN
	#undef N_ULONG_MAX
	#undef N_LONG_MIN
	#undef N_LONG_MAX
#endif
#ifndef N_NO_FLOAT
	#undef N_SINGLE_MIN
	#undef N_SINGLE_MAX
	#undef N_SINGLE_EPSILON
	#undef N_DOUBLE_MIN
	#undef N_DOUBLE_MAX
	#undef N_DOUBLE_EPSILON
	#undef N_FLOAT_MIN
	#undef N_FLOAT_MAX
	#undef N_FLOAT_EPSILON
#endif
#undef NTrue
#undef NFalse
#ifndef N_NO_UNICODE
	#undef N_WCHAR_SIZE
#endif
#undef N_SIZE_TYPE_MIN
#undef N_SIZE_TYPE_MAX
#undef N_POS_TYPE_MIN
#undef N_POS_TYPE_MAX

#ifdef N_MSVC
	const NUInt8 N_UINT8_MIN = 0x00ui8;
	const NUInt8 N_UINT8_MAX = 0xFFui8;
	const NInt8 N_INT8_MIN = 0x80i8;
	const NInt8 N_INT8_MAX = 0x7Fi8;
	const NUInt16 N_UINT16_MIN = 0x0000ui16;
	const NUInt16 N_UINT16_MAX = 0xFFFFui16;
	const NInt16 N_INT16_MIN = 0x8000i16;
	const NInt16 N_INT16_MAX = 0x7FFFi16;
	const NUInt32 N_UINT32_MIN = 0x00000000ui32;
	const NUInt32 N_UINT32_MAX = 0xFFFFFFFFui32;
	const NInt32 N_INT32_MIN = 0x80000000i32;
	const NInt32 N_INT32_MAX = 0x7FFFFFFFi32;
#else
	const NUInt8 N_UINT8_MIN = ((NUInt8)0x00u);
	const NUInt8 N_UINT8_MAX = ((NUInt8)0xFFu);
	const NInt8 N_INT8_MIN = ((NInt8)0x80);
	const NInt8 N_INT8_MAX = ((NInt8)0x7F);
	const NUInt16 N_UINT16_MIN = ((NUInt16)0x0000u);
	const NUInt16 N_UINT16_MAX = ((NUInt16)0xFFFFu);
	const NInt16 N_INT16_MIN = ((NInt16)0x8000);
	const NInt16 N_INT16_MAX = ((NInt16)0x7FFF);
	const NUInt32 N_UINT32_MIN = 0x00000000u;
	const NUInt32 N_UINT32_MAX = 0xFFFFFFFFu;
	const NInt32 N_INT32_MIN = 0x80000000;
	const NInt32 N_INT32_MAX = 0x7FFFFFFF;
#endif

#ifndef N_NO_INT_64
	#ifdef N_MSVC
		const NUInt64 N_UINT64_MIN = 0x0000000000000000ui64;
		const NUInt64 N_UINT64_MAX = 0xFFFFFFFFFFFFFFFFui64;
		const NInt64 N_INT64_MIN = 0x8000000000000000i64;
		const NInt64 N_INT64_MAX = 0x7FFFFFFFFFFFFFFFi64;
	#else
		const NUInt64 N_UINT64_MIN = 0x0000000000000000ull;
		const NUInt64 N_UINT64_MAX = 0xFFFFFFFFFFFFFFFFull;
		const NInt64 N_INT64_MIN = 0x8000000000000000ll;
		const NInt64 N_INT64_MAX = 0x7FFFFFFFFFFFFFFFll;
	#endif
#endif

const NByte N_BYTE_MIN = N_UINT8_MIN;
const NByte N_BYTE_MAX = N_UINT8_MAX;
const NSByte N_SBYTE_MIN = N_INT8_MIN;
const NSByte N_SBYTE_MAX = N_INT8_MAX;
const NUShort N_USHORT_MIN = N_UINT16_MIN;
const NUShort N_USHORT_MAX = N_UINT16_MAX;
const NShort N_SHORT_MIN = N_INT16_MIN;
const NShort N_SHORT_MAX = N_INT16_MAX;
const NUInt N_UINT_MIN = N_UINT32_MIN;
const NUInt N_UINT_MAX = N_UINT32_MAX;
const NInt N_INT_MIN = N_INT32_MIN;
const NInt N_INT_MAX = N_INT32_MAX;

#ifndef N_NO_INT_64
	const NULong N_ULONG_MIN = N_UINT64_MIN;
	const NULong N_ULONG_MAX = N_UINT64_MAX;
	const NLong N_LONG_MIN = N_INT64_MIN;
	const NLong N_LONG_MAX = N_INT64_MAX;
#endif

#ifndef N_NO_FLOAT
	const NSingle N_SINGLE_MIN = 1.175494351e-38F;
	const NSingle N_SINGLE_MAX = 3.402823466e+38F;
	const NSingle N_SINGLE_EPSILON = 1.192092896e-07F;
	const NDouble N_DOUBLE_MIN = 2.2250738585072014e-308;
	const NDouble N_DOUBLE_MAX = 1.7976931348623158e+308;
	const NDouble N_DOUBLE_EPSILON = 2.2204460492503131e-016;

	const NFloat N_FLOAT_MIN = N_SINGLE_MIN;
	const NFloat N_FLOAT_MAX = N_SINGLE_MAX;
	const NFloat N_FLOAT_EPSILON = N_SINGLE_EPSILON;
#endif

const NBool NTrue = 1;
const NBool NFalse = 0;

#ifndef N_NO_UNICODE
	#if defined(N_WINDOWS) || (defined(__SIZEOF_WCHAR_T__) && __SIZEOF_WCHAR_T__ == 2)
		const NSizeType N_WCHAR_SIZE = 2;
	#else // !defined(N_WINDOWS) && (!defined(__SIZEOF_WCHAR_T__) || __SIZEOF_WCHAR_T__ != 2)
		const NSizeType N_WCHAR_SIZE = 4;
	#endif // !defined(N_WINDOWS) && (!defined(__SIZEOF_WCHAR_T__) || __SIZEOF_WCHAR_T__ != 2)
#endif // !N_NO_UNICODE

#ifdef N_64
	const NSizeType N_SIZE_TYPE_MIN = N_UINT64_MIN;
	const NSizeType N_SIZE_TYPE_MAX = N_UINT64_MAX;
#else
	const NSizeType N_SIZE_TYPE_MIN = N_UINT32_MIN;
	const NSizeType N_SIZE_TYPE_MAX = N_UINT32_MAX;
#endif

#ifndef N_NO_INT_64
	const NPosType N_POS_TYPE_MIN = N_INT64_MIN;
	const NPosType N_POS_TYPE_MAX = N_INT64_MAX;
#else
	const NPosType N_POS_TYPE_MIN = N_INT32_MIN;
	const NPosType N_POS_TYPE_MAX = N_INT32_MAX;
#endif

void N_NO_RETURN NThrowOverflowException();
}

// Define various types
#if defined(N_FRAMEWORK_MFC)
	#include <afx.h>
	#include <atltime.h>

namespace Neurotec
{
	#define N_CLASS(name) C##name

	typedef CString NString;

	inline NInt NGetStringLength(const NString & value)
	{
		return value.GetLength();
	}

	inline const NChar * NGetConstStringBuffer(const NString & value)
	{
		return value.GetString();
	}

	inline NChar * NGetStringBuffer(NString & value, NInt length)
	{
		return value.GetBuffer(length + 1);
	}

	inline NString & NReleaseStringBuffer(NString & value, NInt length)
	{
		value.ReleaseBuffer(length);
		return value;
	}

	#define N_FRAMEWORK_DATE_TIME_SUPPORTED

	typedef CFileTime NDateTime;

	inline NLong ToRawDateTime(NDateTime value)
	{
		const NLong Win32FileEpoch = 504911232000000000LL;
		const NLong Max = 3155378975999999999LL;
		NLong theValue = (NLong)value.GetTime();
		if(Max - Win32FileEpoch < theValue) NThrowOverflowException();
		return Win32FileEpoch + theValue;
	}

	inline NDateTime ToNDateTime(NLong value)
	{
		const NLong Win32FileEpoch = 504911232000000000LL;
		value -= Win32FileEpoch;
		if(value < 0) NThrowOverflowException();
		return CFileTime((ULONGLONG)value);
	}

	inline NLong ToRawDateTime(CTime value)
	{
		const NLong UnixEpoch = 621355968000000000LL;
		const NLong TicksPerSecond = 10000000LL;
		const NLong Max = 3155378975999999999LL;
		NLong theValue = (NLong)(value.GetTime());
		if(theValue < 0 || (Max - UnixEpoch) / TicksPerSecond < theValue) NThrowOverflowException();
		return UnixEpoch + theValue * TicksPerSecond;
	}

	inline CTime ToCTime(NLong value)
	{
		const NLong UnixEpoch = 621355968000000000LL;
		const NLong TicksPerSecond = 10000000LL;
		value -= (value) - UnixEpoch;
		NLong value1 = value / TicksPerSecond;
		if(value < 0 || value1 > (NLong)(((NULong)((__time64_t)-1)) / 2)) NThrowOverflowException();
		if(value - value1 * TicksPerSecond >= TicksPerSecond / 2)
		{
			if (value1 == (NLong)(((NULong)((__time64_t)-1)) / 2)) NThrowOverflowException();
			value1++;
		}
		return CTime((__time64_t)value1);
	}

}
#elif defined(N_FRAMEWORK_WX)
	#include <wx/string.h>
	#include <wx/object.h>
	#include <wx/datetime.h>

namespace Neurotec
{

	#define N_CLASS(name) wx##name

	typedef wxString NString;

	inline NInt NGetStringLength(const NString & value)
	{
		return (NInt)value.Len();
	}

	inline const NChar * NGetConstStringBuffer(const NString & value)
	{
		return value.GetData();
	}

	inline NChar * NGetStringBuffer(NString & value, NInt length)
	{
		return value.GetWriteBuf((NSizeType)(length + 1));
	}

	inline NString & NReleaseStringBuffer(NString & value, NInt length)
	{
		value.UngetWriteBuf((NSizeType)length);
		return value;
	}

	#define N_FRAMEWORK_DATE_TIME_SUPPORTED

	typedef wxDateTime NDateTime;

	inline NLong ToRawDateTime(NDateTime value)
	{
		const NLong UnixEpoch = 621355968000000000LL;
		const NLong TicksPerSecond = 10000000LL;
		const NLong TicksPerMillisecond = 10000LL;
		const NLong Max = 3155378975999999999LL;
		NLong value1 = (NLong)value.GetTicks();
		NLong value2 = (NLong)value.GetMillisecond();
		NLong result;
		if (value1 < 0 || (Max - UnixEpoch) / TicksPerSecond < value1) NThrowOverflowException();
		result = UnixEpoch + value1 * TicksPerSecond;
		if (value2 < 0 || (Max - result) / TicksPerMillisecond < value2) NThrowOverflowException();
		result += value2 * TicksPerMillisecond;
		return result;
	}

	inline NDateTime ToNDateTime(NLong value)
	{
		const NLong UnixEpoch = 621355968000000000LL;
		const NLong TicksPerSecond = 10000000LL;
		const NLong TicksPerMillisecond = 10000LL;
		value -= UnixEpoch;
		NLong value1 = value / TicksPerSecond;
		NLong value2 = (value - value1 * TicksPerSecond) / TicksPerMillisecond; 
		if(value < 0 || value1 > (NLong)(((NULong)((time_t)-1)) / 2)) NThrowOverflowException();
		if(value - value1 * TicksPerSecond - value2 * TicksPerMillisecond >= (TicksPerMillisecond / 2))
		{
			if (++value2 == 1000)
			{
				value2 = 0;
				if (value1 == (NLong)(((NULong)((time_t)-1)) / 2)) NThrowOverflowException();
				value1++;
			}
		}
		wxDateTime dateTime((time_t)value1);
		dateTime.SetMillisecond((wxDateTime::wxDateTime_t)value2);
		dateTime.MakeUTC();
		return dateTime;
	}
}
#else
	#define N_FRAMEWORK_NATIVE

	#include <cstring>
	#include <string>

namespace Neurotec
{

	#define N_CLASS(name) name

	#ifdef N_UNICODE
		typedef std::wstring NString;
	#else
		typedef std::string NString;
	#endif

	inline NInt NGetStringLength(const NString & value)
	{
		return (NInt)value.length();
	}

	inline const NChar * NGetConstStringBuffer(const NString & value)
	{
		return value.c_str();
	}

	inline NChar * NGetStringBuffer(NString & value, NInt length)
	{
		value.reserve((NSizeType)(length + 1));
		value.resize((NSizeType)length);
		return length ? &*value.begin() : NULL;
	}

	inline NString & NReleaseStringBuffer(NString & value, NInt length)
	{
		value.resize((NSizeType)length);
		return value;
	}

}
#endif

#define N_STRING(text) NString(N_T(text))

#endif // !N_TYPES_HPP_INCLUDED
