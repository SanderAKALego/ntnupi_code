#ifndef N_RGB_IMAGE_H_INCLUDED
#define N_RGB_IMAGE_H_INCLUDED

#include <NImage.h>

#ifdef N_CPP
extern "C"
{
#endif

DECLARE_N_OBJECT_HANDLE(NRgbImage)

NResult N_API NRgbImageGetPixel(HNRgbImage hImage, NUInt x, NUInt y, NRgb * pValue);
NResult N_API NRgbImageSetPixel(HNRgbImage hImage, NUInt x, NUInt y, const NRgb * pValue);

#ifdef N_CPP
}
#endif

#endif // !N_RGB_IMAGE_H_INCLUDED
