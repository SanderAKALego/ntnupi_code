#ifndef NM_MATCH_DETAILS_HPP_INCLUDED
#define NM_MATCH_DETAILS_HPP_INCLUDED

#include <NfsmMatchDetails.hpp>
#include <NlsmMatchDetails.hpp>
#include <NesmMatchDetails.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NMMatchDetails.h>
}}

namespace Neurotec { namespace Biometrics
{

inline void MatchDetailsDeserialize(const void * buffer, NSizeType bufferLength, NMMatchDetails ** ppMatchDetails)
{
	NCheck(NMMatchDetailsDeserialize(buffer, bufferLength, ppMatchDetails));
}

inline NSizeType MatchDetailsSerialize(NMMatchDetails * pMatchDetails, void * * pBuffer)
{
	NSizeType bufferLength;
	NCheck(NMMatchDetailsSerialize(pMatchDetails, pBuffer, &bufferLength));
	return bufferLength;
}

}}

#endif // !NM_MATCH_DETAILS_HPP_INCLUDED
