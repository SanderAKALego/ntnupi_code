#ifndef WX_NLVIEW_HPP_INCLUDED
#define WX_NLVIEW_HPP_INCLUDED

#include <wxNView.hpp>
#include <NLTemplate.hpp>
#include <NLExtractor.hpp>

#include <wx/timer.h>

#define MAX_FPS_ENTRIES 30

namespace Neurotec { namespace Biometrics { namespace Gui
{

class wxNLView : public Neurotec::Gui::wxNView
{
public:
	wxNLView(wxWindow *parent, wxWindowID winid = wxID_ANY)
		: wxNView(parent, winid)
	{
		m_details = NULL;
		m_detailsCount = 0;
		m_lastTime = 0;
		m_frameCount = 0;
		m_currentFps = 0;
		m_drawFps = false;
		m_faceRectangleColor = wxColour(0, 255, 0);
		m_faceRectangleWidth = 2;
		SetScrollRate(1, 1);
		SetBackgroundColour(wxColour(0, 0, 0));
	}

	void Clear()
	{
		m_image = wxImage();
		m_bitmap = wxBitmap();
		m_details = NULL;
		m_detailsCount = 0;
		SetViewSize(1, 1);
		Refresh(false);
	}

	void SetImage(const wxImage & image)
	{
		if (GetDrawFps())
		{
			UpdateFps();
		}

		m_image = image;
		m_bitmap = wxBitmap(image);
		SetViewSize(image.GetWidth(), image.GetHeight());
	}

	wxImage GetImage()
	{
		return m_image;
	}

	void SetDetectionDetails(NleDetectionDetails *details, int detailsCount)
	{
		m_details = details;
		m_detailsCount = detailsCount;
		Refresh(false);
	}

	NleDetectionDetails * GetDetectionDetails()
	{
		return m_details;
	}

	int GetDetectionDetailsCount()
	{
		return m_detailsCount;
	}

	void SetFaceRectangleColor(const wxColour & value)
	{
		m_faceRectangleColor = value;
	}

	const wxColour & GetFaceRectangleColor()
	{
		return m_faceRectangleColor;
	}

	void SetFaceRectangleWidth(int value)
	{
		m_faceRectangleWidth = value;
	}

	int GetFaceRectangleWidth()
	{
		return m_faceRectangleWidth;
	}

	void SetDrawFps(bool value)
	{
		m_drawFps = value;
	}

	bool GetDrawFps()
	{
		return m_drawFps;
	}

protected:
#if wxUSE_GRAPHICS_CONTEXT == 1
	virtual void OnDraw(wxGraphicsContext *gc)
	{
		if (m_bitmap.GetRefData())
		{
			gc->DrawBitmap(m_bitmap, 0, 0, m_bitmap.GetWidth(), m_bitmap.GetHeight());
		}

		wxRect imageRect = GetImageRect();
		gc->Clip(0, 0, m_bitmap.GetWidth(), m_bitmap.GetHeight());
		for (int i = 0; i < m_detailsCount; i++)
		{
			wxString faceNumber(wxString::Format(wxT("#%d"), i + 1));
			wxString faceConfidence;
			if (m_details[i].FaceAvailable)
			{
				faceConfidence = wxString::Format(wxT("%4.1f"), m_details[i].Face.Confidence);
			}

			DrawDetectionDetails(gc, &m_details[i], faceNumber, faceConfidence);
		}
		gc->ResetClip();

		if (m_drawFps
			&& m_currentFps > 0.5f
			&& m_currentFps < 1000.0f)
		{
			wxBrush brush(wxColour(127, 127, 127));
			wxGraphicsBrush graphicsBrush = gc->CreateBrush(brush);
			gc->DrawText(wxString::Format(wxT("%4.1f"), m_currentFps), 5, 5, graphicsBrush);
		}
	}

	void DrawDetectionDetails(wxGraphicsContext *gc, NleDetectionDetails *details, const wxString& faceNumber, const wxString& faceConfidence)
	{
		if (details->EyesAvailable)
		{
			wxPen penGreen(wxColour(m_faceRectangleColor.Red(), m_faceRectangleColor.Green(), m_faceRectangleColor.Blue(),
				wxClip((int)(wxMin(details->Eyes.FirstConfidence, details->Eyes.SecondConfidence) * 255.0 / 100.0), 0, 255)),
				m_faceRectangleWidth);
			gc->SetPen(penGreen);
			gc->StrokeLine(details->Eyes.First.X, details->Eyes.First.Y,
				details->Eyes.Second.X, details->Eyes.Second.Y);
			gc->SetPen(wxNullPen);
		}

		if (details->FaceAvailable)
		{
			wxPen penGreen(m_faceRectangleColor, m_faceRectangleWidth);
			penGreen.SetCap(wxCAP_PROJECTING);
			penGreen.SetJoin(wxJOIN_MITER);
			gc->SetPen(penGreen);

			wxGraphicsMatrix oldTransform = gc->GetTransform();

			wxGraphicsMatrix matrix = gc->CreateMatrix();
			matrix.Translate(details->Face.Rectangle.X + details->Face.Rectangle.Width / 2,
				details->Face.Rectangle.Y + details->Face.Rectangle.Height / 2);
			matrix.Rotate(details->Face.Rotation.Roll);
			matrix.Translate(-details->Face.Rectangle.Width / 2, -details->Face.Rectangle.Height / 2);
			gc->ConcatTransform(matrix);

			wxGraphicsPath path = gc->CreatePath();
			path.AddRectangle(0, 0,
				details->Face.Rectangle.Width, details->Face.Rectangle.Height);
			gc->StrokePath(path);

			gc->SetPen(wxNullPen);

			wxBrush brush(wxColour(0, 255, 0));
			wxGraphicsBrush graphicsBrush = gc->CreateBrush(brush);
			if (!faceNumber.IsEmpty())
			{
				gc->DrawText(faceNumber, 0, details->Face.Rectangle.Height + 3, graphicsBrush);
			}
			if (!faceConfidence.IsEmpty())
			{
				double textWidth, textHeight, descent, externalLeading;
				gc->GetTextExtent(faceConfidence, &textWidth, &textHeight, &descent, &externalLeading);
				gc->DrawText(faceConfidence, details->Face.Rectangle.Width - textWidth,
					details->Face.Rectangle.Height + 3, graphicsBrush);
			}

			gc->SetTransform(oldTransform);
		}
	}
#else
	virtual void OnDraw(wxDC& dc)
	{
		if (m_bitmap.GetRefData())
		{
			dc.DrawBitmap(m_bitmap, 0, 0, false);
		}
		for (int i = 0; i < m_detailsCount; i++)
		{
			wxString infoString(wxString::Format(wxT("#%d"), i + 1));
			if (m_details[i].FaceAvailable)
			{
				infoString.Append(wxString::Format(wxT("   %4.1f"), m_details[i].Face.Confidence));
			}
			DrawDetectionDetails(dc, &m_details[i], infoString);
		}
		if (m_drawFps
			&& m_currentFps > 0.5f
			&& m_currentFps < 1000.0f)
		{
			dc.SetTextForeground(wxColour(0, 255, 0));
			dc.DrawText(wxString::Format(wxT("%4.1f"), m_currentFps), 5, 5);
		}
	}

	static inline void RotatePointAt(double x, double y, double centerX, double centerY, double angle, double *ox, double *oy)
	{
		*ox = centerX + cos(angle) * (x - centerX) - sin(angle) * (y - centerY);
		*oy = centerY + sin(angle) * (x - centerX) + cos(angle) * (y - centerY);
	}

	void DrawDetectionDetails(wxDC &dc, NleDetectionDetails *details, const wxString& info)
	{
		if (details->EyesAvailable)
		{
			wxPen penGreen(m_faceRectangleColor, m_faceRectangleWidth);
			dc.SetPen(penGreen);
			wxPoint points[] ={ wxPoint(details->Eyes.First.X, details->Eyes.First.Y),
				wxPoint(details->Eyes.Second.X, details->Eyes.Second.Y) };
			dc.DrawLines(2, points, 0, 0);
			dc.SetPen(wxNullPen);
		}
		if (details->FaceAvailable)
		{
			wxPen penGreen(m_faceRectangleColor, m_faceRectangleWidth);
			penGreen.SetCap(wxCAP_PROJECTING);
			penGreen.SetJoin(wxJOIN_MITER);
			dc.SetPen(penGreen);
			double angle = (double)details->Face.Rotation.Roll * M_PI / 180.0;
			double pt1x, pt1y;
			RotatePointAt(details->Face.Rectangle.X, details->Face.Rectangle.Y,
				(details->Face.Rectangle.X * 2 + details->Face.Rectangle.Width) / 2,
				(details->Face.Rectangle.Y * 2 + details->Face.Rectangle.Height) / 2,
				angle, &pt1x, &pt1y);
			double pt2x, pt2y;
			RotatePointAt(details->Face.Rectangle.X + details->Face.Rectangle.Width,
				details->Face.Rectangle.Y,
				(details->Face.Rectangle.X * 2 + details->Face.Rectangle.Width) / 2,
				(details->Face.Rectangle.Y * 2 + details->Face.Rectangle.Height) / 2,
				angle, &pt2x, &pt2y);
			double pt3x, pt3y;
			RotatePointAt(details->Face.Rectangle.X + details->Face.Rectangle.Width,
				details->Face.Rectangle.Y + details->Face.Rectangle.Height,
				(details->Face.Rectangle.X * 2 + details->Face.Rectangle.Width) / 2,
				(details->Face.Rectangle.Y * 2 + details->Face.Rectangle.Height) / 2,
				angle, &pt3x, &pt3y);
			double pt4x, pt4y;
			RotatePointAt(details->Face.Rectangle.X,
				details->Face.Rectangle.Y + details->Face.Rectangle.Height,
				(details->Face.Rectangle.X * 2 + details->Face.Rectangle.Width) / 2,
				(details->Face.Rectangle.Y * 2 + details->Face.Rectangle.Height) / 2,
				angle, &pt4x, &pt4y);
			wxPoint points[] ={ wxPoint((int)pt1x, (int)pt1y),
				wxPoint((int)pt2x, (int)pt2y),
				wxPoint((int)pt3x, (int)pt3y),
				wxPoint((int)pt4x, (int)pt4y),
				wxPoint((int)pt1x, (int)pt1y) };
			dc.DrawLines(5, points, 0, 0);
			dc.SetPen(wxNullPen);

			dc.SetTextForeground(wxColour(0, 255, 0));
			dc.DrawRotatedText(info, (wxCoord)pt4x, (wxCoord)pt4y, (double) -details->Face.Rotation.Roll);
		}
	}
#endif

private:
	void UpdateFps()
	{
		wxLongLong currentTime = ::wxGetLocalTimeMillis();
		float previousFps = m_currentFps;
		m_currentFps = 0.0f;
		if (currentTime - m_lastTime > 5000)
		{
			m_frameCount = 0;
			m_firstTime = currentTime;
		}
		else
		{
			m_frameCount++;
			if (m_frameCount == MAX_FPS_ENTRIES)
			{
				m_currentFps = (float)((m_frameCount.ToDouble() * 1000.0) / (double)(currentTime - m_firstTime).ToDouble());
			}
			else if (m_frameCount > MAX_FPS_ENTRIES)
			{
				float frameFps = (float)(1000.0 / (currentTime - m_lastTime).ToDouble());
				const float newFpsWeight = (float)1.0f / (float)MAX_FPS_ENTRIES;
				m_currentFps = previousFps * (1.0f - newFpsWeight) + frameFps * newFpsWeight;
			}
		}
		m_lastTime = currentTime;
	}

	wxImage m_image;
	wxBitmap m_bitmap;
	NleDetectionDetails *m_details;
	int m_detailsCount;
	wxColour m_faceRectangleColor;
	int m_faceRectangleWidth;
	wxLongLong m_lastTime;
	wxLongLong m_firstTime;
	wxLongLong m_frameCount;
	float m_currentFps;
	bool m_drawFps;
};

}}}

#endif // !WX_NLVIEW_HPP_INCLUDED
