#ifndef NLS_MATCHER_HPP_INCLUDED
#define NLS_MATCHER_HPP_INCLUDED

#include <NLMatcher.hpp>
#include <NlsmMatchDetails.hpp>
#include <NlsMatcherParams.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NlsMatcher.h>
}}

namespace Neurotec { namespace Biometrics
{

class N_CLASS(NlsMatcher) : public N_CLASS(NObject)
{
private:
	static HNlsMatcher Create()
	{
		HNlsMatcher handle;
		NCheck(NlsmCreate(&handle));
		return handle;
	}

public:
	static N_CLASS(NlsMatcher) * FromHandle(HNlsMatcher handle, bool ownsHandle = true)
	{
		return new N_CLASS(NlsMatcher)(handle, ownsHandle);
	}

	explicit N_CLASS(NlsMatcher)(HNlsMatcher handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NlsMatcher), false)
	{
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	N_CLASS(NlsMatcher)()
		: N_CLASS(NObject)(Create(), N_NATIVE_TYPE_OF(NlsMatcher), true)
	{
	}

	NDouble Verify(const void * pTemplate1, NSizeType template1Size,
		const void * pTemplate2, NSizeType template2Size,
		NlsmMatchDetails * * ppMatchDetails = NULL)
	{
		NDouble score;
		NCheck(NlsmVerify((HNlsMatcher)GetHandle(),
			pTemplate1, template1Size,
			pTemplate2, template2Size,
			ppMatchDetails, &score));
		return score;
	}

	void IdentifyStart(const void * pTemplate, NSizeType templateSize,
		NlsmMatchDetails * * ppMatchDetails = NULL)
	{
		NCheck(NlsmIdentifyStart((HNlsMatcher)GetHandle(), pTemplate, templateSize, ppMatchDetails));
	}

	NDouble IdentifyNext(const void * pTemplate, NSizeType templateSize,
		NlsmMatchDetails * pMatchDetails)
	{
		NDouble score;
		NCheck(NlsmIdentifyNext((HNlsMatcher)GetHandle(), pTemplate, templateSize, pMatchDetails, &score));
		return score;
	}

	void IdentifyEnd()
	{
		NCheck(NlsmIdentifyEnd((HNlsMatcher)GetHandle()));
	}

	NDouble GetMatchingThreshold()
	{
		return GetParameterAsDouble(NLSM_PART_NONE, NLSMP_MATCHING_THRESHOLD);
	}

	void SetMatchingThreshold(NDouble value)
	{
		SetParameter(NLSM_PART_NONE, NLSMP_MATCHING_THRESHOLD, value);
	}

	NlmSpeed GetMatchingSpeed()
	{
		return (NlmSpeed)GetParameterAsInt32(NLSM_PART_NLM, NLMP_MATCHING_SPEED);
	}

	void SetMatchingSpeed(NlmSpeed value)
	{
		SetParameter(NLSM_PART_NLM, NLMP_MATCHING_SPEED, (NInt)value);
	}

	N_DECLARE_OBJECT_TYPE(NlsMatcher)
};

}}

#endif // !NLS_MATCHER_HPP_INCLUDED
