#ifndef N_EXTRACTORS_HPP_INCLUDED
#define N_EXTRACTORS_HPP_INCLUDED

#include <NCore.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NExtractors.h>
}}

namespace Neurotec { namespace Biometrics
{

class NExtractors
{
private:
	NExtractors()
	{
	}

public:
	static void GetInfo(NLibraryInfo * pValue)
	{
		NCheck(NExtractorsGetInfo(pValue));
	}
};

}}

#endif // !N_EXTRACTORS_HPP_INCLUDED
