#ifndef N_MATCHERS_H_INCLUDED
#define N_MATCHERS_H_INCLUDED

#include <NCore.h>

#ifdef N_CPP
extern "C"
{
#endif

#ifndef N_NO_ANSI_FUNC
NResult N_API NMatchersGetInfoA(NLibraryInfoA * pValue);
#endif
#ifndef N_NO_UNICODE
NResult N_API NMatchersGetInfoW(NLibraryInfoW * pValue);
#endif
#ifdef N_DOCUMENTATION
NResult N_API NMatchersGetInfo(NLibraryInfo * pValue);
#endif
#define NMatchersGetInfo N_FUNC_AW(NMatchersGetInfo)

#ifdef N_CPP
}
#endif

#endif // !N_MATCHERS_H_INCLUDED
