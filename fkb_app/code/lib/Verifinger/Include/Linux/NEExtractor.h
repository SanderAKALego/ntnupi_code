#ifndef NE_EXTRACTOR_H_INCLUDED
#define NE_EXTRACTOR_H_INCLUDED

#include <NGrayscaleImage.h>
#include <NGeometry.h>
#include <NERecord.h>
#include <NExtractors.h>
#include <NEExtractorParams.h>

#ifdef N_CPP
extern "C"
{
#endif

#define NEE_BOUNDARY_POINT_COUNT 32

typedef struct NeeSegmentationDetails_
{
	NBool OuterBoundaryAvailable;
	NPoint OuterBoundaryPoints[NEE_BOUNDARY_POINT_COUNT];
} NeeSegmentationDetails;

typedef enum NeeExtractionStatus_
{
	neeesSucceeded = 1,
	neeesTemplateCreated = neeesSucceeded,
	neeesSegmentationFailed = 2,
	neeesQualityCheckFailed = 100,
} NeeExtractionStatus;

typedef enum NeeImageKind_
{
	neeikUncropped = 1,
	neeikVga = 2,
	neeikCropped = 3,
	neeikCroppedAndMasked = 4,
} NeeImageKind;

DECLARE_N_OBJECT_HANDLE(NEExtractor);

NResult N_API NeeCreate(HNEExtractor *pHExtractor);

NResult N_API NeeExtract(HNEExtractor hExtractor, HNGrayscaleImage hImage, NEPosition position,
	NeeSegmentationDetails * pDetails, NeeExtractionStatus * pStatus, HNERecord * pHRecord);

NResult N_API NeeExtractEx(HNEExtractor hExtractor, HNGrayscaleImage hImage, NeeImageKind imageKind, NEPosition position,
	NeeSegmentationDetails * pDetails, NeeExtractionStatus * pExtractionStatus, HNERecord * pHRecord);

#define NeeFree(hExtractor) NObjectFree(hExtractor)
#define NeeCopyParameters(hDstExtractor, hSrcExtractor) NObjectCopyParameters(hDstExtractor, hSrcExtractor)
#define NeeGetParameterA(hExtractor, parameterId, pValue) NTypeGetParameterA(NEExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NeeGetParameterW(hExtractor, parameterId, pValue) NTypeGetParameterW(NEExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NeeGetParameter(hExtractor, parameterId, pValue) NTypeGetParameter(NEExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NeeSetParameterA(hExtractor, parameterId, pValue) NTypeSetParameterA(NEExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NeeSetParameterW(hExtractor, parameterId, pValue) NTypeSetParameterW(NEExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NeeSetParameter(hExtractor, parameterId, pValue) NTypeSetParameter(NEExtractorTypeOf(), hExtractor, 0, parameterId, pValue)
#define NeeReset(hExtractor) NObjectReset(hExtractor)

#ifdef N_MSVC
	#pragma deprecated("NeeFree", "NeeCopyParameters", "NeeGetParameterA", "NeeGetParameterW", "NeeGetParameter", "NeeSetParameterA", "NeeSetParameterW", "NeeSetParameter", "NeeReset")
#endif

#ifdef N_CPP
}
#endif

#endif // !NE_EXTRACTOR_H_INCLUDED
