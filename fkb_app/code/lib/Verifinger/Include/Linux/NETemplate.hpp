#ifndef NE_TEMPLATE_HPP_INCLUDED
#define NE_TEMPLATE_HPP_INCLUDED

#include <NERecord.hpp>
namespace Neurotec { namespace Biometrics
{
#include <NETemplate.h>
}}

namespace Neurotec { namespace Biometrics
{
#undef NET_MAX_RECORD_COUNT
#undef NET_PROCESS_FIRST_RECORD_ONLY

const NInt NET_MAX_RECORD_COUNT = 255;
const NUInt NET_PROCESS_FIRST_RECORD_ONLY = 0x00000100;

class N_CLASS(NETemplate) : public N_CLASS(NObject)
{
public:
	class RecordCollection : public ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NERecord)>
	{
		RecordCollection(N_CLASS(NETemplate) * pOwner)
			: ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NERecord)>(pOwner, NETemplateGetRecordCount, NETemplateGetRecord, FromHandle,
			NETemplateGetRecordCapacity, NETemplateSetRecordCapacity, NETemplateRemoveRecord, NULL, NETemplateClearRecords)
		{
		}

		static N_CLASS(NERecord) * FromHandle(HNERecord handle)
		{
			return N_CLASS(NERecord)::FromHandle(handle, false);
		}

		friend class N_CLASS(NETemplate);

	public:
		N_CLASS(NERecord)* Add(NUShort width, NUShort height, NUInt flags = 0)
		{
			HNERecord hRecord;
			NCheck(NETemplateAddRecord(GetOwner()->GetHandle(), width, height, flags, &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NERecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NETemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}

		N_CLASS(NERecord)* Add(const void * pBuffer, NSizeType bufferSize, NUInt flags = 0)
		{
			HNERecord hRecord;
			NCheck(NETemplateAddRecordFromMemory(GetOwner()->GetHandle(), pBuffer, bufferSize, flags, &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NERecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NETemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}

		N_CLASS(NERecord)* AddCopy(N_CLASS(NERecord)* pSrcRecord)
		{
			HNERecord hRecord;
			if (pSrcRecord == NULL)
			{
				NThrowArgumentNullException(N_T("pSrcRecord"));
			}
			
			NCheck(NETemplateAddRecordCopy(GetOwner()->GetHandle(), pSrcRecord->GetHandle(), &hRecord));
			try
			{
				return ::Neurotec::Collections::N_CLASS(NObjectCollection)<N_CLASS(NERecord)>::AddItem(hRecord);
			}
			catch(...)
			{
				NETemplateRemoveRecord(GetOwner()->GetHandle(), GetCount() - 1); // try to undo the operation
				throw;
			}
		}
	};

private:
	static HNETemplate Create(NUInt flags)
	{
		HNETemplate handle;
		NCheck(NETemplateCreateEx(flags, &handle));
		return handle;
	}

	static HNETemplate Create(const void * pBuffer, NSizeType bufferSize,
		NUInt flags, NETemplateInfo * pInfo)
	{
		HNETemplate handle;
		NCheck(NETemplateCreateFromMemory(pBuffer, bufferSize, flags, pInfo, &handle));
		return handle;
	}

	std::auto_ptr<RecordCollection> records;

	void Init()
	{
		records = std::auto_ptr<RecordCollection>(new RecordCollection(this));
	}

public:
	static N_CLASS(NETemplate)* FromHandle(HNETemplate handle, bool ownsHandle = true)
	{
		return new N_CLASS(NETemplate)(handle, ownsHandle);
	}

	static NSizeType CalculateSize(NInt recordCount, NSizeType * arRecordSizes)
	{
		NSizeType value;
		NCheck(NETemplateCalculateSize(recordCount, arRecordSizes, &value));
		return value;
	}

	static NSizeType Pack(NInt recordCount, const void * * arPRecords, NSizeType * arRecordSizes, void * pBuffer, NSizeType bufferSize)
	{
		NSizeType value;
		NCheck(NETemplatePack(recordCount, arPRecords, arRecordSizes, pBuffer, bufferSize, &value));
		return value;
	}

	static void Unpack(const void * pBuffer, NSizeType bufferSize,
		NByte * pMajorVersion, NByte * pMinorVersion, NUInt * pSize, NByte * pHeaderSize,
		NInt * pRecordCount, const void * * arPRecords, NSizeType * arRecordSizes)
	{
		NCheck(NETemplateUnpack(pBuffer, bufferSize,
			pMajorVersion, pMinorVersion, pSize, pHeaderSize,
			pRecordCount, arPRecords, arRecordSizes));
	}

	static void Check(const void * pBuffer, NSizeType bufferSize)
	{
		NCheck(NETemplateCheck(pBuffer, bufferSize));
	}

	static NInt GetRecordCount(const void * pBuffer, NSizeType bufferSize)
	{
		NInt value;
		NCheck(NETemplateGetRecordCountMem(pBuffer, bufferSize, &value));
		return value;
	}

	explicit N_CLASS(NETemplate)(HNETemplate handle, bool ownsHandle = true)
		: N_CLASS(NObject)(handle, N_NATIVE_TYPE_OF(NETemplate), false)
	{
		Init();
		if (ownsHandle)
		{
			ClaimHandle();
		}
	}

	explicit N_CLASS(NETemplate)(NUInt flags = 0)
		: N_CLASS(NObject)(Create(flags), N_NATIVE_TYPE_OF(NETemplate), true)
	{
		Init();
	}

	N_CLASS(NETemplate)(const void * pBuffer, NSizeType bufferSize,
		NUInt flags = 0, NETemplateInfo * pInfo = NULL)
		: N_CLASS(NObject)(Create(pBuffer, bufferSize, flags, pInfo), N_NATIVE_TYPE_OF(NETemplate), true)
	{
		Init();
	}

	RecordCollection * GetRecords() const
	{
		return records.get();
	}

	N_CLASS(NObject) * Clone() const
	{
		HNETemplate handle;
		NCheck(NETemplateClone((HNETemplate)GetHandle(), &handle));
		try
		{
			return FromHandle(handle);
		}
		catch(...)
		{
			NObjectFree(handle);
			throw;
		}
	}

	NSizeType GetSize(NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NETemplateGetSize((HNETemplate)GetHandle(), flags, &value));
		return value;
	}

	NSizeType Save(void * pBuffer, NSizeType bufferSize, NUInt flags = 0) const
	{
		NSizeType value;
		NCheck(NETemplateSaveToMemory((HNETemplate)GetHandle(), pBuffer, bufferSize, flags, &value));
		return value;
	}

	N_DECLARE_OBJECT_TYPE(NETemplate)
};

}}

#endif // !NE_TEMPLATE_HPP_INCLUDED
