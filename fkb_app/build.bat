@echo off
REM
REM Begin timing the compile time
REM
ctime -begin "fkb_app.ctm"

set VCVarsPath="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

REM 
REM Built and tested with Visual Studio 2015, 
REM if you're using a different compiler or platform you'll have to create your own build script
REM 
REM 
REM 
REM There are 2 ways to make this compile (if using Visual Studio), 
REM 1) You either set VCVarsPath
REM    to your own local copy of it. This is useful
REM    if compiling from your editor.
REM 2) Call this bat file from Visual Studio x64 Native Tools Command Promt
REM 
where cl >nul 2>nul
if %errorlevel%==1 (
	if not exist %VCVarsPath% (
		echo "You are not running this from the Visual Studio Command Promt (VSCP) and you are not specifying a valid path to the vcvarsall.bat file, either run this script through the VSCP or alter the path of vcvarsall.bat in the build.bat file."
		goto end
	)
	if exist %VCVarsPath% call %VCVarsPath% x64
) else (
	call VC/vcvarsall.bat x64
)

set dir=%~dp0
set Mode=Debug
REM set Mode=Release
set ENABLE_HOTRELOADER=0

set CommonCompilerFlags=-Zi -MTd -Gm -nologo -EHa -Od -wd4311 -wd4312 -DHOT_RELOADER_ENABLED=%ENABLE_HOTRELOADER% -DFKB_DEBUG /I %dir%code\lib /I "%dir%code\lib\sdl2\include" /I %dir%code\lib\sdl2_net\include /I%dir%code\lib\openssl-1.1.0e-vs2015\include64 /I %dir%code\lib\Verifinger\Include\Windows
set CommonLinkerFlags=/incremental:no /opt:ref -LIBPATH:%dir%code\lib\sdl2\lib\x64 -LIBPATH:%dir%code\lib\sdl2_net\lib\x64 "-LIBPATH:%dir%code\lib\openssl-1.1.0e-vs2015\lib64" -LIBPATH:%dir%code\lib\Verifinger\Lib\Win64_x64 SDL2.lib SDL2main.lib SDL2_net.lib Advapi32.lib libcryptoMDd.lib Comdlg32.lib  NLicensing.dll.lib NCore.dll.lib NImages.dll.lib NExtractors.dll.lib NTemplates.dll.lib

if %Mode%==Release (
	set CommonCompilerFlags=-Zi -MT -Gm -nologo -EHa -O2 -wd4311 -wd4312 -DENABLE_HOTRELOADER=%ENABLE_HOTRELOADER% -DRELEASE /I %dir%code\lib /I "%dir%code\lib\sdl2\include" /I %dir%code\lib\sdl2_net\include /I%dir%code\lib\openssl-1.1.0e-vs2015\include64 /I %dir%code\lib\Verifinger\Include\Windows
	set CommonLinkerFlags=/incremental:no /opt:ref -LIBPATH:%dir%code\lib\sdl2\lib\x64 -LIBPATH:%dir%code\lib\sdl2_net\lib\x64 "-LIBPATH:%dir%code\lib\openssl-1.1.0e-vs2015\lib64" -LIBPATH:%dir%code\lib\Verifinger\Lib\Win64_x64 SDL2.lib SDL2main.lib SDL2_net.lib Advapi32.lib libcryptoMDd.lib Comdlg32.lib  NLicensing.dll.lib NCore.dll.lib NImages.dll.lib NExtractors.dll.lib NTemplates.dll.lib
)

echo %Mode% build

if not exist %dir%build mkdir %dir%build
if %Mode%==Release if not exist %dir%build\release mkdir %dir%build\release 

pushd %dir%build

if %Mode%==Release del * /S /Q > NUL 2> NUL

tasklist /FI "IMAGENAME eq fkb_app_demo.exe" 2>NUL | find /I /N "fkb_app_demo.exe">NUL
if not "%ERRORLEVEL%"=="0" cl %CommonCompilerFlags% -Fefkb_app_demo %dir%code\build.cpp /link %CommonLinkerFlags% -SUBSYSTEM:CONSOLE

if exist fkb_app_demo.pdb copy fkb_app_demo.pdb fkb_app_demo.pdb.temp >NUL
if exist vc140.pdb copy vc140.pdb vc140.pdb.temp >NUL

del *.pdb > NUL 2> NUL
echo WAITING FOR PDB > lock.tmp
del lock.tmp

if %ENABLE_HOTRELOADER%==1 (
	cl %CommonCompilerFlags% -Fefkb_app_demo %dir%code\build.cpp -DCOMPILE_AS_DLL -LD /link %CommonLinkerFlags% -PDB:fkb_app_demo_dll_%random%.pdb -EXPORT:UpdateAndRender
)

if exist fkb_app_demo.pdb.temp copy fkb_app_demo.pdb.temp fkb_app_demo.pdb >NUL
if exist fkb_app_demo.pdb.temp del fkb_app_demo.pdb.temp

if exist vc140.pdb.temp copy vc140.pdb.temp vc140.pdb >NUL
if exist vc140.pdb.temp del vc140.pdb.temp

if not exist libcryptoMDd.dll copy ..\code\lib\openssl-1.1.0e-vs2015\bin64\libcryptoMDd.dll libcryptoMDd.dll
if not exist NCore.dll copy ..\code\lib\Verifinger\Bin\Win64_x64\NCore.dll NCore.dll
if not exist NExtractors.dll copy ..\code\lib\Verifinger\Bin\Win64_x64\NExtractors.dll NExtractors.dll
if not exist NImages.dll copy ..\code\lib\Verifinger\Bin\Win64_x64\NImages.dll NImages.dll
if not exist NLicensing.dll if not exist libcryptoMD.dll copy ..\code\lib\Verifinger\Bin\Win64_x64\NLicensing.dll NLicensing.dll
if not exist NTemplates.dll copy ..\code\lib\Verifinger\Bin\Win64_x64\NTemplates.dll NTemplates.dll
if not exist SDL2_net.dll copy ..\code\lib\sdl2_net\lib\x64\SDL2_net.dll SDL2_net.dll
if not exist SDL2.dll copy ..\code\lib\sdl2\lib\x64\SDL2.dll SDL2.dll
if not exist GUCGenerator.exe copy ..\misc\GUCGenerator.exe GUCGenerator.exe
if not exist GUCMatcher.exe copy ..\misc\GUCMatcher.exe 

if %Mode%==Release xcopy ..\data\* /S /Y

set LastError=%ERRORLEVEL%
popd

:end
ctime -end "fkb_app.ctm" %ERRORLEVEL%

