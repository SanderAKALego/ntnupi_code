@echo off
ctime -begin "fkb_secret_releaser.ctm"

REM set VCVarsPath="A:/SYSTEM/Visual Studio 2015/VC/vcvarsall.bat"
set VCVarsPath="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
if not exist %VCVarsPath% (
	echo vcvarsall.bat not found, please provide a valid path
	goto end
)

if not defined DevEnvDir (
    call %VCVarsPath% x64
)

set dir=%~dp0

set CommonCompilerFlags=-Zi -MD -nologo -EHa -O2 -wd4311 -wd4312 -DDEBUG /I %dir%code\lib /I "%dir%code\lib\sdl2\include"
set CommonLinkerFlags=/incremental:no /opt:ref /SUBSYSTEM:CONSOLE -LIBPATH:%dir%code\lib\sdl2\lib\x64 SDL2.lib SDL2main.lib

pushd %dir%build

cl %CommonCompilerFlags% -Fefkb_secret_releaser -DSECRET_RELEASER_STANDALONE %dir%code\fkb_secret_releaser.cpp /link %CommonLinkerFlags%

set LastError=%ERRORLEVEL%
popd

:end
ctime -end "fkb_secret_releaser.ctm" %ERRORLEVEL%

